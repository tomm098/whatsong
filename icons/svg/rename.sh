#!/usr/bin/env bash

for file in *_24px.svg
do
    mv -i "${file}" "${file/_24px.svg/.svg}"
done

for file in *_18px.svg
do
    rm -rf "${file}"
done

for file in *_48px.svg
do
    rm -rf "${file}"
done