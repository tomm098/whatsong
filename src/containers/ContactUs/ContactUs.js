import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './ContactUs.scss';

export default class ContactUs extends Component {
  static propTypes = {

  }
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    return (
      <div className="contact-us">
        <div className="main">
          <div className="content">
            <h1>Email</h1>
            <p>You can contact us regarding anything including feedback, feature requests, movie requests, advertising queries or anything else with our email below:
              <em>staff [at] <a href="#">what-song.com</a></em></p>
            <h2>Social Media</h2>
            <p>You can also follow us on Facebook and Twitter to keep up to date with the latest additions and updates.</p>
          </div>
        </div>
      </div>
    );
  }
}
