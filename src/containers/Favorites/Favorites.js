import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Favorites.scss';
import {} from './FavoritesSidebar.scss';
import { FavoritePage, TrendingSongs } from 'components';
import { connect } from 'react-redux';
import { asyncConnect } from 'redux-connect';
import * as favoritesActions from 'redux/modules/favorites';
import * as notificationsActions from 'redux/modules/notifications';
import Helmet from 'react-helmet';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import * as trendingSongsActions from '../../redux/modules/trendingSongs';
import Sticky from 'react-stickynode';

const list_types = ['songs', 'movies', 'tv'];

const trending_tv_songs = {
  name: 'trending_tv_songs',
  label: 'trending in Television',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-tv?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-tv?filter=month'
    }
  }
};

const trending_movies_songs = {
  name: 'trending_movies_songs',
  label: 'trending in Film',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-movies?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-movies?filter=month'
    }
  }
};

@connect(
  state => ({
    favorites: state.favorites
  }),
  {
    clear: favoritesActions.clear,
    loadMore: favoritesActions.loadMore,
    dislike: favoritesActions.dislike,
    pushNotification: notificationsActions.push
  }
)

@asyncConnect([{
  promise: ({params, store: {dispatch, getState}}) => {
    const promises = [];
    const { favorites } = getState();
    const trending_state = getState().trendingSongs; // eslint-disable-line
    list_types.forEach(c=>{
      if (!favorites[c] || !favorites[c].loaded || !favorites[c].data /*&& (data.user.username !== params.username)*/) {
        promises.push(dispatch(favoritesActions.load(params.username, c)));
      }
    });

    if (!trending_state[trending_tv_songs.name]) {
      dispatch(trendingSongsActions.register(trending_tv_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_tv_songs.name)));
    }
    if (!trending_state[trending_movies_songs.name]) {
      dispatch(trendingSongsActions.register(trending_movies_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_movies_songs.name)));
    }

    return Promise.all(promises);
  }
}])

export default class Favorites extends Component {
  static propTypes = {
    favorites: PropTypes.object,
    loadMore: PropTypes.func,
    params: PropTypes.object,
    clear: PropTypes.func,
    dislike: PropTypes.func,
    pushNotification: PropTypes.func
  }


  constructor(props) {
    super(props);
    this.state = {
      active_tab: 0
    };
  }

  componentWillUnmount() {
    this.props.clear();
  }

  render() {
    const {favorites, params, loadMore, dislike, pushNotification } = this.props;
    const { active_tab } = this.state;
    return (
      <div className="favorites">

        <Helmet {...{
          htmlAttributes: {
            lang: 'en'
          },
          title: `${params.username}'s Favorites - WhatSong`,
          meta: [
            {charset: 'utf-8'},
            {property: 'og:site_name', content: 'What-song'},
            {property: 'og:locale', content: 'en_US'},
            {property: 'og:title', content: `${params.username}'s Favorites - WhatSong`},
            {property: 'og:image', content: '/logo.jpg'},
            {property: 'og:image:width', content: '200'},
            {property: 'og:image:height', content: '200'}
          ]
        }} />

        <div className="main">
          <div className="content">
            <div className="favorites__wrapp">
              <h1>YOUR FAVORITES</h1>
              <div className="favorites__tab-wrapp">
                <div className="favorites__song-count">
                  {!!favorites[list_types[active_tab]] ? favorites[list_types[active_tab]].total_items_count : ''}
                  {[' songs', ' movies', ' TV shows'][active_tab]}
                </div>
                <Tabs
                  onSelect={i=>this.setState({active_tab: i})}
                  selectedIndex={active_tab}
                >
                  <TabList>
                    <Tab>
                    Songs
                    </Tab>
                    <Tab>
                    Movies
                    </Tab>
                    <Tab>
                    Tv shows
                    </Tab>
                  </TabList>

                  <TabPanel>
                    <FavoritePage.SongsList
                      loadMore={()=>loadMore(params.username, 'songs', 10)}
                      handleDislike={id=>dislike(params.username, 'songs', id)}
                      pushNotification={pushNotification}
                      list={favorites.songs}
                      type="songs"
                    />
                  </TabPanel>
                  <TabPanel>
                    <FavoritePage.TvMoviesList
                      loadMore={()=>loadMore(params.username, 'movies', 10)}
                      handleDislike={id=>dislike(params.username, 'movies', id)}
                      handleLike={p=>pushNotification('favorite_movie', p)}
                      list={favorites.movies}
                      type="movies"
                    />
                  </TabPanel>
                  <TabPanel>
                    <div>
                      <FavoritePage.TvMoviesList
                        loadMore={()=>loadMore(params.username, 'tv', 10)}
                        list={favorites.tv}
                        type="tv"
                        handleDislike={id=>dislike(params.username, 'tv', id)}
                        handleLike={p=>pushNotification('favorite_tv_show', p)}
                      />
                    </div>
                  </TabPanel>
                </Tabs>
              </div>
            </div>

          </div>
          <div className="sidebar">
            <Sticky bottomBoundary={'.content'}>
              <div className="sidebar-sticky">
                <TrendingSongs name={trending_tv_songs.name}/>
                <TrendingSongs name={trending_movies_songs.name}/>
              </div>
            </Sticky>
          </div>
        </div>
      </div>
    );
  }
}
