import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { ItemPage, SpotifyList, TrendingSongs, Contributors, BriefIntro, ExternalLink, GoogleAd } from 'components'; // eslint-disable-line
import { ScreenType } from 'elements';
import Sticky from 'react-stickynode';
import { asyncConnect } from 'redux-connect';
import * as tvSeasonActions from 'redux/modules/tvShowSeason';
import { connect } from 'react-redux';
import { NotFound } from 'containers';
import * as trendingSongsActions from 'redux/modules/trendingSongs'; // eslint-disable-line
import { urlGenerator } from 'utils/helpers';
import * as notificationsActions from 'redux/modules/notifications';
import Helmet from 'react-helmet';

const trending_tv_songs = {
  name: 'trending_tv_songs',
  label: 'trending in Television',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-tv?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-tv?filter=month'
    }
  }
};

@connect(
  state => ({
    tv_show_season: state.tvShowSeason.data,
    loaded: state.tvShowSeason.loaded,
    error: state.tvShowSeason.error,
    loading: state.tvShowSeason.loading
  }),
  {
    clear: tvSeasonActions.clear,
    addEpisode: tvSeasonActions.addEpisode,
    editEpisode: tvSeasonActions.editEpisode,
    pushNotification: notificationsActions.push
  }
)

@asyncConnect([{
  promise: ({params, store: {dispatch, getState}}) => {
    const promises = [];
    const state = getState();
    const trending_state = state.trendingSongs; // eslint-disable-line

    if (!state.tvShowSeason.loaded || state.tvShowSeason.data.season._id != params.id) { // eslint-disable-line
      promises.push(dispatch(tvSeasonActions.load(params.id)));
    }

    if (!trending_state[trending_tv_songs.name]) {
      dispatch(trendingSongsActions.register(trending_tv_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_tv_songs.name)));
    }

    return Promise.all(promises);
  }
}])

export default class TvShowSeason extends Component {
  static propTypes = {
    tv_show_season: PropTypes.object,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    error: PropTypes.any,
    clear: PropTypes.func,
    pushNotification: PropTypes.func,
    addEpisode: PropTypes.func,
    editEpisode: PropTypes.func,
    params: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { tv_show_season, loading, loaded, error, pushNotification, addEpisode, editEpisode, params } = this.props;

    if (!tv_show_season || !tv_show_season.tv_show || !tv_show_season.season) {
      return <NotFound />;
    }

    const page_name = `${tv_show_season.tv_show.title}`;

    return (
      error && !loaded && !loading ? <NotFound /> :
      tv_show_season &&
      <div className="tv-show">

        <Helmet {...{
          htmlAttributes: {
            lang: 'en'
          },
          title: `${page_name} Season ${tv_show_season.season.season} Soundtrack & List of Songs | WhatSong`,
          meta: [
            {name: 'description', content: `Find the complete list of songs from ${page_name} - Season ${tv_show_season.season.season}, with scene descriptions, sorted by episode. Download & listen to the music, ost, list of songs & score.`},
            {name: 'keywords', content: `${page_name} soundtrack, ${page_name} list of songs, ${page_name} music, ost, download, listen`},
            {charset: 'utf-8'},
            {property: 'og:site_name', content: 'WhatSong'},
            {property: 'og:locale', content: 'en_US'},
            {property: 'og:title', content: `'${page_name} Soundtrack - Season ${tv_show_season.season.season} List of Songs`},
            {property: 'og:description', content: `List of Songs from ${page_name.toUpperCase()} - Season ${tv_show_season.season.season}, w/ scene descriptions, broken down by episode. Download & listen to the ost soundtrack, list of songs & score music.`},
            {property: 'og:image', content: '/logo.jpg'},
            {property: 'og:image:width', content: '200'},
            {property: 'og:image:height', content: '200'}
          ],
          link: [
            {rel: 'canonical', href: `https://www.what-song.com/Tvshow/${tv_show_season.tv_show._id}/${tv_show_season.tv_show.slug}/s/${tv_show_season.season._id}`}
          ]
        }} />

        <ItemPage.Banner
          info={tv_show_season.tv_show}
          banner={tv_show_season.banner}
          like_type="tv_show"
          headline_first={`${tv_show_season.tv_show.title} - Season ${tv_show_season.season.season} Soundtrack`}
          headline_second={`${tv_show_season.episodes.length} Episode${tv_show_season.episodes.length > 1 ? 's' : '' }`}
          pushNotification={(...args)=>pushNotification('favorite_tv_show', ...args)}
          fb_like_props={{
            url_type: 'tv_show',
            url_props: {id: params.tv_show_id, slug: params.tv_show_title},
            height: '21',
            width: '130'
          }}
        />

        <div className="main">
          <div className="content">
            <ItemPage.Breadcrumbs
              crumbs={[
                {name: tv_show_season.tv_show.title, link: urlGenerator('tv_show', {id: tv_show_season.tv_show._id, slug: (tv_show_season.tv_show.slug || '')})},
                {name: `Season ${tv_show_season.season.season}`}
              ]}
            />

            <ItemPage.Episodes
              banner={<div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Header"></div>}
              showBannerWhenLimit={3}
              showBannerAfterIndex={3}
              handleAdd={data=>addEpisode({...data, season: tv_show_season.season._id})}
              handleEdit={data=>editEpisode({...data, season: tv_show_season.season._id})}
              episodes={tv_show_season.episodes}
              tv_show={tv_show_season.tv_show}
            />
            <div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Footer"></div>
            <ItemPage.PopularSongs
              list={tv_show_season.PopularSongs}
              title={`Popular Music from ${tv_show_season.tv_show.title}`}
            />
            <div className="GRVAd leaderboard-fixed" data-ad-type="leaderboard-fixed" data-ad-sizes="[[320,50],[728,90]]" data-ad-sizeMapping="leaderboardNoBillboard" data-ad-fixedMapping="leaderboard" data-ad-refreshMapping="always"></div>

            {/*<ItemPage.Discussion hide_question_form={true} />*/}

          </div>
          <ScreenType.Desktop>
            <div className="sidebar">
              <Sticky bottomBoundary={'.content'}>
                <div className="sidebar-sticky">

                  <ItemPage.ThemeSong song={tv_show_season.theme_song}/>
                  <div className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="TopRail"></div>
                  {/*<SpotifyList list={tv_show_season.PopularSongs} />*/}
                  <BriefIntro title={'COMPOSER'} content={tv_show_season.tv_show.composer} />
                  <BriefIntro title={'MUSIC SUPERVISOR'} content={tv_show_season.tv_show.music_supervisor} />
                  <BriefIntro title={'NETWORK'} content={tv_show_season.tv_show.network} />
                  {tv_show_season.Contributors && <Contributors contributors={tv_show_season.Contributors}/> }
                  <BriefIntro title={'EXTERNAL LINKS'}>
                    {(tv_show_season.tv_show.imdb_id) ?
                    <div>
                      <ExternalLink title={'iMDB'} content={tv_show_season.tv_show.imdb_id ? `http://www.imdb.com/title/tt${tv_show_season.tv_show.imdb_id}/` : ''} />
                      <ExternalLink title={'TMDB'} content={tv_show_season.tv_show.moviedb_id ? `https://www.themoviedb.org/tv/${tv_show_season.tv_show.moviedb_id}` : ''} />
                    </div>
                    : null}
                  </BriefIntro>
                  <TrendingSongs name={trending_tv_songs.name}/>
                  <div className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="BottomRail"></div>
                </div>
              </Sticky>
            </div>
          </ScreenType.Desktop>

          <ScreenType.TabletMax>
            <div className="tablet_brief">
              <ItemPage.ThemeSong song={tv_show_season.theme_song}/>
              <BriefIntro title={'COMPOSER'} content={tv_show_season.tv_show.composer} />
              <BriefIntro title={'MUSIC SUPERVISOR'} content={tv_show_season.tv_show.music_supervisor} />
              <BriefIntro title={'NETWORK'} content={tv_show_season.tv_show.network} />
              <Contributors contributors={tv_show_season.Contributors}/>
              <BriefIntro title={'EXTERNAL LINKS'}>
              {(tv_show_season.tv_show.imdb_id || tv_show_season.tv_show.moviedb_id) ?
              <div>
                <ExternalLink title={'iMDB'} content={tv_show_season.tv_show.imdb_id ? `http://www.imdb.com/title/tt${tv_show_season.tv_show.imdb_id}/` : ''} />
                <ExternalLink title={'TMDB'} content={tv_show_season.tv_show.moviedb_id ? `https://www.themoviedb.org/movie/${tv_show_season.tv_show.moviedb_id}` : ''} />
              </div>
              : null}
              </BriefIntro>
            </div>
          </ScreenType.TabletMax>
        </div>
      </div>
    );
  }
}
