import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { ItemPage, TrendingSongs, Contributors, GoogleAd } from 'components'; // eslint-disable-line
import { ScreenType } from 'elements';
import Sticky from 'react-stickynode';
import { asyncConnect } from 'redux-connect';
import * as tvEpisodeActions from 'redux/modules/tvShowEpisode';
import { connect } from 'react-redux';
import { NotFound } from 'containers';
import * as trendingSongsActions from 'redux/modules/trendingSongs'; // eslint-disable-line
import { urlGenerator, printDate } from 'utils/helpers';
import * as notificationsActions from 'redux/modules/notifications';
import Helmet from 'react-helmet';

const trending_tv_songs = {
  name: 'trending_tv_songs',
  label: 'trending in Television',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-tv?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-tv?filter=month'
    }
  }
};

@connect(
  state => ({
    tv_show_episode: state.tvShowEpisode.data,
    loaded: state.tvShowEpisode.loaded,
    error: state.tvShowEpisode.error,
    loading: state.tvShowEpisode.loading,
    user: state.auth.user
  }),
  {
    clear: tvEpisodeActions.clear,
    pushNotification: notificationsActions.push
  }
)

@asyncConnect([{
  promise: ({params, store: {dispatch, getState}}) => {
    const promises = [];
    const state = getState();
    const trending_state = state.trendingSongs; // eslint-disable-line

    if (params.id) promises.push(dispatch(tvEpisodeActions.load(params.id)));

    /*
    if (!state.tvShowEpisode.loaded || state.tvShowEpisode.data.episode._id != params.id) { // eslint-disable-line
      promises.push(dispatch(tvEpisodeActions.load(params.id)));
    }
    */

    if (!trending_state[trending_tv_songs.name]) {
      dispatch(trendingSongsActions.register(trending_tv_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_tv_songs.name)));
    }

    return Promise.all(promises);
  }
}])

export default class TvShowEpisode extends Component {
  static propTypes = {
    tv_show_episode: PropTypes.object,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    error: PropTypes.any,
    clear: PropTypes.func,
    user: PropTypes.object,
    pushNotification: PropTypes.func,
    params: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { tv_show_episode, loading, loaded, error, pushNotification, user, params } = this.props;
    if (!tv_show_episode || !tv_show_episode.tv_show || !tv_show_episode.season || !tv_show_episode.episode) {
      return <NotFound />;
    }

    const page_name = `${tv_show_episode.tv_show.title} - S${tv_show_episode.season.season}E${tv_show_episode.episode.number} "${tv_show_episode.episode.name}"`;

    return (
      error && !loaded && !loading ? <NotFound /> :
      tv_show_episode &&
      <div className="tv-show">

        <Helmet {...{
          htmlAttributes: {
            lang: 'en'
          },
          title: `${page_name} - Music and List of Songs`,
          meta: [
            {name: 'description', content: `List of songs from ${page_name}, with scene descriptions. Download & listen to the music, ost, list of songs & score.`},
            {charset: 'utf-8'},
            {property: 'og:site_name', content: 'What-song'},
            {property: 'og:locale', content: 'en_US'},
            {property: 'og:title', content: `${page_name} - Music and List of Songs`},
            {property: 'og:description', content: `List of songs from ${page_name}, w/ scene descriptions. Download & listen to the ost soundtrack, list of songs & score music.`},
            {property: 'og:image', content: '/logo.jpg'},
            {property: 'og:image:width', content: '200'},
            {property: 'og:image:height', content: '200'}
          ],
          link: [
            {rel: 'canonical', href: `https://www.what-song.com/Tvshow/${tv_show_episode.tv_show._id}/${tv_show_episode.tv_show.slug}/e/${tv_show_episode.episode._id}`}
          ]
        }} />

        <ItemPage.Banner
          info={tv_show_episode.tv_show}
          banner={tv_show_episode.banner}
          like_type="tv_show"
          season_number={tv_show_episode.season.season}
          episode_number={tv_show_episode.episode.number}
          headline_first={`${tv_show_episode.tv_show.title} - S${tv_show_episode.season.season}E${tv_show_episode.episode.number} Soundtrack`}
          headline_second={tv_show_episode.episode.name}
          pushNotification={(...args)=>pushNotification('favorite_tv_show', ...args)}
          fb_like_props={{
            url_type: 'tv_show',
            url_props: {id: params.tv_show_id, slug: params.tv_show_title},
            height: '21',
            width: '130'
          }}
        />

        <div className="main">
          <div className="content">
            <ItemPage.Breadcrumbs
              crumbs={[
                {
                  name: tv_show_episode.tv_show.title,
                  link: urlGenerator('tv_show', {id: tv_show_episode.tv_show._id, slug: (tv_show_episode.tv_show.slug || '')})
                },
                {
                  name: `Season ${tv_show_episode.season.season}`,
                  link: urlGenerator('tv_show_season', {
                    id: tv_show_episode.tv_show._id,
                    slug: (tv_show_episode.tv_show.slug || ''),
                    season_id: tv_show_episode.season._id
                  })
                },
                {
                  name: `Episode ${tv_show_episode.episode.number} ${tv_show_episode.episode.name ? ('-' + tv_show_episode.episode.name) : ''}`
                }
              ]}
            >
              {tv_show_episode.episode.date_released && <div className="date-episode">{printDate(tv_show_episode.episode.date_released)}</div>}
            </ItemPage.Breadcrumbs>
            <ItemPage.MainSongs
              list={tv_show_episode.CompleteListOfSongs}
              title="Complete List of Songs"
              sorting_enabled={true}
              default_sort={2}
              limit={9e9}
              page_type="episode"
              episode_name={tv_show_episode.episode.name}
              inject_index={[1, 9, 19, 29, 39, 49, 59, 69]}
              inject_node={
                <div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Header"></div>
              }
            />

            <div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Footer"></div>
            <ItemPage.Discussion />
            <div className="GRVAd leaderboard-fixed" data-ad-type="leaderboard-fixed" data-ad-sizes="[[320,50],[728,90]]" data-ad-sizeMapping="leaderboardNoBillboard" data-ad-fixedMapping="leaderboard" data-ad-refreshMapping="always"></div>
          </div>
          <ScreenType.Desktop>
            <div className="sidebar">
              <Sticky bottomBoundary={'.content'}>
                <div className="sidebar-sticky">

                <div className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="TopRail"></div>
                  {user && <ItemPage.EditPageButton>Add Songs</ItemPage.EditPageButton>}
                  <ItemPage.ThemeSong song={tv_show_episode.theme_song}/>
                  <Contributors contributors={tv_show_episode.Contributors || []}/>
                  <TrendingSongs name={trending_tv_songs.name}/>
                  <div className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="BottomRail"></div>
                </div>
              </Sticky>
            </div>
          </ScreenType.Desktop>

          <ScreenType.TabletMax>
            <ItemPage.ThemeSong song={tv_show_episode.theme_song}/>
          </ScreenType.TabletMax>

        </div>
      </div>
    );
  }
}
