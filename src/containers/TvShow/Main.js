import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { ItemPage, SpotifyList, /*TrendingSongs,*/ Contributors, GoogleAd, Intro, BriefIntro, ExternalLink } from 'components'; // eslint-disable-line
import { ScreenType, Popup } from 'elements';
import Sticky from 'react-stickynode';
import { asyncConnect } from 'redux-connect';
import * as tvMainActions from 'redux/modules/tvShowMain';
import { connect } from 'react-redux';
import { NotFound } from 'containers';
// import * as trendingSongsActions from 'redux/modules/trendingSongs'; // eslint-disable-line
import * as notificationsActions from 'redux/modules/notifications';
import Helmet from 'react-helmet';

// const trending_tv_songs = {
//   name: 'trending_tv_songs',
//   label: 'trending in Television',
//   page_size: 30,
//   variants: {
//     this_week: {
//       label: 'Week',
//       api_endpoint: '/trend-tv?filter=week'
//     },
//     popular: {
//       label: 'month',
//       api_endpoint: '/trend-tv?filter=month'
//     }
//   }
// };

@connect(
  state => ({
    tv_show: state.tvShowMain.data,
    user: state.auth.user,
    loaded: state.tvShowMain.loaded,
    error: state.tvShowMain.error,
    loading: state.tvShowMain.loading
  }),
  {
    clear: tvMainActions.clear,
    addSeason: tvMainActions.addSeason,
    pushNotification: notificationsActions.push
  }
)

@asyncConnect([{
  promise: ({params, store: {dispatch}}) => {
    const promises = [];
    //const state = getState();
    // const trending_state = state.trendingSongs; // eslint-disable-line

    if (params.id) promises.push(dispatch(tvMainActions.load(params.id)));

    /*
    if (!state.tvShowMain.loaded || state.tvShowMain.data.tv_show._id != params.id) { // eslint-disable-line
      promises.push(dispatch(tvMainActions.load(params.id)));
    }
    */

    // if (!trending_state[trending_tv_songs.name]) {
    //   dispatch(trendingSongsActions.register(trending_tv_songs));
    //   promises.push(dispatch(trendingSongsActions.load(trending_tv_songs.name)));
    // }

    return Promise.all(promises);
  }
}])

export default class TvShow extends Component {
  static propTypes = {
    tv_show: PropTypes.object,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    error: PropTypes.any,
    clear: PropTypes.func,
    addSeason: PropTypes.func,
    user: PropTypes.object,
    params: PropTypes.object,
    pushNotification: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      edit_popup_opened: false
    };
  }

  render() {
    const { tv_show, loading, loaded, error, user, addSeason, pushNotification, params } = this.props;
    const { edit_popup_opened } = this.state;

    return (
      error && !loaded && !loading ? <NotFound /> :
      tv_show &&
      <div className="tv-show">

        <Helmet {...{
          htmlAttributes: {
            lang: 'en'
          },
          title: `${tv_show.tv_show.title} Soundtrack - Complete List of Songs | WhatSong`,
          meta: [
            {name: 'description', content: `All ${tv_show.tv_show.song_count} songs from ${tv_show.tv_show.title}, with scene descriptions, sorted by episode. Download & listen to the music, ost, list of songs & score.`},
            {name: 'keywords', content: `${tv_show.tv_show.title} soundtrack, ${tv_show.tv_show.title} list of songs, ${tv_show.tv_show.title} music, ost, download, listen`},
            {charset: 'utf-8'},
            {property: 'og:site_name', content: 'WhatSong'},
            {property: 'og:locale', content: 'en_US'},
            {property: 'og:title', content: `${tv_show.tv_show.title} Soundtrack - Complete List of Songs`},
            {property: 'og:description', content: `All ${tv_show.tv_show.song_count} songs from ${tv_show.tv_show.title.toUpperCase()}, w/ scene descriptions, broken down by episode. Download & listen to the ost soundtrack, list of songs & score music.`},
            {property: 'og:image', content: '/logo.jpg'},
            {property: 'og:image:width', content: '200'},
            {property: 'og:image:height', content: '200'}
          ],
          link: [
            {rel: 'canonical', href: `https://www.what-song.com/Tvshow/${tv_show.tv_show._id}/${tv_show.tv_show.slug}`}
          ]
        }} />

        <ItemPage.Banner
          info={{...tv_show.tv_show, seasons_count: tv_show.seasons.length}}
          banner={tv_show.banner}
          like_type="tv_show"
          pushNotification={(...args)=>pushNotification('favorite_tv_show', ...args)}
          handleEdit={user && user.role === 'ADMIN' ? ()=>this.setState({edit_popup_opened: true}) : undefined}
          fb_like_props={{
            url_type: 'tv_show',
            url_props: {id: params.id, slug: params.title},
            height: '21',
            width: '130'
          }}
        />

        <div className="main">
          <ScreenType.Desktop>
            <div className="sidebar">
              <Sticky bottomBoundary={'.content'}>
                <div className="sidebar-sticky">
                  {
                    user && user.role === 'ADMIN' &&
                    <ItemPage.EditPageButton path="soundtracks">Add Soundtracks</ItemPage.EditPageButton>
                  }

                  <div className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="TopRail"></div>
                  <ItemPage.ThemeSong song={tv_show.theme_song}/>
                  {/*<SpotifyList list={tv_show.spotify_id} clear={true}/>*/}
                  <BriefIntro title={'COMPOSER'} content={tv_show.tv_show.composer} />
                  <BriefIntro title={'MUSIC SUPERVISOR'} content={tv_show.tv_show.music_supervisor} />
                  <BriefIntro title={'NETWORK'} content={tv_show.tv_show.network} />
                  <Contributors contributors={tv_show.Contributors}/>
                  <BriefIntro title={'EXTERNAL LINKS'}>
                    {(tv_show.tv_show.imdb_id) ?
                    <div>
                      <ExternalLink title={'iMDB'} content={tv_show.tv_show.imdb_id ? `http://www.imdb.com/title/tt${tv_show.tv_show.imdb_id}/` : ''} />
                      <ExternalLink title={'TMDB'} content={tv_show.tv_show.moviedb_id ? `https://www.themoviedb.org/tv/${tv_show.tv_show.moviedb_id}` : ''} />
                    </div>
                    : null}
                  </BriefIntro>
                  <div className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="BottomRail"></div>

                </div>
              </Sticky>
            </div>
          </ScreenType.Desktop>
          <div className="content">
            <Intro
              text={tv_show.tv_show.intro}
            />
            <ItemPage.Seasons
              banner={<div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Header"></div>}
              showBannerWhenLimit={2}
              showBannerAfterIndex={2}
              tv_show={tv_show.tv_show}
              seasons={tv_show.seasons}
              handleAdd={()=>addSeason(tv_show.tv_show._id)}
              allowAdd={user && user.role === 'ADMIN'}
            />
            <div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Footer"></div>
            <ItemPage.OfficialSoundtrack
              albums={tv_show.albums}
              title={tv_show.tv_show.title}
            />

            <ItemPage.PopularSongs
              list={tv_show.PopularSongs}
              title={`Popular Music from ${tv_show.tv_show.title}`}
            />
            <div className="GRVAd leaderboard-fixed" data-ad-type="leaderboard-fixed" data-ad-sizes="[[320,50],[728,90]]" data-ad-sizeMapping="leaderboardNoBillboard" data-ad-fixedMapping="leaderboard" data-ad-refreshMapping="always"></div>

            <ItemPage.Discussion hide_question_form={true} episode_link={true} />

            {/*<ItemPage.Discussion />*/}
          </div>
          <ScreenType.TabletMax>
            <div className="tablet_brief">
              <ItemPage.ThemeSong song={tv_show.tv_show.theme_song}/>
              <BriefIntro title={'COMPOSER'} content={tv_show.tv_show.composer} />
              <BriefIntro title={'MUSIC SUPERVISOR'} content={tv_show.tv_show.music_supervisor} />
              <BriefIntro title={'NETWORK'} content={tv_show.tv_show.network} />
              <Contributors contributors={tv_show.Contributors}/>
              <BriefIntro title={'EXTERNAL LINKS'}>
              {(tv_show.tv_show.imdb_id || tv_show.tv_show.moviedb_id) ?
              <div>
                <ExternalLink title={'iMDB'} content={tv_show.tv_show.imdb_id ? `http://www.imdb.com/title/tt${tv_show.tv_show.imdb_id}/` : ''} />
                <ExternalLink title={'TMDB'} content={tv_show.tv_show.moviedb_id ? `https://www.themoviedb.org/movie/${tv_show.tv_show.moviedb_id}` : ''} />
              </div>
              : null}
              </BriefIntro>
            </div>
          </ScreenType.TabletMax>

        </div>

        <ItemPage.Navigation
          next={Array.isArray(tv_show.tv_show.next) ? tv_show.tv_show.next[0] : tv_show.tv_show.next}
          prev={Array.isArray(tv_show.tv_show.previous) ? tv_show.movie.previous[0] : tv_show.tv_show.previous}
          link_type="tv_show"
        />

        <Popup
          is_opened={edit_popup_opened}
          handleClose={()=>this.setState({edit_popup_opened: false})}
          title="Edit TV show"
        >
          <ItemPage.EditTvShow tv_show={tv_show.tv_show}/>
        </Popup>
      </div>
    );
  }
}
