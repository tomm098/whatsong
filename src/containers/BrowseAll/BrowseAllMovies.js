import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { BrowseAllPage } from 'components';
import * as browseAllActions from 'redux/modules/browseAll';
import { asyncConnect } from 'redux-connect';
import { connect } from 'react-redux';
import { debounce, isInViewport } from 'utils/helpers';
import Sticky from 'react-stickynode';
import { Link } from 'react-router';
import Helmet from 'react-helmet';

@connect(
  state => ({
    data: state.browseAll.data,
    loading: state.browseAll.loading,
    loaded: state.browseAll.loaded,
    active_letters: state.browseAll.active_letters
  }),
  {
    clear: browseAllActions.clear,
    activateLetter: browseAllActions.activateLetter
  }
)

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];
    const state = getState();
    if (!state.browseAll.loaded && !state.browseAll.loading || state.browseAll.type !== '/all-movies') {
      promises.push(dispatch(browseAllActions.load('/all-movies')));
    }
    return Promise.all(promises);
  }
}])


export default class BrowseAllMovies extends Component {
  static propTypes = {
    data: PropTypes.array,
    clear: PropTypes.func,
    activateLetter: PropTypes.func,
    active_letters: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {
    };
    this.checkVisibility = debounce(this.checkVisibility.bind(this), 300);
  }

  componentDidMount() {
    if (window) {
      window.addEventListener('scroll', this.checkVisibility);
    }
  }

  componentWillUnmount() {
    //this.props.clear();
    if (window) {
      window.removeEventListener('scroll', this.checkVisibility);
    }
  }

  checkVisibility() {
    const { active_letters, activateLetter } = this.props;
    const letters = Object.keys(active_letters);
    letters.forEach(c=>{
      if (!isInViewport('letter_' + c)) {
        activateLetter(c, false);
      }
    });
  }

  render() {
    const { data, activateLetter, active_letters } = this.props;
    return (
      <div className="browse-all">

        <Helmet {...{
          htmlAttributes: {
            lang: 'en'
          },
          title: 'Browse Movies - WhatSong',
          meta: [
            {name: 'description', content: 'Browse the complete list of movies A-Z on WhatSong'},
            {charset: 'utf-8'},
            {property: 'og:site_name', content: 'What-song'},
            {property: 'og:locale', content: 'en_US'},
            {property: 'og:title', content: 'Browse Movies - WhatSong'},
            {property: 'og:description', content: 'Browse the complete list of movies A-Z on WhatSong'},
            {property: 'og:image', content: '/logo.jpg'},
            {property: 'og:image:width', content: '200'},
            {property: 'og:image:height', content: '200'}
          ]
        }} />

        <div className="content-header">
          <div className="content-header-wrapper">
            <div className="content-header-title">Browse movies</div>
            <div className="content-header-box">
              <Sticky>
                <BrowseAllPage.Navigation checkVisibility={this.checkVisibility} active_letters={active_letters} data={data.map(c=>c.letter)}/>
                <Link to="/tvshow/browse" className="browse-tv-shows">browse tv shows</Link>
              </Sticky>
            </div>
          </div>
        </div>
        <div className="main">
          <div className="browse-all-wrapp">
            <BrowseAllPage.List data={data} activateLetter={activateLetter} link_type="movie"/>
          </div>
        </div>
      </div>
    );
  }
}
