import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import Helmet from 'react-helmet';
import config from './../../config';
import { List, TrendingSongs } from 'components';
import { ScreenType } from 'elements';
import { asyncConnect } from 'redux-connect';
import * as listActions from '../../redux/modules/lists'; // eslint-disable-line
import Sticky from 'react-stickynode';
import * as trendingSongsActions from '../../redux/modules/trendingSongs'; // eslint-disable-line
import { connect } from 'react-redux';

const movies_page_main_list = {
  name: 'movies_page_main_list',
  label: 'Films',
  page_size: 24,
  url_type: 'movie',
  like_type: 'movie',
  notification_type: 'favorite_movie',
  static_links: [
    {
      label: 'List all',
      to: '/movies/browse'
    }
  ],
  variants: {
    lastest: {
      label: 'latest',
      api_endpoint: '/recent-movies'
    },
    popular: {
      label: 'popular',
      api_endpoint: '/popular-movies'
    }
  }
};

const movies_by_year_list = {
  name: 'movies_by_year_list',
  url_type: 'movie',
  like_type: 'movie',
  notification_type: 'favorite_movie',
  label: 'Popular by year',
  page_size: 12,
  static_links: [],
  variants_view: 'select',

  variants: {
    ...((years)=>{
      const q_year = (new Date()).getFullYear();
      const res = {};
      for (let i = 0; i < years; i++) {
        const year = q_year - i;
        res[`by_year_${year}`] = {
          label: '' + year,
          api_endpoint: `/api/popular-year?model_name=Movie&year=${year}`
        };
      }
      return res;
    })(20)
  }
};

const movies_page_main_list_mobile = {
  ...movies_page_main_list,
  name: 'movies_page_main_list_mobile',
  page_size: 8
};

const movies_by_year_list_mobile = {
  ...movies_by_year_list,
  name: 'movies_by_year_list_mobile',
  page_size: 4
};

const trending_tv_songs = {
  name: 'trending_tv_songs',
  label: 'trending in Television',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-tv?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-tv?filter=month'
    }
  }
};

const trending_movies_songs = {
  name: 'trending_movies_songs',
  label: 'trending in Film',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-movies?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-movies?filter=month'
    }
  }
};

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];
    const list_state = getState().lists; // eslint-disable-line
    const trending_state = getState().trendingSongs; // eslint-disable-line
    if (!list_state[movies_page_main_list.name]) {
      dispatch(listActions.register(movies_page_main_list));
      promises.push(dispatch(listActions.load(movies_page_main_list.name)));
    }
    if (!list_state[movies_by_year_list.name]) {
      dispatch(listActions.register(movies_by_year_list));
      promises.push(dispatch(listActions.load(movies_by_year_list.name)));
    }

    if (!trending_state[trending_tv_songs.name]) {
      dispatch(trendingSongsActions.register(trending_tv_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_tv_songs.name)));
    }
    if (!trending_state[trending_movies_songs.name]) {
      dispatch(trendingSongsActions.register(trending_movies_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_movies_songs.name)));
    }

    return Promise.all(promises);
  }
}])

@connect(
  state => ({
    lists: state.lists
  }),
  {
    registerList: listActions.register,
    loadList: listActions.load,
  }
)


export default class Movies extends Component {
  static propTypes = {
    loadList: PropTypes.func,
    registerList: PropTypes.func,
    lists: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { loadList, registerList, lists } = this.props;
    return (
      <div className="container-movies">
        <Helmet {...config.app.movies} />
        <div className="main">
          <aside className="sidebar">
            <Sticky bottomBoundary={'.content'}>
              <div className="sidebar-sticky">
                <TrendingSongs name={trending_tv_songs.name}/>
                <TrendingSongs name={trending_movies_songs.name}/>
              </div>
            </Sticky>
          </aside>
          <div className="content">
            <ScreenType.TabletMin>
              <div>
                <List name={movies_page_main_list.name} />
                <List name={movies_by_year_list.name} />
              </div>
            </ScreenType.TabletMin>

            <ScreenType.Mobile
              onEnter = {()=>{
                if (!lists[movies_page_main_list_mobile.name]) {
                  registerList(movies_page_main_list_mobile);
                  loadList(movies_page_main_list_mobile.name);
                }

                if (!lists[movies_by_year_list_mobile.name]) {
                  registerList(movies_by_year_list_mobile);
                  loadList(movies_by_year_list_mobile.name);
                }
              }}
            >
              <div>
                <List name={movies_page_main_list_mobile.name} />
                <List name={movies_by_year_list_mobile.name} />
              </div>
            </ScreenType.Mobile>
          </div>
        </div>
      </div>
    );
  }
}
