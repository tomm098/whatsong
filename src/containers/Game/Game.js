import React, {Component, PropTypes} from 'react';
import { ItemPage, Contributors, Intro, SecondNav, BriefIntro, SoundtrackInfo, /*, AmazonAd */} from 'components';
import { asyncConnect } from 'redux-connect';
import * as gameActions from 'redux/modules/game';
import { connect } from 'react-redux';
import { NotFound } from 'containers';
import Sticky from 'react-stickynode';
import { ScreenType, Popup } from 'elements';
import * as notificationsActions from 'redux/modules/notifications';
import Helmet from 'react-helmet';
import {} from '../Movie/Movie.scss';

@connect(
  state => ({
    dispatch: state.dispatch,
    game: state.game.data,
    loaded: state.game.loaded,
    error: state.game.error,
    loading: state.game.loading,
    user: state.auth.user
  }),
  {
    clear: gameActions.clear,
    pushNotification: notificationsActions.push
  }
)

@asyncConnect([{
  promise: ({params, store: {dispatch, getState}}) => {
    const promises = [];
    const state = getState();

    if (!state.game.loaded || state.game.data.game._id != params.id) { // eslint-disable-line
      promises.push(dispatch(gameActions.load(params.id)));
    }

    return Promise.all(promises);
  }
}])

export default class Game extends Component {
  static propTypes = {
    game: PropTypes.object,
    dispatch: PropTypes.func,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    error: PropTypes.any,
    clear: PropTypes.func,
    user: PropTypes.object,
    pushNotification: PropTypes.func,
    params: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {
      edit_popup_opened: false,
      soundtrack_info_open: false
    };

    this.closeModal = this.closeModal.bind(this);
  }

  componentWillUnmount() {
    //this.props.clear();
  }

  parseTextToView(text) {
    return !!text ? <div className="intro" dangerouslySetInnerHTML={{__html: text}}/> : null;
  }

  closeModal() {
    this.setState({soundtrack_info_open: false});
  }

  render() {
    const { game, loading, loaded, error, user, pushNotification, params } = this.props;
    const { edit_popup_opened, soundtrack_info_open } = this.state;
    // const soundtraks = (movie && movie.CompleteListOfSongs || []).filter(c=>c.is_soundtrack);
    const main_songs = (game && game.CompleteListOfSongs || []).filter(c=>c.is_song);
    const nof_songs = game.albums.reduce((p, c) => p + (c.songs.length || 0), game.CompleteListOfSongs.length || 0);

    let title = `${game.game.title} (${game.game.year}) Soundtrack - Complete List of Songs | WhatSong`;

    if (params.id === '102770') {
      title = `Bumblebee (2018) Soundtrack & List of Songs | What-Song`;
    }

    if (params.id === '102948') {
      title = `The Intruder (2019) Soundtrack & List of Songs | What-Song`;
    }

    if (params.id === '102891') {
      title = `Five Feet Apart (2019) Soundtrack & List of Songs | What-Song`;
    }

    if (params.id === '102913') {
      title = `Shazam! (2019) Soundtrack & List of Songs | What-Song`;
    }

    if (params.id === '102875') {
      title = `Greta (2019) Soundtrack - Complete List of Songs | What-Song`;
    }

    if (params.id === '102745') {
      title = `Green Book (2018) Soundtrack & List of Songs | WhatSong`;
    }

    if (params.id === '102257') {
      title = `Baby Driver (2017) Soundtrack & List of Songs | WhatSong`;
    }

    return (
      error && !loaded && !loading ? <NotFound /> :
        game &&
        <div className="movie">
          <Helmet {...{
            htmlAttributes: {
              lang: 'en'
            },
            title,
            meta: [
              {name: 'description', content: `All ${nof_songs} songs from the ${game.game.title} (${game.game.year}) game soundtrack, with scene descriptions. Listen to and download the music, ost, score, list of songs and trailers.`},
              {name: 'keywords', content: `${game.game.title} soundtrack, ${game.game.title} music, ${game.game.title} list of songs, ost, score, game, download, music, trailers`},
              {charset: 'utf-8'},
              {property: 'og:site_name', content: 'What-song'},
              {property: 'og:locale', content: 'en_US'},
              {property: 'og:title', content: title},
              {property: 'og:description', content: `All ${nof_songs} songs from ${game.game.title} (${game.game.year}), with scene descriptions. Listen to the music, ost, score, list of songs and trailers.`},
              {property: 'og:image', content: '/logo.jpg'},
              {property: 'og:image:width', content: '200'},
              {property: 'og:image:height', content: '200'}
            ],
            link: [
              {rel: 'canonical', href: `https://www.what-song.com/Games/Soundtrack/${game.game._id}/${game.game.slug}`}
            ]
          }} />

          <ItemPage.Banner
            like_type="movie"
            info={game.game}
            banner={game.banner}
            pushNotification={(...args)=>pushNotification('favorite_game', ...args)}
            handleEdit={user && user.role === 'ADMIN' ? ()=>this.setState({edit_popup_opened: true}) : undefined}
            fb_like_props={{
              url_type: 'movie',
              url_props: {id: params.id, slug: params.title},
              height: '21',
              width: '130'
            }}
          />

          <div className="wedge-space"></div>

          <SecondNav info={game.game} />

          <div className="main">
            <div className="content">

              {(user && user.role === 'ADMIN') &&
              <div className="add-song-div">
                <button
                  className="btn btn-add-song"
                  onClick={()=> this.setState({soundtrack_info_open: true})}
                >{`${game && game.game && game.game.soundtrack_info ? 'Edit' : 'Add'} Soundtrack Info`}</button>
              </div>}

              {game && game.game && game.game.soundtrack_info && this.parseTextToView(game.game.soundtrack_info)}

              {/*
              (main_songs.length !== 0) &&
              <div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Header"></div>
            */}

              <ItemPage.MainSongs
                list={main_songs}
                title="Complete List of Songs"
                importBtn={<ItemPage.AddToAppleMusic songs={main_songs} title={game.game.title} />}
                id="section-1"
                inject_node={
                  <div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Header"></div>
                }
                inject_index={[1, 9, 19, 29, 39, 49, 59, 69]}
              />

              <div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Footer"></div>

              <ItemPage.OfficialSoundtrack
                albums={game.albums}
                title={game.game.title}
                default_expanded={true}
                id="section-2"
              />
              <ItemPage.TrailerMusic trailers={game.TrailerMusic} id="section-3" />
              <ItemPage.Discussion id="section-4" />
              <div className="GRVAd leaderboard-fixed" data-ad-type="leaderboard-fixed" data-ad-sizes="[[320,50],[728,90]]" data-ad-sizeMapping="leaderboardNoBillboard" data-ad-fixedMapping="leaderboard" data-ad-refreshMapping="always"></div>

            </div>
            <ScreenType.Desktop>
              <aside className="sidebar">
                <Intro
                  text={game.game.intro}
                />
                <Sticky bottomBoundary={'.content'}>
                  <div className="sidebar-sticky">
                    {/* {user && <ItemPage.EditPageButton>Add Songs</ItemPage.EditPageButton>} */}

                    <div className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="TopRail"></div>

                    <BriefIntro title={'COMPOSER'} content={game.game.composer} />
                    <BriefIntro title={'MUSIC SUPERVISOR'} content={game.game.music_supervisor} />
                    <Contributors contributors={game.Contributors}/>
                    <div className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="BottomRail"></div>
                  </div>
                </Sticky>
              </aside>
            </ScreenType.Desktop>

            <ScreenType.TabletMax>
              <div className="tablet_brief">
                <BriefIntro title={'COMPOSER'} content={game.game.composer} />
                <BriefIntro title={'MUSIC SUPERVISOR'} content={game.game.music_supervisor} />
                <Contributors contributors={game.Contributors}/>
              </div>
            </ScreenType.TabletMax>
          </div>

          {/* <ItemPage.Navigation
          next={Array.isArray(movie.movie.next) ? movie.movie.next[0] : movie.movie.next}
          prev={Array.isArray(movie.movie.previous) ? movie.movie.previous[0] : movie.movie.previous}
          link_type="movie"
        /> */}
          <Popup
            is_opened={edit_popup_opened}
            handleClose={()=>this.setState({edit_popup_opened: false})}
            title="Edit game"
          >
            <ItemPage.EditGame game={game}/>
          </Popup>

          <Popup
            is_opened={soundtrack_info_open}
            handleClose={this.closeModal}
            title="Add soundtrack info"
          >
            <SoundtrackInfo movie={game} onClose={this.closeModal}/>
          </Popup>
        </div>
    );
  }
}
