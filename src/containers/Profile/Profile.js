import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Profile.scss';
import { ProfilePage } from 'components';
import { connect } from 'react-redux';
import { asyncConnect } from 'redux-connect';
import * as profileActions from 'redux/modules/profile';
import { NotFound } from 'containers';
import Helmet from 'react-helmet';

@connect(
  state => ({
    profile: state.profile.data,
    loaded: state.profile.loaded,
    loading: state.profile.loading,
    error: state.profile.error
  }),
  {
    clear: profileActions.clear
  }
)

@asyncConnect([{
  promise: ({params, store: {dispatch, getState}}) => {
    const promises = [];
    const { profile: { loaded, data } } = getState();
    if (!loaded || data && (data.user.username !== params.username)) {
      promises.push(dispatch(profileActions.load(params.username)));
    }
    return Promise.all(promises);
  }
}])

export default class Profile extends Component {
  static propTypes = {
    clear: PropTypes.func,
    profile: PropTypes.object,
    loaded: PropTypes.bool,
    loading: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentWillUnmount() {
    //this.props.clear();
  }

  render() {
    const { profile, loaded, loading, error} = this.props; // eslint-disable-line
    return (
      error && !loaded && !loading ? <NotFound /> :
      <div className="profile">

        <Helmet {...{
          htmlAttributes: {
            lang: 'en'
          },
          title: `${profile.user.username} Profile - WhatSong`,
          meta: [
            {charset: 'utf-8'},
            {property: 'og:site_name', content: 'What-song'},
            {property: 'og:locale', content: 'en_US'},
            {property: 'og:title', content: `${profile.user.username} Profile - WhatSong`},
            {property: 'og:image', content: '/logo.jpg'},
            {property: 'og:image:width', content: '200'},
            {property: 'og:image:height', content: '200'}
          ]
        }} />

        <div className="main">
          <div className="content">
            <section className="profile__wrapp">
              <h1 className="profile__title">Profile</h1>
              <ProfilePage.User user={profile.user}/>
              <ProfilePage.Stats data={profile.user}/>
              <ProfilePage.RecentlyFavoritedSongs user={profile.user} songs={profile.RecentlyFavoritedSongs}/>
              <ProfilePage.RecentlyFavoritedTitles user={profile.user} titles={profile.RecentlyFavoritedMovies}/>
            </section>
          </div>
          <aside className="sidebar">
            <ProfilePage.RecentlyThanked thanks={profile.RecentlyThankUsers}/>
            <ProfilePage.Thanked profile_user={profile.user}/>
          </aside>
        </div>
      </div>
    );
  }
}
