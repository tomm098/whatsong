import React, {Component, PropTypes} from 'react'; //eslint-disable-line
import { SettingsPage } from 'components';
import {} from './Settings.scss';
import Helmet from 'react-helmet';

export default class Settings extends Component {

  static propTypes = {}

  render() {
    return (
      <div className="settings">
        <Helmet title="Settings - WhatSong"/>
        <div className="settings__wrapp">
          <div className="settings__title">SETTINGS</div>
          <SettingsPage.SettingsForm />
        </div>
      </div>
    );
  }
}

