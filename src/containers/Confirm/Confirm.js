import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { Auth, InfoCard } from 'components'; //eslint-disable-line
import { Banner } from 'elements';
import * as authActions from 'redux/modules/auth';
import { executionEnvironment } from 'utils/helpers';
const image = require('./img1.jpg');

@connect(
    state => ({
      confirm_status: state.auth.confirm_status
    }),
  {
    confirm: authActions.confirm
  }
)
export default class Confirm extends Component {

  static propTypes = {
    confirm_status: PropTypes.string,
    confirm: PropTypes.func,
    params: PropTypes.object
  }

  componentDidMount() {
    if (executionEnvironment().canUseDOM) {
      this.props.confirm(this.props.params.confirm_code);
    }
  }

  getStatusMessage() {
    switch (this.props.confirm_status) {
      case 'progress':
        return (
          <div>
            checking...
          </div>
        );
      case 'success':
        return (
          <div>
            <p>You are now the newest member of our community. You can now start contributing to movies, television shows and games. You can
            also start making playlists of your favorite television and movie songs, scores and soundtracks.</p>
            <p>We hope you enjoy your time here.</p>
            <p>Much love.</p>
          </div>
        );
      case 'fail':
        return (
          <div>
            <p>You seemed to have used the wrong confirmation link. Please email staff to see what the problem is.</p>
          </div>
        );
      default:
        return (
          <div>
            unknown status
          </div>
        );
    }
  }

  render() {
    const { confirm_status } = this.props;
    return (
      <div className="container-confirm">
        <Banner image={image}>
          {confirm_status === 'success' && <div className="banner__message">Welcome to our community!</div>}
        </Banner>
        <InfoCard
          title={confirm_status === 'success' ? 'Success' : confirm_status === 'fail' ? 'UNSUCCESSFUL' : '' }
        >
        {
          this.getStatusMessage()
        }
        </InfoCard>
      </div>
    );
  }
}
