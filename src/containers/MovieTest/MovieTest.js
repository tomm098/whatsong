import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { ItemPage, TrendingSongs, Contributors, Intro/*, AmazonAd */} from 'components';
import { asyncConnect } from 'redux-connect';
import * as movieActions from 'redux/modules/movie';
import { connect } from 'react-redux';
import { NotFound } from 'containers';
import Sticky from 'react-stickynode';
import * as trendingSongsActions from '../../redux/modules/trendingSongs'; // eslint-disable-line
import { ScreenType, Popup } from 'elements';
import * as notificationsActions from 'redux/modules/notifications';
import Helmet from 'react-helmet';

const trending_movies_songs = {
  name: 'trending_movies_songs',
  label: 'trending songs',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-movies?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-movies?filter=month'
    }
  }
};

@connect(
  state => ({
    movie: state.movie.data,
    loaded: state.movie.loaded,
    error: state.movie.error,
    loading: state.movie.loading,
    user: state.auth.user
  }),
  {
    clear: movieActions.clear,
    pushNotification: notificationsActions.push
  }
)

@asyncConnect([{
  promise: ({params, store: {dispatch, getState}}) => {
    const promises = [];
    const state = getState();
    const trending_state = state.trendingSongs; // eslint-disable-line

    if (!state.movie.loaded || state.movie.data.movie._id != params.id) { // eslint-disable-line
      promises.push(dispatch(movieActions.load(params.id)));
    }

    if (!trending_state[trending_movies_songs.name]) {
      dispatch(trendingSongsActions.register(trending_movies_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_movies_songs.name)));
    }

    return Promise.all(promises);
  }
}])

export default class Movie extends Component {
  static propTypes = {
    movie: PropTypes.object,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    error: PropTypes.any,
    clear: PropTypes.func,
    user: PropTypes.object,
    pushNotification: PropTypes.func,
    params: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {
      edit_popup_opened: false
    };
  }

  componentWillUnmount() {
    //this.props.clear();
  }

  render() {
    const { movie, loading, loaded, error, user, pushNotification, params } = this.props;
    const { edit_popup_opened } = this.state;
    // const soundtraks = (movie && movie.CompleteListOfSongs || []).filter(c=>c.is_soundtrack);
    const main_songs = (movie && movie.CompleteListOfSongs || []).filter(c=>c.is_song);

    const nof_songs = movie.albums.reduce((p, c) => p + (c.songs.length || 0), movie.CompleteListOfSongs.length || 0);
    return (
      error && !loaded && !loading ? <NotFound /> :
      movie &&
      <div className="movie">

        <Helmet {...{
          htmlAttributes: {
            lang: 'en'
          },
          title: `${movie.movie.title} Soundtrack & Complete List of Songs`,
          meta: [
            {name: 'description', content: `All ${nof_songs} songs from the ${movie.movie.title.toUpperCase()} movie soundtrack, w/ scene descriptions. Listen to the music, ost, score, list of songs and trailers. `},
            {charset: 'utf-8'},
            {property: 'og:site_name', content: 'What-song'},
            {property: 'og:locale', content: 'en_US'},
            {property: 'og:title', content: `${movie.movie.title} (${movie.movie.year}) Soundtrack and Complete List of Songs`},
            {property: 'og:description', content: `All ${nof_songs} songs from ${movie.movie.title} (${movie.movie.year}), with scene descriptions. Listen to the music, ost, score, list of songs and trailers.`},
            {property: 'og:image', content: '/logo.jpg'},
            {property: 'og:image:width', content: '200'},
            {property: 'og:image:height', content: '200'}
          ]
        }} />

        <ItemPage.Banner
          like_type="movie"
          info={movie.movie}
          banner={movie.banner}
          pushNotification={(...args)=>pushNotification('favorite_movie', ...args)}
          handleEdit={user && user.role === 'ADMIN' ? ()=>this.setState({edit_popup_opened: true}) : undefined}
          fb_like_props={{
            url_type: 'movie',
            url_props: {id: params.id, slug: params.title},
            height: '21',
            width: '130'
          }}
        />

        <div className="main">
            <div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Header"></div>
          <div className="content">

            <Intro
              text={movie.movie.intro}
            />
            <ItemPage.MainSongs
              list={main_songs}
              title="Complete List of Songs"
              /*inject_node={
                <ScreenType.Mobile>
                  <GoogleAd
                    slot="3016675009"
                    display="block"
                    width="300px"
                    height="250px"
                  />
                </ScreenType.Mobile>
              }
              inject_index={2}*/
            />

            <div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Footer"></div>

            <ItemPage.OfficialSoundtrack
              albums={movie.albums}
              title={movie.movie.title}
              default_expanded={true}
            />
            <ItemPage.TrailerMusic trailers={movie.TrailerMusic}/>
            <ItemPage.Discussion />
          </div>
          <ScreenType.Desktop>
            <aside className="sidebar">
              <Sticky bottomBoundary={'.content'}>
                <div className="sidebar-sticky">
                  {user && <ItemPage.EditPageButton>Add Songs</ItemPage.EditPageButton>}

                  <div className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="TopRail"></div>

                  <Contributors contributors={movie.Contributors}/>
                  <TrendingSongs name={trending_movies_songs.name}/>
                  <div className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="BottomRail"></div>
                </div>
              </Sticky>
            </aside>
          </ScreenType.Desktop>

          <ScreenType.TabletMax>
            <Contributors contributors={movie.Contributors}/>
          </ScreenType.TabletMax>
        </div>

        <ItemPage.Navigation
          next={Array.isArray(movie.movie.next) ? movie.movie.next[0] : movie.movie.next}
          prev={Array.isArray(movie.movie.previous) ? movie.movie.previous[0] : movie.movie.previous}
          link_type="movie"
        />
        <Popup
          is_opened={edit_popup_opened}
          handleClose={()=>this.setState({edit_popup_opened: false})}
          title="Edit movie"
        >
          <ItemPage.EditMovie movie={movie}/>
        </Popup>

      </div>
    );
  }
}
