import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import Helmet from 'react-helmet';
import config from './../../config';
import { List, TrendingSongs, SourceSelect } from 'components';
import { asyncConnect } from 'redux-connect';
import * as listActions from '../../redux/modules/lists'; // eslint-disable-line
import Sticky from 'react-stickynode';
import * as trendingSongsActions from '../../redux/modules/trendingSongs'; // eslint-disable-line
import { ScreenType } from 'elements';
import { connect } from 'react-redux';

const games_page_main_list = {
  name: 'games_page_main_list',
  label: 'Games',
  like_type: 'game',
  url_type: 'games',
  page_size: 24,
  static_links: [
    {
      label: 'List all',
      to: '/games/browse'
    }
  ],
  variants: {
    lastest: {
      label: 'Lastest',
      api_endpoint: '/latest-games'
    },
    popular: {
      label: 'Popular',
      api_endpoint: '/popular-games'
    }
  }
};

const games_by_year_list = {
  name: 'games_by_year_list',
  url_type: 'games',
  label: 'Popular by year',
  notification_type: 'favorite_game',
  like_type: 'game',
  page_size: 12,
  static_links: [],
  variants: {
    by_year: {
      label: '' + (new Date()).getFullYear(),
      api_endpoint: '/api/popular-year?model_name=Game'
    }
  }
};

const games_page_main_list_mobile = {
  ...games_page_main_list,
  name: 'games_page_main_list_mobile',
  page_size: 8
};

const games_by_year_list_mobile = {
  ...games_by_year_list,
  name: 'games_by_year_list_mobile',
  page_size: 4
};

const trending_tv_songs = {
  name: 'trending_tv_songs',
  label: 'trending in Television',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-tv?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-tv?filter=month'
    }
  }
};

const trending_movies_songs = {
  name: 'trending_movies_songs',
  label: 'trending in Film',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-movies?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-movies?filter=month'
    }
  }
};

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];
    const list_state = getState().lists; // eslint-disable-line
    const trending_state = getState().trendingSongs; // eslint-disable-line
    if (!list_state[games_page_main_list.name]) {
      dispatch(listActions.register(games_page_main_list));
      promises.push(dispatch(listActions.load(games_page_main_list.name)));
    }
    if (!list_state[games_by_year_list.name]) {
      dispatch(listActions.register(games_by_year_list));
      promises.push(dispatch(listActions.load(games_by_year_list.name)));
    }

    if (!trending_state[trending_tv_songs.name]) {
      dispatch(trendingSongsActions.register(trending_tv_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_tv_songs.name)));
    }
    if (!trending_state[trending_movies_songs.name]) {
      dispatch(trendingSongsActions.register(trending_movies_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_movies_songs.name)));
    }
    return Promise.all(promises);
  }
}])

@connect(
  state => ({
    lists: state.lists
  }),
  {
    registerList: listActions.register,
    loadList: listActions.load,
  }
)

export default class Games extends Component {
  static propTypes = {
    loadList: PropTypes.func,
    registerList: PropTypes.func,
    lists: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { loadList, registerList, lists } = this.props;
    return (
      <div className="container-games">
        <Helmet {...config.app.games} />
        <div className="main">
          <div className="sidebar">
            <Sticky bottomBoundary={'.content'}>
              <div className="sidebar-sticky">
                <SourceSelect />
                <TrendingSongs name={trending_tv_songs.name}/>
                <TrendingSongs name={trending_movies_songs.name}/>
              </div>
            </Sticky>
          </div>
          <div className="content">
            <ScreenType.TabletMin>
              <div>
                <List name={games_page_main_list.name} />
                <List name={games_by_year_list.name} />
              </div>
            </ScreenType.TabletMin>

            <ScreenType.Mobile
              onEnter = {()=>{
                if (!lists[games_page_main_list_mobile.name]) {
                  registerList(games_page_main_list_mobile);
                  loadList(games_page_main_list_mobile.name);
                }

                if (!lists[games_by_year_list_mobile.name]) {
                  registerList(games_by_year_list_mobile);
                  loadList(games_by_year_list_mobile.name);
                }
              }}
            >
              <div>
                <List name={games_page_main_list_mobile.name} />
                <List name={games_by_year_list_mobile.name} />
              </div>
            </ScreenType.Mobile>
          </div>
        </div>
      </div>
    );
  }
}
