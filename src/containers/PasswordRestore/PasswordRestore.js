import React, {Component, PropTypes} from 'react';
import { Auth } from 'components'; //eslint-disable-line
import { Banner } from 'elements';
import { InfoCard } from 'components';
import {} from './PasswordRestore.scss';
const image = require('./img2.jpg');

export default class PasswordRestore extends Component {

  static propTypes = {
    params: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {
      success_message_visible: false
    };
  }

  handleSuccess() {
    this.setState({success_message_visible: true});
  }

  render() {
    const {params: {restore_code}} = this.props;
    console.log(restore_code);
    return (
    restore_code &&
      <div className="container-password-restore">
        {
          this.state.success_message_visible ?
            <div>
              <Banner image={image}>
                <div className="banner__message">Password Successfully Changed</div>
              </Banner>
              <InfoCard
                title="SUCCESS"
              >
                <div>
                  <p>You have successfully changed your password. We’re so proud of you. You can now login here.</p>
                  <p>Now get back in there soldier.</p>
                </div>
              </InfoCard>
            </div>
            :
            <InfoCard
              title="SET YOUR NEW PASSWORD"
              modifier="gray"
            >
              <div>
                <p>Simply enter your new desired password below and you’re good to go! </p>
                <Auth.PasswordRestoreForm
                  handleSuccess={::this.handleSuccess}
                  restore_code={restore_code}
                />
              </div>
            </InfoCard>
        }
      </div>
    );
  }
}
