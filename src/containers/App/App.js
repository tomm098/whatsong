import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { asyncConnect } from 'redux-connect';
import Helmet from 'react-helmet';
import { Header, Footer, Auth, MobilePlayer, Player, Loader } from 'components';
import { ScreenType } from 'elements';
import { isLoaded as isAuthLoaded, load as loadAuth } from 'redux/modules/auth';
import config from '../../config';
import {} from './App.scss';

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];
    const state = getState();
    if (!state.reduxAsyncConnect.loaded && !isAuthLoaded(state) && !state.auth.error) {
      promises.push(dispatch(loadAuth()));
    }
    return Promise.all(promises);
  }
}])
@connect(
  state => ({
    user: state.auth.user,
    routing: state.routing,
    async_connect_loaded: state.reduxAsyncConnect.loaded
  }),
  {
    pushState: push
  }
)
export default class App extends Component {
  static propTypes = {
    children: PropTypes.object,
    history: PropTypes.object,
    location: PropTypes.object,
    user: PropTypes.object,
    pushState: PropTypes.func.isRequired,
    params: PropTypes.object,
    routing: PropTypes.object
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentDidMount() {
    /*eslint-disable */
    if (typeof window !== 'undefined') {
      require('autotrack/lib/plugins/clean-url-tracker');
      require('autotrack/lib/plugins/outbound-link-tracker');
      require('autotrack/lib/plugins/url-change-tracker');
      ga('create', 'UA-8107331-1', 'auto');
      ga('require', 'cleanUrlTracker');
      ga('require', 'outboundLinkTracker');
      ga('require', 'urlChangeTracker');
      ga('send', 'pageview');
    }
    if (!this.props.user) {
      this.updateGRVAds();
    }
    /*eslint-enable */
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.user && nextProps.user) {
      // login
      const {location: { pathname }} = this.props;
      this.props.pushState(pathname);
    } else if (this.props.user && !nextProps.user) {
      // logout
      this.props.pushState('/');
    }
  }

  componentDidUpdate(oldProps) {
    if (oldProps.location.pathname !== this.props.location.pathname) {
      if (!this.props.user) {
        this.updateGRVAds();
      }
    }
  }

  updateGRVAds() {
    try {
      if (typeof GRVAds !== 'undefined') {
        GRVAds.reset(); // eslint-disable-line
        GRVAds.findNewAdContainers(); // eslint-disable-line
      }
    } catch (err) {
      console.warn('GRVAds fail', err);
    }
  }

  render() {
    const { user, location: { pathname }, params, async_connect_loaded } = this.props; // eslint-disable-line
    const route_class =
      (pathname.replace((new RegExp(Object.keys(params).map(c=>params[c].replace(/[^\w\s]/g, '\\$&')).join('|'), 'g')), '')
        .replace(/^\/+|\/+$/g, '')
        .replace(/\/+/g, '-')
        .toLowerCase() || 'home')
        + '-route';
    return (
      <div className={'app ' + route_class}>
        <Helmet {...config.app.head} />
        <Header />

        <div key={pathname + (user ? 'logged' : 'not_logged')} className="wrapper">{this.props.children}</div>
        {!async_connect_loaded &&
          <div className="page-loader">
            <Loader />
          </div>
        }
        <Footer />
        <Auth.Modal />

        <ScreenType.Desktop>
          <Player />
        </ScreenType.Desktop>

        <ScreenType.TabletMax>
          <MobilePlayer />
        </ScreenType.TabletMax>

        <ScreenType.Detector />
      </div>
    );
  }
}
