import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './PrivacyPolicy.scss';

export default class PrivacyPolicy extends Component {
  static propTypes = {

  }
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    return (
      <div className="privacy-policy">
        <div className="main">
          <div className="content">
            <h1>Our commitment to privacy</h1>
            <p>Privacy is of paramount important to us. To better protect your privacy we provide this notice explaining our online information practices and the choices you can make about the way your information is collected and used.</p>
            <h2>The information we collect</h2>
            <p>We currently do not collect or store any information for visiting unauthenticated users. For users wishing to create accounts, on signup, we collect and store your email, a given username and a given password. This information will never be revealed or sold to any other person, company or entity. Your password is encrypted and stored within our database. It is unavailable to be accessed by any other person. It is however always recommended to use unique passwords. I.e. do not use exact username and password matches between different websites and applications.</p>
            <h2>Cookies & Interest-Based Advertisements</h2>
            <p>We use third party advertising networks to deliver our adverts. These networks may use online behavioural advertising cookies to enable them to tailor the adverts and to make them more relevant to you. You can opt-out and manage your setting for these types of cookies by visiting the Network Advertising (NAI) opt-out page at <a href="http://optout.networkadvertising.org">optout.networkadvertising.org</a></p>
            <p>Our website which links to this Interest-Based Advertising Policy allows third parties to collect certain information about you during your visit to the website for analytics purposes. These third parties can collect certain information about you to provide interest-based advertising to you.</p>
            <h2>Questions?</h2>
            <p>If you have any other questions regarding our policy please do not hesitate to email us at <em>staff [at] <a href="#">what-song.com</a></em> .</p>
          </div>
        </div>
      </div>
    );
  }
}
