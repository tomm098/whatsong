import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './TrendingMusic.scss';
import { TrendingMusic, Loader } from 'components';
import * as trendingMusicActions from 'redux/modules/trendingMusic';
import Helmet from 'react-helmet';
import { asyncConnect } from 'redux-connect';
import { connect } from 'react-redux';

@connect(
  state => ({
    data: state.trendingMusic.data,
    loading: state.trendingMusic.loading,
    type_variants: state.trendingMusic.type_variants,
    time_variants: state.trendingMusic.time_variants,
    active_type: state.trendingMusic.active_type,
    active_time: state.trendingMusic.active_time
  }),
  {
    load: trendingMusicActions.load,
    clear: trendingMusicActions.clear,
    setTime: trendingMusicActions.setTime,
    setType: trendingMusicActions.setType
  }
)

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];
    const state = getState();
    if (!state.trendingMusic.loaded && !state.trendingMusic.loading) {
      promises.push(dispatch(trendingMusicActions.load()));
    }
    return Promise.all(promises);
  }
}])

export default class TrendingMusicPage extends Component {
  static propTypes = {
    load: PropTypes.func,
    data: PropTypes.array,
    loading: PropTypes.bool,
    type_variants: PropTypes.array,
    time_variants: PropTypes.array,
    active_type: PropTypes.object,
    active_time: PropTypes.object,
    setTime: PropTypes.func,
    setType: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { data, active_time, active_type, setTime, setType, type_variants, time_variants, loading } = this.props;
    return (
      <div className="trending-music">

        <Helmet {...{
          htmlAttributes: {
            lang: 'en'
          },
          title: 'Trending Music - The Most Played Songs on WhatSong',
          meta: [
            {name: 'description', content: 'The top 100 songs from the latest tv shows and movies, filtered by day, week, month and more.'},
            {charset: 'utf-8'},
            {property: 'og:site_name', content: 'What-song'},
            {property: 'og:locale', content: 'en_US'},
            {property: 'og:title', content: 'Trending Music - The Most Played Songs on WhatSong'},
            {property: 'og:description', content: 'The top 100 songs from the latest tv shows and movies, filtered by day, week, month and more.'},
            {property: 'og:image', content: '/logo.jpg'},
            {property: 'og:image:width', content: '200'},
            {property: 'og:image:height', content: '200'}
          ]
        }} />

        <div className="main">
          <div className="trending-music__header">
            <div className="trending-music__header-panel">
              <div className="trending-music__header-left">
                <h1>Trending Music</h1>
                <p>The 100 Most Clicked Songs</p>
              </div>
              <div className="trending-music__header-right">
                { loading && <Loader type="type4" />}
                <TrendingMusic.Filter
                  {...{active_time, active_type, setTime, setType, type_variants, time_variants}}
                />
              </div>
            </div>
          </div>
          <TrendingMusic.Grid
            list={data}
            {...{active_time}}
          />
        </div>
      </div>
    );
  }
}
