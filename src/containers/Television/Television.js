import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import Helmet from 'react-helmet';
import config from './../../config';
import { List, TrendingSongs } from 'components';
import { asyncConnect } from 'redux-connect';
import * as listActions from '../../redux/modules/lists'; // eslint-disable-line
import Sticky from 'react-stickynode';
import * as trendingSongsActions from '../../redux/modules/trendingSongs'; // eslint-disable-line
import { ScreenType } from 'elements';
import { connect } from 'react-redux';

const television_page_main_list = {
  name: 'television_page_main_list',
  label: 'Television',
  page_size: 24,
  like_type: 'tvshow',
  url_type: 'tv_show',
  notification_type: 'favorite_tv_show',
  static_links: [
    {
      label: 'List all',
      to: '/tvshow/browse'
    }
  ],
  variants: {
    this_week: {
      label: 'This week',
      api_endpoint: '/recent-tv'
    },
    popular: {
      label: 'popular',
      api_endpoint: '/popular-tv-show'
    }
  }
};

const television_by_year_list = {
  name: 'television_by_year_list',
  label: 'Popular by year',
  like_type: 'tv_show',
  url_type: 'tv_show',
  notification_type: 'favorite_tv_show',
  page_size: 12,
  static_links: [],
  variants_view: 'select',
  variants: {
    ...((years)=>{
      const q_year = (new Date()).getFullYear();
      const res = {};
      for (let i = 0; i < years; i++) {
        const year = q_year - i;
        res[`by_year_${year}`] = {
          label: '' + year,
          api_endpoint: `/api/popular-year?model_name=TvShow&year=${year}`
        };
      }
      return res;
    })(20)
  }
};

const television_page_main_list_mobile = {
  ...television_page_main_list,
  name: 'television_page_main_list_mobile',
  page_size: 8
};

const television_by_year_list_mobile = {
  ...television_by_year_list,
  name: 'television_by_year_list_mobile',
  page_size: 4
};

const trending_tv_songs = {
  name: 'trending_tv_songs',
  label: 'trending in Television',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-tv?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-tv?filter=month'
    }
  }
};

const trending_movies_songs = {
  name: 'trending_movies_songs',
  label: 'trending in Film',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-movies?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-movies?filter=month'
    }
  }
};

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];
    const list_state = getState().lists; // eslint-disable-line
    const trending_state = getState().trendingSongs; // eslint-disable-line
    if (!list_state[television_page_main_list.name]) {
      dispatch(listActions.register(television_page_main_list));
      promises.push(dispatch(listActions.load(television_page_main_list.name)));
    }
    if (!list_state[television_by_year_list.name]) {
      dispatch(listActions.register(television_by_year_list));
      promises.push(dispatch(listActions.load(television_by_year_list.name)));
    }

    if (!trending_state[trending_tv_songs.name]) {
      dispatch(trendingSongsActions.register(trending_tv_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_tv_songs.name)));
    }
    if (!trending_state[trending_movies_songs.name]) {
      dispatch(trendingSongsActions.register(trending_movies_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_movies_songs.name)));
    }
    return Promise.all(promises);
  }
}])

@connect(
  state => ({
    lists: state.lists
  }),
  {
    registerList: listActions.register,
    loadList: listActions.load,
  }
)

export default class Television extends Component {
  static propTypes = {
    loadList: PropTypes.func,
    registerList: PropTypes.func,
    lists: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { loadList, registerList, lists } = this.props;
    return (
      <div className="container-television">
        <Helmet {...config.app.tvShows} />
        <div className="main">
          <div className="sidebar">
            <Sticky bottomBoundary={'.content'}>
              <div className="sidebar-sticky">
                <TrendingSongs name={trending_tv_songs.name}/>
                <TrendingSongs name={trending_movies_songs.name}/>
              </div>
            </Sticky>
          </div>
          <div className="content">

            <ScreenType.TabletMin>
              <div>
                <List name={television_page_main_list.name} />
                <List name={television_by_year_list.name} />
              </div>
            </ScreenType.TabletMin>

            <ScreenType.Mobile
              onEnter = {()=>{
                if (!lists[television_page_main_list_mobile.name]) {
                  registerList(television_page_main_list_mobile);
                  loadList(television_page_main_list_mobile.name);
                }

                if (!lists[television_by_year_list_mobile.name]) {
                  registerList(television_by_year_list_mobile);
                  loadList(television_by_year_list_mobile.name);
                }
              }}
            >
              <div>
                <List name={television_page_main_list_mobile.name} />
                <List name={television_by_year_list_mobile.name} />
              </div>
            </ScreenType.Mobile>

          </div>
        </div>
      </div>
    );
  }
}
