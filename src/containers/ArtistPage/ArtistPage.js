import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './ArtistPage.scss';
import { Song, SongList } from 'components';
import { ScreenType } from 'elements';
import * as artistPageActions from 'redux/modules/artistPage';
import { NotFound } from 'containers';
import { asyncConnect } from 'redux-connect';
import { connect } from 'react-redux';
import Sticky from 'react-stickynode';
import * as notificationsActions from 'redux/modules/notifications';
import Helmet from 'react-helmet';
import BriefIntro from '../../components/BriefIntro/BriefIntro';

@connect(
  state => ({
    data: state.artistPage.data,
    loaded: state.artistPage.loaded,
    error: state.artistPage.error,
    loading: state.artistPage.loading
  }),
  {
    pushNotification: notificationsActions.push
  }
)

@asyncConnect([{
  promise: ({params, store: {dispatch, getState}}) => {
    const promises = [];
    const state = getState();

    if (!state.artistPage.loaded || state.artistPage.data.Artist._id != params.id) { // eslint-disable-line
      promises.push(dispatch(artistPageActions.load(params.id)));
    }

    return Promise.all(promises);
  }
}])

export default class ArtistPage extends Component {
  static propTypes = {
    data: PropTypes.object,
    loading: PropTypes.bool,
    loaded: PropTypes.bool,
    error: PropTypes.any,
    pushNotification: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  get_ad_inject_indexes(arr = []) {
    const position = 12;
    const length = Array.isArray(arr) && arr.length || 0;
    const result = [];
    const iterations = Math.floor(length / position) + 1;
    let i = 1;

    if (length < position) {
      return [];
    }

    for (; i < iterations; i++) {
      result.push((position * i) - 1);
    }

    return result;
  }

  filter_by_time(arr) {
    if (!Array.isArray(arr)) {
      return arr;
    }

    arr.sort((a, b) => {
      const aTime = a.from ? new Date(a.from.time_released).getTime() : 0;
      const bTime = b.from ? new Date(b.from.time_released).getTime() : 0;
      return bTime - aTime;
    });
    return arr;
  }

  render() {
    const { data, loading, loaded, error, pushNotification } = this.props;

    return (
      (error && !loaded && !loading) || (loaded && (!data || !data.Artist)) ? <NotFound /> :
      <div className="artist-page">

        <Helmet {...{
          htmlAttributes: {
            lang: 'en'
          },
          title: `${data.Artist.name} - List of Songs heard in Movies & TV Shows`,
          meta: [
            {name: 'description', content: `List of all the songs by ${data.Artist.name.toUpperCase()}, heard in movies and tv shows. See scene descriptions, listen to their music and download songs.`},
            {charset: 'utf-8'},
            {property: 'og:site_name', content: 'What-song'},
            {property: 'og:locale', content: 'en_US'},
            {property: 'og:title', content: `${data.Artist.name} - List of Songs heard in Movies & TV Shows`},
            {property: 'og:description', content: `List of all ${data.SongList.length || 0} songs by ${data.Artist.name}, heard in movies and tv shows. See scene descriptions, listen to their music and download songs.`},
            {property: 'og:image', content: '/logo.jpg'},
            {property: 'og:image:width', content: '200'},
            {property: 'og:image:height', content: '200'}
          ],
          link: [
            {rel: 'canonical', href: `https://www.what-song.com/Artist/${data.Artist._id}/${data.Artist.slug}`}
          ]
        }} />
        <div className="main">
          <div className="content">
            <h1 className="artist-header">{data.Artist.name} - TV & Movie Placements</h1>
            <h2 className="artist-subtitle">The following is the complete list of all {data.SongList.length || 0} movies and tv shows that {data.Artist.name} have been heard or featured in.</h2>
            <br></br>
              <section className="artist-page__wrapp">
                <div className="artist-page__list-title">
                  <div className="artist-page__list-cell"></div>
                  <div className="artist-page__list-cell">SONG TITL0E</div>
                  <div className="artist-page__list-cell">MOVIE/SHOW</div>
                  <div className="artist-page__list-cell"></div>
                  <div className="artist-page__list-cell"></div>
                </div>
                <SongList songs={this.filter_by_time(data.SongList) || []}
                          inject_index={this.get_ad_inject_indexes(data.SongList || [])}
                          inject_node={
                            <div className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Header"></div>
                          }
                          limit={999999}>
                  <Song.SongArtist
                    modifier="artist"
                    handleLike={(...args)=>pushNotification('favorite_song', ...args)}
                  />
                </SongList>
              </section>
          </div>
          <aside className="sidebar">
            <Sticky bottomBoundary={'.content'}>
              <div className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="BottomRail"></div>
              <BriefIntro title={'EXTERNAL LINKS'}/>
            </Sticky>
          </aside>
        <ScreenType.Mobile>
          <div className="GRVAd leaderboard-fixed" data-ad-type="leaderboard-fixed" data-ad-sizes="[[320,50],[728,90]]" data-ad-sizeMapping="leaderboardNoBillboard" data-ad-fixedMapping="leaderboard" data-ad-refreshMapping="always"></div>
        </ScreenType.Mobile>
        </div>
      </div>
    );
  }
}
