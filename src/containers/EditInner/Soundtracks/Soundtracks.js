import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Soundtracks.scss';
import { EditPage } from 'components';
import { connect } from 'react-redux';
import * as iTunesSearchActions from 'redux/modules/iTunesSearch';
import { push } from 'react-router-redux';

@connect(
  state => ({
    search_results: state.iTunesSearch.album_data,
    search_loaded: state.iTunesSearch.album_loaded,
    search_loading: state.iTunesSearch.album_loading
  }),
  {
    clearItunes: iTunesSearchActions.clear,
    searchAlbums: iTunesSearchActions.searchAlbums,
    loadAlbumTracks: iTunesSearchActions.loadAlbumTracks,
    pushState: push,
    importTracks: iTunesSearchActions.importTracks
  }
)


export default class Shortlist extends Component {
  static propTypes = {
    pushState: PropTypes.func,
    clearItunes: PropTypes.func,
    searchAlbums: PropTypes.func,
    search_results: PropTypes.array,
    loadAlbumTracks: PropTypes.func,
    importTracks: PropTypes.func,
    search_loading: PropTypes.bool,
    search_loaded: PropTypes.bool
  }

  static contextTypes = {
    params: PropTypes.object,
    type: PropTypes.string,
    base_url: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentWillMount() {
    this.props.clearItunes();
  }

  render() {
    const { search_results, searchAlbums, loadAlbumTracks, importTracks, pushState, search_loaded, search_loading } = this.props;
    const { type, params: { id }, base_url } = this.context;
    return (
      <div className="shortlist">
        <EditPage.Title title="SOUNDTRACK" titleInfo="YOU ARE ADDING THE OFFICIAL SOUNDTRACK"/>
        <EditPage.MainSearch loading={search_loading} handleSearch={searchAlbums}/>
        {
          !search_results.length ? search_loaded ?
            <div className="no-matches">No matches found</div> : '' :
          <EditPage.SoundtrackResult
            loadAlbumTracks={loadAlbumTracks}
            results={search_results}
            handleImport={(...arg)=>importTracks(id, type, ...arg).then(()=>pushState(base_url))}
          />
        }
      </div>
    );
  }
}
