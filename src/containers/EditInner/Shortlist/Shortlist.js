import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Shortlist.scss';
import { EditPage } from 'components';
import { connect } from 'react-redux';
import * as editPageActions from 'redux/modules/editPage';
import { push } from 'react-router-redux';
import * as notificationsActions from 'redux/modules/notifications';

@connect(
  state => ({
    shortlist: state.editPage.shortlist
  }),
  {
    importSongs: editPageActions.importSongs,
    pushState: push,
    pushNotification: notificationsActions.push,
    clear: editPageActions.clear
  }
)

export default class Shortlist extends Component {
  static propTypes = {
    shortlist: PropTypes.array,
    importSongs: PropTypes.func,
    pushState: PropTypes.func,
    pushNotification: PropTypes.func,
    clear: PropTypes.func
  }

  static contextTypes = {
    base_url: PropTypes.string,
    params: PropTypes.object,
    type: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleImport() {
    const { importSongs, pushState, clear } = this.props;
    const { base_url, params, type } = this.context;
    importSongs({
      type: type,
      id: params.id
    })
    .then(()=>{clear(); pushState(base_url);});
  }

  render() {
    const { shortlist, pushNotification } = this.props;
    const { params } = this.context;
    return (
      <div className="shortlist">
        <EditPage.Title title="Shortlist" titleInfo="SONGS YOU ARE ADDING TO LIVE"/>
        {
          shortlist.length ?
            <div>
              <EditPage.Shortlist list={ shortlist } />
              <div className="shortlist__btn-wrapp">
                <button
                  className="btn btn--success"
                  onClick={()=>{
                    pushNotification('songs_add', {to: params.title.replace(/-/g, ' '), nof_songs: shortlist.length});
                    this.handleImport();
                  }}
                >
                  ADD {shortlist.length} SONG{shortlist.length !== 1 && 'S'}
                </button>
              </div>
            </div>
          :
            <div>Nothing to add</div>
        }
      </div>
    );
  }
}
