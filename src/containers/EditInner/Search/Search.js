import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { EditPage } from 'components';
import { Popup } from 'elements';
import {} from './Search.scss';
import { connect } from 'react-redux';
import * as iTunesSearchActions from 'redux/modules/iTunesSearch';
import * as editPageActions from 'redux/modules/editPage';
import { push } from 'react-router-redux';
import * as notificationsActions from 'redux/modules/notifications';

@connect(
  state => ({
    iTunes_search_result: state.iTunesSearch.data,
    iTunes_search_loading: state.iTunesSearch.loading,
    iTunes_search_loaded: state.iTunesSearch.loaded
  }),
  {
    searchItunes: iTunesSearchActions.search,
    clearItunes: iTunesSearchActions.clear,
    addToShortlist: editPageActions.addToShortlist,
    pushNotification: notificationsActions.push,
    pushState: push
  }
)

export default class EditPageSearchContainer extends Component {
  static propTypes = {
    searchItunes: PropTypes.func,
    clearItunes: PropTypes.func,
    iTunes_search_result: PropTypes.array,
    iTunes_search_loading: PropTypes.bool,
    iTunes_search_loaded: PropTypes.bool,
    addToShortlist: PropTypes.func,
    pushState: PropTypes.func,
    pushNotification: PropTypes.func
  }

  static contextTypes = {
    base_url: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {
      is_manual_add_opened: false
    };
  }

  componentWillMount() {
    this.props.clearItunes();
  }

  closeManualAddForm() {
    this.setState({is_manual_add_opened: false});
  }

  render() {
    const { searchItunes, iTunes_search_result, addToShortlist, pushNotification, /*pushState,*/ iTunes_search_loading, iTunes_search_loaded } = this.props;
    const handleAdd = (...args)=>{
      addToShortlist(...args);
      pushNotification('add_to_shortlist', {name: args[0].title, by: args[0].artist && args[0].artist.name});
      //pushState(this.context.base_url + '/edit/shortlist');
    };
    return (
      <div className="search">
        <EditPage.Title title="Search" titleInfo="YOU ARE SEARCHING ITUNES"/>
        <EditPage.MainSearch loading={iTunes_search_loading} handleSearch={searchItunes}/>
        {
          !iTunes_search_result.length ? iTunes_search_loaded ?
          <div className="no-matches">No matches found</div> : '' :
          <EditPage.MainSearchResult handleAdd={ handleAdd } results={ iTunes_search_result }/>
        }
        <div className="edit-search__info">
          <span>Can’t Find it in iTunes Search?&nbsp;</span>
          <a
            href="#"
            onClick={e => e.preventDefault() + this.setState({is_manual_add_opened: true})}
          >
            manual add
          </a>
        </div>
        <Popup
          is_opened={this.state.is_manual_add_opened}
          handleClose={::this.closeManualAddForm}
          title="Add Song Manually"
        >
          <EditPage.ManualAddForm handleAdd={ handleAdd }/>
        </Popup>
      </div>
    );
  }
}
