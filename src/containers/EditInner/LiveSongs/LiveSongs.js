import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './LiveSongs.scss';
import { EditPage } from 'components';
export default class LiveSongs extends Component {
  static propTypes = {

  }
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    return (
      <div className="live-songs">
        <EditPage.Title title="Live songs" titleInfo="SONGS THAT ARE CURRENTLY LIVE"/>
        <EditPage.Shortlist />
        <div className="live-songs__text">Currently there are no live songs on the database for this title. Add some from the Search tab on the right.</div>
      </div>
    );
  }
}
