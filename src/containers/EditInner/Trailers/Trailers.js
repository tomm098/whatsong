import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Trailers.scss';
import { EditPage } from 'components';
import * as editTrailerActions from 'redux/modules/editTrailers';
import * as youtubeSearchActions from 'redux/modules/youtubeSearch';


import { connect } from 'react-redux';

@connect(
  state => ({
    saved_trailers: state.editTrailers.saved_trailers,
    search_results: state.youtubeSearch.data,
    search_loaded: state.youtubeSearch.loaded,
    search_loading: state.youtubeSearch.loading
  }),
  {
    load: editTrailerActions.loadTrailers,
    clear: editTrailerActions.clear,
    search: youtubeSearchActions.search,
    clear_youtube: youtubeSearchActions.clear,
    addTrailer: editTrailerActions.addTrailer,
    removeTrailer: editTrailerActions.removeTrailer
  }
)

export default class Trailers extends Component {
  static propTypes = {
    load: PropTypes.func,
    clear: PropTypes.func,
    clear_youtube: PropTypes.func,
    search: PropTypes.func,
    addTrailer: PropTypes.func,
    removeTrailer: PropTypes.func,
    search_results: PropTypes.array,
    saved_trailers: PropTypes.array,
    search_loaded: PropTypes.bool,
    search_loading: PropTypes.bool
  }

  static contextTypes = {
    params: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    this.props.load(this.context.params.id);
  }

  componentWillUnmount() {
    this.props.clear();
    this.props.clear_youtube();
  }

  render() {
    const { saved_trailers, search_results, search, addTrailer, removeTrailer, search_loading, search_loaded } = this.props;
    // don't show already added trailers in search results
    const filtered_results = search_results.filter(c=>saved_trailers.findIndex(item=>c.id.videoId === item.youtube_id) === -1);
    return (
      <div className="trailers">
        <EditPage.Title title="TRAILERS" titleInfo="YOU ARE ADDING TRAILERS & SONGS"/>
        <EditPage.MainSearch loading={search_loading} handleSearch={(q=>search(q, 5))}/>
        {
          !filtered_results.length ? search_loaded ?
          <div className="no-matches">No matches found</div> : '' :
          <EditPage.TrailersSearchResults
            handleAdd={yt=>addTrailer(this.context.params.id, yt)}
            results={filtered_results}
          />
        }
        <EditPage.SavedTrailers trailers={saved_trailers} handleRemove={removeTrailer}/>
      </div>
    );
  }
}
