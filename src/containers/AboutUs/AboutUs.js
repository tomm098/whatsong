import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './AboutUs.scss';
import Helmet from 'react-helmet';
const image = require('./img1.jpg');

export default class AboutUs extends Component {
  static propTypes = {

  }
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    return (
      <div className="about-us">

        <Helmet {...{
          htmlAttributes: {
            lang: 'en'
          },
          title: 'About Us - WhatSong',
          meta: [
            {charset: 'utf-8'},
            {property: 'og:site_name', content: 'What-song'},
            {property: 'og:locale', content: 'en_US'},
            {property: 'og:title', content: 'About Us - WhatSong'},
            {property: 'og:image', content: '/logo.jpg'},
            {property: 'og:image:width', content: '200'},
            {property: 'og:image:height', content: '200'}
          ]
        }} />

        <div className="content-header">
          <div className="content-header-wrapper">
            <div className="content-header-title">About Us</div>
          </div>
        </div>
        <div className="main">
          <div className="content">
            <ul className="about-us__list">
              <li>
                <h4>Introduction</h4>
                <p>Whatsong was started way back in 2008 when I found myself hearing a lot of great music from film and television. I
                could find no resource to help find what it was. I assumed I wasnt the only one, so set off to teach myself
                web development and got started. I must say I was also mildy influenced by the movie Knocked Up. If you’ve seen the
                movie you may see the similarities.</p>
              </li>
              <li>
                <h4>What we do?</h4>
                <p>Not only does WhatSong provide the official soundtrack from the latest movies and television shows, we provide the complete list of
                songs with scene descriptions. Our content is generated from a combination of our admins as well as our users, so feel free to
                sign up if you want to contribute.</p>
              </li>
              <li>
                <h4>How do we stream our music?</h4>
                <p>All of our music is legally streamed from various API’s. Our 30 second sample samples are provided courtesy of iTunes while
                our full-length music videos are provided through the YouTube API. We also provide Spotify 30 second samples through the site
                at various times, however Spotify are yet to offer full-length playback through their API.</p>
              </li>
              <li>
                <h4>Can I add contribute to the website myself?</h4>
                <p>Yes of course. Songs, scene descriptions and trailers can all be added by our users. All you have to do is simply create an account.
                Unfortuantely TV shows must be added by our admin for the time being, but if you would like to request one, please send us an email
                and we’ll add it straight away.</p>
              </li>
            </ul>
            <div className="about-us__wrapp-form">
              <h4>Here’s our first version back in 2008. Behold.</h4>
              <img src={image} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
