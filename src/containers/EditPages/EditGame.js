import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './EditPage.scss';
import { EditPage } from 'components';
import { Link } from 'react-router';
import { urlGenerator } from 'utils/helpers';
import * as editPageActions from 'redux/modules/editPage';
import Sticky from 'react-stickynode';
import { ScreenType } from 'elements';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';

@connect(
  state => ({
    shortlist_length: state.editPage.shortlist.length,
    user: state.auth.user
  }),
  {
    clear: editPageActions.clear
  }
)


export default class EditGame extends Component {
  static propTypes = {
    children: PropTypes.node,
    params: PropTypes.object,
    clear: PropTypes.func,
    shortlist_length: PropTypes.number,
    user: PropTypes.object
  }

  static childContextTypes = {
    base_url: PropTypes.string,
    type: PropTypes.string,
    params: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  getChildContext() {
    const { params } = this.props;
    return {
      base_url: urlGenerator('games', {id: params.id, slug: params.title}),
      type: 'game',
      params
    };
  }

  componentDidMount() {
    this.props.clear(this.props.params.id);
  }

  render() {
    const { children, shortlist_length, params, user } = this.props;
    return (
      <div className="edit-page">

        <Helmet {...{
          title: 'Editing Game'
        }} />

        <div className="content-header">
          <div className="content-header-wrapper">
            <div className="content-header-title">
              Add Songs
              <div className="edit-page__title">
                <Link
                  to={urlGenerator('games', {id: params.id, slug: params.title})}
                >
                  &larr;
                  {params.title.replace(/-/g, ' ')}
                </Link>
              </div>
            </div>
            <div className="content-header-tabs">
              <EditPage.Navbar
                links={[
                  {
                    title: 'Search',
                    link: '/edit/search'
                  },
                  {
                    title: `Shortlist (${shortlist_length})`,
                    link: '/edit/shortlist'
                  },
                  ...(user && user.role === 'ADMIN' ? [{
                    title: 'Soundtracks',
                    link: '/edit/soundtracks'
                  },
                  {
                    title: 'Trailers',
                    link: '/edit/trailers'
                  }] : [])
                ]}
              />
            </div>
          </div>
        </div>
        <div className="main">
          <div className="sidebar">

            <ScreenType.Desktop>
              <Sticky bottomBoundary={'.content'}>
                <div className="edit-page__information">
                  <h3>INFORMATION…</h3>
                  <p>This form searches the iTunes
                  database making it easier for you
                  to find the song you’re looking for.</p>
                  <p>Please note once you have added
                  all the songs to the shortlist, you
                  must add them into the database
                  from the shortlist tab.</p>
                  <p>You can add further properties, like
                  scene descriptions and YouTube
                  links to your songs from the shortlist
                  tab.</p>
                  <h3>TIPS!</h3>
                  <p>“Less is More” - Try to type less in the
                  search form, and avoid using
                  special characters like the ones
                  below:</p>
                  <div className="edit-page__symbols">
                    & &nbsp; " &nbsp; ' &nbsp; ( &nbsp; ) &nbsp; ! &nbsp; $ &nbsp; [ &nbsp; ]
                  </div>
                </div>

              </Sticky>
            </ScreenType.Desktop>

          </div>
          <div className="content">
            <div className="content__wrapp">
              {children}
            </div>
          </div>
        </div>
        {console.log('edit game page')}
      </div>
    );
  }
}
