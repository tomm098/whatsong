import React, {Component, PropTypes} from 'react';
import {} from './SearchResult.scss';
import { connect } from 'react-redux';
import * as searchResultsActions from 'redux/modules/searchResults';
import { SearchResultTypedColumn, Loader } from 'components';


@connect(
  state => ({
    q: state.routing.locationBeforeTransitions.query.q,
    loaded: state.searchResults.loaded,
    loading: state.searchResults.loading,
    results: state.searchResults.data
  }),
  {
    search: searchResultsActions.search,
    clear: searchResultsActions.clear
  }
)

export default class SearchResult extends Component {

  static propTypes = {
    q: PropTypes.string,
    loaded: PropTypes.bool,
    loading: PropTypes.bool,
    search: PropTypes.func,
    clear: PropTypes.func,
    results: PropTypes.array
  }

  componentDidMount() {
    if (this.props.q) {
      this.props.search(this.props.q);
    }
  }

  componentWillReceiveProps(newProps) {
    if (this.props.q !== newProps.q) {
      if (newProps.q) {
        this.props.search(newProps.q);
      }
    }
  }

  componentWillUnmount() {
    this.props.clear();
  }

  render() {
    const { q, loaded, results, loading } = this.props;
    return (
      <div className="search-result" key={'q=' + q || 'noquery'}>
        <div className="search-result__wrapp">
          <div className="search-result__title">{q ? `SEARCH RESULTS for “${decodeURIComponent(q)}”` : 'No search query'}</div>
          {
            loaded && !loading &&
            <div className="search-result__row">
              {
                results.map((c, i)=>
                  <SearchResultTypedColumn key={i} list={c} />
                )
              }
            </div>
          }
          {
            loading && <Loader type="type2"/>
          }
          <div className="search-result__message">Can’t find what you’re looking for? Send us an email <a href="#">here</a> to request a title.</div>
        </div>
      </div>
    );
  }
}

