import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import Helmet from 'react-helmet';
import config from './../../config';
import {} from './Home.scss';
import { Banner, ScreenType } from 'elements';
import { Auth, List, TrendingSongs } from 'components'; // AmazonAd
import { asyncConnect } from 'redux-connect';
import * as listActions from '../../redux/modules/lists'; // eslint-disable-line
import * as trendingSongsActions from '../../redux/modules/trendingSongs'; // eslint-disable-line
import { connect } from 'react-redux';
import Sticky from 'react-stickynode';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

const homepage_films_list = {
  name: 'homepage_films_list',
  label: 'Movies',
  url_type: 'movie',
  like_type: 'movie',
  notification_type: 'favorite_movie',
  page_size: 12,
  static_links: [
    {
      label: 'List all',
      to: 'movies/browse'
    }
  ],
  variants: {
    lastest: {
      label: 'latest',
      api_endpoint: '/recent-movies'
    },
    popular: {
      label: 'popular',
      api_endpoint: '/popular-movies'
    }
  }
};

const homepage_tv_list = {
  name: 'homepage_tv_list',
  label: 'TV Shows',
  page_size: 12,
  like_type: 'tv_show',
  url_type: 'tv_show',
  notification_type: 'favorite_tv_show',
  static_links: [
    {
      label: 'List all',
      to: '/tvshow/browse'
    }
  ],
  variants: {
    this_week: {
      label: 'This week',
      api_endpoint: '/recent-tv'
    },
    popular: {
      label: 'popular',
      api_endpoint: '/popular-tv-show'
    }
  }
};

const trending_tv_songs = {
  name: 'trending_tv_songs',
  label: 'trending in Television',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-tv?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-tv?filter=month'
    }
  }
};

const trending_movies_songs = {
  name: 'trending_movies_songs',
  label: 'trending in Film',
  page_size: 30,
  variants: {
    this_week: {
      label: 'Week',
      api_endpoint: '/trend-movies?filter=week'
    },
    popular: {
      label: 'month',
      api_endpoint: '/trend-movies?filter=month'
    }
  }
};

const adPadding = {
  paddingBottom: '15px'
};

@asyncConnect([{
  promise: ({store: {dispatch, getState}}) => {
    const promises = [];
    const list_state = getState().lists; // eslint-disable-line
    const trending_state = getState().trendingSongs; // eslint-disable-line
    if (!list_state[homepage_films_list.name]) {
      dispatch(listActions.register(homepage_films_list));
      promises.push(dispatch(listActions.load(homepage_films_list.name)));
    }
    if (!list_state[homepage_tv_list.name]) {
      dispatch(listActions.register(homepage_tv_list));
      promises.push(dispatch(listActions.load(homepage_tv_list.name)));
    }

    if (!trending_state[trending_tv_songs.name]) {
      dispatch(trendingSongsActions.register(trending_tv_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_tv_songs.name)));
    }
    if (!trending_state[trending_movies_songs.name]) {
      dispatch(trendingSongsActions.register(trending_movies_songs));
      promises.push(dispatch(trendingSongsActions.load(trending_movies_songs.name)));
    }
    return Promise.all(promises);
  }
}])

@connect(
  state => ({
    user: state.auth.user
  }),
  {

  }
)

export default class Home extends Component {
  static propTypes = {
    user: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { user } = this.props;
    return (
      <div className="container-home">
        <Helmet {...config.app.home} />
        {!user && <Banner seo={{alt: 'WhatSong Soundtracks - Stream Songs from the Latest Movies & TV Shows'}}><Auth.HomepageSignUp /></Banner>}
        <div className="GRVAd leaderboard-fixed" data-ad-type="leaderboard-fixed" data-ad-sizes="[[320,50],[728,90]]" data-ad-sizeMapping="leaderboardNoBillboard" data-ad-fixedMapping="leaderboard" data-ad-refreshMapping="always"></div>
        <ScreenType.TabletMin>
          <div className="main">
            <div className="content">
              <List name={homepage_films_list.name} />
              <div style={adPadding} className="GRVAd" data-ad-type="leaderboard" data-ad-sizeMapping="Header"></div>
              <List name={homepage_tv_list.name} />
            </div>
            <aside className="sidebar">
              <Sticky bottomBoundary={'.content'}>
                <div className="sidebar-sticky">
                  <TrendingSongs name={trending_tv_songs.name}/>
                  <div style={adPadding} className="GRVAd" data-ad-type="mpu" data-ad-sizeMapping="TopRail"></div>
                  <TrendingSongs name={trending_movies_songs.name}/>
                </div>
              </Sticky>
            </aside>
          </div>
        </ScreenType.TabletMin>

        <ScreenType.Mobile>
          <div className="content">
            <Tabs>
              <TabList>
                <Tab>
                Films
                </Tab>
                <Tab>
                Television
                </Tab>
                <Tab>
                Music
                </Tab>
              </TabList>

              <TabPanel>
                <List name={homepage_films_list.name} />
              </TabPanel>
              <TabPanel>
                <List name={homepage_tv_list.name} />
              </TabPanel>
              <TabPanel>
                <div>
                  <TrendingSongs name={trending_tv_songs.name}/>
                  <TrendingSongs name={trending_movies_songs.name}/>
                </div>
              </TabPanel>
            </Tabs>
          </div>
        </ScreenType.Mobile>
      </div>
    );
  }
}
