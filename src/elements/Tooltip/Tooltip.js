import React, {Component, PropTypes} from 'react';
//import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {} from './Tooltip.scss';

class Tooltip extends Component {
  static propTypes = {
    trigger: PropTypes.node,
    children: PropTypes.object,
    className: PropTypes.string
  }

  static defaultProps = {
    className: ''
  }

  constructor(props) {
    super(props);
    this.close = this.close.bind(this);
    this.state = {
      is_open: false
    };
  }

  open() {
    this.setState({is_open: true});
  }

  close() {
    this.setState({is_open: false});
  }

  render() {
    const { className, trigger, children} = this.props;
    const { is_open } = this.state;
    return (
      <div onMouseEnter={::this.open} onMouseLeave={::this.close} className={'dropdown-hover ' + className + (is_open ? ' active' : '')}>
        <div className="dropdown-hover__trigger" onClick={::this.open}>
          {trigger}
        </div>

        {
          is_open &&
          <div className="dropdown-hover__content open">
            {children}
          </div>
        }

        {/*<ReactCSSTransitionGroup transitionName="dropdown-hover" component="div" transitionLeaveTimeout={200} transitionEnterTimeout={200}>
        </ReactCSSTransitionGroup>*/}
      </div>
    );
  }
}
export default Tooltip;
