import React, {Component, PropTypes} from 'react';
import {} from '../Form/Select/Select.scss';
import {} from './SimpleSelect.scss';
import { Form } from 'elements';

export default class SimpleSelect extends Component {
  static propTypes = {
    className: PropTypes.string,
    classNameInput: PropTypes.string,
    defaultLabel: PropTypes.string,
    list: PropTypes.array
  }

  static defaultProps = {
    className: '',
    multiple: false
  }

  constructor(props) {
    super(props);
  }

  findDefault() {
    const {list} = this.props;
    let result = false;
    if (list.length > 0) {
      list.map((item) => {
        if (item.defaultValue) {
          result = true;
          return false;
        }
      });
    }
    return result;
  }

  render() {
    const { className, classNameInput, defaultLabel, list, ...rest } = this.props;
    return (
      <Form.Group>
        <div className={className}>
          {list.length > 0 &&
          <select
            {...rest}
            className={'form-control ' + (classNameInput ? classNameInput : '')}
          >
            {!this.findDefault() && defaultLabel &&
            <option disabled value={''}>{defaultLabel}</option>
            }
            {list.map((item, i) => {
              if (!item.disabled) {
                return (
                  <option key={i} value={item.value}>{item.label}</option>
                );
              }
              return (
                <option key={i} value={item.value} disabled>{item.label}</option>
              );
            })}
          </select>}
        </div>
      </Form.Group>
    );
  }
}
