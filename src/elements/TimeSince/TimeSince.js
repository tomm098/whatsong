import React, {Component, PropTypes} from 'react';
import { timeSince } from 'utils/helpers';

const presets = {
  min3: [
    { name: 'sec', limit: 60, in_seconds: 1 },
    { name: 'min', limit: 3600, in_seconds: 60 },
    { name: 'hrs', limit: null, in_seconds: 3600 }
  ]
};

export default class TimeSince extends Component {
  static propTypes = {
    time: PropTypes.string,
    preset: PropTypes.string,
    plural: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state = {
      interval: false,
      date: (new Date(this.props.time)).valueOf(),
      result: ''
    };
  }

  componentDidMount() {
    this.initInterval();
  }

  componentWillReceiveProps(newProps) {
    this.parseDate(newProps.time);
  }

  componentWillUnmount() {
    if (this.state.interval) {
      clearInterval(this.state.interval);
    }
  }

  initInterval() {
    this.setState({interval: setInterval(::this.calculateTime, 5000)});
    this.calculateTime();
  }

  calculateTime() {
    this.setState({result: timeSince(this.state.date, this.props.preset && presets[this.props.preset], this.props.plural)});
  }

  parseDate(time) {
    this.setState({date: (new Date(time)).valueOf()});
  }

  render() {
    return (
      <span>{this.state.result} ago</span>
    );
  }
}
