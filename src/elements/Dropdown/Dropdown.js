import React, {Component, PropTypes} from 'react';
// import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import {} from './Dropdown.scss';

class Dropdown extends Component {
  static propTypes = {
    trigger: PropTypes.node,
    children: PropTypes.object,
    className: PropTypes.string,
    self_closing: PropTypes.bool
  }

  static defaultProps = {
    className: ''
  }

  constructor(props) {
    super(props);
    this.close = this.close.bind(this);
    this.state = {
      is_open: false,
      cant_close: false
    };
  }

  componentDidMount() {
    window.addEventListener('click', this.close);
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.close);
  }

  toggle() {
    this.setState({is_open: !this.state.is_open, cant_close: true});
    setTimeout(()=>this.setState({cant_close: false}), 200);
  }

  close(e) {
    if (!this.state.cant_close && this.state.is_open && !this.refs.content.contains(e.target)) {
      this.setState({is_open: false});
    }
  }

  render() {
    const { className, trigger, children} = this.props;
    const { is_open } = this.state;
    return (
      <div className={'dropdown ' + className + (is_open ? ' active' : '')}>
        <div className="dropdown__trigger" onClick={::this.toggle}>
          {trigger}
        </div>
        {
          is_open &&
          <div onClick={()=>this.props.self_closing && this.toggle()} className="dropdown__content open" ref="content">
            {children}
          </div>
        }
        {/*<ReactCSSTransitionGroup transitionName="dropdown" component="div" transitionLeaveTimeout={200} transitionEnterTimeout={200}>
        </ReactCSSTransitionGroup>*/}
      </div>
    );
  }
}
export default Dropdown;
