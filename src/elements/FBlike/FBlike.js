import React, {Component, PropTypes} from 'react';
import { urlGenerator } from 'utils/helpers';
import {} from './FBLike.scss';

export default class FBlike extends Component {
  static propTypes = {
    url_type: PropTypes.string,
    url_props: PropTypes.object,
    height: PropTypes.string,
    width: PropTypes.string,
    className: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { height, width, className, url_type, url_props } = this.props;
    return (
      <div className="fb-like">
        <iframe
          className={className}
          src={`https://www.facebook.com/plugins/like.php?href=${encodeURIComponent('http://www.what-song.com' + urlGenerator(url_type, url_props))}&width=${width}&layout=button_count&action=like&size=small&show_faces=false&share=true&height=${height}&appId=260016127347363`}
          width={width}
          height={height}
          scrolling="no"
          frameBorder="0"
          allowTransparency="true"
        ></iframe>
      </div>
    );
  }
}
