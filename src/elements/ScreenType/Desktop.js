import React from 'react';
import { AllowScreen } from './';

const ScreenDesktop = ({children, ...rest}) =>
  <AllowScreen
    {...rest}
    condition={type=>type === 'desktop'}
  >
    {children}
  </AllowScreen>;

export default ScreenDesktop;
