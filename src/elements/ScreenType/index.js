export Detector from './Detector';
export Desktop from './Desktop';
export Tablet from './Tablet';
export TabletMin from './TabletMin';
export TabletMax from './TabletMax';
export Mobile from './Mobile';
export AllowScreen from './AllowScreen';
