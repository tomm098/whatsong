import React from 'react';
import { AllowScreen } from './';

const ScreenTabletMax = ({children, ...rest}) =>
  <AllowScreen
    {...rest}
    condition={type => type === 'tablet' || type === 'mobile'}
    >
    {children}
  </AllowScreen>;

export default ScreenTabletMax;
