import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import * as screenTypeActions from 'redux/modules/screenType';
import { connect } from 'react-redux';
import { getDeviceType, debounce } from '../../utils/helpers';

@connect(
  state => ({
    type: state.screenType.type
  }),
  {
    setScreenType: screenTypeActions.setScreen
  }
)

class ScreenTypeDetector extends Component {
  static propTypes = {
    setScreenType: PropTypes.func,
    type: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {

    };
    this.handleResize = debounce(this.handleResize.bind(this), 100);
  }

  componentDidMount() {
    if (window && window.addEventListener) {
      window.addEventListener('resize', this.handleResize);
      this.handleResize();
    }
  }

  componentWillUnmount() {
    if (window && window.addEventListener) {
      window.removeEventListener('resize', this.handleResize);
    }
  }

  handleResize() {
    const new_screen = getDeviceType();
    if (new_screen !== this.props.type) {
      this.props.setScreenType(new_screen);
    }
  }

  render() {
    return null;
  }
}
export default ScreenTypeDetector;

