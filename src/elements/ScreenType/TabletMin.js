import React from 'react';
import { AllowScreen } from './';

const ScreenTabletMin = ({children, ...rest}) =>
  <AllowScreen
    {...rest}
    condition={type=> type === 'tablet' || type === 'desktop'}
    >
    {children}
  </AllowScreen>;

export default ScreenTabletMin;
