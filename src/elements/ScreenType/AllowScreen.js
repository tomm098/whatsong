import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { connect } from 'react-redux';

@connect(
  state => ({
    type: state.screenType.type
  }),
  {

  }
)

class AllowScreen extends Component {
  static propTypes = {
    type: PropTypes.string,
    condition: PropTypes.func,
    children: PropTypes.node,
    onEnter: PropTypes.func
  }

  static defaultProps = {
    onEnter: ()=>{}
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    const { type, condition, onEnter } = this.props;
    if (condition(type)) { onEnter(); }
  }

  componentWillReceiveProps(newProps) {
    const { type, condition, onEnter } = this.props;
    if ((type !== newProps.type) && condition(newProps.type)) { onEnter(); }
  }


  render() {
    const {type, condition, children} = this.props;
    const allowed = condition(type);
    return (
      allowed ? children : null
    );
  }
}
export default AllowScreen;
