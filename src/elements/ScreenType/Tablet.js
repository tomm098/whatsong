import React from 'react';
import { AllowScreen } from './';

const ScreenTablet = ({children, ...rest}) =>
  <AllowScreen
    {...rest}
    condition={type=>type === 'tablet'}
    >
    {children}
  </AllowScreen>;

export default ScreenTablet;
