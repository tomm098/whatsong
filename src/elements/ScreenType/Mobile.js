import React from 'react';
import { AllowScreen } from './';

const ScreenMobile = ({children, ...rest}) =>
  <AllowScreen
    {...rest}
    condition={type=>type === 'mobile'}
    >
    {children}
  </AllowScreen>;

export default ScreenMobile;
