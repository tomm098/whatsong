import React from 'react';
const FormCaption = ({children, ...rest}) =>
  <div {...rest} className="caption">
    {children}
  </div>;
export default FormCaption;
