import React, { Component, PropTypes } from 'react';
import { Form } from 'elements';
import {} from './Text.scss';

export default class Text extends Component {
  static propTypes = {
    className: PropTypes.string,
    children: PropTypes.node.isRequired,
    label: PropTypes.string
  }

  static defaultProps = {
    className: ''
  }

  constructor(props) {
    super(props);
  }

  render() {
    const { className, children, label } = this.props;
    if (!children) return null;
    return (
      <Form.Group>
        {label ? <label className="form-control-text__label">{label}</label> : null}
        <div className={`form-control form-control-text ${className}`}>
          {children}
        </div>
      </Form.Group>
    );
  }
}

