import React, { Component, PropTypes } from 'react';
import { Form, SimpleSelect } from 'elements';
import {} from '../Select/Select.scss';
import {} from './DateSelects.scss';

import { Field } from 'redux-form';

const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'].map((c, i)=> ({label: c, value: i}));
const getYears = start => {
  let length = (new Date()).getFullYear() - start;
  const res = [];
  while (length) {
    const year = length + start;
    res.push({label: year, value: year});
    length --;
  }
  return res;
};
const years = getYears(1900);

class DateSelects extends Component {
  static propTypes = {
    label: PropTypes.string,
    caption: PropTypes.string,
    meta: PropTypes.object,
    input: PropTypes.object,
    error_hidden: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state = {
      day: '',
      month: '',
      year: ''
    };
  }

  getDays() {
    const { year, month } = this.parseDate();
    return Array.apply(null, Array(new Date(year, month + 1, 0).getDate())).map((c, i)=>({label: i + 1, value: i + 1}));
  }

  parseDate() {
    const { input: { value } } = this.props;
    const parsed_date = new Date(value);
    return parsed_date.valueOf() ? {
      day: parsed_date.getUTCDate(),
      month: parsed_date.getUTCMonth(),
      year: parsed_date.getUTCFullYear()
    } : {
      day: this.state.day,
      month: this.state.month,
      year: this.state.year
    };
  }

  handleUpdate() {
    const { year, month, day } = this.state;
    const { input: {onChange} } = this.props;
    const parsed_date = new Date(0);

    parsed_date.setUTCFullYear(year);
    parsed_date.setUTCMonth(month);
    parsed_date.setUTCDate(day);

    const is_day_in_month = parsed_date.getUTCMonth() === month;
    if (parsed_date.valueOf() && year && day && (month || month === 0) && is_day_in_month) {
      onChange(parsed_date.toISOString());
    } else {
      onChange('');
    }
    if ((month || month === 0) && !is_day_in_month) {
      this.setState({day: ''});
    }
  }

  render() {
    const handleUpdate = ::this.handleUpdate;
    const selected_date = this.parseDate();
    const { label, caption, meta: { error, touched }, error_hidden } = this.props;
    return (
      <div className="date-selects">
        {label && <p>{label}</p>}
        <div className="select-wrapp">
          <SimpleSelect
            list={this.getDays()}
            defaultLabel="Day"
            value={selected_date.day}
            onChange={v=>this.setState({day: v.target.value | 0}, handleUpdate)}
          />
          <SimpleSelect
            list={months}
            defaultLabel="Month"
            value={selected_date.month}
            onChange={v=>this.setState({month: v.target.value | 0}, handleUpdate)}
          />
          <SimpleSelect
            list={years}
            defaultLabel="Year"
            value={selected_date.year}
            onChange={v=>this.setState({year: v.target.value | 0}, handleUpdate)}
          />
          {caption && <p>{caption}</p> }
        </div>
        {!error_hidden && <Form.Error message={error && touched ? error : ''} />}
      </div>
    );
  }
}
export default props=> <Field {...props} component={DateSelects} />;
