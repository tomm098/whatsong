import React, { Component, PropTypes } from 'react';
import autosize from '../../../libs/autosize/autosize';
import ReactDOM from 'react-dom';
import { Form } from 'elements';
import {} from './TextareaAutosize.scss';
import { Field } from 'redux-form';

const UPDATE = 'autosize:update';
const DESTROY = 'autosize:destroy';

class TextareaAutosize extends Component {
  static propTypes = {
    autoComplete: PropTypes.string,
    className: PropTypes.string,
    label: PropTypes.string,
    maxLength: PropTypes.number,
    row: PropTypes.number,
    onKeyPress: React.PropTypes.func,
    meta: PropTypes.object,
    input: PropTypes.object
  }

  static defaultProps = {
    className: ''
  }

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    setTimeout(() => {
      const area = ReactDOM.findDOMNode(this.refs.textarea);
      if (area) {
        autosize(area);
      }
    }, 0);
  }

  componentWillReceiveProps(nextProps) { // nextProps
    if (this.getValue(nextProps) !== this.getValue(this.props)) {
      this.dispatchEvent(UPDATE, true);
    }
  }

  componentWillUnmount() {
    this.dispatchEvent(DESTROY);
  }

  getValue(props) {
    if (props) {
      return props.valueLink ? props.valueLink.value : props.value;
    }
  }

  dispatchEvent(EVENT_TYPE, defer) {
    const event = document.createEvent('Event');
    event.initEvent(EVENT_TYPE, true, false);
    const dispatch = () => ReactDOM.findDOMNode(this.refs.textarea).dispatchEvent(event);
    if (defer) {
      setTimeout(dispatch);
    } else {
      dispatch();
    }
  }

  render() {
    const { autoComplete, className, label, maxLength, onKeyPress, row, input, input: { name }, meta: { error, touched }, ...rest } = this.props;
    return (
      <Form.Group>
        <div className="form-textarea-autosize">
          <div className={className + (error && touched ? 'has-error ' : '')}>
          <label htmlFor={'form-input-' + name}>{label}</label>
          <textarea
            {...rest}
            {...input}
            className="form-control"
            autoComplete={autoComplete}
            onKeyPress={onKeyPress ? onKeyPress : ''}
            name={name}
            ref="textarea"
            rows={row || 1}
            maxLength={maxLength}
            placeholder={label}
          />
          <Form.Error message={error && touched ? error : ''} />
        </div>
        </div>
      </Form.Group>
    );
  }
}
export default props=> <Field {...props} component={TextareaAutosize} />;
