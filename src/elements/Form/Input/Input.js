import React, { Component, PropTypes } from 'react';
import { Form } from 'elements';
import {} from './Input.scss';
import { Field } from 'redux-form';

class Input extends Component {
  static propTypes = {
    autoComplete: PropTypes.bool,
    className: PropTypes.string,
    classNameInput: PropTypes.string,
    classNameGroup: PropTypes.string,
    label: PropTypes.string,
    type: PropTypes.string.isRequired,
    meta: PropTypes.object,
    input: PropTypes.object,
    caption: PropTypes.string,
    error_hidden: PropTypes.bool
  }

  static defaultProps = {
    autoComplete: false,
    className: ''
  }

  constructor(props) {
    super(props);
  }

  render() {
    const { autoComplete, className, classNameInput, input, input: { name }, meta: {error, touched}, label, caption, error_hidden, classNameGroup, ...rest } = this.props;
    return (
      <Form.Group className={classNameGroup}>
        <div className={className + ' input ' + (error && touched ? ' has-error ' : '')}>
          {label && <label className="input__label" htmlFor={'form-input-' + name}>{label}</label>}
            <div className="input__wrapper">
              <input
                {...rest}
                {...input}
                autoComplete={autoComplete ? 'on' : 'off'}
                className={'form-control ' + (classNameInput ? `${classNameInput} ` : '') + (error && touched ? 'is-error ' : '')}
                id={'form-input-' + name}
              />
              {caption && <div className="input__caption">{caption}</div>}
              {!error_hidden && <Form.Error message={error && touched ? error : ''} />}
            </div>

        </div>
      </Form.Group>
    );
  }
}

export default props=> <Field {...props} component={Input} />; // eslint-disable-line
