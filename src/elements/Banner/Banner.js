import React from 'react';
import {} from './Banner.scss';
const default_image = require('./img1.jpg');

const Banner = ({image, children, seo = {}}) =>
  <div className="banner">
    <section className="banner">
      <div className="banner__img-wrapp">
        <img {...seo} src={image || default_image}/>
      </div>
      <div className="banner__overlay"></div>
      <div className="banner__container">
        {children}
      </div>
    </section>;
  </div>;

export default Banner;

