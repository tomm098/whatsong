import React, { Component, PropTypes } from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Form } from 'elements';

import styles from './Popup.scss'; // eslint-disable-line no-unused-vars

export default class Popup extends Component {
  static propTypes = {
    children: PropTypes.node,
    is_opened: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    transitionLeaveTimeout: PropTypes.number,
    transitionEnterTimeout: PropTypes.number,
    title: PropTypes.string,
    modifier: PropTypes.string
  };

  static defaultProps = {
    transitionLeaveTimeout: 200,
    transitionEnterTimeout: 200
  };

  constructor(props) {
    super(props);
    this.close = this.close.bind(this);
  }


  componentDidMount() {
    window.addEventListener('keydown', this.close);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.is_opened !== nextProps.is_opened) {
      if (window && window.document && window.document.body) {
        // const classes = (window.document.body.className || '');
        // window.document.body.className = nextProps.is_opened ? (classes + ' modal-open') : classes.replace(/modal-open/, '');
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.close);
  }

  close(event) {
    if (this.props.is_opened && event.keyCode === 27) {
      this.props.handleClose();
    }
  }

  handleClickClose(event) {
    if (!(this.refs.modal && this.refs.modal.contains(event.target))) {
      this.props.handleClose();
    }
  }

  render() {

    const {
      children,
      is_opened,
      handleClose,
      transitionEnterTimeout,
      transitionLeaveTimeout,
      title,
      modifier
    } = this.props;

    const childrenWithProps = React.Children.map(children, (child) => {
      return React.cloneElement(child, { closePopup: this.props.handleClose });
    });
    return (
      <ReactCSSTransitionGroup component="div" transitionName="modal" transitionLeaveTimeout={transitionLeaveTimeout} transitionEnterTimeout={transitionEnterTimeout}>
        {is_opened &&
        <div onClick={::this.handleClickClose} className="modal">
          <div className={`modal-dialog ${modifier ? 'modal-dialog--' + modifier : ''}`}>
            <div className="modal-content">
             <div ref="modal">
               <div className="modal-header">
                 {title && <h4 className="modal-title">{title}</h4>}
                 <Form.Button
                   className="btn modal-close"
                   onClick={handleClose}
                   type="button"
                   >
                   x
                 </Form.Button>
               </div>
               <div className="modal-body">
                 {childrenWithProps}
               </div>
             </div>
            </div>
          </div>
        </div>}
      </ReactCSSTransitionGroup>
    );
  }
}
