import superagent from 'superagent';

class MusicKitClient {
  constructor() {
    this.music = null;
  }
  _init() {
    this.music = window.MusicKit.getInstance();
  }

  _autorize() {
    if (!this.music) {
      this._init();
    }
    return this.music.authorize();
  }

  _request({ method, path, params, data }) {
    const request = superagent[method](`https://api.music.apple.com/v1${path}`);
    return new Promise((resolve, reject) => {
      request.set('Authorization', `Bearer ${this.music.developerToken}`);
      request.set('Music-User-Token', this.music.musicUserToken);
      if (params) {
        request.query(params);
      }
      if (data) {
        request.send(data);
      }
      request.end((err, res = {}) => {
        return err ? reject(res.body, err, request.xhr) : resolve(res.body, request.xhr);
      });
    });
  }

  async createPlaylist(name, description = '') {
    await this._autorize();
    return this._request({
      method: 'post',
      path: '/me/library/playlists',
      data: {
        attributes: { name, description }
      }
    });
  }

  async addTracks(data = [], id) {
    await this._autorize();
    return this._request({
      method: 'post',
      path: `/me/library/playlists/${id}/tracks`,
      data: { data }
    });
  }

  async importToNewPlaylist(name, description, data) {
    const playlist = await this.createPlaylist(name, description);
    return this.addTracks(data, playlist.data[0].id);
  }
}

const musicKitClient = new MusicKitClient();

export default musicKitClient;
