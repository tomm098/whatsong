import superagent from 'superagent';
import config from 'config';
import cookie from 'react-cookie';
import cookieServer from '../utils/cookie';

const methods = ['get', 'post', 'put', 'patch', 'del'];
function formatUrl(path) {
  const adjustedPath = path[0] !== '/' ? '/' + path : path;
  // if (__SERVER__) {
  //   // Prepend host and port of the API server to the path.
  //   return 'http://' + config.apiHost + ':' + config.apiPort + adjustedPath;
  // }
  // // Prepend `/api` to relative URL, to proxy to API server.
  // return '/api' + adjustedPath;
  if (config.proxy === 'true') {
    if (__SERVER__) {
      // Prepend host and port of the API server to the path.
      // To connect to the localDBChange string below on 'http://' + 'localhost' + '8081' + adjustedPath;
      // For official API 'https://' + config.apiHost + (config.apiPort ? ':' + config.apiPort : '') + adjustedPath;
      return 'https://' + config.apiHost + (config.apiPort ? ':' + config.apiPort : '') + adjustedPath;
    }
    // Prepend `/api` to relative URL, to proxy to API server.
    return '/api' + adjustedPath;
  }
  //at offficial 'http://' + config.apiHost + (config.apiPort ? ':' + config.apiPort : '') + adjustedPath;
  return 'https://' + config.apiHost + (config.apiPort ? ':' + config.apiPort : '') + adjustedPath;
}

/*
 * This silly underscore is here to avoid a mysterious "ReferenceError: ApiClient is not defined" error.
 * See Issue #14. https://github.com/erikras/react-redux-universal-hot-example/issues/14
 *
 * Remove it at your own risk.
 */
class _ApiClient {
  constructor(req) {
    methods.forEach((method) =>
      this[method] = (path, { params, data, headers, attachments, handleProgress } = {}) => new Promise((resolve, reject) => {
        const request = superagent[method](formatUrl(path));
        if (params) {
          request.query(params);
        }

        if (__SERVER__ && req.get('cookie')) {
          request.set('cookie', req.get('cookie'));
          let tokenServer = cookieServer.getServer(req.get('cookie'), 'tokens');

          if (tokenServer) {
            tokenServer = JSON.parse(tokenServer);
          }

          if (path === '/me' && !tokenServer) {
            return reject({}, {}, {});
          }

          if (tokenServer && tokenServer.access) {
            request.set('Authorization', tokenServer.access.value);
          }

        } else if (__CLIENT__) {
          const tokenClient = cookie.load('tokens');

          if (tokenClient && tokenClient.access) {
            request.set('Authorization', tokenClient.access.value);
          }
        }

        if (headers) {
          for (const header in headers) {
            if (headers.hasOwnProperty(header)) {
              request.set(header, headers[header]);
            }
          }
        }

        if (attachments && typeof attachments === 'object') {
          Object.keys(attachments).forEach(c=>
            typeof attachments[c] !== 'object' ?
              request.field(c, attachments[c]) :
              request.attach(c, attachments[c])
          );
        }

        if (handleProgress) {
          request.on('progress', handleProgress);
        }

        if (data) {
          request.send(data);
        }

        request.end((err, res = {}) => {
          //return err ? reject(err, res.body, request.xhr) : resolve(res.body, request.xhr);
          return err ? reject(res.body, err, request.xhr) : resolve(res.body, request.xhr);
        });
      }));
  }
}

const ApiClient = _ApiClient;

export default ApiClient;
