import React, {Component, PropTypes} from 'react';
import ReactDOM from 'react-dom/server';
import serialize from 'serialize-javascript';
import Helmet from 'react-helmet';
import { Portlet } from 'components';

/**
 * Wrapper component containing HTML metadata and boilerplate tags.
 * Used in server-side code only to wrap the string output of the
 * rendered route component.
 *
 * The only thing this component doesn't (and can't) include is the
 * HTML doctype declaration, which is added to the rendered output
 * by the server.js file.
 */
export default class Html extends Component {
  static propTypes = {
    assets: PropTypes.object,
    component: PropTypes.node,
    store: PropTypes.object
  };

  render() {
    const {assets, component, store} = this.props;
    const content = component ? ReactDOM.renderToString(component) : '';
    const head = Helmet.rewind();

    return (
      <html lang="en-us">
        <head>
          {head.base.toComponent()}
          {head.title.toComponent()}
          {head.meta.toComponent()}
          {head.link.toComponent()}
          {head.script.toComponent()}

          <link rel="shortcut icon" href="/favicons/favicon.ico" />
          <meta name="msapplication-TileImage" content="/favicons/192x192.png" />
          <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon-180x180.png" />
          <link rel="apple-touch-icon" sizes="152x152" href="/favicons/apple-touch-icon-152x152.png" />
          <link rel="apple-touch-icon" sizes="144x144" href="/favicons/apple-touch-icon-144x144.png" />
          <link rel="apple-touch-icon" sizes="120x120" href="/favicons/apple-touch-icon-120x120.png" />
          <link rel="apple-touch-icon" sizes="114x114" href="/favicons/apple-touch-icon-114x114.png" />
          <link rel="apple-touch-icon" sizes="76x76" href="/favicons/apple-touch-icon-76x76.png" />
          <link rel="apple-touch-icon" sizes="72x72" href="/favicons/apple-touch-icon-72x72.png" />
          <link rel="apple-touch-icon" sizes="60x60" href="/favicons/apple-touch-icon-60x60.png" />
          <link rel="apple-touch-icon" sizes="57x57" href="/favicons/apple-touch-icon-57x57.png" />
          <link rel="android-chrome" sizes="36x36" type="image/png" href="/favicons/android-chrome-36x36.png" />
          <link rel="android-chrome" sizes="48x48" type="image/png" href="/favicons/android-chrome-48x48.png" />
          <link rel="android-chrome" sizes="72x72" type="image/png" href="/favicons/android-chrome-72x72.png" />
          <link rel="android-chrome" sizes="96x96" type="image/png" href="/favicons/android-chrome-96x96.png" />
          <link rel="android-chrome" sizes="144x144" type="image/png" href="/favicons/android-chrome-144x144.png" />
          <link rel="android-chrome" sizes="192x192" type="image/png" href="/favicons/android-chrome-192x192.png" />
          <link rel="icon" sizes="96x96" type="image/png" href="/favicons/favicon-96x96.png" />
          <link rel="icon" sizes="32x32" type="image/png" href="/favicons/favicon-32x32.png" />
          <link rel="icon" sizes="16x16" type="image/png" href="/favicons/favicon-16x16.png" />
          <meta name="msapplication-TileColor" content="#62bf7c" />
          <meta name="msapplication-TileImage" content="/favicons/apple-touch-icon-144x144.png" />
          <meta name="theme-color" content="#ffffff" />

          <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes" />
          {/* styles (will be present only in production with webpack extract text plugin) */}
          {Object.keys(assets.styles).map((style, key) =>
            <link href={assets.styles[style]} key={key} rel="stylesheet" type="text/css" charSet="UTF-8"/>
          )}

          {/* (will be present only in development mode) */}
          {/* outputs a <style/> tag with all bootstrap styles + App.scss + it could be CurrentPage.scss. */}
          {/* can smoothen the initial style flash (flicker) on page load in development mode. */}
          {/* ideally one could also include here the style for the current page (Home.scss, About.scss, etc) */}
          { Object.keys(assets.styles).length === 0 ? <style dangerouslySetInnerHTML={{__html: require('../containers/App/App.scss')._style}}/> : null }
          <script async src="//www.google-analytics.com/analytics.js"></script>
          <script src="//s.grvmedia.com/js/what-song-loader-min.js?v=2" async></script>
        </head>
        <body>
          <div id="content" dangerouslySetInnerHTML={{__html: content}}/>
          <script dangerouslySetInnerHTML={{__html: `window.__data=${serialize(store.getState())};`}} charSet="UTF-8"/>
          <script async src={assets.javascript.main} />
        </body>
      </html>
    );
  }
}
