import React from 'react';
import {IndexRoute, Route} from 'react-router';
import { isLoaded as isAuthLoaded, load as loadAuth } from 'redux/modules/auth';
import {
  App,
  Home,
  NotFound,
  Confirm,
  PasswordRestore,
  Movies,
  Television,
  Games,
  Movie,
  Game,
  MovieTest,
  TvShow,
  SearchResult,
  Settings,
  Profile,
  Favorites,
  EditMovie,
  EditTvShowEpisode,
  EditInner,
  BrowseAll,
  AboutUs,
  ArtistPage,
  PrivacyPolicy,
  ContactUs,
  TrendingMusic,
  EditTvShow
} from 'containers';
import EditGame from './containers/EditPages/EditGame';

export default (store) => {
  const requireLogin = (nextState, replace, cb) => {
    function checkAuth() {
      const { auth: { user }} = store.getState();
      if (!user) {
        // oops, not logged in, so can't be here!
        replace('/');
      }
      cb();
    }

    if (!isAuthLoaded(store.getState())) {
      store.dispatch(loadAuth()).then(checkAuth, checkAuth);
    } else {
      checkAuth();
    }
  };

  const requireAdmin = (nextState, replace, cb) => {
    const { auth: { user }} = store.getState();
    if ( user && user.role !== 'ADMIN') {
      // oops, not logged in, so can't be here!
      replace('/');
    }
    cb();
  };

  const loggedRedirect = (nextState, replace, cb) => {
    function checkAuth() {
      const { auth: { user }} = store.getState();
      if (user) {
        // oops, not logged in, so can't be here!
        replace('/');
      }
      cb();
    }

    if (isAuthLoaded(store.getState())) {
      store.dispatch(loadAuth()).then(checkAuth);
    } else {
      checkAuth();
    }
  };

  /**
   * Please keep routes in alphabetical order
   */
  return (
    <Route path="/" component={App}>
      { /* Home (main) route */ }
      <IndexRoute component={Home} />
      <Route path="/movies" component={Movies} />
      <Route path="/television" component={Television} />
      <Route path="/games" component={Games} />

      <Route path="/About-Us" component={AboutUs} />
      <Route path="/Privacy-Policy" component={PrivacyPolicy} />
      <Route path="/Contact-Us" component={ContactUs} />

      <Route path="/Trending-Music" component={TrendingMusic} />

      <Route path="/Artist/:id/:name" component={ArtistPage} />

      <Route path="/Movies/Soundtrack/:id/:title" component={Movie} />
      <Route path="/Games/Soundtrack/:id/:title" component={Game} />
      <Route onEnter={requireLogin}>
        <Route path="/Games/Soundtrack/:id/:title/edit" component={EditGame} >
          <Route path="search" component={EditInner.Search} />
          <Route path="shortlist" component={EditInner.Shortlist} />
          <Route onEnter={requireAdmin} path="soundtracks" component={EditInner.Soundtracks} />
          <Route onEnter={requireAdmin} path="trailers" component={EditInner.Trailers} />
        </Route>
      </Route>

      <Route path="/MoviesTest/Soundtrack/:id/:title" component={MovieTest} />

      <Route onEnter={requireLogin}>
        <Route path="/Movies/Soundtrack/:id/:title/edit" component={EditMovie} >
          <Route path="search" component={EditInner.Search} />
          <Route path="shortlist" component={EditInner.Shortlist} />
          <Route onEnter={requireAdmin} path="soundtracks" component={EditInner.Soundtracks} />
          <Route onEnter={requireAdmin} path="trailers" component={EditInner.Trailers} />
        </Route>
        <Route path="/Settings" component={Settings} />
      </Route>

      <Route onEnter={requireLogin}>
        <Route path="/Tvshow/:id/:title/e/:episode_id/edit" component={EditTvShowEpisode} >
          <Route path="search" component={EditInner.Search} />
          <Route path="shortlist" component={EditInner.Shortlist} />
        </Route>
      </Route>

      <Route onEnter={requireLogin}>
        <Route path="/Tvshow/:id/:title/edit" component={EditTvShow} >
          <Route onEnter={requireAdmin} path="soundtracks" component={EditInner.Soundtracks} />
        </Route>
      </Route>

      <Route path="/Activity/Search" component={SearchResult} />

      <Route path="/Profile/:username" component={Profile} />

      <Route path="/Favorites/:username" component={Favorites} />

      <Route path="/movies/browse" component={BrowseAll.Movies} />
      <Route path="/tvshow/browse" component={BrowseAll.TvShows} />
      <Route path="/games/browse" component={BrowseAll.Games} />

      <Route path="/Tvshow/:id/:title" component={TvShow.Main} />
      <Route path="/Tvshow/:tv_show_id/:tv_show_title/s/:id" component={TvShow.Season} />
      <Route path="/Tvshow/:tv_show_id/:tv_show_title/e/:id" component={TvShow.Episode} />

      <Route path="/confirm/:confirm_code" component={Confirm} />
      <Route path="/restore/:restore_code" component={PasswordRestore} />
      { /* Routes requiring login */ }
      <Route onEnter={requireLogin} />

      { /* Routes */ }
      <Route onEnter={loggedRedirect} />

      { /* Catch all route */ }
      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
};
