import { init as initDiscussion, clear as clearDiscussion } from './discussion';

const LOAD = 'tvShowEpisode/LOAD';
const LOAD_SUCCESS = 'tvShowEpisode/LOAD_SUCCESS';
const LOAD_FAIL = 'tvShowEpisode/LOAD_FAIL';

const CLEAR = 'tvShowEpisode/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  data: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result, error: null};
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function load(id) {
  return dispatch => { // eslint-disable-line
    return dispatch({
      types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
      promise: (client) => client.get('/episode-info', { params: { episodeID: id }}).then(data=>{
        const { discussions, ...rest } = data.data;
        if (discussions && Array.isArray(discussions)) {
          dispatch(initDiscussion(discussions, {episode: rest.episode._id}));
        }
        return rest;
      })
    });
  };
}

export function clear() {
  return dispatch => {
    dispatch(clearDiscussion());
    return dispatch({type: CLEAR});
  };
}
