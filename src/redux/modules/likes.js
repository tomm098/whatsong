import { wrappWithObj } from 'utils/helpers';

const LIKE = 'like/LIKE';
const LIKE_SUCCESS = 'like/LIKE_SUCCESS';
const LIKE_FAIL = 'like/LIKE_FAIL';

const initialState = {};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LIKE_SUCCESS:
      return {
        ...state,
        ...(wrappWithObj(action.like_type, {
          ...(state[action.like_type] || {}),
          ...(wrappWithObj(action.id, {liked: action.liked, like_count: action.like_count}))
        }))
      };
    default:
      return state;
  }
}

export function like(type, id, like_value, count) {
  return {
    types: [LIKE, LIKE_SUCCESS, LIKE_FAIL],
    promise: (client) => client.post('/user-action/favourite', {
      data: {
        type,
        itemID: id,
        like: like_value
      }
    }),
    like_type: type,
    id,
    liked: like_value,
    like_count: like_value ? count + 1 : count - 1
  };
}
