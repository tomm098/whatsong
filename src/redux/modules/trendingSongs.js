import { wrappWithObj } from '../../utils/helpers.js';

const REGISTER = 'trendingSongs/register';

const LOAD = 'trendingSongs/LOAD';
const LOAD_SUCCESS = 'trendingSongs/LOAD_SUCCESS';
const LOAD_FAIL = 'trendingSongs/LOAD_FAIL';

const initialState = {

};

const default_list = {
  active_variant: '',
  variants: {},
  loaded: false,
  loading: false,
  content: []
};

function listReducer(state, action) {

  if (!state) {return {};}

  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        content: action.result.data,
        active_variant: action.variant
      };
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error || action.result};
    default:
      return state;
  }
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case REGISTER:
      return {...state, ...(action.result)};
    case LOAD:
    case LOAD_SUCCESS:
    case LOAD_FAIL:
      return {
        ...state,
        ...(wrappWithObj(action.list_name, listReducer(state[action.list_name], action)))
      };
    default:
      return state;
  }
}

export function register({name, variants, ...rest}) {
  return {
    type: REGISTER,
    result: wrappWithObj(name,
      {
        ...default_list,
        ...rest,
        variants,
        active_variant: Object.keys(variants)[0]
      }
    )
  };
}

export function load(name, variant) {
  return (dispatch, getState) => {
    const list = getState().trendingSongs[name];
    if (list) {
      const active_variant = variant || list.active_variant;
      const url = list.variants[active_variant].api_endpoint;
      return dispatch(
        {
          types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
          promise: (client) => client.get(url, {params: {limit: list.page_size, skip: 0}}),
          list_name: name,
          variant: active_variant
        }
      );
    }
  };
}
