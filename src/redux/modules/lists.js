import { wrappWithObj } from '../../utils/helpers.js';

const REGISTER = 'lists/register';

const LOAD = 'lists/LOAD';
const LOAD_SUCCESS = 'lists/LOAD_SUCCESS';
const LOAD_FAIL = 'lists/LOAD_FAIL';
const PAGINATE = 'lists/PAGINATE';
const PAGINATE_SUCCESS = 'lists/PAGINATE_SUCCESS';
const PAGINATE_FAIL = 'lists/PAGINATE_FAIL';
const initialState = {

};

const default_list = {
  offset: 0,
  active_variant: '',
  variants: {},
  loaded: false,
  loading: false,
  content: []
};

function listReducer(state, action) {

  if (!state) {return {};}

  switch (action.type) {
    case PAGINATE:
    case LOAD:
      return {...state, loading: true};
    case PAGINATE_SUCCESS:
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        content: action.result.data,
        active_variant: action.variant,
        offset: action.offset,
        items_count: action.result.total_items_count
      };
    case PAGINATE_FAIL:
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error || action.result};
    default:
      return state;
  }
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case REGISTER:
      return {...state, ...(action.result)};
    case LOAD:
    case LOAD_SUCCESS:
    case LOAD_FAIL:
    case PAGINATE:
    case PAGINATE_SUCCESS:
    case PAGINATE_FAIL:
      return {
        ...state,
        ...(wrappWithObj(action.list_name, listReducer(state[action.list_name], action)))
      };
    default:
      return state;
  }
}

export function register({name, variants, page_size, ...rest}) {
  return {
    type: REGISTER,
    result: wrappWithObj(name,
      {
        ...default_list,
        ...rest,
        items_count: page_size,
        page_size,
        variants,
        active_variant: Object.keys(variants)[0]
      }
    )
  };
}

export function load(name, variant) {
  return (dispatch, getState) => {
    const list = getState().lists[name];
    if (list) {
      const active_variant = variant || list.active_variant;
      const url = list.variants[active_variant].api_endpoint;
      const offset = list.active_variant === active_variant ? list.offset : 0;
      return dispatch(
        {
          types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
          promise: (client) => client.get(url, {params: {limit: list.page_size, skip: offset}}),
          list_name: name,
          variant: active_variant,
          offset
        }
      );
    }
  };
}

export function paginate(name, offset) {
  return (dispatch, getState) => {
    const list = getState().lists[name];
    if (list) {
      const url = list.variants[list.active_variant].api_endpoint;
      return dispatch(
        {
          types: [PAGINATE, PAGINATE_SUCCESS, PAGINATE_FAIL],
          promise: (client) => client.get(url, {params: {limit: list.page_size, skip: offset}}),
          list_name: name,
          variant: list.active_variant,
          offset
        }
      );
    }
  };
}
