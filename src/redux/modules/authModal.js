const OPEN = 'authModal/OPEN';
const CLOSE = 'authModal/CLOSE';

const initialState = {
  is_open: false,
  view_type: 'sign-in',
  initial: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case OPEN:
      return {...state, is_open: true, ...(action.result)};
    case CLOSE:
      return {...state, is_open: false};
    default:
      return state;
  }
}

export function open(view, initial = {}) {
  const result = {};
  result.initial = initial;
  if (view) {
    result.view_type = view;
  }
  return {
    type: OPEN,
    result
  };
}

export function close() {
  return {
    type: CLOSE
  };
}
