const REMOVE = 'notifications/REMOVE';
const PUSH = 'notifications/PUSH';
const CLEAR = 'notifications/CLEAR';

const initialState = {
  notifications: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case PUSH:
      return {...state, notifications: [action.notification, ...state.notifications]};
    case REMOVE:
      return {
        ...state,
        notifications: state.notifications.filter(c=>c.id === action.id ? (clearTimeout(c.timeout) && 0) : 1)
      };
    case CLEAR:
      state.notifications.forEach(c=>clearTimeout(c.timeout));
      return {...state, notifications: []};
    default:
      return state;
  }
}

export function remove(id) {
  return {type: REMOVE, id};
}

export function clear() {
  return { type: CLEAR };
}

export function push(notification_type, payload, time = 3000) {
  return dispatch => {
    if (notification_type) {
      const id = (new Date()).valueOf();
      return dispatch({
        type: PUSH,
        notification: {
          timeout: setTimeout(()=>dispatch(remove(id)), time),
          id, type: notification_type, payload
        }
      });
    }
  };
}
