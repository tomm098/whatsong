const SEARCH = 'iTunesSearch/SEARCH';
const SEARCH_SUCCESS = 'iTunesSearch/SEARCH_SUCCESS';
const SEARCH_FAIL = 'iTunesSearch/SEARCH_FAIL';

const ALBUM_SEARCH = 'iTunesSearch/ALBUM_SEARCH';
const ALBUM_SEARCH_SUCCESS = 'iTunesSearch/ALBUM_SEARCH_SUCCESS';
const ALBUM_SEARCH_FAIL = 'iTunesSearch/ALBUM_SEARCH_FAIL';

const LOAD_ALBUM_TRACKS = 'iTunesSearch/LOAD_ALBUM_TRACKS';
const LOAD_ALBUM_TRACKS_SUCCESS = 'iTunesSearch/LOAD_ALBUM_TRACKS_SUCCESS';
const LOAD_ALBUM_TRACKS_FAIL = 'iTunesSearch/LOAD_ALBUM_TRACKS_FAIL';

const CLEAR = 'iTunesSearch/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  album_loading: false,
  album_loaded: false,
  data: [],
  album_data: []
};

const formatSong = song => ({
  //original_format: song,
  temp: true,
  _id: `tmp_itunes_${song.trackId}`,
  scene_description: '',
  itunes_id: song.trackId,
  artist: {
    name: song.artistName,
    itunes_id: song.artistId
  },
  album: {
    title: song.collectionCensoredName,
    itunes_id: song.collectionId,
    thumbnail: song.artworkUrl100,
    time_released: song.releaseDate,
    length: song.trackTimeMillis
  },
  title: song.trackName,
  preview_url: song.previewUrl,
  itunes_url: song.trackViewUrl,
  track_number: song.trackNumber,
  is_soundtrack: 0
});

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SEARCH:
      return {...state, loading: true};
    case SEARCH_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result.data.results.map(c=>formatSong(c)), error: null};
    case SEARCH_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case ALBUM_SEARCH:
      return {...state, album_loading: true};
    case ALBUM_SEARCH_SUCCESS:
      return {...state, album_loading: false, album_loaded: true, album_data: action.result.data.results, album_error: null};
    case ALBUM_SEARCH_FAIL:
      return {...state, album_loading: false, album_loaded: false, album_error: action.error};
    case LOAD_ALBUM_TRACKS_SUCCESS:
      return {...state, album_data: state.album_data.map(
        c=>c.collectionId === action.id ? {...c, tracklist: action.result.data.results} : c
      )};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function search(q, limit = 50) {
  return {
    types: [SEARCH, SEARCH_SUCCESS, SEARCH_FAIL],
    promise: (client) => client.get('/itunes-search', { params: {limit, name: q }})
  };
}

export function searchAlbums(q) {
  return {
    types: [ALBUM_SEARCH, ALBUM_SEARCH_SUCCESS, ALBUM_SEARCH_FAIL],
    promise: (client) => client.get('/itunes-album-search', { params: {limit: 25, name: q }})
  };
}

export function loadAlbumTracks(id) {
  return {
    types: [LOAD_ALBUM_TRACKS, LOAD_ALBUM_TRACKS_SUCCESS, LOAD_ALBUM_TRACKS_FAIL],
    promise: (client) => client.get('/itunes-album_song-search', { params: {id}}),
    id
  };
}

export function importTracks(type_id, type, id) {
  return {
    types: [LOAD_ALBUM_TRACKS, LOAD_ALBUM_TRACKS_SUCCESS, LOAD_ALBUM_TRACKS_FAIL],
    promise: (client) => client.get('/itunes-album_song-import', { params: {id, type, type_id}}),
    id
  };
}

export function clear() {
  return dispatch => {
    return dispatch({type: CLEAR});
  };
}
