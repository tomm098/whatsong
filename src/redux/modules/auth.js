import cookie from 'react-cookie';
import { executionEnvironment } from 'utils/helpers';

const LOAD = 'auth/LOAD';
const LOAD_SUCCESS = 'auth/LOAD_SUCCESS';
const LOAD_FAIL = 'auth/LOAD_FAIL';

const LOGOUT = 'auth/LOGOUT';

const LOGIN = 'auth/LOGIN';
const LOGIN_SUCCESS = 'auth/LOGIN_SUCCESS';
const LOGIN_FAIL = 'auth/LOGIN_FAIL';

const REGISTER = 'auth/REGISTER';
const REGISTER_SUCCESS = 'auth/REGISTER_SUCCESS';
const REGISTER_FAIL = 'auth/REGISTER_FAIL';

const CONFIRM = 'auth/CONFIRM';
const CONFIRM_SUCCESS = 'auth/CONFIRM_SUCCESS';
const CONFIRM_FAIL = 'auth/CONFIRM_FAIL';

const SEND_PASS_RESTORE = 'auth/SEND_PASS_RESTORE';
const SEND_PASS_RESTORE_SUCCESS = 'auth/SEND_PASS_RESTORE_SUCCESS';
const SEND_PASS_RESTORE_FAIL = 'auth/SEND_PASS_RESTORE_FAIL';

const RESTORE_PASS = 'auth/RESTORE_PASS';
const RESTORE_PASS_SUCCESS = 'auth/RESTORE_PASS_SUCCESS';
const RESTORE_PASS_FAIL = 'auth/RESTORE_PASS_FAIL';

const SOCIAL_AUTH = 'auth/SOCIAL_AUTH';
const SOCIAL_AUTH_SUCCESS = 'auth/SOCIAL_AUTH_SUCCESS';
const SOCIAL_AUTH_FAIL = 'auth/SOCIAL_AUTH_FAIL';

const UPDATE_PROFILE = 'auth/UPDATE_PROFILE';
const UPDATE_PROFILE_SUCCESS = 'auth/UPDATE_PROFILE_SUCCESS';
const UPDATE_PROFILE_FAIL = 'auth/UPDATE_PROFILE_FAIL';

const initialState = {
  loading: false,
  loaded: false,
  loggingIn: false,
  loggingOut: false,
  user: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state, loading: false, loaded: true, user: action.result.data, error: null};
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};

    case LOGIN:
      return {...state, loggingIn: true, error: null};
    case SOCIAL_AUTH_SUCCESS:
    case LOGIN_SUCCESS:
      return {...state, loggingIn: false, loaded: true, user: action.result.data.user, error: null};
    case LOGIN_FAIL:
      return {...state, loggingIn: false, loaded: false, error: action.error};

    case CONFIRM:
      return {...state, confirm_status: 'progress', error: null};
    case CONFIRM_SUCCESS:
      return {...state, confirm_status: 'success', loaded: true, user: action.result.data.user};
    case CONFIRM_FAIL:
      return {...state, confirm_status: 'fail', error: action.error};

    case UPDATE_PROFILE_SUCCESS:
      return {...state, user: {...state.user, ...action.result.data}};

    case LOGOUT:
      return {initialState};

    default:
      return state;
  }
}

const saveTokens = (data) => {
  if (executionEnvironment().canUseDOM) {
    cookie.save('tokens', {access: data.data.accessToken, refresh: data.data.refreshToken}, {path: '/'});
    return data;
  }
};

export function isLoaded(store) {
  return !!store.auth && !!store.auth.user;
}

export function load() {
  if (executionEnvironment().canUseDOM && !cookie.load('tokens')) {
    return {
      type: LOAD_FAIL,
    };
  }
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/me')
  };
}

export function logOut() {
  if (executionEnvironment().canUseDOM) {
    cookie.remove('tokens', {path: '/'});
  }
  return {
    type: LOGOUT
  };
}

export function logIn({login, password}) {
  return {
    types: [LOGIN, LOGIN_SUCCESS, LOGIN_FAIL],
    promise: client => client.get('/sign-in', {params: {login, password}}).then(saveTokens)
  };
}

export function register({email, username, password}) {
  return {
    types: [REGISTER, REGISTER_SUCCESS, REGISTER_FAIL],
    promise: client => client.post('/sign-up', {data: {email, username, password}})
  };
}

export function confirm(confirm_code) {
  return {
    types: [CONFIRM, CONFIRM_SUCCESS, CONFIRM_FAIL],
    promise: client => client.get('/confirm', {params: {confirm_code}}).then(saveTokens)
  };
}

export function sendPasswordRestore({ login }) {
  return {
    types: [SEND_PASS_RESTORE, SEND_PASS_RESTORE_SUCCESS, SEND_PASS_RESTORE_FAIL],
    promise: client => client.get('/send-restore', {params: {login}})
  };
}

export function restorePassword({restore_code, password}) {
  return {
    types: [RESTORE_PASS, RESTORE_PASS_SUCCESS, RESTORE_PASS_FAIL],
    promise: client => client.get('/restore', {params: {restore_code, password}})
  };
}

export function socialAuth(token) {
  return {
    types: [SOCIAL_AUTH, SOCIAL_AUTH_SUCCESS, SOCIAL_AUTH_FAIL],
    promise: client => client.get('/facebook-login', {params: {token}}).then(saveTokens)
  };
}

export function updateProfile(data) {
  return {
    types: [UPDATE_PROFILE, UPDATE_PROFILE_SUCCESS, UPDATE_PROFILE_FAIL],
    promise: client => client.post('/user-action/edit-profile', {attachments: {...data}})
  };
}

export function updatePassword(new_password, old_password) {
  return {
    types: [UPDATE_PROFILE, UPDATE_PROFILE_SUCCESS, UPDATE_PROFILE_FAIL],
    promise: client => client.post('/update-password', {data: {newPassword: new_password, oldPassword: old_password}})
  };
}
