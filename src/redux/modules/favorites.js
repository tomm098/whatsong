import { wrappWithObj } from 'utils/helpers';

const LOAD = 'favorites/LOAD';
const LOAD_SUCCESS = 'favorites/LOAD_SUCCESS';
const LOAD_FAIL = 'favorites/LOAD_FAIL';

const DISLIKE = 'favorites/DISLIKE';

const CLEAR = 'favorites/CLEAR';

const initialType = {
  loading: false,
  loaded: false,
  is_last: false,
  loaded_index: 0,
  total_items_count: 0,
  data: []
};

const initialState = {};

function typeReducer(state = initialType, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state,
        loading: false,
        loaded: true,
        data: [...state.data, ...action.result.data],
        total_items_count: action.result.total_items_count,
        is_last: action.offset + action.limit >= action.result.total_items_count,
        loaded_index: state.data.length + action.result.data.length,
        error: null
      };
    case DISLIKE:
      return {
        ...state,
        total_items_count: state.total_items_count - 1,
        loaded_index: state.loaded_index - 1,
        data: state.data.filter(c=>c._id !== action.id)
      };
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    default:
      return state;
  }
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
    case LOAD_SUCCESS:
    case DISLIKE:
    case LOAD_FAIL:
      return {...state, ...(wrappWithObj(action.list_type, typeReducer(state[action.list_type], action)))};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function load(username, type, offset = 0, limit = 10) { // eslint-disable-line
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get(`/list-of-${type}-favorited`, { params: { skip: offset, limit, username }}),
    list_type: type,
    offset, limit
  };
}

export function loadMore(username, type, n = 10) {
  return (dispatch, getState) => {
    const state = getState().favorites[type];
    if (!state || state.loading || state.is_last) { return false; }
    return dispatch(load(username, type, state.loaded_index, n));
  };
}

export function dislike(username, type, id) {
  return (dispatch, getState) => {
    const state = getState();
    if (state.auth.user.username === username) {
      dispatch({
        type: DISLIKE,
        list_type: type,
        id
      });
    }
  };
}

export function clear() {
  return {type: CLEAR};
}

