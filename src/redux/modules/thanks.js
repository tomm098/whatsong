import { wrappWithObj } from 'utils/helpers';

const THANK_USER = 'like/THANK_USER';
const THANK_USER_SUCCESS = 'like/THANK_USER_SUCCESS';
const THANK_USER_FAIL = 'like/THANK_USER_FAIL';

const THANK_SONG = 'like/THANK_SONG';
const THANK_SONG_SUCCESS = 'like/THANK_SONG_SUCCESS';
const THANK_SONG_FAIL = 'like/THANK_SONG_FAIL';

const initialState = {
  users: {},
  songs: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case THANK_USER_SUCCESS:
      return {
        ...state,
        users: {
          ...state.users,
          ...(wrappWithObj(action.user_id, {
            is_thanked: action.thank,
            count_thanks: action.thank ? action.count + 1 : action.count - 1
          }))
        }
      };
    case THANK_SONG:
      return {
        ...state,
        users: {
          ...state.users,
          ...(wrappWithObj(action.user_id, {
            ...(state.users[action.user_id] || {}),
            count_thanks: action.thank ? action.count + 1 : action.count - 1
          }))
        },
        songs: {
          ...state.songs,
          ...(wrappWithObj(action.song_id, {
            is_thanked: action.thank
          }))
        }
      };
    default:
      return state;
  }
}

export function thankUser(user_id, thank, count) {
  return {
    types: [THANK_USER, THANK_USER_SUCCESS, THANK_USER_FAIL],
    promise: (client) => client.post('/user-action/add-thank-user', { data: { thank: thank + '', user_id }}),
    user_id,
    thank,
    count
  };
}

export function thankSong(song_id, user_id, thank, count) {
  return {
    types: [THANK_SONG, THANK_SONG_SUCCESS, THANK_SONG_FAIL],
    promise: (client) => client.post('/user-action/add-thank-user', { data: { song_id, thank: thank + '' }}),
    song_id,
    user_id,
    thank,
    count
  };
}
