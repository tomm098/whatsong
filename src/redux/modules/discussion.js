const INIT = 'discussion/INIT';
const CLEAR = 'discussion/CLEAR';

const POST = 'discussion/POST';
const POST_SUCCESS = 'discussion/POST_SUCCESS';
const POST_FAIL = 'discussion/POST_FAIL';

const REPLAY = 'discussion/REPLAY';
const REPLAY_SUCCESS = 'discussion/REPLAY_SUCCESS';
const REPLAY_FAIL = 'discussion/REPLAY_FAIL';

const THUMBS_UP = 'discussion/THUMBS_UP';
const THUMBS_UP_SUCCESS = 'discussion/THUMBS_UP_SUCCESS';
const THUMBS_UP_FAIL = 'discussion/THUMBS_UP_FAIL';

const VOTE_UP = 'discussion/VOTE_UP';
const VOTE_UP_SUCCESS = 'discussion/VOTE_UP_SUCCESS';
const VOTE_UP_FAIL = 'discussion/VOTE_UP_FAIL';

const VOTE_DOWN = 'discussion/VOTE_DOWN';
const VOTE_DOWN_SUCCESS = 'discussion/VOTE_DOWN_SUCCESS';
const VOTE_DOWN_FAIL = 'discussion/VOTE_DOWN_FAIL';

const SUBSCRIBE = 'discussion/SUBSCRIBE';
const SUBSCRIBE_SUCCESS = 'discussion/SUBSCRIBE_SUCCESS';
const SUBSCRIBE_FAIL = 'discussion/SUBSCRIBE_FAIL';

const EDIT_QUESTION = 'discussion/EDIT_QUESTION';
const EDIT_QUESTION_SUCCESS = 'discussion/EDIT_QUESTION_SUCCESS';
const EDIT_QUESTION_FAIL = 'discussion/EDIT_QUESTION_FAIL';

const EDIT_REPLY = 'discussion/EDIT_REPLY_QUESTION';
const EDIT_REPLY_SUCCESS = 'discussion/EDIT_REPLY_SUCCESS';
const EDIT_REPLY_FAIL = 'discussion/EDIT_REPLY_FAIL';

const DELETE_QUESTION = 'discussion/DELETE_QUESTION';
const DELETE_QUESTION_SUCCESS = 'discussion/DELETE_QUESTION_SUCCESS';
const DELETE_QUESTION_FAIL = 'discussion/DELETE_QUESTION_FAIL';

const DELETE_REPLY = 'discussion/DELETE_REPLY_QUESTION';
const DELETE_REPLY_SUCCESS = 'discussion/DELETE_REPLY_SUCCESS';
const DELETE_REPLY_FAIL = 'discussion/DELETE_REPLY_FAIL';

const initialState = {
  discussions: [],
  discussion_type: {}
};

const updateReply = (discussions, action) => {
  const new_discussions = [...discussions];
  const index = new_discussions.findIndex(c=>c._id + '' === action.reply_id);
  if (new_discussions[index]) {
    new_discussions[index] = {...new_discussions[index], childs: [...new_discussions[index].childs, action.result.data]};
  }
  return new_discussions;
};

const updateThumbsUp = (discussions, action) => {
  const new_discussions = discussions.map(d => {
    if (d.childs.length !== 0) {
      const newReplies = d.childs.map(reply => {
        if (reply._id !== action.result.data._id) {
          return reply;
        } else {
          return Object.assign({}, reply, {
            isThumbsUP: true,
            thumbs_up: action.result.data.thumbs_up
          });
        }
      });
      return Object.assign({}, d, {
        childs: [...newReplies]
      });
    } else {
      return d;
    }
  });

  return new_discussions;
};

const updateVote = (discussions, action) => {
  const new_discussions = discussions.map(d => {
    if (d._id !== action.result.data._id) {
      return d;
    }

    return Object.assign({}, d, {
      isVoted: true,
      vote: action.result.data.vote
    });
  });

  return new_discussions;
};

const updateEditQuestion = (discussions, action) => {
  const new_discussions = discussions.map(d => {
    if (d._id !== action.result.data._id) {
      return d;
    }

    return Object.assign({}, d, {
      body: action.result.data.body
    });
  });

  return new_discussions;
};

const updateEditReply = (discussions, action) => {
  console.log(action.result.data);
  const new_discussions = discussions.map(d => {
    if (d.childs.length !== 0) {
      const newReplies = d.childs.map(reply => {
        if (reply._id !== action.result.data._id) {
          return reply;
        } else {
          return Object.assign({}, reply, {
            body: action.result.data.body
          });
        }
      });
      return Object.assign({}, d, {
        childs: [...newReplies]
      });
    } else {
      return d;
    }
  });

  return new_discussions;
};

const updateDeleteQuestion = (discussions, action) => {
  const new_discussions = discussions.filter(d => {
    if (d._id !== action.result._id) {
      return d;
    }
  });

  return new_discussions;
};

const updateDeleteReply = (discussions, action) => {
  const new_discussions = discussions.map(d => {
    if (d.childs.length !== 0) {
      const newReplies = d.childs.filter(reply => {
        if (reply._id !== action.result._id) {
          return reply;
        }
      });
      return Object.assign({}, d, {
        childs: [...newReplies]
      });
    } else {
      return d;
    }
  });

  return new_discussions;
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case INIT:
      return {...state, discussions: action.data, discussion_type: action.discussion_type};
    case CLEAR:
      return initialState;
    case POST_SUCCESS:
      return {...state, discussions: [...state.discussions, action.result.data]};
    case REPLAY_SUCCESS:
      return {...state, discussions: updateReply(state.discussions, action)};
    case THUMBS_UP_SUCCESS:
      return {...state, discussions: updateThumbsUp(state.discussions, action)};
    case VOTE_UP_SUCCESS:
      return {...state, discussions: updateVote(state.discussions, action)};
    case VOTE_DOWN_SUCCESS:
      return {...state, discussions: updateVote(state.discussions, action)};
    case EDIT_QUESTION_SUCCESS:
      return {...state, discussions: updateEditQuestion(state.discussions, action)};
    case EDIT_REPLY_SUCCESS:
      return {...state, discussions: updateEditReply(state.discussions, action)};
    case DELETE_QUESTION_SUCCESS:
      return {...state, discussions: updateDeleteQuestion(state.discussions, action)};
    case DELETE_REPLY_SUCCESS:
      return {...state, discussions: updateDeleteReply(state.discussions, action)};
    default:
      return state;
  }
}

export function init(data, type) {
  return {
    type: INIT,
    data,
    discussion_type: type
  };
}

export function clear() {
  return {
    type: CLEAR
  };
}

export function post(body, reply_id, email, episode_id) {
  return (dispatch, getState)=>{
    const state = getState();
    const user_id = state.auth.user && state.auth.user._id;
    const discussion_type = state.discussion.discussion_type;

    if (episode_id) {
      return dispatch({
        types: [REPLAY, REPLAY_SUCCESS, REPLAY_FAIL],
        promise: client => client.post('/discussion', {data: {user: user_id, body, reply_to: reply_id, episode: episode_id}}),
        reply_id
      });
    }

    if (!reply_id) {
      if (email) {
        return dispatch({
          types: [POST, POST_SUCCESS, POST_FAIL],
          promise: client => client.post('/discussion', {data: {email, body, ...discussion_type}})
        });
      } else {
        return dispatch({
          types: [POST, POST_SUCCESS, POST_FAIL],
          promise: client => client.post('/discussion', {data: {user: user_id, body, ...discussion_type}})
        });
      }
    } else {
      if (email) {
        return dispatch({
          types: [REPLAY, REPLAY_SUCCESS, REPLAY_FAIL],
          promise: client => client.post('/discussion', {data: {email, body, reply_to: reply_id, ...discussion_type}}),
          reply_id
        });
      } else {
        return dispatch({
          types: [REPLAY, REPLAY_SUCCESS, REPLAY_FAIL],
          promise: client => client.post('/discussion', {data: {user: user_id, body, reply_to: reply_id, ...discussion_type}}),
          reply_id
        });
      }
    }
  };
}

export function thumbsUp(reply_id) {
  return (dispatch)=>{
    return dispatch({
      types: [THUMBS_UP, THUMBS_UP_SUCCESS, THUMBS_UP_FAIL],
      promise: client => client.post('/discussion/thumbs_up', {data: {discussion: reply_id}})
    });
  };
}

export function voteUp(discussion_id) {
  return (dispatch)=>{
    return dispatch({
      types: [VOTE_UP, VOTE_UP_SUCCESS, VOTE_UP_FAIL],
      promise: client => client.post('/discussion/vote', {data: {discussion: discussion_id, up: true}})
    });
  };
}

export function voteDown(discussion_id) {
  return (dispatch)=>{
    return dispatch({
      types: [VOTE_DOWN, VOTE_DOWN_SUCCESS, VOTE_DOWN_FAIL],
      promise: client => client.post('/discussion/vote', {data: {discussion: discussion_id, up: false}})
    });
  };
}

export function subscribe(discussion_id, email) {
  return (dispatch)=>{
    return dispatch({
      types: [SUBSCRIBE, SUBSCRIBE_SUCCESS, SUBSCRIBE_FAIL],
      promise: client => client.post('/discussion/subscribe', {data: {email: email, discussion: discussion_id}})
    });
  };
}

export function editQuestion(discussion_id, name) {
  return (dispatch)=>{
    return dispatch({
      types: [EDIT_QUESTION, EDIT_QUESTION_SUCCESS, EDIT_QUESTION_FAIL],
      promise: client => client.post('/discussion/qa/edit', {data: {_id: discussion_id, body: name}})
    });
  };
}

export function editReply(discussion_id, name) {
  return (dispatch)=>{
    return dispatch({
      types: [EDIT_REPLY, EDIT_REPLY_SUCCESS, EDIT_REPLY_FAIL],
      promise: client => client.post('/discussion/qa/edit', {data: {_id: discussion_id, body: name}})
    });
  };
}

export function deleteQuestion(discussion_id, success_cb, fail_cb) {
  return async (dispatch, getState, {client})=>{
    dispatch({type: DELETE_QUESTION});

    try {
      const response = await client.get('/discussion/qa/delete', {params: {_id: discussion_id}});

      success_cb();
      dispatch({type: DELETE_QUESTION_SUCCESS, result: {_id: response._id}});
    } catch (e) {
      fail_cb();
      dispatch({type: DELETE_QUESTION_FAIL});
    }
  };
}

export function deleteReply(discussion_id, success_cb, fail_cb) {
  return async (dispatch, getState, {client})=>{
    dispatch({type: DELETE_REPLY});

    try {
      const response = await client.get('/discussion/qa/delete', {params: {_id: discussion_id}});

      success_cb();
      dispatch({type: DELETE_REPLY_SUCCESS, result: {_id: response._id}});
    } catch (e) {
      fail_cb();
      dispatch({type: DELETE_REPLY_FAIL});
    }
  };
}
