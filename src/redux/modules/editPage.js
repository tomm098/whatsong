const ADD_TO_SHORTLIST = 'editPage/ADD_TO_SHORTLIST';
const EDIT_SHORTLIST = 'editPage/EDIT_SHORTLIST';
const REMOVE_FROM_SHORTLIST = 'editPage/REMOVE_FROM_SHORTLIST';
const CLEAR = 'editPage/CLEAR';

const IMPORT = 'editPage/IMPORT';
const IMPORT_SUCCESS = 'editPage/IMPORT_SUCCESS';
const IMPORT_FAIL = 'editPage/IMPORT_FAIL';


const initialState = {
  shortlist: [],
  id: ''
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_TO_SHORTLIST:
      return {...state, shortlist: [...state.shortlist, action.song]};
    case EDIT_SHORTLIST:
      return {...state, shortlist: state.shortlist.map(c=>c._id === action.id ? {...c, ...action.changes} : c)};
    case REMOVE_FROM_SHORTLIST:
      return {...state, shortlist: state.shortlist.filter(c=>c._id !== action.id)};
    case CLEAR:
      return state.id !== action.id ? {...initialState, id: action.id} : state;
    default:
      return state;
  }
}

export function addToShortlist(song) {
  return (dispatch, getState)=>{
    const state = getState().editPage;
    if (state.shortlist.findIndex(c=>c._id === song._id) === -1) {
      return dispatch({
        type: ADD_TO_SHORTLIST,
        song
      });
    }
  };
}

export function editShortlist(id, changes) {
  return {
    type: EDIT_SHORTLIST,
    id,
    changes
  };
}

export function removeFromShortlist(id) {
  return {
    type: REMOVE_FROM_SHORTLIST,
    id
  };
}

export function clear(id) {
  return { type: CLEAR, id };
}

export function importSongs(to) {
  return (dispatch, getState) => {
    const state = getState().editPage;
    if (state.shortlist.length && to && to.type && to.id) {
      const formatted_shortlist = state.shortlist.map(({temp, _id, ...rest})=>rest); // eslint-disable-line
      return dispatch({
        types: [IMPORT, IMPORT_SUCCESS, IMPORT_FAIL],
        promise: client => client.post('/user-action/import-songs', {data: {...to, songs: formatted_shortlist}})
      });
    }
  };
}
