import { init as initDiscussion, clear as clearDiscussion } from './discussion'; // eslint-disable-line

const LOAD = 'tvShowSeason/LOAD';
const LOAD_SUCCESS = 'tvShowSeason/LOAD_SUCCESS';
const LOAD_FAIL = 'tvShowSeason/LOAD_FAIL';

const ADD_EPISODE = 'tvShowSeason/ADD_EPISODE';
const ADD_EPISODE_SUCCESS = 'tvShowSeason/ADD_EPISODE_SUCCESS';
const ADD_EPISODE_FAIL = 'tvShowSeason/ADD_EPISODE_FAIL';

const EDIT_EPISODE = 'tvShowSeason/EDIT_EPISODE';
const EDIT_EPISODE_SUCCESS = 'tvShowSeason/EDIT_EPISODE_SUCCESS';
const EDIT_EPISODE_FAIL = 'tvShowSeason/EDIT_EPISODE_FAIL';

const CLEAR = 'tvShowSeason/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  data: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result, error: null};
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case ADD_EPISODE_SUCCESS:
      return {...state, data: {...state.data, episodes: [...state.data.episodes, action.result.data]}};
    case EDIT_EPISODE_SUCCESS:
      const episodesList = [...state.data.episodes];
      const myIndex = episodesList.findIndex(({_id}) => _id === action.result.data._id);
      episodesList[myIndex].name = action.result.data.name;
      episodesList[myIndex].date_released = action.result.data.date_released;
      return {...state, data: {...state.data, episodes: episodesList}};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function load(id) {
  return dispatch => { // eslint-disable-line
    return dispatch({
      types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
      promise: (client) => client.get('/season-info', { params: { seasonID: id }}).then(data=>{
        const { discussions, ...rest } = data.data; // eslint-disable-line
        /*if (discussions && Array.isArray(discussions)) {
          dispatch(initDiscussion(discussions, {tv_show: rest.tv_show._id}));
        }*/
        return rest;
      })
    });
  };
}

export function clear() {
  return dispatch => {
    //dispatch(clearDiscussion());
    return dispatch({type: CLEAR});
  };
}

export function addEpisode(data) {
  return {
    types: [ADD_EPISODE, ADD_EPISODE_SUCCESS, ADD_EPISODE_FAIL],
    promise: (client) => client.post('/add-tv-show-episode', {
      data: {...data, date_released: (new Date(data.date_released).valueOf())}
    })
  };
}

export function editEpisode(data) {
  return {
    types: [EDIT_EPISODE, EDIT_EPISODE_SUCCESS, EDIT_EPISODE_FAIL],
    promise: (client) => client.put('/edit-tv-show-episode', {
      data: {...data, date_released: (new Date(data.date_released).valueOf())}
    })
  };
}
