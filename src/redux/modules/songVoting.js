import { wrappWithObj } from 'utils/helpers';

const VOTE = 'songVoting/VOTE';
const VOTE_SUCCESS = 'songVoting/VOTE_SUCCESS';
const VOTE_FAIL = 'songVoting/VOTE_FAIL';

const initialState = {};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case VOTE_SUCCESS:
      const { data: { _id, ...rest } } = action.result;
      return {...state, ...(wrappWithObj(_id, rest))};
    default:
      return state;
  }
}

export function vote(id, vote, type) { // eslint-disable-line
  return {
    types: [VOTE, VOTE_SUCCESS, VOTE_FAIL],
    promise: (client) => client.post('/approve', {
      data: {
        type,
        song_id: id,
        voting: vote
      }
    }),
    id
  };
}
