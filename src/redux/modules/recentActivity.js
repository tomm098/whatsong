const LOAD = 'recentActivity/LOAD';
const LOAD_SUCCESS = 'recentActivity/LOAD_SUCCESS';
const LOAD_FAIL = 'recentActivity/LOAD_FAIL';

const CLEAR = 'recentActivity/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  data: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result.data, error: null};
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function load() {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/latest-activity', { params: { limit: 15 }})
  };
}

export function clear() {
  return {type: CLEAR};
}

