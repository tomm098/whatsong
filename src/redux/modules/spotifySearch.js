const SEARCH = 'spotifySearch/SEARCH';
const SEARCH_SUCCESS = 'spotifySearch/SEARCH_SUCCESS';
const SEARCH_FAIL = 'spotifySearch/SEARCH_FAIL';

const CLEAR = 'spotifySearch/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  data: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SEARCH:
      return {...state, loading: true};
    case SEARCH_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result.data.tracks.items, error: null};
    case SEARCH_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function search(q, limit) {
  return {
    types: [SEARCH, SEARCH_SUCCESS, SEARCH_FAIL],
    promise: (client) => client.get('/spotify-search', { params: {limit, name: q }})
  };
}

export function clear() {
  return dispatch => {
    return dispatch({type: CLEAR});
  };
}
