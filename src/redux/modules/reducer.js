import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import {reducer as reduxAsyncConnect} from 'redux-connect';
import {reducer as form} from 'redux-form';

import auth from './auth';
import authModal from './authModal';
import lists from './lists';
import player from './player';
import trendingSongs from './trendingSongs';
import screenType from './screenType';
import movie from './movie';
import game from './game';
import discussion from './discussion';
import likes from './likes';
import thanks from './thanks';
import songVoting from './songVoting';
import tvShowMain from './tvShowMain';
import tvShowSeason from './tvShowSeason';
import tvShowEpisode from './tvShowEpisode';
import searchResults from './searchResults';
import miniSearch from './miniSearch';
import profile from './profile';
import editPage from './editPage';
import iTunesSearch from './iTunesSearch';
import youtubeSearch from './youtubeSearch';
import spotifySearch from './spotifySearch';
import editTrailers from './editTrailers';
import favorites from './favorites';
import notifications from './notifications';
import recentActivity from './recentActivity';
import songEdit from './songEdit';
import browseAll from './browseAll';
import artistPage from './artistPage';
import counters from './counters';
import trendingMusic from './trendingMusic';

export default combineReducers({
  routing: routerReducer,
  reduxAsyncConnect,
  form,
  auth,
  authModal,
  lists,
  player,
  trendingSongs,
  screenType,
  movie,
  game,
  discussion,
  likes,
  thanks,
  songVoting,
  tvShowMain,
  tvShowSeason,
  tvShowEpisode,
  searchResults,
  miniSearch,
  profile,
  editPage,
  iTunesSearch,
  youtubeSearch,
  spotifySearch,
  editTrailers,
  favorites,
  notifications,
  recentActivity,
  songEdit,
  browseAll,
  artistPage,
  counters,
  trendingMusic
});
