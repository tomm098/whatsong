import { wrappWithObj } from 'utils/helpers';

const ADD_SCENE = 'songEdit/ADD_SCENE';
const ADD_SCENE_DESC_SUCCESS = 'songEdit/ADD_SCENE_DESC_SUCCESS';
const ADD_SCENE_TIMEPLAY_SUCCESS = 'songEdit/ADD_SCENE_TIMEPLAY_SUCCESS';
const ADD_SCENE_FAIL = 'songEdit/ADD_SCENE_FAIL';

const EDIT_SONG = 'songEdit/EDIT_SONG';
const EDIT_SONG_SUCCESS = 'songEdit/EDIT_SONG_SUCCESS';
const EDIT_SONG_FAIL = 'songEdit/EDIT_SONG_FAIL';

const DELETE_SONG = 'songEdit/DELETE_SONG';
const DELETE_SONG_SUCCESS = 'songEdit/DELETE_SONG_SUCCESS';
const DELETE_SONG_FAIL = 'songEdit/DELETE_SONG_FAIL';

const initialState = {
  changed_songs: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ADD_SCENE_DESC_SUCCESS:
      return {
        ...state,
        changed_songs: {...state.changed_songs, ...wrappWithObj(action.song._id, {
          ...(state.changed_songs[action.song._id] || {}),
          ...action.song,
          scene_description: action.result.data.scene_description
        })}
      };
    case ADD_SCENE_TIMEPLAY_SUCCESS:
      return {
        ...state,
        changed_songs: {...state.changed_songs, ...wrappWithObj(action.song._id, {
          ...(state.changed_songs[action.song._id] || {}),
          ...action.song,
          time_play: action.result.data.time_play
        })}
      };
    case EDIT_SONG_SUCCESS:
      return {
        ...state,
        changed_songs: {...state.changed_songs, ...wrappWithObj(action.song._id, {
          ...(state.changed_songs[action.song._id] || {}),
          ...action.song,
          ...action.result.data
        })}
      };
    case DELETE_SONG_SUCCESS:
      return {
        ...state,
        changed_songs: {...state.changed_songs, ...wrappWithObj(action.song._id, {
          ...(state.changed_songs[action.song._id] || {}),
          ...action.song,
          is_deleted: true
        })}
      };
    default:
      return state;
  }
}

export function addSceneDescription(song, scene_description) {
  return {
    types: [ADD_SCENE, ADD_SCENE_DESC_SUCCESS, ADD_SCENE_FAIL],
    promise: (client) => client.post('/add-scene-description', {data: {songID: song._id, scene_description, to_null: 'false'}}),
    song: {...song}
  };
}

export function addSceneTimePlay(song, time_play) {
  return {
    types: [ADD_SCENE, ADD_SCENE_TIMEPLAY_SUCCESS, ADD_SCENE_FAIL],
    promise: (client) => client.post('/add-time-play', {data: {songID: song._id, time_play, to_null: 'false'}}),
    song: {...song}
  };
}

export function editSong(song, data) {
  return {
    types: [EDIT_SONG, EDIT_SONG_SUCCESS, EDIT_SONG_FAIL],
    promise: (client) => client.post('/edit-song', {data: {song_id: song._id, ...data}}),
    song: {...song}
  };
}

export function deleteSong(song) {
  return {
    types: [DELETE_SONG, DELETE_SONG_SUCCESS, DELETE_SONG_FAIL],
    promise: (client) => client.put('/delete-song', {data: {songID: song._id}}),
    song: {...song}
  };
}
