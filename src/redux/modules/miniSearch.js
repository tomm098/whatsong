const SEARCH = 'miniSearch/SEARCH';
const SEARCH_SUCCESS = 'miniSearch/SEARCH_SUCCESS';
const SEARCH_FAIL = 'miniSearch/SEARCH_FAIL';

const CLEAR = 'miniSearch/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  data: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SEARCH:
      return {...state, loading: true};
    case SEARCH_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result.data, error: null};
    case SEARCH_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function search(q) {
  return {
    types: [SEARCH, SEARCH_SUCCESS, SEARCH_FAIL],
    promise: (client) => client.get('/search', { params: {limit: 7, type: 'all', field: q }})
  };
}

export function clear() {
  return dispatch => {
    //dispatch(clearDiscussion());
    return dispatch({type: CLEAR});
  };
}
