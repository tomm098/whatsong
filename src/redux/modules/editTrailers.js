const LOAD_TRAILERS = 'editTrailers/LOAD_TRAILERS';
const LOAD_TRAILERS_SUCCESS = 'editTrailers/LOAD_TRAILERS_SUCCESS';
const LOAD_TRAILERS_FAIL = 'editTrailers/LOAD_TRAILERS_FAIL';

const ADD_TRAILER = 'editTrailers/ADD_TRAILER';
const ADD_TRAILER_SUCCESS = 'editTrailers/ADD_TRAILER_SUCCESS';
const ADD_TRAILER_FAIL = 'editTrailers/ADD_TRAILER_FAIL';

const REMOVE_TRAILER = 'editTrailers/REMOVE_TRAILER';
const REMOVE_TRAILER_SUCCESS = 'editTrailers/REMOVE_TRAILER_SUCCESS';
const REMOVE_TRAILER_FAIL = 'editTrailers/REMOVE_TRAILER_FAIL';

const ADD_TRAILER_SONG = 'editTrailers/ADD_TRAILER_SONG';
const ADD_TRAILER_SONG_SUCCESS = 'editTrailers/ADD_TRAILER_SONG_SUCCESS';
const ADD_TRAILER_SONG_FAIL = 'editTrailers/ADD_TRAILER_SONG_FAIL';

const CLEAR = 'editTrailers/CLEAR';

const initialState = {
  saved_trailers: [],
  loading: false,
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case CLEAR:
      return initialState;
    case LOAD_TRAILERS:
      return {...state, loading: true};
    case LOAD_TRAILERS_SUCCESS:
      return {...state, loading: false, loaded: true, saved_trailers: action.result.data, error: null};
    case LOAD_TRAILERS_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case ADD_TRAILER_SUCCESS:
      return {...state, saved_trailers: [...state.saved_trailers, action.result.data]};
    case REMOVE_TRAILER_SUCCESS:
      return {...state, saved_trailers: state.saved_trailers.filter(c=>c._id !== action.id)};
    case ADD_TRAILER_SONG_SUCCESS:
      return {
        ...state,
        saved_trailers: state.saved_trailers.map(c=>
          c._id === action.trailer_id ?
            {...c, song_list: [...c.song_list, action.result.data]}
          :
            c
        )
      };
    default:
      return state;
  }
}

export function loadTrailers(id) {
  return {
    types: [LOAD_TRAILERS, LOAD_TRAILERS_SUCCESS, LOAD_TRAILERS_FAIL],
    promise: client => client.get('/list-trailers', {params: {movieID: id}})
  };
}

export function addTrailer(movie, yt) {
  return {
    types: [ADD_TRAILER, ADD_TRAILER_SUCCESS, ADD_TRAILER_FAIL],
    promise: client => client.get('/youtube-add', {params: {youtube_id: yt, movie}})
  };
}

export function removeTrailer(id) {
  return {
    types: [REMOVE_TRAILER, REMOVE_TRAILER_SUCCESS, REMOVE_TRAILER_FAIL],
    promise: client => client.put('/delete-trailer', {data: {trailerID: id}}),
    id
  };
}

export function addSong(trailer_id, itunes_id) {
  return {
    types: [ADD_TRAILER_SONG, ADD_TRAILER_SONG_SUCCESS, ADD_TRAILER_SONG_FAIL],
    promise: client => client.post('/song/add/to', {data: {trailer: trailer_id, itunes_id}}),
    trailer_id
  };
}

export function clear() {
  return { type: CLEAR };
}
