import { init as initDiscussion, clear as clearDiscussion } from './discussion';
import { push } from 'react-router-redux';
import { urlGenerator } from 'utils/helpers';

const LOAD = 'game/LOAD';
const LOAD_SUCCESS = 'game/LOAD_SUCCESS';
const LOAD_FAIL = 'game/LOAD_FAIL';

const ADD = 'game/ADD';
const ADD_SUCCESS = 'game/ADD_SUCCESS';
const ADD_FAIL = 'game/ADD_FAIL';

const EDIT = 'game/EDIT';
const EDIT_SUCCESS = 'game/EDIT_SUCCESS';
const EDIT_FAIL = 'game/EDIT_FAIL';

const EDIT_SOUNDTRACK_INFO = 'game/EDIT_SOUNDTRACK_INFO';
const EDIT_SOUNDTRACK_INFO_SUCCESS = 'game/EDIT_SOUNDTRACK_INFO_SUCCESS';
const EDIT_SOUNDTRACK_INFO_FAIL = 'game/EDIT_SOUNDTRACK_INFO_FAIL';

const CLEAR = 'game/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  data: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result, error: null};
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case EDIT_SUCCESS:
      return {...state, data: {...state.data, game: {...state.data.game, ...action.result.data.game}}};
    case EDIT_SOUNDTRACK_INFO_SUCCESS:
      action && action.cb && action.cb('success');
      const {game} = state.data;
      game.soundtrack_info = action.result && action.result.data && action.result.data.soundtrack_info;
      return {...state, data: {...state.data, game: game}};
    case EDIT_SOUNDTRACK_INFO_FAIL:
      action && action.cb && action.cb('error');
      return {...state};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function load(id) {
  return dispatch => { // eslint-disable-line
    return dispatch({
      types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
      promise: (client) => client.get('/game-info', { params: { gameID: id, fromDb: true }}).then(data=>{
        const { discussions, ...rest } = data.data;
        if (discussions && Array.isArray(discussions)) {
          dispatch(initDiscussion(discussions, {game: rest.game._id}));
        }
        return rest;
      })
    });
  };
}

export function clear() {
  return dispatch => {
    dispatch(clearDiscussion());
    return dispatch({type: CLEAR});
  };
}

export function add({ banner, poster, ...rest }) {
  return dispatch => {
    return dispatch({
      types: [ADD, ADD_SUCCESS, ADD_FAIL],
      promise: (client) => client.post('games', {
        attachments: {
          banner: banner[0],
          poster: poster[0],
          ...rest
        }
      }).then(data => {
        dispatch(push(urlGenerator('games', {id: data.data.game._id, slug: data.data.game.slug || 'no_slug'})));
        return data;
      })
    });
  };
}

export function edit({ banner, poster, ...rest }) {
  console.log('I AM INSIDE GAME ACTION');
  return {
    types: [EDIT, EDIT_SUCCESS, EDIT_FAIL],
    promise: (client) => client.post('add-game', {
      attachments: {
        ...(banner ? {banner: banner[0]} : {}),
        ...(poster ? {poster: poster[0]} : {}),
        ...rest
      }
    })
  };
}

export function edit_soundtrack_info(id, msg, cb) {
  return {
    types: [EDIT_SOUNDTRACK_INFO, EDIT_SOUNDTRACK_INFO_SUCCESS, EDIT_SOUNDTRACK_INFO_FAIL],
    promise: (client) => client.post('/add_game_soundtrack_info', {
      params: {
        game: id,
        soundtrack_info: msg
      }
    }),
    cb,
  };
}
