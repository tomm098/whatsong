const LOAD = 'artistPage/LOAD';
const LOAD_SUCCESS = 'artistPage/LOAD_SUCCESS';
const LOAD_FAIL = 'artistPage/LOAD_FAIL';

const CLEAR = 'artistPage/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  data: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result.data, error: null};
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function load(id) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/artist/songs/list', { params: { artist: id }})
  };
}

export function clear() {
  return {type: CLEAR};
}
