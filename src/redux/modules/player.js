import cookie from 'react-cookie';
import { executionEnvironment } from 'utils/helpers';
import * as counterActions from './counters';


const PLAY = 'player/PLAY';
const PAUSE = 'player/PAUSE';
const END = 'player/END';
const STOP = 'player/STOP';
const SET_SOURCE = 'player/SET_SOURCE';

const initialState = {
  source: 'youtube',
  playing: false,
  active_song: null,
  playlist: [],
  playlist_index: -1
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case PLAY:
      return {...state, playing: true, ...(action.result)};
    case PAUSE:
      return {...state, playing: false};
    case SET_SOURCE:
      return {...state, source: action.source};
    case STOP:
      return {...state, playing: false, playlist: [], active_song: null, playlist_index: -1};
    case END:
      return {...state, playing: false, active_song: null};
    default:
      return state;
  }
}

export function play(song, playlist) {
  return (dispatch, getState) => {
    if (!song || !song._id) {
      return dispatch({
        type: PLAY,
        result: {}
      });
    }
    const state = getState().player;
    const screen_type = getState().screenType.type;
    let url = '';
    const yt_id = song.youtube_id;
    let real_source = state.source;
    if (state.source === 'youtube') {
      url = yt_id ? `https://www.youtube.com/watch?v=${yt_id}&origin=https://www.what-song.com` : song.preview_url || '';
      real_source = yt_id ? 'youtube' : song.preview_url ? 'itunes' : 'none';
    }

    if (state.source === 'itunes' || screen_type === 'mobile' || screen_type === 'tablet') {
      url = song.preview_url || (yt_id ? `https://www.youtube.com/watch?v=${yt_id}&origin=https://www.what-song.com` : '');
      real_source = song.preview_url ? 'itunes' : yt_id ? 'youtube' : 'none';
    }

    let playlist_index = -1;
    if (playlist && playlist.length) {
      playlist_index = playlist.findIndex(c=>c._id === song._id);
    }

    if (real_source !== 'none') {
      dispatch(counterActions.increase('Song', song._id));
    }

    return dispatch({
      type: PLAY,
      result: {
        active_song: {
          ...song,
          play_url: url,
          real_source
        },
        playlist: playlist || [],
        playlist_index
      }
    });
  };
}

export function pause() {
  return {
    type: PAUSE
  };
}

export function setSource(source) {
  if (executionEnvironment().canUseDOM) {
    cookie.save('player_source', source, {path: '/', expires: (new Date(3000, 1, 1))});
  }
  return {
    type: SET_SOURCE,
    source
  };
}

export function end() {
  return (dispatch, getState)=>{
    const { player: {playlist, playlist_index} } = getState();
    if (playlist_index < 0 || (playlist_index + 1 >= playlist.length)) {
      return dispatch( {
        type: END
      });
    } else { // eslint-disable-line
      return dispatch(play(playlist[playlist_index + 1], playlist));
    }
  };
}

export function next() {
  return (dispatch, getState)=>{
    const { player: {playlist, playlist_index} } = getState();
    if ((playlist_index + 1) < playlist.length) {
      return dispatch(play(playlist[playlist_index + 1], playlist));
    }
  };
}

export function prev() {
  return (dispatch, getState)=>{
    const { player: {playlist, playlist_index} } = getState();
    if (playlist.length && playlist_index > 0) {
      return dispatch(play(playlist[playlist_index - 1], playlist));
    }
  };
}

export function stop() {
  return {
    type: STOP
  };
}
