import { init as initDiscussion, clear as clearDiscussion } from './discussion'; // eslint-disable-line
import { push } from 'react-router-redux';
import { urlGenerator } from 'utils/helpers';

const LOAD = 'tvShowMain/LOAD';
const LOAD_SUCCESS = 'tvShowMain/LOAD_SUCCESS';
const LOAD_FAIL = 'tvShowMain/LOAD_FAIL';

const ADD_SEASON = 'tvShowMain/ADD_SEASON';
const ADD_SEASON_SUCCESS = 'tvShowMain/ADD_SEASON_SUCCESS';
const ADD_SEASON_FAIL = 'tvShowMain/ADD_SEASON_FAIL';

const ADD_SHOW = 'tvShowMain/ADD_SHOW';
const ADD_SHOW_SUCCESS = 'tvShowMain/ADD_SHOW_SUCCESS';
const ADD_SHOW_FAIL = 'tvShowMain/ADD_SHOW_FAIL';

const EDIT_SHOW = 'tvShowMain/EDIT_SHOW';
const EDIT_SHOW_SUCCESS = 'tvShowMain/EDIT_SHOW_SUCCESS';
const EDIT_SHOW_FAIL = 'tvShowMain/EDIT_SHOW_FAIL';

const CLEAR = 'tvShowMain/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  data: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result, error: null};
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case ADD_SEASON_SUCCESS:
      return {...state, data: {...state.data, seasons: [...state.data.seasons, action.result.data]}};
    case EDIT_SHOW_SUCCESS:
      return {...state, data: {...state.data, tv_show: {...state.data.tv_show, ...action.result.data.tv_show}}};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function load(id) {
  return dispatch => { // eslint-disable-line
    return dispatch({
      types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
      promise: (client) => client.get('/tv-info', { params: { tvshowID: id }}).then(data=>{
        const { discussions, ...rest } = data.data; // eslint-disable-line
        if (discussions && Array.isArray(discussions)) {
          dispatch(initDiscussion(discussions, {tv_show: rest.tv_show._id}));
        }
        return rest;
      })
    });
  };
}

export function clear() {
  return dispatch => {
    //dispatch(clearDiscussion());
    return dispatch({type: CLEAR});
  };
}

export function addSeason(id) {
  return {
    types: [ADD_SEASON, ADD_SEASON_SUCCESS, ADD_SEASON_FAIL],
    promise: (client) => client.post('/add-tv-show-season', { data: { tv_show: id }})
  };
}

export function addShow({ banner, poster, ...rest }) {
  return dispatch => {
    return dispatch({
      types: [ADD_SHOW, ADD_SHOW_SUCCESS, ADD_SHOW_FAIL],
      promise: (client) => client.post('add-tv-show', {
        attachments: {
          banner: banner[0],
          poster: poster[0],
          ...rest
        }
      }).then(data => {
        dispatch(push(urlGenerator('tv_show', {id: data.data.tv_show._id, slug: data.data.tv_show.slug || 'no_slug'})));
        return data;
      })
    });
  };
}

export function editShow({ banner, poster, ...rest }) {
  return {
    types: [EDIT_SHOW, EDIT_SHOW_SUCCESS, EDIT_SHOW_FAIL],
    promise: (client) => client.post('add-tv-show', {
      attachments: {
        ...(banner ? {banner: banner[0]} : {}),
        ...(poster ? {poster: poster[0]} : {}),
        ...rest
      }
    })
  };
}
