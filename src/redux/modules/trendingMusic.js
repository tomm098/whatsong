const LOAD = 'trendingMusic/LOAD';
const LOAD_SUCCESS = 'trendingMusic/LOAD_SUCCESS';
const LOAD_FAIL = 'trendingMusic/LOAD_FAIL';

const CLEAR = 'trendingMusic/CLEAR';

const SET_TYPE = 'trendingMusic/SET_TYPE';
const SET_TIME = 'trendingMusic/SET_TIME';

const time_variants = [

  {
    name: 'today',
    label: 'Today',
    counter: 'listened_today'
  },
  {
    name: 'yesterday',
    label: 'Yesterday',
    counter: 'listened_yesterday'
  },
  {
    name: 'week',
    label: 'This Week',
    counter: 'listened_week'
  },
  {
    name: 'month',
    label: 'This Month',
    counter: 'listened_month'
  },
  {
    name: 'year',
    label: 'This Year',
    counter: 'listened_year'
  },
  {
    name: 'all',
    label: 'All Time',
    counter: 'played_time'
  }
];

const type_variants = [
  {
    name: 'both',
    label: 'Both'
  },
  {
    name: 'movie',
    label: 'Movies Only'
  },
  {
    name: 'tv_show',
    label: 'Television Only'
  }
];

const initialState = {
  loading: false,
  loaded: false,
  time_variants,
  type_variants,
  active_type: type_variants[0],
  active_time: time_variants[0],
  data: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result.data, error: null};
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case SET_TYPE:
      return {...state, active_type: action.variant};
    case SET_TIME:
      return {...state, active_time: action.variant};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function load() {
  return (dispatch, getState) => {
    const state = getState().trendingMusic;
    return dispatch({
      types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
      promise: (client) => client.get('/songs/trend/filter', {
        params: {
          media: state.active_type.name,
          time: state.active_time.name,
          limit: 100,
          skip: 0
        }
      })
    });
  };
}

export function setTime(time) {
  return dispatch => {
    const variant = time_variants.find(c=>c.name === time);
    if (variant) {
      dispatch({type: SET_TIME, variant});
      return dispatch(load());
    }
  };
}

export function setType(type) {
  return dispatch => {
    const variant = type_variants.find(c=>c.name === type);
    if (variant) {
      dispatch({type: SET_TYPE, variant});
      return dispatch(load());
    }
  };
}

export function clear() {
  return {type: CLEAR};
}
