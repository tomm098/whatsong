const INCREASE = 'counters/INCREASE';
const INCREASE_SUCCESS = 'counters/INCREASE_SUCCESS';
const INCREASE_FAIL = 'counters/INCREASE_FAIL';

const initialState = {
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {

    default:
      return state;
  }
}

export function increase(type, id) {
  return {
    types: [INCREASE, INCREASE_SUCCESS, INCREASE_FAIL],
    promise: (client) => client.get('/add-views-count', { params: { type, id }})
  };
}
