const SET_SCREEN = 'screenType/SET_SCREEN';

const initialState = {
  type: 'desktop'
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_SCREEN:
      return {...state, type: action.result};
    default:
      return state;
  }
}

export function setScreen(type) {
  return {
    type: SET_SCREEN,
    result: type
  };
}
