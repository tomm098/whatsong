const LOAD = 'profile/LOAD';
const LOAD_SUCCESS = 'profile/LOAD_SUCCESS';
const LOAD_FAIL = 'profile/LOAD_FAIL';

const CLEAR = 'profile/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  data: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result.data, error: null};
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function load(username) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get('/user-by-name', { params: { username }})
  };
}

export function clear() {
  return {type: CLEAR};
}

