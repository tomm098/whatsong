import { init as initDiscussion, clear as clearDiscussion } from './discussion';
import { push } from 'react-router-redux';
import { urlGenerator } from 'utils/helpers';

const LOAD = 'movie/LOAD';
const LOAD_SUCCESS = 'movie/LOAD_SUCCESS';
const LOAD_FAIL = 'movie/LOAD_FAIL';

const ADD = 'movie/ADD';
const ADD_SUCCESS = 'movie/ADD_SUCCESS';
const ADD_FAIL = 'movie/ADD_FAIL';

const EDIT = 'movie/EDIT';
const EDIT_SUCCESS = 'movie/EDIT_SUCCESS';
const EDIT_FAIL = 'movie/EDIT_FAIL';

const EDIT_SOUNDTRACK_INFO = 'movie/EDIT_SOUNDTRACK_INFO';
const EDIT_SOUNDTRACK_INFO_SUCCESS = 'movie/EDIT_SOUNDTRACK_INFO_SUCCESS';
const EDIT_SOUNDTRACK_INFO_FAIL = 'movie/EDIT_SOUNDTRACK_INFO_FAIL';

const CLEAR = 'movie/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  data: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result, error: null};
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case EDIT_SUCCESS:
      return {...state, data: {...state.data, movie: {...state.data.movie, ...action.result.data.movie}}};
    case EDIT_SOUNDTRACK_INFO_SUCCESS:
      action && action.cb && action.cb('success');
      const {movie} = state.data;
      movie.soundtrack_info = action.result && action.result.data && action.result.data.soundtrack_info;
      return {...state, data: {...state.data, movie: movie}};
    case EDIT_SOUNDTRACK_INFO_FAIL:
      action && action.cb && action.cb('error');
      return {...state};
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function load(id) {
  return dispatch => { // eslint-disable-line
    return dispatch({
      types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
      promise: (client) => client.get('/movie-info', { params: { movieID: id, fromDb: true }}).then(data=>{
        const { discussions, ...rest } = data.data;
        if (discussions && Array.isArray(discussions)) {
          dispatch(initDiscussion(discussions, {movie: rest.movie._id}));
        }
        return rest;
      })
    });
  };
}

export function clear() {
  return dispatch => {
    dispatch(clearDiscussion());
    return dispatch({type: CLEAR});
  };
}

export function add({ banner, poster, ...rest }) {
  return dispatch => {
    return dispatch({
      types: [ADD, ADD_SUCCESS, ADD_FAIL],
      promise: (client) => client.post('movie', {
        attachments: {
          banner: banner[0],
          poster: poster[0],
          ...rest
        }
      }).then(data => {
        dispatch(push(urlGenerator('movie', {id: data.data.movie._id, slug: data.data.movie.slug || 'no_slug'})));
        return data;
      })
    });
  };
}

export function edit({ banner, poster, ...rest }) {
  return {
    types: [EDIT, EDIT_SUCCESS, EDIT_FAIL],
    promise: (client) => client.post('movie', {
      attachments: {
        ...(banner ? {banner: banner[0]} : {}),
        ...(poster ? {poster: poster[0]} : {}),
        ...rest
      }
    })
  };
}

export function edit_soundtrack_info(id, msg, cb) {
  return {
    types: [EDIT_SOUNDTRACK_INFO, EDIT_SOUNDTRACK_INFO_SUCCESS, EDIT_SOUNDTRACK_INFO_FAIL],
    promise: (client) => client.post('/add_movie_soundtrack_info', {
      params: {
        movie_id: id,
        soundtrack_info: msg
      }
    }),
    cb,
  };
}
