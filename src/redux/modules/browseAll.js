import { wrappWithObj } from 'utils/helpers';

const LOAD = 'browseAll/LOAD';
const LOAD_SUCCESS = 'browseAll/LOAD_SUCCESS';
const LOAD_FAIL = 'browseAll/LOAD_FAIL';

const ACTIVATE_LETTER = 'profile/ACTIVATE_LETTER';
const DEACTIVATE_LETTER = 'profile/DEACTIVATE_LETTER';

const CLEAR = 'profile/CLEAR';

const initialState = {
  loading: false,
  loaded: false,
  type: '',
  data: [],
  active_letters: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {...state, loading: true};
    case LOAD_SUCCESS:
      return {...state, loading: false, loaded: true, data: action.result.data, type: action.path, error: null};
    case LOAD_FAIL:
      return {...state, loading: false, loaded: false, error: action.error};
    case ACTIVATE_LETTER:
      return {...state, active_letters: {...state.active_letters, ...wrappWithObj(action.letter, true)}};
    case DEACTIVATE_LETTER:
      return (()=> {
        const active_letters = {...state.active_letters};
        delete active_letters[action.letter];
        return {...state, active_letters};
      })();
    case CLEAR:
      return initialState;
    default:
      return state;
  }
}

export function load(path) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: (client) => client.get(path, {}),
    path
  };
}

export function activateLetter(letter, action) {
  return {
    type: action ? ACTIVATE_LETTER : DEACTIVATE_LETTER,
    letter
  };
}

export function clear() {
  return {type: CLEAR};
}

