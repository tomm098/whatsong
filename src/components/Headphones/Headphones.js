import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Headphones.scss';
import { nFormatter } from 'utils/helpers';

export default class Headphones extends Component {
  static propTypes = {
    count: PropTypes.number
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <div className="headphones"><i className="icon-font icon-font-play-track-button"></i><span className="headphones__text">{nFormatter(this.props.count, 1)}</span></div>
    );
  }
}
