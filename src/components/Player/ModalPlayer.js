import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import Modal from 'react-modal';
import ReactPlayer from 'react-player';
import {} from './ModalPlayer.scss';

export default class ModalPlayer extends Component {
  static propTypes = {
    ytId: PropTypes.string,
    onCloseModal: PropTypes.func
  }
  constructor(props) {
    super(props);
    this.state = {
      showModal: true
    };
  }

  closeModal = () => {
    this.setState({
      showModal: false
    });
    if (this.props.onCloseModal) {
      this.props.onCloseModal();
    }
  }

  render() {
    const { ytId } = this.props;
    const e = window.document.getElementById('content');
    return (
      <Modal
        isOpen={this.state.showModal}
        onRequestClose={this.closeModal}
        className="ModalPlayer"
        appElement={e}
      >
        <div className="modal-dialog">
          <ReactPlayer
            url={`https://www.youtube.com/watch?v=${ytId}&origin=https://www.what-song.com`}
          />
        </div>
      </Modal>
    );
  }
}
