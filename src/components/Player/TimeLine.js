import React from 'react';
import InputRange from 'react-input-range';

const PlayerTimeLine = ({ handleMouseDown, handleChange, handleMouseUp, played }) =>{
  return (
    <div className="player__timeline">
      <div onMouseDown={handleMouseDown}>
        <InputRange
          minValue={0} maxValue={1} step={0.0005}
          labelPrefix="sdsfsd"
          value={played}
          onChange={handleChange}
          formatLabel={()=>''}
          onChangeComplete={handleMouseUp}
          className="player__range"
        />
      </div>
    </div>
  );
};
export default PlayerTimeLine;
