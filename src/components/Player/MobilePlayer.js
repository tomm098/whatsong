import React, {Component, PropTypes} from 'react'; // eslint-disable-line

import { connect } from 'react-redux';
import { Controls, Info, /* YoutubeLink*/ Options } from './';
import { Like } from 'components';
import * as playerActions from 'redux/modules/player';
import * as notificationsActions from 'redux/modules/notifications';
import {} from './Player.scss';

@connect(
  state => ({
    playing: state.player.playing,
    song: state.player.active_song
  }),
  {
    handleEnd: playerActions.end,
    pushNotification: notificationsActions.push,
    stop: playerActions.stop
  }
)


class MobilePlayer extends Component {
  static propTypes = {
    playing: PropTypes.bool,
    song: PropTypes.object,
    handleEnd: PropTypes.func,
    pushNotification: PropTypes.func,
    stop: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.initPlayer = this.initPlayer.bind(this);
    this.audio = null;
    this.state = {
      volume: 0.8,
      played: 0,
      loaded: 0,
      duration: 0,
      is_player_init: false
    };
  }

  componentDidMount() {
    this.props.stop();
    window.addEventListener('touchstart', this.initPlayer);
    this.audio = new Audio();
    this.audio.onended = this.props.handleEnd;
  }

  componentWillReceiveProps(nextProps) {
    if (this.audio) {
      if (nextProps.song) {
        if (nextProps.playing) {
          if ((this.props.song && this.props.song.play_url || '') !== nextProps.song.play_url) {
            this.audio.src = nextProps.song.play_url;
          }
          this.audio.play();
        } else {
          this.audio.pause();
        }
      } else {
        this.audio.pause();
      }
    }
  }

  componentDidUpdate() {
    const { song, handleEnd } = this.props;
    if (song && !song.play_url) {
      handleEnd();
    }
  }

  componentWillUnmount() {
    this.props.stop();
    this.audio.pause();
    window.removeEventListener('touchstart', this.initPlayer);
  }


  initPlayer() {
    if (!this.state.is_player_init) {
      this.audio.play();
      this.setState({is_player_init: true});
    }
  }

  render() {
    const { song, pushNotification, stop } = this.props;
    return (
      (song || null) &&
      <div className="player">
         {/* <YoutubeLink yt_id={song.youtube_id}/> */}
        <div className="player__all">

          <Info song={song}/>
            {
              !song.temp &&
              <Like
                type="song"
                id={song._id}
                liked={song.is_favorited}
                like_count={song.favourited}
                onLike={()=>pushNotification('favorite_song', {name: song.title, by: song.artist && song.artist.name})}
              />
            }
            <Options
              song={song}
              player_visible={false}
              togglePlayer = {()=>{}}
              handleClose={()=>stop()}
            />
            {song.spotify_uri && <div className="song__link-spotify"><a href={song.spotify_uri}><span data-tooltip="Open in Spotify"><i className="icon-font icon-font-spotify"></i></span></a></div>}
          <div className="player__center">
            <Controls />
          </div>
        </div>
      </div>
    );
  }
}
export default MobilePlayer;

