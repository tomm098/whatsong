import React from 'react';
import { timeInterval } from 'utils/helpers';

const formatter = interval => `${interval.hours * 60 + interval.minutes}:${('0' + interval.seconds).slice(-2)}`;
const PlayerTime = ({ played, duration, type }) =>{
  const time_interval = type === 'start' ? timeInterval(played * duration * 1e3) : timeInterval(duration * (1 - played) * 1e3);
  return (
    <div className={`player__time ${'player__time--' + type}`}>
      {
        type === 'start' ?
        <div className="player__time-start">{formatter(time_interval)}</div> :
        <div className="player__time-end">-{formatter(time_interval)}</div>
      }
    </div>
  );
};
export default PlayerTime;
