import React, {Component, PropTypes} from 'react'; // eslint-disable-line

export default class YoutubeLink extends Component {
  static propTypes = {
    yt_id: PropTypes.string
  }
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    const { yt_id } = this.props;
    return (
      (yt_id || null) &&
      <div className="youtube-link">
        <a href={`https://www.youtube.com/watch?v=${yt_id}`} className="youtube-link__wrapp">
          <div className="youtube-link__container">
            <i className="icon-font icon-font-youtube-logotype"></i><span>play in youtube</span>
          </div>
        </a>
      </div>
    );
  }
}
