import React, {Component, PropTypes} from 'react'; // eslint-disable-line
// const image = require('./img1.jpg');

export default class PlayerInfo extends Component {
  static propTypes = {
    song: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { song } = this.props;
    return (
      <div className="player__info">
        {song.spotifyImg64 && <img src={song.spotifyImg64} />}
        <div className="player__text">
          <div className="player__album">{song.title}</div>
          <div className="player__title">{song.artist.name}</div>
          <button className="btn btn--clear btn-download visible-xs"><i className="icon-font icon-font-download-song"></i></button>
        </div>
      </div>
    );
  }
}
