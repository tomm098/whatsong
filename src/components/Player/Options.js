import React, {Component, PropTypes} from 'react'; // eslint-disable-line
// import { Dropdown } from 'elements';
import { Song } from 'components';

export default class Options extends Component {
  static propTypes = {
    song: PropTypes.object,
    player_visible: PropTypes.bool,
    togglePlayer: PropTypes.func,
    handleClose: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { song, player_visible, togglePlayer, handleClose } = this.props;
    return (
      <div className="player__options">
        <button data-tooltip="close player" className="btn btn--clear btn-close-player" onClick={handleClose}><i className="icon-font icon-font-close-player"></i></button>
        {
          song && song.real_source === 'youtube' &&
          <button
            className="btn btn--clear btn-video"
            onClick={togglePlayer}
          >
            {
              player_visible ?
                <span data-tooltip="hide video"><i className="icon-font icon-font-eye-close"></i> </span> :
                <span data-tooltip="show video"><i className="icon-font icon-font-eye-open"></i></span>
            }
          </button>
        }


        <Song.SongLinks className="player-too" song={song} />

        {/*<Dropdown
          trigger = {
            <button data-tooltip="download" className="btn btn--clear btn-download"><i className="icon-font icon-font-download-song"></i></button>
            }
        >
          <Song.DownloadList song={song} className={'dropdown-menu dropdown-menu--center dropdown-menu--arrow-bottom dropdown-menu--download-player dropdown-menu--top'} />
        </Dropdown>
      */}
      </div>
    );
  }
}
