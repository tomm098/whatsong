import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import ReactPlayer from 'react-player';
import { connect } from 'react-redux';
import { TimeLine, Controls, Options, Info, Time, /* YoutubeLink */ } from './';
import { Like } from 'components';
import * as playerActions from 'redux/modules/player';
import * as notificationsActions from 'redux/modules/notifications';
import {} from './Player.scss';

@connect(
  state => ({
    playing: state.player.playing,
    song: state.player.active_song
  }),
  {
    handleEnd: playerActions.end,
    pushNotification: notificationsActions.push,
    stop: playerActions.stop
  }
)


class Player extends Component {
  static propTypes = {
    playing: PropTypes.bool,
    song: PropTypes.object,
    handleEnd: PropTypes.func,
    pushNotification: PropTypes.func,
    stop: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      volume: 0.8,
      played: 0,
      loaded: 0,
      duration: 0,
      player_visible: true
    };
  }

  componentDidMount() {
    this.props.stop();
  }

  componentDidUpdate() {
    const { song, handleEnd } = this.props;
    if (song && !song.play_url) {
      handleEnd();
    }
  }

  onSeekMouseDown = () => {
    this.setState({ seeking: true });
  }

  onSeekChange = (_, val) => {
    this.setState({ played: val });
  }

  onSeekMouseUp = (_, val) => {
    this.setState({ seeking: false });
    this.refs.player.seekTo(val);
  }

  onProgress = state => {
    // We only want to update time slider if we are not currently seeking
    if (!this.state.seeking) {
      this.setState(state);
    }
  }

  setVolume = e => {
    this.setState({ volume: parseFloat(e.target.value) });
  }

  togglePlayer() {
    this.setState({player_visible: !this.state.player_visible});
  }

  render() {
    const { playing, song, handleEnd, pushNotification, stop } = this.props;
    const { played, volume, player_visible, duration } = this.state;
    return (
      (song || null) &&
      <div className="player">
        { /* <YoutubeLink yt_id={song.youtube_id}/> */}
        <TimeLine
          played={played}
          handleMouseDown={this.onSeekMouseDown}
          handleChange={this.onSeekChange}
          handleMouseUp={this.onSeekMouseUp}
        />
        <div key={song._id} className={song.real_source === 'youtube' ? (player_visible ? 'visible' : 'hidden') : 'hidden'}>
          <ReactPlayer
            ref="player"
            playing={playing}
            volume={volume}
            url={song.play_url || ''}
            onEnded={handleEnd}
            className={'react-player'}
            onProgress={this.onProgress}
            onDuration={duration_ => this.setState({ duration: duration_ })}
            onError={handleEnd}
            progressFrequency={300}
          />
        </div>
        <div className="player__all">
          <Time
            type="start"
            played={played}
            duration={duration}
          />
          <Info song={song}/>
          {
            !song.temp &&
            <Like
              type="song"
              id={song._id}
              liked={song.is_favorited}
              like_count={song.favourited}
              onLike={()=>pushNotification('favorite_song', {name: song.title, by: song.artist && song.artist.name})}
            />
          }
          <Time
            type="end"
            played={played}
            duration={duration}
          />
          <Options
            song={song}
            player_visible={player_visible}
            togglePlayer = {::this.togglePlayer}
            handleClose={()=>stop()}
          />
          <div className="player__center">
            <Controls />
          </div>
        </div>
      </div>
    );
  }
}
export default Player;

