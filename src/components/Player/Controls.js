import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { connect } from 'react-redux';

import * as playerActions from 'redux/modules/player';

@connect(
  state => ({
    playing: state.player.playing,
    song: state.player.active_song,
    playlist: state.player.playlist,
    playlist_index: state.player.playlist_index
  }),
  {
    play: playerActions.play,
    pause: playerActions.pause,
    prev: playerActions.prev,
    next: playerActions.next,
  }
)

export default class PlayerControls extends Component {
  static propTypes = {
    playing: PropTypes.bool,
    song: PropTypes.object,
    play: PropTypes.func,
    pause: PropTypes.func,
    prev: PropTypes.func,
    next: PropTypes.func,
    playlist: PropTypes.array,
    playlist_index: PropTypes.number
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { playing, play, pause, prev, next, playlist, playlist_index } = this.props; // eslint-disable-line
    const is_prev_active = playlist.length && playlist_index > 0;
    const is_next_active = (playlist_index + 1) < playlist.length;
    return (
      <div className="player__controls">
        {
          <button className={`btn btn--clear btn-prev${!is_prev_active ? ' is-disabled' : ''}`} onClick={()=>is_prev_active && prev()}><i className="icon-font icon-font-prev-track-button"></i></button>
        }
        {
          playing ?
            <button className="btn btn--clear btn-play" onClick={pause}><i className="icon-font icon-font-pause-track-button"></i></button>
          :
            <button className="btn btn--clear btn-play" onClick={play}><i className="icon-font icon-font-play-track-button"></i></button>
        }
        {
          <button className={`btn btn--clear btn-next${!is_next_active ? ' is-disabled' : ''}`} onClick={()=>is_next_active && next()}><i className="icon-font icon-font-next-track-button"></i></button>
        }
      </div>
    );
  }
}
