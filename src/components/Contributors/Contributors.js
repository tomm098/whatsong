import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Contributors.scss';
import { Item } from './';
export default class Contributors extends Component {
  static propTypes = {
    contributors: PropTypes.array.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      this.props.contributors &&
      <section className="contributors">
      <header className="contributors__header">
        <h3 className="contributors__title">CONTRIBUTORS</h3>
      </header>
        <div className="contributors__container">
          {
            this.props.contributors.map((c, i)=>
                <Item key={i} contributor={c}/>
            )
            }
        </div>
      </section>
    );
  }
}

