import React, {Component, PropTypes} from 'react';
import { abbreviateNumber } from '../../utils/helpers';

export default class ContributorsBadge extends Component {
  static propTypes = {
    contributor: PropTypes.object,
  }

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const { contributor: { role, scores } } = this.props;
    const role_lowercased = role.toLowerCase();

    if (scores === 0) {
      return null;
    }

    return (
      <div className={`contributors__badge ${role_lowercased}`}>
        <span>{role_lowercased}</span>
        <span>{abbreviateNumber(scores)}</span>
      </div>
    );
  }
}
