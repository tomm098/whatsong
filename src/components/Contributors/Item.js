import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Contributors.scss';
import { ThankCount } from 'components';
import { nFormatter } from 'utils/helpers';
import { ContributorsBadge } from './';

export default class ContributorsItem extends Component {
  static propTypes = {
    contributor: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { contributor } = this.props;

    return (
      contributor &&
      <div className="contributors__item">
        <div className="contributors__item-top">
          <div className="contributors__item-left">
            <div className="contributors__name">{contributor.username}</div>
            <ThankCount highlight={contributor.role === 'ADMIN'} score={contributor.scores}/>
            <ContributorsBadge contributor={contributor} />
          </div>
          <div className="contributors__item-right">
            {/* <ContributorsThank contributor={contributor} /> */}
          </div>
        </div>
        <div className="contributors__item-bottom">
          {`${nFormatter(contributor.added_songs_count, 1)} songs • ${nFormatter(contributor.added_scenes_count, 1)} scenes`}
        </div>
      </div>
    );
  }
}
