import React, {Component, PropTypes} from 'react';
import * as thankActions from 'redux/modules/thanks';
import * as authMoadalActions from 'redux/modules/authModal';
import * as notificationsActions from 'redux/modules/notifications';
import { connect } from 'react-redux';

@connect(
  (state, props) => ({
    changed: state.thanks.users[props.contributor._id],
    user: state.auth.user
  }),
  {
    thank: thankActions.thankUser,
    openAuthModal: authMoadalActions.open,
    pushNotification: notificationsActions.push
  }
)

export default class ContributorsThank extends Component {
  static propTypes = {
    contributor: PropTypes.object,
    changed: PropTypes.object,
    thank: PropTypes.func,
    openAuthModal: PropTypes.func,
    user: PropTypes.object,
    pushNotification: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleThank(count_thanks, is_thanked) {
    const { contributor, contributor: { _id }, thank, openAuthModal, user, pushNotification } = this.props;
    if (!user) {return openAuthModal('sign-in');}
    thank(_id, !is_thanked, count_thanks).then(()=>{
      if (!is_thanked) {pushNotification('thank_user', {user: contributor});}
    });
  }

  render() {
    const { changed, contributor } = this.props;
    const { count_thanks } = changed || contributor;
    const { is_thanked } = (changed && typeof changed.is_thanked === 'boolean' ? changed : null) || contributor;
    return (
      <div data-tooltip="thank this contributor" onClick={()=>this.handleThank(count_thanks, is_thanked)} className={'contributors__top-like tooltip--left' + (is_thanked ? ' contributors__top-like--is-active' : '')}>
        <i className="icon-font icon-font-btn-like"></i>
        <span>{count_thanks}</span>
      </div>
    );
  }
}
