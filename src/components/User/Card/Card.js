import React from 'react';
import {} from './UserCard.scss';
const image = require('../../../images/default-avatar.svg');

const UserCard = ({user}) => // eslint-disable-line
  <div className="user-card">
    <div className="user-photo" style={{backgroundImage: (user.photo || image) ? `url(${user.photo || image})` : null}}></div>
    <span>{user.first_name && user.last_name ? `${user.first_name} ${user.last_name}` : user.username }</span>
  </div>;

export default UserCard;
