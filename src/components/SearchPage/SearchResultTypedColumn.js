import React from 'react';
import { urlGenerator, search2urlTypes } from 'utils/helpers';
import { Link } from 'react-router';

const SearchResultTypedColumn = ({list})=>
  <div className="search-result__col">
    <div className="search-result__category">{list.group}</div>
    <ul>
      {
        list.data && list.data.length ? list.data.map((c, i)=>
          <li key={i}>
            <Link
              to={
                urlGenerator(
                  search2urlTypes(list.group),
                  {id: c._id, slug: c.slug}
                )
              }
            >
              {c.title || c.name}
            </Link>
            <span>{c.year || ''}</span>
          </li>
        ) :
          <li>
            <span>No titles found</span>
          </li>
      }
    </ul>
  </div>;
export default SearchResultTypedColumn;
