import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Notifications.scss';
import { connect } from 'react-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';


const image1 = require('./heart2.svg');
const image2 = require('./cinema-step.svg');
const image3 = require('./cinema.svg');
const image4 = require('./tv.svg');
const image5 = require('./check2.svg');
const image6 = require('./close.svg');
const image7 = require('./melody.svg');
const image8 = require('./like2.svg');

const notifications = {
  favorite_song: ({name, by})=>
    <div className="alert-notification__content">
      <img src={image1} />
      <div className="alert-notification__text">
        <span>You have favorited “{name}” {by ? ' by ' + by : ''}.</span>
        <p>You can see all your favorites from the dropdown in the navigation bar.</p>
      </div>
    </div>,

  add_description: ({})=>
    <div className="alert-notification__content">
      <img src={image2} />
      <div className="alert-notification__text">
        <span>Your scene description has been saved! Thank you greatly.</span>
        <p>You will be awarded your points once other users or our admin have verified it.</p>
      </div>
    </div>,

  favorite_movie: ({name})=>
    <div className="alert-notification__content">
      <img src={image3} />
      <div className="alert-notification__text">
        <span>You have favorited “{name}”. Great choice.</span>
        <p>All your favorites can be found in your profile.</p>
      </div>
    </div>,

  favorite_tv_show: ({name})=>
    <div className="alert-notification__content">
      <img src={image4} />
      <div className="alert-notification__text">
        <span>You have favorited “{name}”. Great choice.</span>
        <p>All your favorites can be found in your profile.</p>
      </div>
    </div>,

  vote_correct: ({name, by})=>
    <div className="alert-notification__content">
      <img src={image5} />
      <div className="alert-notification__text">
        <span>You have voted “{name}” {by ? ' by ' + by : ''} as correct.</span>
        <p>Thank you for helping keep our site accurate. You have been awarded 1 point.</p>
      </div>
    </div>,

  vote_incorrect: ({name, by})=>
    <div className="alert-notification__content">
      <img src={image6} />
      <div className="alert-notification__text">
        <span>You have voted “{name}” {by ? ' by ' + by : ''} as incorrect.</span>
        <p>Thank you for helping keep our site accurate. You have been awarded 1 point.</p>
      </div>
    </div>,

  songs_add: ({to, nof_songs})=>
    <div className="alert-notification__content">
      <img src={image7} />
      <div className="alert-notification__text">
        <span>You have added {nof_songs} song{nof_songs !== 1 ? 's' : ''} to {to}.</span>
        <p>Thank you so much! Why not try adding scene descriptions to earn more points and thanks.</p>
      </div>
    </div>,

  thank_user: ({user})=>
    <div className="alert-notification__content">
      <img src={image8} />
      <div className="alert-notification__text">
        <span>You have thanked <a href="#">{user.username}</a> for their contribution. We’ll notify them at once.</span>
        <p>Thanking other users is what keeps our community going. Keep up the good work.</p>
      </div>
    </div>,

  settings_saved: ({})=>
    <div className="alert-notification__content">
      <div className="alert-notification__text">
        <span>Your changes were saved successfully.</span>
      </div>
    </div>,

  add_to_shortlist: ({name, by})=>
    <div className="alert-notification__content">
      <div className="alert-notification__text">
        <span>You have added “{name}” {by ? ' by ' + by : ''} to shortlist</span>
      </div>
    </div>,

  sign_in_success: ({})=>
    <div className="alert-notification__content">
      <div className="alert-notification__text">
        <span>You have successfully logged in. Have fun.</span>
      </div>
    </div>,

  sign_up_success: ({})=>
    <div className="alert-notification__content">
      <div className="alert-notification__text">
        <span>You have successfully registered.</span>
        <p>Please confirm you email, and then log in.</p>
      </div>
    </div>,

  restore_sent: ({})=>
    <div className="alert-notification__content">
      <div className="alert-notification__text">
        <span>Reset Password link sent.</span>
        <p>Please check your email.</p>
      </div>
    </div>
};

@connect(
  state => ({
    notification_list: state.notifications.notifications
  }),
  {

  }
)

export default class Notifications extends Component {
  static propTypes = {
    notification_list: PropTypes.array,
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { notification_list } = this.props;
    return (
      <div className="alert-notification">
        <ReactCSSTransitionGroup component="div" transitionName="notification" transitionLeaveTimeout={1000} transitionEnterTimeout={1}>
          {
            notification_list.map(c=>
              <div key={c.id} className="alert-notification__wrapp">
                {notifications[c.type] && notifications[c.type](c.payload)}
              </div>
            )
          }
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}
