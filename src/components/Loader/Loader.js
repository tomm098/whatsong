import React from 'react';
import {} from './Loader.scss';

const Loader = ({type}) =>
  <div className={'loader-wrapp' + (type ? ` loader-wrapp--${type}` : '')}>
    <div className="loader"></div>
  </div>;
export default Loader;
