import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Dropdown } from 'elements';
import DownloadList from './DownloadList';

export default class SongMore extends Component {
  static propTypes = {
    innerClassName: PropTypes.string,
    song: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { innerClassName, song } = this.props;
    return (
      <div className="song__download">
        <Dropdown
          trigger = {
            <button className="btn btn--more">more</button>
            }
        >
          <ul className={innerClassName}>
            <li><strong>spotify</strong></li>
            <li><a href="#"><span>Add to Your Music</span></a></li>
            <li className="is-active">
              <a href="#"><span>Add to Playlist</span></a>
              <ul className="second">
                <li>
                  <a href="#"><em className="ico-plus">+</em><span>WhatSong</span></a>
                </li>
                <li>
                  <a href="#"><em className="ico-plus">+</em><span>My Shazaam Tracks</span></a>
                </li>
                <li>
                  <a href="#"><em className="ico-plus">+</em><span>For Bel</span></a>
                </li>
                <li>
                  <a href="#"><i className="icon-font icon-font-check-dropdown"></i><span>My iTunes</span></a>
                </li>
              </ul>
            </li>
            <li><div className="song__separator"></div></li>
            <li><strong>download</strong></li>
            <DownloadList song={song}/>
          </ul>
        </Dropdown>
      </div>
    );
  }
}
