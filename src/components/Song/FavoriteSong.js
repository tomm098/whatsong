import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Like } from 'components';
import { ScreenType } from 'elements';
import SongMobile from './SongMobile';
import DropFavorite from './DropFavorite';
import {} from './Song.scss';
import {} from './FavoriteSong.scss';
import { fireEvent } from 'utils/helpers';
import { TimeSince } from 'elements';
import { Link } from 'react-router';
import { urlGenerator } from 'utils/helpers';

export default class FavoutiteSong extends Component {
  static propTypes = {
    song: PropTypes.object,
    play: PropTypes.func,
    pause: PropTypes.func,
    playing: PropTypes.bool,
    is_active: PropTypes.bool,
    handleSongVote: PropTypes.func,
    handleSceneVote: PropTypes.func,
    voting: PropTypes.bool,
    display_options: PropTypes.bool,
    handleDislike: PropTypes.func,
    handleLike: PropTypes.func
  }

  static defaultProps = {
    display_options: true
  }

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  playPause() {
    const { is_active, playing, play, pause, song } = this.props;
    if (!song.youtube_id && !song.preview_url) {return false;}
    if (is_active) {
      playing ? pause() : play();
    } else {
      play(song);
    }
  }

  handleMouseLeave() {
    fireEvent(window.document.body, 'click');
  }


  render() {
    const { song, is_active, playing, handleDislike, handleLike } = this.props;
    const disabled = !song.youtube_id && !song.preview_url;
    return (
      (song || null ) &&
      (song.approved_count > -2) &&
      <div className={`song song--favorite ${is_active && playing ? ' is-playing' : ''} ${disabled ? ' is-disabled' : ''}`}>
        <ScreenType.TabletMin>
          <div onMouseLeave={::this.handleMouseLeave} className="song__container-desktop">
            <div onClick={::this.playPause} className="song__btn-play-box">
              <div className="song__mask">
                <button className="btn btn--clear">
                  <i className="icon-font icon-font-btn-play"></i>
                  <i className="icon-font icon-font-btn-pause"></i>
                </button>
              </div>
              {song.spotifyImg64 && <img src={song.spotifyImg64} alt=""/>}
            </div>

            <div className="song__properties">
              <div className="song__top">
                <div className="song__title-check">
                  <div className="song__title">{song.title}</div>
                  <div className="song__heard">
                    {song.from && song.from.name && <p><span className="gray">heard in</span> {song.from.name}</p>}
                  </div>
                </div>

                <div className="song__name-like">
                  {
                    song.artist &&
                    <div className="song__album"><Link to={urlGenerator('artist', {id: song.artist._id, slug: song.artist.slug})}>{song.artist.name}</Link></div>
                  }
                  {song.from && <div className="song__from"><Link to={urlGenerator(song.from.type, song.from)}>{song.from.name}</Link></div>}
                </div>
                <div className="song__right-block">
                  <DropFavorite
                    song={song}
                    innerClassName="dropdown-menu dropdown-menu--favorites"
                    like = {
                      <Like
                        onDislike={()=>handleDislike(song._id)}
                        onLike={()=>handleLike({name: song.title, by: song.artist.name})}
                        type="song"
                        id={song._id}
                        liked={song.is_favorited}
                        like_count={song.favourited}
                        modifier="favorites"
                      >
                        <div>
                          <span className="dropp-add">Add to Favorites</span>
                          <span className="dropp-remove">Remove from Favorites</span>
                        </div>
                      </Like>
                    }
                  />
                  <div className="song__times"><TimeSince time={song.created_at}/></div>
                </div>
              </div>
              <div className="song__middle">
                <div className="song__album">{song.artist.name}</div>
              </div>
            </div>
          </div>
        </ScreenType.TabletMin>

        <ScreenType.Mobile>
          <SongMobile
            playPause={::this.playPause}
            song={song}
            onDislike={()=>handleDislike(song._id)}
            onLike={()=>handleLike({name: song.title, by: song.artist.name})}
          />
        </ScreenType.Mobile>
      </div>
    );
  }
}
