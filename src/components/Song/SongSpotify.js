import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Dropdown } from 'elements';

export default class SongSpotify extends Component {
  static propTypes = {
    innerClassName: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { innerClassName } = this.props;
    return (
      <div className="song__download">
        <Dropdown
          trigger = {
            <button className="btn btn--spotify"><i className="icon-font icon-font-spotify"></i>spotify</button>
            }
        >
          <ul className={innerClassName}>
            <li><a href="#"><em className="ico-plus">+</em><span>Save to your music</span></a></li>
            <li className="is-active">
              <a href="#"><em className="ico-plus"></em><span>Add to playlist</span></a>
              <ul className="second">
                <li>
                  <a href="#"><i className="ico-plus">+</i><span>WhatSong</span></a>
                </li>
                <li>
                  <a href="#"><i className="ico-plus">+</i><span>My Shazaam Tracks</span></a>
                </li>
                <li>
                  <a href="#"><em className="ico-plus">+</em><span>For Bel</span></a>
                </li>
                <li>
                  <a href="#"><i className="icon-font icon-font-check-dropdown"></i><span>My iTunes</span></a>
                </li>
              </ul>
            </li>
            <li><div className="song__separator"></div></li>
            <li><a href="#"><em className="ico-plus"></em><span>Open in Spotify</span></a></li>
          </ul>
        </Dropdown>
      </div>
    );
  }
}
