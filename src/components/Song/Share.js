import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Dropdown } from 'elements';
export default class SongShare extends Component {
  static propTypes = {
  }

  constructor(props) {
    super(props);
    this.state = {
      url: ''
    };
  }

  updateMassage() {
    this.setState({url: encodeURIComponent(window.location.href)});
  }

  render() {
    const { url } = this.state;
    return (
    <Dropdown
      trigger={
        <div onClick={()=>this.updateMassage()} className="share"><i className="icon-font icon-font-share"></i> <span className="share__text">share</span></div>
      }
    >
      <ul className="dropdown-menu dropdown-menu--share dropdown-menu--arrow-left">
        <li>
          <a target="_blank" href={`mailto:?&body=${url}`}>Email</a>
        </li>
        <li>
          <a target="_blank" href={`https://twitter.com/home?status=${url}`}>Twitter</a>
        </li>
        <li>
          <a target="_blank" href={`https://www.facebook.com/sharer/sharer.php?u=${url}`}>Facebook</a>
        </li>
      </ul>
    </Dropdown>
    );
  }
}
