import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Like, Headphones, TvEpisodeLink } from 'components';
import { ScreenType } from 'elements';
import SongMobile from './SongMobile';
import SongLinks from './SongLinks';
import { Link } from 'react-router';
import {} from './Song.scss';
import {} from './SongPopular.scss';
import {} from './SongMobile.scss';
import { urlGenerator, timeInterval } from 'utils/helpers';


export default class ItemPageSong extends Component {
  static propTypes = {
    modifier: PropTypes.string,
    song: PropTypes.object,
    play: PropTypes.func,
    pause: PropTypes.func,
    playing: PropTypes.bool,
    is_active: PropTypes.bool,
    pushNotification: PropTypes.func
  }

  static defaultProps = {
    display_options: true
  }

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  playPause() {
    const { is_active, playing, play, pause, song } = this.props;
    if (!song.youtube_id && !song.preview_url) {return false;}
    if (is_active) {
      playing ? pause() : play();
    } else {
      play(song);
    }
  }


  render() {
    const { modifier, song, is_active, playing, pushNotification } = this.props;

    const disabled = !song.youtube_id && !song.preview_url;
    const time_interval = timeInterval((song.time_play || 0) * 6e4);
    const time_played = !song.time_play ? '' : `${time_interval.hours}:${('0' + time_interval.minutes).slice(-2)}`;

    return (
    (song || null ) &&
    (song.approved_count > -2) && !song.is_deleted &&
    <div
      className={`song song--popular ${modifier ? ('song--' + modifier) : ''}${is_active && playing ? ' is-playing' : ''}${disabled ? ' is-disabled' : ''}`}
      // itemType="http://schema.org/MusicRecording"
      // itemProp="track"
      // itemScope={true}
    >
      <ScreenType.TabletMin>
        <div className="song__container-desktop">
          <div onClick={::this.playPause} className="song__btn-play-box">
            <button className="btn btn--clear">
              <i className="icon-font icon-font-btn-play"></i>
              <i className="icon-font icon-font-btn-pause"></i>
            </button>
          </div>

          <div className="song__properties">
            <div className="song__properties-left">
              <div className="song__top">
                <div className="song__title-check">
                  <div itemProp="name" className="song__title">{song.title}</div>
                </div>

                <div itemProp="byArtist" className="song__name-like">
                  {song.artist && <div className="song__album"><Link to={urlGenerator('artist', {id: song.artist._id, slug: song.artist.slug || 'no-slug'})}>{song.artist.name}</Link></div>}
                </div>
                <div className="song__like-headphones">
                  <Like
                    type="song"
                    id={song._id}
                    liked={song.is_favorited}
                    like_count={song.favourited}
                    onLike={()=>pushNotification('favorite_song', {by: song.artist && song.artist.name, name: song.title})}
                  />
                  <Headphones count={song.played_time}/>
                </div>
              </div>
              <div className="song__middle">
                {
                  (song.time_play || song.scene_description) &&
                  <div className="song__description">
                    <div className="song__info">
                      {song.time_play && <div className="song__time">{time_played}</div>}
                      {song.scene_description && <span itemProp="comment">{song.scene_description}</span>}
                    </div>
                  </div>
                }
                {song.artist && <div className="song__album">{song.artist.name}</div>}
              </div>

              <div className="song__bottom">
                {
                  song.season && song.episode &&
                  <TvEpisodeLink
                    season={song.season}
                    episode={song.episode}
                  />
                }
              </div>
            </div>
            <div className="song__properties-right">
              <SongLinks song={song} />
            </div>
          </div>
        </div>
      </ScreenType.TabletMin>

      <ScreenType.Mobile>
        <SongMobile
          playPause={::this.playPause}
          song={song}
          show_scene={true}
          time_played = {song.time_play ? time_played : ''}
          show_image={false}
          modifier={'main'}
          onLike={()=>pushNotification('favorite_song', {by: song.artist && song.artist.name, name: song.title})}
        />
      </ScreenType.Mobile>

    </div>
    );
  }
}

