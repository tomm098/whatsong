import React from 'react';
import {Form} from 'elements';
import {} from './SongDeletModal.scss';

const SongDeleteForm = ({song, handleDelete, closePopup})=>
  <div className="song-delete">
    <div className="song-delete__question">Are you sure?</div>
    <div className="song-delete__wrapp-btn">
      <Form.Button className="btn--success" onClick={()=>handleDelete(song).then(closePopup)}>yes</Form.Button>
      <Form.Button className="btn--danger2" onClick={closePopup}>no</Form.Button>
    </div>
  </div>;
export default SongDeleteForm;
