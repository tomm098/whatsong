import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Dropdown } from 'elements';

export default class SongOptionsMenu extends Component {
  static propTypes = {
    handleEdit: PropTypes.func,
    handleDelete: PropTypes.func,
    can_delete: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { handleEdit, handleDelete, can_delete } = this.props;
    return (
      <Dropdown
        trigger={
          <div className="three-dots"><i className="icon-font icon-font-three-dots"></i></div>
        }
      >
      <ul className="dropdown-menu dropdown-menu--three-dots dropdown-menu--arrow-left">
        <li>
          <a onClick={e=>{e.preventDefault(); handleEdit();}}>edit song/scene</a>
        </li>
        {
          can_delete &&
          <li>
            <a onClick={e=>{e.preventDefault(); handleDelete();}}>delete song</a>
          </li>
        }
      </ul>
    </Dropdown>
    );
  }
}
