import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Dropdown } from 'elements';
import DownloadList from './DownloadList';

export default class SongDownload extends Component {
  static propTypes = {
    innerClassName: PropTypes.string,
    song: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { innerClassName, song } = this.props;
    return (
      <div className="song__download">
        <Dropdown
          trigger = {
            <button className="btn btn--download">download</button>
          }
        >
          <DownloadList song={song} className={innerClassName} />
        </Dropdown>
      </div>
    );
  }
}
