import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
import { getExternalLink, getAmazonLink } from '../../utils/helpers';

@connect(
  state => ({
    geolocation: state.auth.location,
  }),
  {}
)
export default class SongDownloadList extends Component {
  static propTypes = {
    geolocation: PropTypes.object,
    song: PropTypes.object,
    className: PropTypes.string
  }
  render() {
    const { className, song, geolocation } = this.props;
    return (
      <ul className={className}>
        {song.itunes_url && <li><a target="_blank" href={song.itunes_url}><i className="icon-font icon-font-itunes"></i><span>itunes</span></a></li>}
        {song.title && <li>
          <a
            target="_blank"
            rel="nofollow"
            href={getExternalLink(getAmazonLink(geolocation && geolocation.country, encodeURIComponent(song.title + (song.artist.name ? ' by ' + song.artist.name : ''))))}
          >
            <i className="icon-font icon-font-amazon"></i>
            <span>amazon</span>
          </a>
        </li>}
        {song.spotify_url && <li><a target="_blank" rel="nofollow" href={getExternalLink(song.spotify_url)}><i className="icon-font icon-font-spotify"></i><span>spotify</span></a></li>}
        {song.youtube_id && <li><a target="_blank" href={`https://www.youtube.com/watch?v=${song.youtube_id}`}><i className="icon-font icon-font-youtube"></i><span>youtube</span></a></li>}
      </ul>
    );
  }
}

