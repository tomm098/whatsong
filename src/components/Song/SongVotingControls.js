import React from 'react';
const SongVotingControl = ({vote, count, active, tooltip_name}) =>
  <div className="song__check">
    {
      count < 4 ?
        <div>
          <span data-tooltip={`${tooltip_name} correct`}>
            <i onClick={()=>vote(1)} className={'icon-font icon-font-plus' + (active === 1 ? ' is-active' : '')} />
          </span>
          <span data-tooltip={`${tooltip_name} incorrect`}>
            <i onClick={()=>vote(-1)} className={'icon-font icon-font-minus' + (active === -1 ? ' is-active' : '')} />
          </span>
        </div>
        :
        <div>
          <span data-tooltip="song correct">
            <i className="icon-font icon-font-check" />
          </span>
        </div>
    }
  </div>;
export default SongVotingControl;
