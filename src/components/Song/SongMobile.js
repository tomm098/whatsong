import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import SongLinks from './SongLinks';
// import SpotifyLink from './SpotifyLink';
//import { Like, Headphones } from 'components';
import {} from './SongMobile.scss';
import { urlGenerator, printDate } from 'utils/helpers';
import { Link } from 'react-router';

export default class SongMobile extends Component {
  static propTypes = {
    song: PropTypes.object,
    playPause: PropTypes.func,
    time_played: PropTypes.string,
    handleEdit: PropTypes.func,
    handleAddScene: PropTypes.func,
    show_scene: PropTypes.bool,
    is_artist: PropTypes.bool,
    show_image: PropTypes.bool,
    modifier: PropTypes.string,
    onLike: PropTypes.func,
    onDislike: PropTypes.func
  }

  static defaultProps ={
    onLike: ()=>{},
    onDislike: ()=>{}
  }

  constructor(props) {
    super(props);
    this.state = {
    };
  }


  render() {
    const { modifier, song, playPause, handleAddScene, time_played, show_scene, show_image = true, is_artist } = this.props;
    // const { modifier, song, playPause, handleEdit, handleAddScene, time_played, show_scene, show_image = true, onLike, onDislike } = this.props;
    song.approvedTime_count = !!song.approvedTime_count || 0;
    return (
      <div ref="song" className={`song__container-mobile ${modifier ? 'song__container-mobile--' + modifier : ''}` }>
        <div className="song__mobile">
          <div className="song__mobile-left">
            <div className="song__mobile-img">
              <div onClick={playPause} className="song__btn-play-box">
                <div className="song__mask">
                  <button className="btn btn--clear">
                    <i className="icon-font icon-font-btn-play"></i>
                    <i className="icon-font icon-font-btn-pause"></i>
                  </button>
                </div>
                {show_image && song.spotifyImg64 && <img src={song.spotifyImg64} alt=""/>}
              </div>
            </div>
            <div className="song__mobile-info">
              <div className="song__mobile-info-wrapp">
                <div className="song__mobile-info-left">
                  <div className="song__mobile-title">{song.title}</div>
                </div>
              </div>
              {!is_artist && <div className="song__mobile-album">
              <Link
                      to={urlGenerator('artist', {id: song.artist._id, slug: song.artist.slug || 'no-slug'})}>{song.artist.name}</Link>
              </div>}
              {!is_artist && <div className="song__heard">
                {song.from && song.from.name && <p><span>heard in</span> <Link to={urlGenerator(song.from.type, song.from)}>{song.from.name}</Link></p>}
              </div>}

              {
                show_scene && (song.approvedTime_count > -2) &&
                <div>
                  {
                    song.time_play || song.scene_description ?
                      <div className="song__description">
                          <div className="song__info">
                            <div className="song__time">{time_played} {song.scene_description}</div>
                            {/* <div className="song__mobile-info-bottom">
                              <Like
                                type="song"
                                id={song._id}
                                liked={song.is_favorited}
                                like_count={song.favourited}
                                onLike={onLike}
                                onDislike={onDislike}
                              />
                              <Headphones count={song.played_time}/>
                              {handleEdit && <div onClick={handleEdit} className="three-dots"><i className="icon-font icon-font-three-dots"></i></div>}
                            </div> */}
                          </div>
                      </div>
                      :
                    handleAddScene && <span className="song__add-scene" onClick={handleAddScene}>
                          Add scene description <span className="green-highlight">+5 points</span>
                        </span> ||
                    null
                  }
                </div>
              }
            </div>
          </div>
          <div className="song__mobile-bottom">
          </div>
          <div className={`song__links-box ${is_artist ? 'artist' : ''}`}>
                <SongLinks song={song} />
                {/* <div className="song__links-right">
                  <SpotifyLink song={song} />
                </div> */}
              </div>
        </div>
        {is_artist && <div className="song__artist line"/>}
        {is_artist && <div className="song__from artist">
          {song.from && song.from.name && <p><Link to={urlGenerator(song.from.type, song.from)}>{song.from.name}</Link></p>}
          {song.from && song.from.time_released && <div className="song__mobile release_date">{printDate(song.from.time_released)}</div>}
        </div>}
      </div>
    );
  }
}
