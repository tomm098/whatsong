import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Like } from 'components';
const itunes = require('../../images/itunes.png');

export default class SongMobileDropdownNew extends Component {
  static propTypes = {
    song: PropTypes.object,
    handleEdit: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { song, handleEdit } = this.props;
    return (
     <div className="song__mobile-dropp">

       <div className="song__mobile-icons">
         <div className="song__mobile-image">
           <a href="#"><img src={itunes} /></a>
         </div>
         <Like
           modifier="song-mobile"
           type="song"
           id={song._id}
           liked={song.is_favorited}
           like_count={song.favourited}
         />
         <div className="song__mobile-spotify-link">
           {song.spotify_uri && <a href={song.spotify_uri}><i className="icon-font icon-font-spotify"></i></a>}
         </div>
       </div>
       {handleEdit && <div className="song__mobile-edit-text" onClick={handleEdit}>edit</div>}
     </div>
    );
  }
}
