import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import Swipeable from 'react-swipeable';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import { Like } from 'components';
export default class SongMobileDropdown extends Component {
  static propTypes = {
    song: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {
      is_open: false
    };
  }

  open() {
    this.setState({is_open: true});
  }

  close() {
    this.setState({is_open: false});
  }

  render() {
    const { is_open } = this.state;
    const { song } = this.props;
    return (
      <div className="song__mobile-dropdown dropdown">
        <div className="song__mobile-dropdown-trigger"></div>
        <Swipeable
          onSwipedRight={::this.close}
          onSwipedLeft={::this.open}
        >
          <div className="song__swipe-block">
            <i onClick={::this.open} className="icon-font icon-font-three-dots"></i>
            <ReactCSSTransitionGroup transitionName="mobile-song-dropdown" component="div" transitionLeaveTimeout={200} transitionEnterTimeout={200}>
              {
                is_open &&
                <ul className="song__mobile-swipe-content">
                  <li><div className="song__mobile-spotify"><i className="icon-font icon-font-spotify"></i></div></li>
                  <li><div className="song__mobile-edit">EDIT</div></li>
                  <li><div className="song__mobile-share">SHARE</div></li>
                  <li>
                    <div className="song__mobile-like">
                      <Like
                        modifier="song-mobile"
                        type="song"
                        id={song._id}
                        liked={song.is_favorited}
                        like_count={song.favourited}
                      />
                    </div>
                  </li>
                  <li><div className="song__mobile-download">DOWNLOAD</div></li>
                </ul>
              }
            </ReactCSSTransitionGroup>
          </div>
        </Swipeable>
      </div>
    );
  }
}
