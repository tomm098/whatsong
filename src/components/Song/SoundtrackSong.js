import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Like, Headphones } from 'components';
import { ScreenType } from 'elements';
//import SongLinks from './SongLinks';
import SpotifyLink from './SpotifyLink';
import { Link } from 'react-router';
import {} from './Song.scss';
import {} from './SoundtrackSong.scss';
import { urlGenerator } from 'utils/helpers';


export default class ItemPageSong extends Component {
  static propTypes = {
    modifier: PropTypes.string,
    song: PropTypes.object,
    play: PropTypes.func,
    pause: PropTypes.func,
    playing: PropTypes.bool,
    is_active: PropTypes.bool,
    pushNotification: PropTypes.func
  }

  static defaultProps = {
    display_options: true
  }

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  playPause() {
    const { is_active, playing, play, pause, song } = this.props;
    if (!song.youtube_id && !song.preview_url) {return false;}
    if (is_active) {
      playing ? pause() : play();
    } else {
      play(song);
    }
  }


  render() {
    const { modifier, song, is_active, playing, pushNotification } = this.props;

    const disabled = !song.youtube_id && !song.preview_url;

    return (
    (song || null ) &&
    (song.approved_count > -2) && !song.is_deleted &&
    <div
      className={`song song--soundtrack ${modifier ? ('song--' + modifier) : ''}${is_active && playing ? ' is-playing' : ''}${disabled ? ' is-disabled' : ''}`}
      // itemType="http://schema.org/MusicRecording"
      // itemProp="track"
      // itemScope={true}
    >
      <ScreenType.TabletMin>
        <div className="song__container-desktop">
          <div onClick={::this.playPause} className="song__btn-play-box-soundtrack">
            <button className="btn btn--clear">
              <i className="icon-font icon-font-btn-play"></i>
              <i className="icon-font icon-font-btn-pause"></i>
            </button>
          </div>

          <div className="song__properties">
            <div className="song__soundtrack-properties-left">
              <div className="song__top">
                <div className="song__title-check">
                  <div itemProp="name" className="song__title-soundtrack">{song.title}</div>
                </div>

                <div className="song__name-like">
                  {song.artist && <div className="song__album--soundtrack-artist"><Link itemProp="byArtist" to={urlGenerator('artist', {id: song.artist._id, slug: song.artist.slug || 'no-slug'})}>{song.artist.name}</Link></div>}
                </div>
              </div>
            </div>
            <div className="song__soundtrack-properties-right">
              <div className="song__like-headphones">
                <Like
                  type="song"
                  id={song._id}
                  liked={song.is_favorited}
                  like_count={song.favourited}
                  onLike={()=>pushNotification('favorite_song', {by: song.artist && song.artist.name, name: song.title})}
                />
                <Headphones count={song.played_time}/>
              </div>
              {/*<SongLinks className="song__links--official" song={song} />*/}
              <SpotifyLink song={song} />
            </div>
          </div>
        </div>
      </ScreenType.TabletMin>

      <ScreenType.Mobile>
        <div className="song__container-desktop">
          <div onClick={::this.playPause} className="song__btn-play-box-soundtrack">
            <button className="btn btn--clear">
              <i className="icon-font icon-font-btn-play"></i>
              <i className="icon-font icon-font-btn-pause"></i>
            </button>
          </div>

          <div className="song__properties">
            <div className="song__soundtrack-properties-left">
              <div className="song__top">
                <div className="song__title-check">
                  <div className="song__title">{song.title} • {song.artist && <span className="song__album"><Link to={urlGenerator('artist', {id: song.artist._id, slug: song.artist.slug || 'no-slug'})}>{song.artist.name}</Link></span>}</div>
                  {/* <div className="song__name-like">
                    {song.artist && <div className="song__album"><Link to={urlGenerator('artist', {id: song.artist._id, slug: song.artist.slug || 'no-slug'})}>{song.artist.name}</Link></div>}
                  </div> */}
                </div>
                <div onClick={e=>e.stopPropagation()} className="song__properties-right">
                  <div className="song__like-headphones">
                    <Like
                      type="song"
                      id={song._id}
                      liked={song.is_favorited}
                      like_count={song.favourited}
                      onLike={()=>pushNotification('favorite_song', {by: song.artist && song.artist.name, name: song.title})}
                    />
                    {/* <Headphones count={song.played_time}/> */}
                  </div>
                  {/*<SongLinks className="song__links--official" song={song} />*/}
                  {/* <SpotifyLink song={song} /> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </ScreenType.Mobile>
    </div>
    );
  }
}

