import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {Like, Headphones, TvEpisodeLink} from 'components';
import {ScreenType, Popup} from 'elements';
import SongMobile from './SongMobile';
import SongOptions from './SongOptions';
import SongThank from './SongThank';
import SongEdit from './SongEdit';
import SongLinks from './SongLinks';
//import SpotifyLink from './SpotifyLink';
import SceneAddForm from './SceneAddForm';
import SongDeleteForm from './SongDeleteForm';
import {Link} from 'react-router';
import {} from './Song.scss';
import {timeInterval, fireEvent, urlGenerator} from 'utils/helpers';

export default class ItemPageSong extends Component {
  static propTypes = {
    modifier: PropTypes.string,
    song: PropTypes.object,
    play: PropTypes.func,
    pause: PropTypes.func,
    playing: PropTypes.bool,
    is_active: PropTypes.bool,
    handleSongVote: PropTypes.func,
    handleSceneVote: PropTypes.func,
    voting: PropTypes.bool,
    display_options: PropTypes.bool,
    pushNotification: PropTypes.func,
    addScene: PropTypes.func,
    user_role: PropTypes.string,
    username: PropTypes.string,
    checkUser: PropTypes.func,
    editSong: PropTypes.func,
    deleteSong: PropTypes.func
  }

  static defaultProps = {
    display_options: true
  }

  constructor(props) {
    super(props);
    this.state = {
      edit_popup_opened: false,
      add_scene_popup_opened: false,
      delete_song_popup_opened: false
    };
  }

  playPause() {
    const {is_active, playing, play, pause, song} = this.props;
    if (!song.youtube_id && !song.preview_url) {
      return false;
    }
    if (is_active) {
      playing ? pause() : play();
    } else {
      play(song);
    }
  }

  handleMouseLeave() {
    fireEvent(window.document.body, 'click');
  }

  closeEditPopup() {
    this.setState({edit_popup_opened: false});
  }

  render() {
    const {username, user_role, editSong, deleteSong, modifier, checkUser, song, is_active, playing, display_options, pushNotification, addScene} = this.props;
    const {add_scene_popup_opened, edit_popup_opened, delete_song_popup_opened} = this.state;
    const time_interval = timeInterval((song.time_play || 0) * 6e4);
    const time_played = !song.time_play ? '' : `${time_interval.hours}:${('0' + time_interval.minutes).slice(-2)}`;

    const disabled = !song.youtube_id && !song.preview_url;

    return (
      (song || null ) &&
      (song.approved_count > -2) && !song.is_deleted &&
      <div
        className={`song ${modifier ? ('song--' + modifier) : ''}${is_active && playing ? ' is-playing' : ''}${disabled ? ' is-disabled' : ''}`}
        // itemScope={true}
        // itemType="http://schema.org/MusicRecording"
        // itemProp="track"
      >
        <ScreenType.TabletMin>
          <div onMouseLeave={::this.handleMouseLeave} className="song__container-desktop">
            <div onClick={::this.playPause} className="song__btn-play-box">
              <button className="btn btn--clear">
                <i className="icon-font icon-font-btn-play"></i>
                <i className="icon-font icon-font-btn-pause"></i>
              </button>
            </div>

            <div className="song__properties">
              <div className="song__properties-left">
                <div className="song__top">
                  <div className="song__title-check">

                    <div itemProp="name" className="song__title">{song.title} • {song.artist && <span className="song__album"><Link
                      to={urlGenerator('artist', {id: song.artist._id, slug: song.artist.slug || 'no-slug'})}>{song.artist.name}</Link>
                    </span>}</div>
                  </div>

                  {/* <div className="song__name-like">
                    {song.artist && <div className="song__album"><Link
                      to={urlGenerator('artist', {id: song.artist._id, slug: song.artist.slug || 'no-slug'})}>{song.artist.name}</Link>
                    </div>}
                  </div> */}
                </div>
                <div className="song__middle">

                  {song.artist && <div itemProp="byArtist" className="song__album">{song.artist.name}</div>}
                  {
                    (song.approvedTime_count > -2) &&
                    <div>
                      {
                        song.time_play || song.scene_description ?
                          <div className="song__description">
                            <div className="song__info">
                              {time_played && <div className="song__time">{time_played}</div>}
                              <span itemProp="description">{song.scene_description}</span>
                            </div>
                          </div>
                          :
                        addScene && <span className="song__add-scene"
                                          onClick={()=>checkUser() && this.setState({add_scene_popup_opened: true})}>
                      Add scene description <span className="green-highlight">+5</span>
                      </span> ||
                        null
                      }
                    </div>
                  }
                </div>
                <div className="song__bottom">
                  <Like
                    type="song"
                    id={song._id}
                    liked={song.is_favorited}
                    like_count={song.favourited}
                    onLike={()=>pushNotification('favorite_song', {by: song.artist && song.artist.name, name: song.title})}
                  />
                  <Headphones count={song.played_time}/>
                  {song.contributer_user && <SongThank song={song}/>}
                  {
                    display_options &&
                    <SongOptions
                      handleEdit={()=>checkUser() && this.setState({edit_popup_opened: true})}
                      handleDelete={()=>this.setState({delete_song_popup_opened: true})}
                      can_delete={user_role === 'ADMIN' || (song.contributer_user && song.contributer_user.username === username) }
                    />
                  }
                  {
                    song.season && song.episode &&
                    <TvEpisodeLink
                      season={song.season}
                      episode={song.episode}
                    />
                  }
                </div>
              </div>
              <div className="song__properties-right">
                <SongLinks song={song}/>
                {/*<SpotifyLink song={song}/>*/}
              </div>
            </div>
          </div>
        </ScreenType.TabletMin>

        <ScreenType.Mobile>
          <SongMobile
            playPause={::this.playPause}
            song={song}
            time_played={time_played}
            handleAddScene={addScene ? ()=>checkUser() && this.setState({add_scene_popup_opened: true}) : undefined}
            handleEdit={()=>checkUser() && this.setState({edit_popup_opened: true})}
            show_scene={true}
            show_image={false}
            modifier={'main'}
            onLike={()=>pushNotification('favorite_song', {by: song.artist && song.artist.name, name: song.title})}
          />
        </ScreenType.Mobile>

        <Popup
          is_opened={edit_popup_opened}
          handleClose={::this.closeEditPopup}
          title="Edit Song"
          modifier="full-width"
        >
          <SongEdit user_role={user_role} editSong={editSong} song={song}/>
        </Popup>

        <Popup
          is_opened={add_scene_popup_opened}
          handleClose={()=>this.setState({add_scene_popup_opened: false})}
          title="Add scene"
        >
          <SceneAddForm handleEdit={addScene} song={song}/>
        </Popup>

        <Popup
          is_opened={delete_song_popup_opened}
          handleClose={()=>this.setState({delete_song_popup_opened: false})}
          title="Delete song"
        >
          <SongDeleteForm handleDelete={deleteSong} song={song}/>
        </Popup>
      </div>
    );
  }
}

