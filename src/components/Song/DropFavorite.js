import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Dropdown } from 'elements';
import DownloadList from './DownloadList';

export default class DropFavorite extends Component {
  static propTypes = {
    innerClassName: PropTypes.string,
    song: PropTypes.object,
    like: PropTypes.node
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { innerClassName, song, like } = this.props;
    return (
      <div className="song__download">
        <Dropdown
          trigger = {
            <button className="btn btn--clear"><i className="icon-font icon-font-three-dots"></i></button>
            }
        >
          <ul className={innerClassName}>
            <li>{like}</li>
            <li><div className="song__separator"></div></li>
            <DownloadList song={song}/>
          </ul>
        </Dropdown>
      </div>
    );
  }
}
