import React, {Component, PropTypes} from 'react'; // eslint-disable-line
//import { Like } from 'components';
import SongDownload from './SongDownload';
import {} from './Song.scss';
import { fireEvent, nFormatter } from 'utils/helpers';

export default class ProfileSong extends Component {
  static propTypes = {
    modifier: PropTypes.string,
    song: PropTypes.object,
    play: PropTypes.func,
    pause: PropTypes.func,
    playing: PropTypes.bool,
    is_active: PropTypes.bool,
    handleSongVote: PropTypes.func,
    handleSceneVote: PropTypes.func,
    voting: PropTypes.bool,
    display_options: PropTypes.bool
  }

  static defaultProps = {
    display_options: true
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  playPause() {
    const { is_active, playing, play, pause, song } = this.props;
    if (!song.youtube_id && !song.preview_url) {return false;}
    if (is_active) {
      playing ? pause() : play();
    } else {
      play(song);
    }
  }

  handleMouseLeave() {
    fireEvent(window.document.body, 'click');
  }

  render() {
    const { modifier, song, is_active, playing } = this.props;
    const disabled = !song.youtube_id && !song.preview_url;

    return (
    (song || null ) &&
    (song.approved_count > -2) &&
    <div onMouseLeave={::this.handleMouseLeave} className={`song ${modifier ? ('song--' + modifier) : ''}${is_active && playing ? ' is-playing' : ''}${disabled ? ' is-disabled' : ''}`}>
        <div className="song__container-desktop">
          <div onClick={::this.playPause} className="song__btn-play-box">
            <button className="btn btn--clear">
              <i className="icon-font icon-font-btn-play"></i>
              <i className="icon-font icon-font-btn-pause"></i>
            </button>
          </div>

          <div className="song__properties">
            <div className="song__top">
              <div className="song__title-check">
                <div className="song__title">{song.title}</div>
              </div>

              <div className="song__name-like">
                <div className="song__album">{song.artist.name}</div>
                <div className={`like is-liked `}>
                  <i className="icon-font icon-font-heart"></i>
                  <span className="like__like-text">{nFormatter(song.favourited, 1)}</span>
                </div>
              </div>
              <SongDownload song={song} innerClassName="dropdown-menu dropdown-menu--right dropdown-menu--arrow-center dropdown-menu--download"/>
            </div>
            <div className="song__middle">
              <div className="song__heard">
                {song.from && song.from.title && <span><span className="gray">heard in</span> {song.from.title}</span>}
              </div>
              <div className="song__album">{song.artist.name}</div>
            </div>
          </div>
        </div>
    </div>
    );
  }
}

