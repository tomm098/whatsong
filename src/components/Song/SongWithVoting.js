import Song from './Song';
import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import * as songVotingActions from 'redux/modules/songVoting';
import * as authMoadalActions from 'redux/modules/authModal';
import * as notificationsActions from 'redux/modules/notifications';

@connect(
  (state, props) => ({
    votedSong: state.songVoting[props.song._id],
    user: state.auth.user
  }),
  {
    vote: songVotingActions.vote,
    openAuthModal: authMoadalActions.open,
    pushNotification: notificationsActions.push
  }
)

export default class SongWithVoting extends Component {
  static propTypes = {
    song: PropTypes.object,
    votedSong: PropTypes.object,
    vote: PropTypes.func,
    openAuthModal: PropTypes.func,
    user: PropTypes.object,
    pushNotification: PropTypes.func
  }

  static defaultProps = {
    votedSong: {}
  }

  constructor(props) {
    super(props);
    this.state = {

    };
    this.handleSongVote = this.handleSongVote.bind(this);
    this.handleSceneVote = this.handleSceneVote.bind(this);
  }

  handleSongVote(vote_value) {
    const { vote, votedSong: { is_approved }, song: { _id, title, artist }, user, openAuthModal, pushNotification } = this.props;
    if (!user) {return openAuthModal('sign-in');}
    vote(_id, vote_value, 'title').then(()=>{
      if (is_approved !== 1 && vote_value === 1) {
        pushNotification('vote_correct', {name: title, by: artist && artist.name});
      }
      if (is_approved !== -1 && vote_value === -1) {
        pushNotification('vote_incorrect', {name: title, by: artist && artist.name});
      }
    });
  }

  handleSceneVote(vote_value) {
    const { vote, song: { _id }, user, openAuthModal } = this.props;
    if (!user) {return openAuthModal('sign-in');}
    vote(_id, vote_value, 'time');
  }

  render() {
    const {song, votedSong, pushNotification, ...rest} = this.props;
    return (
      <Song
        {...rest}
        voting={true}
        song={{...song, ...votedSong}}
        handleSongVote={this.handleSongVote}
        handleSceneVote={this.handleSceneVote}
        pushNotification={pushNotification}
      />
    );
  }
}
