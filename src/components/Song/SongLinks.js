import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { connect } from 'react-redux';
import { ModalPlayer } from '../Player';
const image1 = require('../../images/youtube.jpg');
const image2 = require('../../images/spotify.jpg');
const image3 = require('../../images/amazon.jpg');
const image4 = require('../../images/itunes.jpg');
import {parseForwardiTunesURL, parseForwardURL} from '../../utils/helpers';

@connect(
  state => ({
    geolocation: state.auth.location,
  }),
  {}
)
export default class SongLinks extends Component {
  static propTypes = {
    geolocation: PropTypes.object,
    song: PropTypes.object,
    className: PropTypes.string
  }

  static defaultProps = {
    className: ''
  }

  constructor(props) {
    super(props);
    this.state = {
      modalYtId: null
    };
  }

  onShowYoutubeModal = (yt_id) => {
    this.setState({
      modalYtId: yt_id
    });
  }

  onCloseModal = () => {
    this.setState({
      modalYtId: null
    });
  }

  render() {
    const { className, song, geolocation } = this.props;
    const { modalYtId } = this.state;
    const spotify_uri = song.spotify_uri ? song.spotify_uri : (song.spotify ? `spotify:track:${song.spotify}` : '');

    return (
      <div className={`song__links ${className}`}>
        {
          modalYtId && <ModalPlayer onCloseModal={this.onCloseModal} ytId={modalYtId} />
        }
        <div className="song__links-wrapp">
          {
            <a
              className={`song__youtube${!song.youtube_id ? ' is-disabled' : ''}`}
              rel="nofollow"
              target="_blank"
              href={'#'}
              onClick={e=>{
                if (song.youtube_id) {
                  e.preventDefault();
                  this.onShowYoutubeModal(song.youtube_id);
                }
              }}
            >
              <span>YouTube</span>
              <img src={image1} alt={`Listen to Song on YouTube - ${song.title}`}/>
            </a>
          }
          {
            <a
              className={`song__spotify${!spotify_uri ? ' is-disabled' : ''}`}
              rel="nofollow"
              href={spotify_uri || '#'}
              onClick={e=>{
                if (!spotify_uri) {
                  e.preventDefault();
                }
              }}
            >
              <span>spotify</span>
              <img src={image2} alt={`Listen to Song on Spotify - ${song.title}`}/>
            </a>
          }
          {
            <a
              className={`song__itunes${!song.itunes_url ? ' is-disabled' : ''}`}
              rel="nofollow"
              target="_blank"
              href={parseForwardiTunesURL(song) || '#'}
              onClick={e=>{
                if (!song.itunes_url) {
                  e.preventDefault();
                }
              }}
            >
              <span>itunes</span>
              <img src={image4} alt={`Download on iTunes - ${song.title}`}/>
            </a>
          }
          {
            <a
              className={`song__amazon${!song.title ? ' is-disabled' : ''}`}
              target="_blank"
              rel="nofollow"
              onClick={e=>{
                if (!song.title) {
                  e.preventDefault();
                }
              }}
              href={parseForwardURL(song, 'amazon', geolocation) || '#'}
            >
              <span>amazon</span>
              <img src={image3} alt={`Download on Amazon - ${song.title}`}/>
            </a>
          }
        </div>
      </div>
    );
  }
}
