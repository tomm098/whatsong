import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Form, Popup } from 'elements';
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import { EditPage } from 'components';

@reduxForm({
  form: 'SongEditForm'
})

export default class SongEdit extends Component {
  static propTypes = {
    song: PropTypes.object,
    initialize: PropTypes.func,
    change: PropTypes.func,
    dispatch: PropTypes.func,
    editSong: PropTypes.func,
    user_role: PropTypes.string,
    handleSubmit: PropTypes.func,
    closePopup: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      is_youtube_opened: false,
      is_itunes_opened: false,
      is_spotify_opened: false
    };
  }

  componentDidMount() {
    const {initialize, song} = this.props;
    initialize({
      title: song.title,
      artist: song.artist && song.artist.name,
      time_play: song.time_play,
      scene_description: song.scene_description,
      youtube_id: song.youtube_id,
      itunes_id: song.itunes_id,
      spotify: song.spotify
    });
  }

  handleSubmit(data) {
    const { song, closePopup, editSong } = this.props;
    editSong(song, data).then(closePopup);
  }

  render() {
    const { is_spotify_opened, is_youtube_opened, is_itunes_opened } = this.state;
    const { change, dispatch, user_role, handleSubmit, song } = this.props; // eslint-disable-line
    return (
      <div className="song-edit">
        <form action="#" onSubmit={handleSubmit(::this.handleSubmit)}>
          {
            user_role === 'ADMIN' &&
            <Form.Group>
              <label>song id: ({song._id})</label>
            </Form.Group>
          }
          <Form.Input
            className=""
            label={"Name"}
            disabled={user_role !== 'ADMIN'}
            name={"title"}
            type={"text"}
          />
          <Form.Input
            className=""
            disabled={user_role !== 'ADMIN'}
            label={"Artist"}
            name={"artist"}
            type={"text"}
          />
          <Form.Input
            className=""
            label={"Time"}
            name={"time_play"}
            type={"text"}
          />
          <Form.Textarea
            label={"Scene"}
            name={"scene_description"}
          >
          </Form.Textarea>
          <Form.Input
            className=""
            label={"Youtube"}
            name={"youtube_id"}
            type={"text"}
            classNameGroup={"group-inline-block"}
          />
          <div className="search-id"><span onClick={()=>this.setState({is_youtube_opened: true})} className="search-id-text">search</span></div>
          <Form.Input
            className=""
            label={"iTunes"}
            name={"itunes_id"}
            type={"text"}
            classNameGroup={"group-inline-block"}
          />
          <div className="search-id"><span onClick={()=>this.setState({is_itunes_opened: true})} className="search-id-text">search</span></div>
          <Form.Input
            className=""
            label={"Spotify"}
            name={"spotify"}
            type={"text"}
            classNameGroup={"group-inline-block"}
          />
          <div className="search-id"><span onClick={()=>this.setState({is_spotify_opened: true})} className="search-id-text">search</span></div>
          <div className="form-btn__container">
            <Form.Button
              type="text"
              className="btn--success"
            >
              <span>Save</span>
            </Form.Button>
          </div>
        </form>

        <Popup
          is_opened={is_youtube_opened}
          handleClose={()=>this.setState({is_youtube_opened: false})}
          title="Search YouTube"
          modifier="full-width"
        >
          <EditPage.YoutubeLinkForm prefill={`${song.title} by ${song.artist && song.artist.name}`} handleEdit={(_, s)=>dispatch(change('youtube_id', s.youtube_id))} />
        </Popup>
        <Popup
          is_opened={is_itunes_opened}
          handleClose={()=>this.setState({is_itunes_opened: false})}
          title="Search iTunes"
          modifier="full-width"
        >
          <EditPage.iTunesLinkForm prefill={`${song.title} by ${song.artist && song.artist.name}`} handleEdit={(_, s)=>dispatch(change('itunes_id', s.itunes_id))}/>
        </Popup>

        <Popup
          is_opened={is_spotify_opened}
          handleClose={()=>this.setState({is_spotify_opened: false})}
          title="Search Spotify"
          modifier="full-width"
        >
          <EditPage.SpotifyLinkForm prefill={`${song.title} ${song.artist && song.artist.name}`} handleEdit={(_, s)=>dispatch(change('spotify', s.spotify))} />
        </Popup>
      </div>
    );
  }
}


