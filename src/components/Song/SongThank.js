import React, {Component, PropTypes} from 'react';
import * as thankActions from 'redux/modules/thanks';
import * as authMoadalActions from 'redux/modules/authModal';
import * as notificationsActions from 'redux/modules/notifications';

import { connect } from 'react-redux';

@connect(
  (state, props) => ({
    changed_song: state.thanks.songs[props.song._id],
    changed_user: state.thanks.users[props.song.contributer_user._id],
    user: state.auth.user
  }),
  {
    thank: thankActions.thankSong,
    openAuthModal: authMoadalActions.open,
    pushNotification: notificationsActions.push
  }
)

export default class SongThank extends Component {
  static propTypes = {
    song: PropTypes.object,
    thank: PropTypes.func,
    changed_song: PropTypes.object,
    changed_user: PropTypes.object,
    openAuthModal: PropTypes.func,
    pushNotification: PropTypes.func,
    user: PropTypes.object,

  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleThank(count_thanks, is_thanked) {
    const {pushNotification, song: { contributer_user, _id }, thank, openAuthModal, user } = this.props; // eslint-disable-line
    if (!user) {return openAuthModal('sign-in');}
    thank(_id, contributer_user._id, !is_thanked, count_thanks).then(()=>{
      if (!is_thanked) {pushNotification('thank_user', {user: contributer_user});}
    });

  }

  render() {
    const { changed_song, changed_user, song, song: { contributer_user }} = this.props;
    const { is_thanked } = changed_song || song;
    const { count_thanks } = changed_user || contributer_user;
    return (
    <div
      data-tooltip={is_thanked ? 'thanked!' : 'click to thank'}
      onClick={()=>this.handleThank(count_thanks, is_thanked)}
      className={'song-user' + (is_thanked ? ' is-active' : '')}
    >
      <i className="icon-font icon-font-check"></i>
      <span className="song-user__text">
        {contributer_user.username}
      </span>
    </div>
    );
  }
}
