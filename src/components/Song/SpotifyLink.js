import React, {Component, PropTypes} from 'react'; // eslint-disable-line

export default class SpotifyLink extends Component {
  static propTypes = {
    song: PropTypes.object
  }
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    const { song } = this.props;
    return (
      <div className="song__link-spotify">
        {song.spotify_uri && <div data-tooltip="Open in Spotify"><a href={song.spotify_uri}><i className="icon-font icon-font-spotify"></i><span>spotify</span></a></div>}
      </div>
    );
  }
}
