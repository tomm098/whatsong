import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Like } from 'components';
import { ScreenType } from 'elements';
import SongMobile from './SongMobile';
// import SongMore from './SongMore';
import {} from './Song.scss';
import {} from './SongArtist.scss';
import { timeInterval, fireEvent, urlGenerator, printDate } from 'utils/helpers';
import { Link } from 'react-router';
import SongLinks from './SongLinks';

export default class SongArtist extends Component {
  static propTypes = {
    song: PropTypes.object,
    play: PropTypes.func,
    pause: PropTypes.func,
    playing: PropTypes.bool,
    is_active: PropTypes.bool,
    handleSongVote: PropTypes.func,
    handleSceneVote: PropTypes.func,
    voting: PropTypes.bool,
    display_options: PropTypes.bool,
    handleLike: PropTypes.func
  }

  static defaultProps = {
    display_options: true
  }

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  playPause() {
    const { is_active, playing, play, pause, song } = this.props;
    if (!song.youtube_id && !song.preview_url) {return false;}
    if (is_active) {
      playing ? pause() : play();
    } else {
      play(song);
    }
  }

  handleMouseLeave() {
    fireEvent(window.document.body, 'click');
  }


  render() {
    const { song, is_active, playing, handleLike } = this.props;
    const disabled = !song.youtube_id && !song.preview_url;
    const time_interval = timeInterval((song.time_play || 0) * 6e4);
    const time_played = !song.time_play ? '' : `${time_interval.hours}:${('0' + time_interval.minutes).slice(-2)}`;
    return (
    (song || null ) &&
    <div className={`song song--artist ${is_active && playing ? ' is-playing' : ''}${disabled ? ' is-disabled' : ''}`}>
      <ScreenType.TabletMin>
        <div onMouseLeave={::this.handleMouseLeave} className="song__container-desktop">
          <div onClick={::this.playPause} className="song__btn-play-box">
            <div className="song__mask">
              <button className="btn btn--clear">
                <i className="icon-font icon-font-btn-play"></i>
                <i className="icon-font icon-font-btn-pause"></i>
              </button>
            </div>
              {song.spotifyImg64 && <img src={song.spotifyImg64} alt=""/>}
          </div>

          <div className="song__properties">
            <div className="song__top">
              <div className="song__title-check song_info">
                <div className="song__title">{song.title}</div>
                <div className="song__info">
                  {time_played && <div className="song__time">{time_played}</div>}
                  <div itemProp="description">{song.scene_description}</div>
                </div>
              </div>

              <div className="song__name-like">
                {song && song.from && song.from.type &&
                  <Link to={urlGenerator(song.from.type, song.from)}>
                    <div className="song__from">
                      {song.from && song.from.name && <p>{song.from.name}{song.from && song.from.type === 'tv_show' && <span> • S{song.season.season}E{song.episode.number}</span>}</p>}
                      {song.from && song.from.time_released && <div className="song__info-tv">{printDate(song.from.time_released)}</div>}
                    </div>
                  </Link>
                }
                <Like
                  onLike={()=>handleLike({name: song.title, by: song.artist.name})}
                  type="song"
                  id={song._id}
                  liked={song.is_favorited}
                  like_count={song.favourited}
                />

              </div>
              {/* <SongMore song={song} innerClassName="dropdown-menu dropdown-menu--more"/> */}
              <SongLinks song={song}/>
            </div>
          </div>
        </div>
      </ScreenType.TabletMin>

      <ScreenType.Mobile>
        <SongMobile
          playPause={::this.playPause}
          show_scene={true}
          is_artist={true}
          song={song}
          onLike={()=>handleLike({name: song.title, by: song.artist.name})}
        />
      </ScreenType.Mobile>
    </div>
    );
  }
}
