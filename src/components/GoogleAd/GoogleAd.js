import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';

@connect(
  state => ({
    user: state.auth.user
  }),
  {

  }
)


export default class GoogleAd extends Component {
  static propTypes = {
    slot: PropTypes.string.isRequired,
    display: PropTypes.string,
    height: PropTypes.string,
    width: PropTypes.string,
    format: PropTypes.string,
    className: PropTypes.string,
    user: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    if (!this.props.user) {
      (window.adsbygoogle = window.adsbygoogle || []).push({});
    }
  }

  render() {
    const { display = 'inline-block', height, width, slot, user, format, className, ...rest } = this.props;
    return (
      (!user || null) &&
      <ins
        {...rest}
        className={`adsbygoogle ${className}`}
        style={{display, height, width}}
        data-ad-client="ca-pub-6559493403759926"
        data-ad-format={format}
        data-ad-slot={slot}
      />
    );
  }
}
