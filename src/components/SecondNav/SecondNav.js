import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-scroll';

import { truncString } from 'utils/helpers';

import {} from './SecondNav.scss';

const ico_question = require('./questions-icon.png');
const ico_song_list = require('./song-list-icon.png');
const ico_soundtrack = require('./soundtrack-icon.png');
const ico_trailer_song = require('./trailer-song-icon.png');

const ico_question_selected = require('./questions-icon-selected.png');
const ico_song_list_selected = require('./song-list-icon-selected.png');
const ico_soundtrack_selected = require('./soundtrack-icon-selected.png');
const ico_trailer_song_selected = require('./trailer-song-icon-selected.png');

@connect(
  state => ({
    posts: state.discussion.discussions,
  }),
  null
)

class SecondNav extends React.Component {
  static propTypes = {
    posts: PropTypes.array,
    info: PropTypes.object,
  }

  constructor(props) {
    super(props);
    this.state = {
      top_position: 56,
      top_margin: 506,
    };
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
    window.addEventListener('resize', this.updateDimensions);
    this.updateDimensions();
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
    window.removeEventListener('resize', this.updateDimensions);
  }

  updateDimensions = () => {
    let top_margin = 506;
    if (window.innerWidth <= 768 ) {
      top_margin = 233;
    } else if (window.innerWidth <= 1024 ) {
      top_margin = 400;
    }

    this.setState({
      top_margin
    }, () => {
      this.handleScroll();
    });
  }

  handleScroll = () => {
    const windowScrollTop = window.pageYOffset;

    if (windowScrollTop >= this.state.top_margin) {
      let top_position = 56;
      if (window.innerWidth <= 768 ) {
        top_position = 44;
      }
      this.setState({
        top_position
      });
    } else {
      this.setState({
        top_position: this.state.top_margin - windowScrollTop,
      });
    }

    const link = document.getElementsByClassName('active');
    if (link.length > 0) {
      this.sec_nav.scrollLeft = link[0].offsetLeft - 70;
    }
  }

  render() {
    const { info, posts } = this.props;
    const { active_item_img } = this.state;
    return (
      <div className="sec-nav" ref={sec_nav=>this.sec_nav = sec_nav} style={{ top: this.state.top_position}}>
        <div className="sec-nav-cnt">
          <div>{truncString(info.title, 20)}</div>
          <ul>
            <Link to="section-1" spy={true} smooth={true} duration={500}><img src={ico_song_list} alt="Credited Songs"/><img src={ico_song_list_selected} />Credited Songs</Link>
            <Link to="section-2" spy={true} smooth={true} duration={500}><img src={ico_soundtrack} alt="Official Soundtrack"/><img src={ico_soundtrack_selected} />Official Soundtrack</Link>
            <Link to="section-3" spy={true} smooth={true} duration={500}><img src={ico_trailer_song} alt="Trailer Music"/><img src={ico_trailer_song_selected} />Trailer Music</Link>
            <Link to="section-4" spy={true} smooth={true} duration={500}><img src={ico_question} alt="Questions"/><img src={ico_question_selected} />Comments ({posts.length})</Link>
          </ul>
          <div>
            {active_item_img &&
              <img
                src={active_item_img}
              />
            }
          </div>
        </div>
      </div>
    );
  }
}

export default SecondNav;
