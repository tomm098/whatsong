import React from 'react';
import {} from './InfoCard.scss';
const InfoCard = ({title, children, modifier = ''}) =>
  <div className={`info-card ${modifier ? ('info-card--' + modifier) : ''}`}>
    <div className="info-card__title">{title}</div>
    <div className="info-card__content">{children}</div>
  </div>;
export default InfoCard;
