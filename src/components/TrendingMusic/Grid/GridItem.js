import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { connect } from 'react-redux';
import {} from './GridMusic.scss';
import { Like, Headphones } from 'components';
const image1 = require('../../../images/itunes.png');
const image2 = require('../../../images/amazon.png');
import { Link } from 'react-router';
import { urlGenerator } from 'utils/helpers';
import { getExternalLink, getAmazonLink } from '../../../utils/helpers';

@connect(
  state => ({
    geolocation: state.auth.location,
  }),
  {}
)
export default class GridItem extends Component {

  static propTypes = {
    geolocation: PropTypes.object,
    song: PropTypes.object,
    play: PropTypes.func,
    pause: PropTypes.func,
    playing: PropTypes.bool,
    is_active: PropTypes.bool,
    active_time: PropTypes.object,
    pushNotification: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  playPause() {
    const { is_active, playing, play, pause, song } = this.props;
    if (!song.youtube_id && !song.preview_url) {return false;}
    if (is_active) {
      playing ? pause() : play();
    } else {
      play(song);
    }
  }

  render() {
    const { song, is_active, playing, active_time, pushNotification, geolocation } = this.props;

    const disabled = !song.youtube_id && !song.preview_url;

    return (
      (song || null ) &&
      <div className={`grid-music__item${is_active && playing ? ' is-playing' : ''}${disabled ? ' is-disabled' : ''}`}>
        <div style={{backgroundImage: song.banner ? `url(${song.banner})` : ''}} className="grid-music__wrapp">

          <div onClick={::this.playPause} className={`grid-music__play${is_active && playing ? ' is-active' : ''}`}>
            <button className="btn btn--clear">
              <i className="icon-font icon-font-btn-play"></i>
              <i className="icon-font icon-font-btn-pause"></i>
            </button>
          </div>

          <div className="grid-music__properties">
            {
              song.from && <Link to={urlGenerator(song.from.type, song.from)} className="grid-music__name">{song.from.name}</Link>
            }
            <div className="grid-music__bottom">
              <Headphones
                count={song[active_time.counter]}
              />
              <Like
                type="song"
                id={song._id}
                liked={song.is_favorited}
                like_count={song.favourited}
                onLike={()=>pushNotification('favorite_song', {by: song.artist && song.artist.name, name: song.title})}
              />
              {
                song.spotify_uri &&
                <div className="grid-music__spotify">
                  <a href={song.spotify_uri}><i className="icon-font icon-font-spotify"></i></a>
                </div>
              }
            </div>
          </div>
        </div>
        <div className="grid-music__information">
          <div className="grid-music__album">{song.title}</div>
          {song.artist && <div className="grid-music__singer">{song.artist.name}</div>}
        </div>
        <div className="grid-music__itunes-amazon">
          {
            song.itunes_url &&
            <a
              className="grid-music__itunes"
              target="_blank"
              rel="nofollow"
              href={getExternalLink(song.itunes_url)}
            >
              <img src={image1} alt="get on itunes"/>
            </a>
          }
          {
            song.title &&
            <a
              className="grid-music__amazon"
              target="_blank"
              rel="nofollow"
              href={getExternalLink(getAmazonLink(geolocation && geolocation.country, encodeURIComponent(song.title + (song.artist.name ? ' by ' + song.artist.name : ''))))}
            >
              <img src={image2} alt="get on amazone"/>
            </a>
          }
        </div>
        <div className="grid-music__bottom grid-music__bottom--mobile">
          <Headphones />
          <Like/>
          <div className="grid-music__spotify">
            <a href="#"><i className="icon-font icon-font-spotify"></i></a>
          </div>
        </div>
      </div>
    );
  }
}
