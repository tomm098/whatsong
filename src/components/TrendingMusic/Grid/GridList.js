import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import GridItem from './GridItem';
import {} from './GridMusic.scss';
import { connect } from 'react-redux';
import * as playerActions from 'redux/modules/player';
import * as notificationsActions from 'redux/modules/notifications';
@connect(
  state => ({
    active_song: state.player.active_song,
    playing: state.player.playing
  }),
  {
    play: playerActions.play,
    pause: playerActions.pause,
    pushNotification: notificationsActions.push
  }
)

export default class GridList extends Component {
  static propTypes = {
    list: PropTypes.array,
    active_song: PropTypes.object,
    playing: PropTypes.bool,
    play: PropTypes.func,
    pause: PropTypes.func,
    pushNotification: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  getSpacers(index) {
    const res = [];
    for (let i = 2; i <= 5; i++) {
      if (index % i === 0) {
        res.push(
          <li key={`spacer_${i}_${index}`} className={`grid-music__li grid-music__li--title${i}`}>
            <div className="grid-music__info">
              <strong>showing</strong> <span>{index + 1}{index === 0 ? 'st' : 'th'}</span> to <span>{index + i}th</span>
            </div>
          </li>
        );
      }
    }
    return res;
  }

  render() {
    const { list, play, pause, playing, active_song, ...rest } = this.props;
    return (
      (list || null) &&
      <div className="grid-music">
        <ul className="grid-music__list">

          {
            list.reduce((p, c, i)=>
              [
                ...p,
                ...this.getSpacers(i),
                <li key={i} className="grid-music__li">
                  <GridItem
                    song={c}
                    {...{
                      ...rest,
                      play: (song)=>play(song, list),
                      pause,
                      playing,
                      is_active: active_song && c._id === active_song._id
                    }}
                  />
                </li>
              ],
            [])
          }

        </ul>
      </div>
    );
  }
}
