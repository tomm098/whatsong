import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Dropdown } from 'elements';
import {} from './TrendingMusicFilter.scss';

export default class TrendingMusicFilter extends Component {
  static propTypes = {
    type_variants: PropTypes.array,
    time_variants: PropTypes.array,
    active_type: PropTypes.object,
    active_time: PropTypes.object,
    setTime: PropTypes.func,
    setType: PropTypes.func
  }
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    const { active_time, active_type, setTime, setType, type_variants, time_variants } = this.props;
    return (
      <div className="select-trending-music">
        <div className="select-trending-music__left">
          <Dropdown
            self_closing={true}
            trigger={
              <div className="first-text">{active_type.label}</div>
              }
          >
            <ul className="dropdown-menu dropdown-menu--select">
              {
                type_variants.map((c, i)=>
                  <li key={i} onClick={()=>setType(c.name)}>
                    <span>{c.label}</span>
                  </li>
                )
              }
            </ul>
          </Dropdown>
        </div>

        <div className="select-trending-music__right">
          <Dropdown
            self_closing={true}
            trigger={
              <div className="first-text">{active_time.label}</div>
              }
          >
            <ul className="dropdown-menu dropdown-menu--select">
              {
                time_variants.map((c, i)=>
                  <li key={i} onClick={()=>setTime(c.name)}>
                    <span>{c.label}</span>
                  </li>
                )
              }
            </ul>
          </Dropdown>
        </div>
      </div>
    );
  }
}
