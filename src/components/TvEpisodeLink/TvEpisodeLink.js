import React from 'react';
import {} from './TvEpisodeLink.scss';
const TvEpisodeLink = ({season = {}, episode = {}, modifier=''}) =>// eslint-disable-line
  <div itemProp="description" className={`tv-episode-link ${modifier ? 'tv-episode-link--' + modifier : ''}`}>
    {
      (typeof season.season === 'number' ? `s${season.season < 10 ? '0' : ''}${season.season}` : '') +
      (typeof episode.number === 'number' ? `e${episode.number < 10 ? '0' : ''}${episode.number}` : '') +
      (episode.name ? (' - ' + episode.name) : '')
    }
  </div>;
export default TvEpisodeLink;
