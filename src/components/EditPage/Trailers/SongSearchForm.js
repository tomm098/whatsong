import React, {Component, PropTypes} from 'react';
import { reduxForm } from 'redux-form'; // eslint-disable-line
import { Form } from 'elements';
import memoize from 'lru-memoize';
import { createValidator, required } from 'utils/validation';
import { connect } from 'react-redux';
import * as iTunesSearchActions from 'redux/modules/iTunesSearch';
import * as editTrailerActions from 'redux/modules/editTrailers';

@connect(
  state => ({
    search_results: state.iTunesSearch.data
  }),
  {
    search: iTunesSearchActions.search,
    clear: iTunesSearchActions.clear,
    addSong: editTrailerActions.addSong
  }
)

@reduxForm({
  form: 'EditTrailersSongSearchForm',
  validate: memoize(10)(createValidator({
    search: [required]
  }))
})

export default class EditTrailersSongSearchForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func,
    closePopup: PropTypes.func,
    handleAdd: PropTypes.func,
    search_results: PropTypes.array,
    search: PropTypes.func,
    clear: PropTypes.func,
    addSong: PropTypes.func,
    trailer_id: PropTypes.any
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentWillUnmount() {
    this.props.clear();
  }

  handleSubmit(data) {
    const { search } = this.props;
    search(data.search, 10);
  }

  handleAdd(itunes_id) {
    const { addSong, trailer_id, closePopup } = this.props;
    addSong(trailer_id, itunes_id).then(()=>closePopup());
  }

  render() {
    const { handleSubmit, search_results } = this.props;
    return (
      <div className="search-form-youtube">
        <form action="#" onSubmit={handleSubmit(::this.handleSubmit)} >
          <Form.Input
            name="search"
            type="text"
            label={"Search"}
            classNameGroup={"group-inline-block"}
          />
          <div className="search-button">
            <Form.Button
              type="submit"
              className="btn--success"
            >
              <span>Search</span>
            </Form.Button>
          </div>
          <ul className="form-list">
            {
              search_results.map((c, i)=>
                <li key={i}>
                  <div className="info-wrapp">
                    <button
                      onClick={e=>{
                        e.preventDefault();
                        this.handleAdd(c.itunes_id);
                      }}
                      className="btn btn--clear btn--plus"
                    >
                      +
                    </button>
                    <div className="info-text">
                      <div>{c.title} ({c.album && c.album.title})</div>
                      {c.artist && c.artist.name && <div>by: {c.artist.name}</div>}
                    </div>
                  </div>
                </li>
              )
            }
          </ul>
        </form>
      </div>
    );
  }
}
