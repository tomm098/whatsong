import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './SavedTrailers.scss';
import TrailerItem from './SavedTrailerItem';

export default class SavedTrailers extends Component {
  static propTypes = {
    trailers: PropTypes.array,
    handleRemove: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { trailers, handleRemove } = this.props;
    return (
      <div className="saved-trailers">
        <div className="saved-trailers__title">
          SAVED TRAILERS
        </div>
        {
          trailers.map((c, i)=>
            <TrailerItem item={c} key={i} handleRemove={handleRemove}/>
          )
        }
      </div>
    );
  }
}
