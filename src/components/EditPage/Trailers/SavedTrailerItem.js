import React, {Component, PropTypes} from 'react';
import ReactPlayer from 'react-player';
import { Popup } from 'elements';
import SongSearchForm from './SongSearchForm';


export default class EditPageSavedTrailerItem extends Component {
  static propTypes = {
    item: PropTypes.object,
    handleRemove: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      is_search_form_opened: false
    };
  }

  render() {
    const { item, handleRemove } = this.props;
    const { is_search_form_opened } = this.state;
    return (
      <div className="saved-trailers__wrapp">
        <div className="saved-trailers__video-wrapp">
          <div className="saved-trailers__video-title">YOUTUBE</div>
          <div className="saved-trailers__video">
            <ReactPlayer
              url={`https://www.youtube.com/watch?v=${item.youtube_id}&origin=https://www.what-song.com`}
              height="100%"
              width="100%"
            />
          </div>
          <div className="saved-trailers__video-delete">
            <button onClick={()=>handleRemove(item._id)} className="btn btn--clear">delete trailer</button>
          </div>
        </div>
        <div className="saved-trailers__info">
          <div className="saved-trailers__video-title">Title</div>
          <div className="saved-trailers__name">{item.title}</div>
          <div className="saved-trailers__video-title">MUSIC</div>

          {
            item.song_list.map((c, i)=>
              <div key={i} className="saved-trailers__name-album">
                <p>{c.title}</p>
                { c.artist && <p>by {c.artist.name}</p> }
              </div>
            )
          }

          <div className="saved-trailers__more">
          <button
            onClick={e=>{e.preventDefault(); this.setState({is_search_form_opened: true});}}
            className="btn btn--clear"
          >
            ADD MORE MUSIC
          </button>
          </div>
        </div>

        <Popup
          is_opened={is_search_form_opened}
          handleClose={()=>this.setState({is_search_form_opened: false})}
          title="Search iTunes"
        >
          <SongSearchForm trailer_id={item._id}/>
        </Popup>
      </div>
    );
  }
}
