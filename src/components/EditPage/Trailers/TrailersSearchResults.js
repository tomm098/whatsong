import React from 'react';
import YoutubeItem from './YoutubeSearchResultItem';

const EditPageTrailerSearchResults = ({ results, handleAdd })=>
  <div>
    <ul className="form-list" >
      {
        results.map((c, i)=>
          <YoutubeItem handleAdd={handleAdd} item={c} key={i}/>
        )
      }
    </ul>
  </div>;
export default EditPageTrailerSearchResults;
