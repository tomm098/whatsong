import React from 'react';
import ReactPlayer from 'react-player';

const EditPageYoutybeLinkFormItem = ({ item, handleAdd }) =>
  <li>
    <div className="info-wrapp">
      <button onClick={e=>{e.preventDefault(); handleAdd(item.id.videoId);}} className="btn btn--clear btn--plus">+</button>
      <div className="embed">
        <ReactPlayer
          url={`https://www.youtube.com/watch?v=${item.id.videoId}&origin=https://www.what-song.com`}
          height="100%"
          width="100%"
        />
      </div>
      <div className="info-text">
        <span>{item.snippet.title}</span>
      </div>
    </div>
  </li>;
export default EditPageYoutybeLinkFormItem;
