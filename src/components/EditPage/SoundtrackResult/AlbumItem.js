import React, {Component, PropTypes} from 'react';

export default class EditPageAlbumItem extends Component {
  static propTypes = {
    item: PropTypes.object,
    loadAlbumTracks: PropTypes.func,
    handleImport: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      is_tracklist_visible: false
    };
  }

  toggleTracklist(e) {
    e.preventDefault();
    const { item, loadAlbumTracks } = this.props;
    const show = ()=> this.setState({is_tracklist_visible: true});
    const hide = ()=> this.setState({is_tracklist_visible: false});
    if (this.state.is_tracklist_visible) {
      hide();
    } else {
      if (Array.isArray(item.tracklist)) {
        show();
      } else {
        loadAlbumTracks(item.collectionId).then(show);
      }
    }
  }

  render() {
    const { item, handleImport } = this.props;
    const { is_tracklist_visible } = this.state;
    return (
      <div className="soundtrack-result__table-row-wrapper">
        <div className="soundtrack-result__table-row">
          <div className="soundtrack-result__table-cell soundtrack-result__table-cell--first">
            <div className="soundtrack-result__title-check">
              <div className="soundtrack-result__name">{item.collectionName}</div>
              <div className="soundtrack-result__artist">{item.artistName}</div>
            </div>
          </div>
          <div className="soundtrack-result__table-cell">
            <div className="soundtrack-result__artist">{item.artistName}</div>
          </div>
          <div className="soundtrack-result__table-cell">
            <div className="soundtrack-result__youtube">
              {item.trackCount}&nbsp; <a onClick={::this.toggleTracklist}>{is_tracklist_visible ? 'HIDE' : 'VIEW'} SONGS</a>
            </div>
          </div>
          <div className="soundtrack-result__table-cell soundtrack-result__table-cell--last">
            <button
              onClick={()=>handleImport(item.collectionId)}
              className="btn btn--clear add-shortlist"
            >
              IMPORT
            </button>
          </div>
        </div>
        {
          is_tracklist_visible && Array.isArray(item.tracklist) &&
          <div className="soundtrack-result__table-row">
            <div colSpan="3" className="soundtrack-result__table-cell soundtrack-result__table-cell--dropp">
              <div className="soundtrack-result__dropp-table">
               {
                 item.tracklist.map((c, i)=>
                   <div className="soundtrack-result__dropp-table-row" key={i}>
                     <div className="soundtrack-result__dropp-table-cell"><span>{c.trackNumber}. </span> {c.trackName}</div>
                     <div className="soundtrack-result__dropp-table-cell">{c.artistName}</div>
                   </div>
                 )
               }
              </div>
            </div>
          </div>
        }
      </div>
    );
  }
}
