import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './SoundtrackResult.scss';
import AlbumItem from './AlbumItem';

export default class SoundtrackResult extends Component {
  static propTypes = {
    results: PropTypes.array,
    loadAlbumTracks: PropTypes.func,
    handleImport: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { results, loadAlbumTracks, handleImport } = this.props;
    return (
      <div className="soundtrack-result">
        <div className="soundtrack-result__title">Search results</div>
        <div className="soundtrack-result__table">
          <div className="soundtrack-result__table-row soundtrack-result__table-row--th">
            <div className="soundtrack-result__table-cell soundtrack-result__table-cell--first">
            TITLE
            </div>
            <div className="soundtrack-result__table-cell">
            ARTIST
            </div>
            <div className="soundtrack-result__table-cell">
            NO. OF SONGS
            </div>
            <div className="soundtrack-result__table-cell soundtrack-result__table-cell--last"></div>
          </div>
          {
            results.map((c, i)=>
              <AlbumItem
                key={i}
                item={c}
                loadAlbumTracks={loadAlbumTracks}
                handleImport={handleImport}
              />
            )
          }
        </div>
      </div>
    );
  }
}
