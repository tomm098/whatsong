import React, {Component, PropTypes} from 'react';
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import { Form } from 'elements';

import memoize from 'lru-memoize';
import { createValidator, minLength, integer, required } from 'utils/validation';

@reduxForm({
  form: 'EditPageSceneAddForm',
  validate: memoize(10)(createValidator({
    scene: [minLength(5), required],
    time: [integer]
  }))
})

export default class EditPageSceneAddForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func,
    closePopup: PropTypes.func,
    song_id: PropTypes.string,
    handleEdit: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleSubmit(data) {
    const { closePopup, handleEdit, song_id } = this.props;
    handleEdit(song_id, {
      scene_description: data.scene,
      time_play: data.time
    });
    closePopup();
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="add-form-time">
        <form action="#" onSubmit={handleSubmit(::this.handleSubmit)}>
          <Form.Input
            name="time"
            type="text"
            label={"Add Time"}
            caption={'minutes into the movie the song appears'}
          />
          <Form.Input
            classNameGroup="input-time"
            name="scene"
            type="text"
            label={"Add Scene"}
          />
          <div className="form-btn__container">
            <Form.Button
              type="submit"
              className="btn--success"
            >
              <span>Save</span>
            </Form.Button>
          </div>
        </form>
      </div>
    );
  }
}
