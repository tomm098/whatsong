import React, {Component, PropTypes} from 'react';
import { Popup } from 'elements';
import SceneAddForm from './SceneAddForm';
import { timeInterval } from 'utils/helpers';
import YoutubeLinkForm from './YoutubeLinkForm';
import SpotifyLinkForm from './SpotifyLinkForm';

export default class EditPageShortlistSong extends Component {
  static propTypes = {
    item: PropTypes.object,
    active: PropTypes.bool,
    playing: PropTypes.bool,
    handlePlay: PropTypes.func,
    handlePause: PropTypes.func,
    handleEdit: PropTypes.func,
    handleRemove: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      is_scene_opened: false,
      is_youtube_opened: false,
      is_spotify_opened: false
    };
  }

  render() {
    const { item, active, playing, handlePlay, handlePause, handleEdit, handleRemove } = this.props;
    const { is_scene_opened, is_youtube_opened, is_spotify_opened } = this.state;

    const time_interval = timeInterval((item.time_play || 0) * 6e4);
    const time_played = !item.time_play ? '' : `${time_interval.hours}:${('0' + time_interval.minutes).slice(-2)}`;

    const prefill_youtube = `${item.title}${item.artist && item.artist.name ? ' by ' + item.artist.name : ''}`;
    const prefill_spotify = `${item.title}${item.artist && item.artist.name ? ' ' + item.artist.name : ''}`;

    const disabled = !item.youtube_id && !item.preview_url;

    return (
      <div className={`edit-song__table-row${disabled ? ' is-disabled' : ''}`}>
        <div className="edit-song__table-cell">
          <div className="edit-song__btn-play-box">
            <button
              className={'btn btn--clear' + (active && playing ? ' is-playing' : '')}
              onClick={()=>{
                if (disabled) {return false;}
                if (active) {
                  playing ? handlePause() : handlePlay();
                } else {
                  handlePlay(item);
                }
              }}
            >
              <i className="icon-font icon-font-btn-play"></i>
              <i className="icon-font icon-font-btn-pause"></i>
            </button>
          </div>
        </div>
        <div className="edit-song__table-cell">
          <div className="edit-song__title-check">
            <div className="edit-song__title">{item.title}</div>
            <div className="edit-song__artist">{item.artist && item.artist.name}</div>
          </div>
          <div onClick={()=>this.setState({is_scene_opened: true})} className="edit-song__middle">
            {
              item.scene_description || item.time_play !== undefined ?
                <div>
                  <div className="edit-song__time">{ time_played }</div>
                  <div className="edit-song__album">{ item.scene_description }</div>
                </div>
                :
                <div className="edit-song__add-scene">ADD SCENE DESCRIPTION</div>
            }
          </div>
        </div>
        <div className="edit-song__table-cell">
          <div className="edit-song__artist">{item.artist && item.artist.name}</div>
        </div>

        <div onClick={()=>this.setState({is_youtube_opened: true})} className="edit-song__table-cell">
          {
            item.youtube_id ?
              <div className="edit-song__youtube edit-song__youtube">LINKED</div>
            :
              <div className="edit-song__youtube edit-song__youtube--un-active">UNLINKED</div>
          }
        </div>

        <div onClick={()=>this.setState({is_spotify_opened: true})} className="edit-song__table-cell">
          {
            item.spotify ?
              <div className="edit-song__spotify edit-song__spotify">LINKED</div>
            :
              <div className="edit-song__spotify edit-song__spotify--un-active">UNLINKED</div>
          }
        </div>

        <div className="edit-song__table-cell">
          <div className="edit-song__delete">
            <button onClick={()=>handleRemove(item._id)} className="btn btn--clear"><i className="icon-font icon-font-close2"></i></button>
          </div>
        </div>

        <Popup
          is_opened={is_scene_opened}
          handleClose={()=>this.setState({is_scene_opened: false})}
          title=""
        >
          <SceneAddForm handleEdit={handleEdit} song_id={item._id}/>
        </Popup>

        <Popup
          is_opened={is_youtube_opened}
          handleClose={()=>this.setState({is_youtube_opened: false})}
          title="Search YouTube"
        >
          <YoutubeLinkForm prefill={prefill_youtube} handleEdit={handleEdit} song_id={item._id}/>
        </Popup>

        <Popup
          is_opened={is_spotify_opened}
          handleClose={()=>this.setState({is_spotify_opened: false})}
          title="Search Spotify"
        >
          <SpotifyLinkForm prefill={prefill_spotify} handleEdit={handleEdit} song_id={item._id}/>
        </Popup>
      </div>
    );
  }
}
