import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './EditSong.scss';
import {} from './SearchFormYoutube.scss';
import SongItem from './SongItem';
import * as playerActions from 'redux/modules/player';
import * as editPageActions from 'redux/modules/editPage';
import { connect } from 'react-redux';

@connect(
  state => ({
    song: state.player.active_song,
    playing: state.player.playing
  }),
  {
    play: playerActions.play,
    pause: playerActions.pause,
    editShortlist: editPageActions.editShortlist,
    removeFromShortlist: editPageActions.removeFromShortlist
  }
)

export default class EditPageShortlist extends Component {
  static propTypes = {
    list: PropTypes.array,
    song: PropTypes.object,
    playing: PropTypes.bool,
    play: PropTypes.func,
    pause: PropTypes.func,
    editShortlist: PropTypes.func,
    removeFromShortlist: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    const { list = [], song, playing, play, pause, editShortlist, removeFromShortlist } = this.props;
    return (
      <div className="edit-song">
        <div className="edit-song__table">
          <div className="edit-song__table-row edit-song__table-row--th">
            <div className="edit-song__table-cell"></div>
            <div className="edit-song__table-cell">
            TITLE
            </div>
            <div className="edit-song__table-cell">
            ARTIST
            </div>
            <div className="edit-song__table-cell">
            YOUTUBE
            </div>
            <div className="edit-song__table-cell">
            SPOTIFY
            </div>
            <div className="edit-song__table-cell"></div>
          </div>
          {
            list.map((c, i) =>
              <SongItem
                key={i}
                item={c}
                active={song && (song._id === c._id)}
                playing={playing}
                handlePlay={play}
                handlePause={pause}
                handleEdit={editShortlist}
                handleRemove={removeFromShortlist}
              />
            )
          }
        </div>
      </div>
    );
  }
}
