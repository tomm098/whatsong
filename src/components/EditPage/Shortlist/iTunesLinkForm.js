import React, {Component, PropTypes} from 'react';
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import { Form } from 'elements';
import memoize from 'lru-memoize';
import { createValidator, required } from 'utils/validation';
import { connect } from 'react-redux';
import * as iTunesSearchSearchActions from 'redux/modules/iTunesSearch';
import YoutubeLinkItem from './iTunesLinkItem';

@connect(
  state => ({
    search_results: state.iTunesSearch.data
  }),
  {
    search: iTunesSearchSearchActions.search,
    clear: iTunesSearchSearchActions.clear
  }
)

@reduxForm({
  form: 'EditPageiTunesLinkForm',
  validate: memoize(10)(createValidator({
    search: [required]
  }))
})

export default class EditPageiTunesLinkForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func,
    closePopup: PropTypes.func,
    song_id: PropTypes.string,
    handleEdit: PropTypes.func,
    search_results: PropTypes.array,
    search: PropTypes.func,
    clear: PropTypes.func,
    prefill: PropTypes.string,
    initialize: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    const { initialize, prefill, search } = this.props;
    if (prefill) {
      initialize({search: prefill});
      search(prefill, 3);
    }
  }

  componentWillUnmount() {
    this.props.clear();
  }

  handleSubmit(data) {
    const { search } = this.props;
    search(data.search, 3);
  }

  handleLink(it_id, it_url) {
    const { handleEdit, closePopup, song_id } = this.props;
    if (!it_id) {return false;}
    handleEdit(song_id, {itunes_id: it_id, itunes_url: it_url});
    closePopup();
  }

  render() {
    const { handleSubmit, search_results } = this.props;
    return (
      <div className="search-form-youtube">
        <form action="#" onSubmit={handleSubmit(::this.handleSubmit)} >
          <Form.Input
            name="search"
            type="text"
            label={"Search"}
            classNameGroup={"group-inline-block"}
          />
          <div className="search-button">
            <Form.Button
              type="submit"
              className="btn--success"
            >
              <span>Search</span>
            </Form.Button>
          </div>
          <ul className="form-list">
            {
              search_results.map((c, i)=>
                <YoutubeLinkItem handleLink={::this.handleLink} item={c} key={i}/>
              )
            }
          </ul>
        </form>
      </div>
    );
  }
}
