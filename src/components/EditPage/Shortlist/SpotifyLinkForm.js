import React, {Component, PropTypes} from 'react';
import { reduxForm } from 'redux-form'; // eslint-disable-line
import { Form } from 'elements';
import memoize from 'lru-memoize';
import { createValidator, required } from 'utils/validation';
import { connect } from 'react-redux';
import * as spotifySearchActions from 'redux/modules/spotifySearch';

@connect(
  state => ({
    search_results: state.spotifySearch.data
  }),
  {
    search: spotifySearchActions.search,
    clear: spotifySearchActions.clear
  }
)

@reduxForm({
  form: 'EditPageSpotifyLinkForm',
  validate: memoize(10)(createValidator({
    search: [required]
  }))
})

export default class EditPageSpotifyLinkForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func,
    closePopup: PropTypes.func,
    song_id: PropTypes.string,
    handleEdit: PropTypes.func,
    search_results: PropTypes.array,
    search: PropTypes.func,
    clear: PropTypes.func,
    prefill: PropTypes.string,
    initialize: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    const { initialize, prefill, search } = this.props;
    if (prefill) {
      initialize({search: prefill});
      search(prefill, 3);
    }
  }

  componentWillUnmount() {
    this.props.clear();
  }

  handleSubmit(data) {
    const { search } = this.props;
    search(data.search, 10);
  }

  handleLink(item) {
    const { handleEdit, closePopup, song_id } = this.props;
    if (!item.id) {return false;}
    handleEdit(song_id, {
      spotify: item.id,
      spotifyAlbumId: item.album && item.album.id,
      spotifyPopularity: item.popularity,
      spotifyPreviewUrl: item.preview_url,
      spotify_uri: item.uri
    });
    closePopup();
  }

  render() {
    const { handleSubmit, search_results } = this.props;
    return (
      <div className="search-form-youtube">
        <form action="#" onSubmit={handleSubmit(::this.handleSubmit)} >
          <Form.Input
            name="search"
            type="text"
            label={"Search"}
            classNameGroup={"group-inline-block"}
          />
          <div className="search-button">
            <Form.Button
              type="submit"
              className="btn--success"
            >
              <span>Search</span>
            </Form.Button>
          </div>
          <ul className="form-list">
            {
              search_results.map((c, i)=>
                <li key={i}>
                  <div className="info-wrapp">
                    <button onClick={e=>{e.preventDefault(); this.handleLink(c);}} className="btn btn--clear btn--plus">+</button>
                    <div className="info-text">
                      <div>{c.name} ({c.album && c.album.name})</div>
                      {Array.isArray(c.artists) && <div>by: {c.artists.map(a=>a.name).join(', ')}</div>}
                    </div>
                  </div>
                </li>
              )
            }
          </ul>
        </form>
      </div>
    );
  }
}
