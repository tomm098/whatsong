import React from 'react';

const EditPageiTunesLinkFormItem = ({ item, handleLink }) =>
  <li>
    <div className="info-wrapp">
      <button onClick={e=>{e.preventDefault(); handleLink(item.itunes_id, item.itunes_url);}} className="btn btn--clear btn--plus">+</button>
      <div className="info-text">
        <span>{item.title} by {item.artist.name} ({item.album.title})</span>
      </div>
    </div>
  </li>;
export default EditPageiTunesLinkFormItem;
