import React, {Component, PropTypes} from 'react';
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import { Form } from 'elements';
import memoize from 'lru-memoize';
import { createValidator, required } from 'utils/validation';
import { connect } from 'react-redux';
import * as youtubeSearchActions from 'redux/modules/youtubeSearch';
import YoutubeLinkItem from './YoutubeLinkItem';

@connect(
  state => ({
    search_results: state.youtubeSearch.data
  }),
  {
    search: youtubeSearchActions.search,
    clear: youtubeSearchActions.clear
  }
)

@reduxForm({
  form: 'EditPageYoutubeLinkForm',
  validate: memoize(10)(createValidator({
    search: [required]
  }))
})

export default class EditPageYoutubeLinkForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func,
    closePopup: PropTypes.func,
    song_id: PropTypes.string,
    handleEdit: PropTypes.func,
    search_results: PropTypes.array,
    search: PropTypes.func,
    clear: PropTypes.func,
    prefill: PropTypes.string,
    initialize: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    const { initialize, prefill, search } = this.props;
    if (prefill) {
      initialize({search: prefill});
      search(prefill, 3);
    }
  }

  componentWillUnmount() {
    this.props.clear();
  }

  handleSubmit(data) {
    const { search } = this.props;
    search(data.search, 3);
  }

  handleLink(yt_id) {
    const { handleEdit, closePopup, song_id } = this.props;
    if (!yt_id) {return false;}
    handleEdit(song_id, {youtube_id: yt_id});
    closePopup();
  }

  render() {
    const { handleSubmit, search_results } = this.props;
    return (
      <div className="search-form-youtube">
        <form action="#" onSubmit={handleSubmit(::this.handleSubmit)} >
          <Form.Input
            name="search"
            type="text"
            label={"Search"}
            classNameGroup={"group-inline-block"}
          />
          <div className="search-button">
            <Form.Button
              type="submit"
              className="btn--success"
            >
              <span>Search</span>
            </Form.Button>
          </div>
          <ul className="form-list">
            {
              search_results.map((c, i)=>
                <YoutubeLinkItem handleLink={::this.handleLink} item={c} key={i}/>
              )
            }
          </ul>
        </form>
      </div>
    );
  }
}
