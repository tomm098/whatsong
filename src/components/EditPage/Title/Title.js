import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './EditTitle.scss';
export default class EditPageTitle extends Component {
  static propTypes = {
    title: PropTypes.string,
    titleInfo: PropTypes.string
  }
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    const { title, titleInfo } = this.props;
    return (
      <div className="edit-title">
        <div className="edit-title__title-wrapp">
          <h1>{title}</h1>
          <div className="edit-title__info"><i className="icon-font icon-font-info">i</i><span>{titleInfo}</span></div>
        </div>
      </div>
    );
  }
}
