import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Link } from 'react-router';
import {} from './Navbar.scss';

export default class EditPageNavbar extends Component {
  static propTypes = {
    links: PropTypes.array
  }

  static contextTypes = {
    base_url: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    const { base_url } = this.context;
    const { links } = this.props;
    return (
      <div className="">
        <ul>
          {
            links.map((c, i)=>
              <li key={i}><Link activeClassName="is-active" to={base_url + c.link}>{c.title}</Link></li>
            )
          }
        </ul>
      </div>
    );
  }
}
