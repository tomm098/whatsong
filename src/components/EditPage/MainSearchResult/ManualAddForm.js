import React, {Component, PropTypes} from 'react';
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import { Form } from 'elements';
import { stringHashCode } from 'utils/helpers';

import memoize from 'lru-memoize';
import { createValidator, required, minLength } from 'utils/validation';

@reduxForm({
  form: 'addSongManuallyForm',
  validate: memoize(10)(createValidator({
    artist: [required, minLength(3)],
    song: [required]
  }))
})

export default class EditPageMainSearchManualAddForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func,
    handleAdd: PropTypes.func.isRequired,
    closePopup: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleSubmit(data) {
    const { handleAdd, closePopup } = this.props;
    handleAdd(
      {
        temp: true,
        _id: `tmp_manual_${stringHashCode(data.song + data.artist)}`,
        artist: {
          name: data.artist
        },
        title: data.song,
        is_soundtrack: 0
      }
    );
    closePopup();
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="add-song-manually">
        <form action="#" onSubmit={handleSubmit(::this.handleSubmit)}>
          <Form.Input
            name="artist"
            type="text"
            label={"Artist Name"}
          />
          <Form.Input
            name="song"
            type="text"
            label={"Song Name"}
          />
          <div className="form-btn__container">
            <Form.Button
              type="submit"
              className="btn--success"
            >
              <span>Add to shortlist</span>
            </Form.Button>
          </div>
        </form>
      </div>
    );
  }
}
