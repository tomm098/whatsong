import React, {Component, PropTypes} from 'react';

export default class EditPageSongSearchSong extends Component {
  static propTypes = {
    item: PropTypes.object,
    active: PropTypes.bool,
    playing: PropTypes.bool,
    handlePlay: PropTypes.func,
    handlePause: PropTypes.func,
    handleAdd: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { item, active, playing, handlePlay, handlePause, handleAdd} = this.props;
    return (
      <div className="edit-search-result__table-row">
        <div className="edit-search-result__table-cell">
          <div className="edit-search-result__btn-play-box">
            <button
              className={'btn btn--clear' + (active && playing ? ' is-playing' : '')}
              onClick={()=>{
                if (active) {
                  playing ? handlePause() : handlePlay();
                } else {
                  handlePlay(item);
                }
              }}
            >
              <i className="icon-font icon-font-btn-play"></i>
              <i className="icon-font icon-font-btn-pause"></i>
            </button>
          </div>
        </div>
        <div className="edit-search-result__table-cell">
          <div className="edit-search-result__title-check">
            <div className="edit-search-result__name">{item.title}</div>
            <div className="edit-search-result__artist">{item.artist && item.artist.name}</div>
          </div>
        </div>
        <div className="edit-search-result__table-cell">
          <div className="edit-search-result__artist">{item.artist && item.artist.name}</div>
        </div>
        <div className="edit-search-result__table-cell">
          <div className="edit-search-result__youtube">{item.album && item.album.title}</div>
        </div>
        <div className="edit-search-result__table-cell">
          <button className="btn btn--clear add-shortlist" onClick={()=>handleAdd(item)}>add to shortlist</button>
        </div>
      </div>
    );
  }
}
