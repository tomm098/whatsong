import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './EditSearchResult.scss';
import * as playerActions from 'redux/modules/player';
import SongItem from './SongItem';

import { connect } from 'react-redux';

@connect(
  state => ({
    song: state.player.active_song,
    playing: state.player.playing
  }),
  {
    play: playerActions.play,
    pause: playerActions.pause
  }
)

export default class PageMainSearchResult extends Component {
  static propTypes = {
    handleAdd: PropTypes.func,
    results: PropTypes.array,
    song: PropTypes.object,
    playing: PropTypes.bool,
    play: PropTypes.func,
    pause: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    const { song, playing, play, pause, results, handleAdd } = this.props;
    return (
      <div className="edit-search-result">
        <div className="edit-search-result__title">Search results</div>
        <div className="edit-search-result__table">
          <div className="edit-search-result__table-row edit-search-result__table-row--th">
            <div className="edit-search-result__table-cell"></div>
            <div className="edit-search-result__table-cell">
            Title
            </div>
            <div className="edit-search-result__table-cell">
            Artist
            </div>
            <div className="edit-search-result__table-cell">
            Album
            </div>
            <div className="edit-search-result__table-cell"></div>
          </div>
          {
            results.map((c, i) =>
              <SongItem
                key={i}
                item={c}
                active={song && (song._id === c._id)}
                playing={playing}
                handlePlay={play}
                handlePause={pause}
                handleAdd={handleAdd}
              />
            )
          }
        </div>
      </div>
    );
  }
}
