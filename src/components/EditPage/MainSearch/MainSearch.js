import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './EditSearch.scss';
import { Form } from 'elements';
import { Loader } from 'components';

import { reduxForm } from 'redux-form';

@reduxForm({
  form: 'form-main-search'
})

export default class EditPageMainSearch extends Component {
  static propTypes = {
    handleSearch: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func,
    loading: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleSubmit(data) {
    const { handleSearch } = this.props;
    if (data.search) {
      handleSearch(data.search);
    }
  }

  render() {
    const { handleSubmit, loading } = this.props;
    return (
      <div className="edit-search">
        <form action="#" onSubmit={handleSubmit(::this.handleSubmit)} className="edit-search__form-search">
          <div className="edit-search__wrapp">
            <Form.Input
              className="big-search"
              name={"search"}
              type={"text"}
            />
            {loading && <Loader type="type3"/>}
            <Form.Button
              type="submit"
              className="btn--clear"
            >
              <span>Search</span>
            </Form.Button>
          </div>
          <div className="edit-search__caption">TIP: For best search results, use minimal words for song titles. Avoid punctuation such as full stops, brackets etc. Avoid adding
          multiple artist names. For example for a song called “Once Upon a Time (Not so Long Ago) by Moby feat. David Bowie”, simply
          search “Once Upon a Time”. It’s better to get more results and choose from the list.
          </div>
        </form>
      </div>
    );
  }
}
