import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './RecentlyThanked.scss';
import { TimeSince } from 'elements';

const image = require('./img1.png');

export default class RecentlyThanked extends Component {
  static propTypes = {
    thanks: PropTypes.array
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { thanks } = this.props;
    return (
      (thanks && !!thanks.length || null) &&
      <div className="recently-thanked">
        <div className="recently-thanked__title">RECENTLY THANKED BY</div>
        <ul className="recently-thanked__list">
          {
            thanks.map((c, i)=>
              <li key={i}>
                <span><TimeSince time={c.created_at}/></span>
                <div className="recently-thanked__avatar"><img src={image} /></div>
                <p>
                  <span>{c.user_from.username} thanked you </span>
                  {
                    c.from && c.from.title &&
                    <span> from <a href="#">{c.from.title}</a></span>
                  }
                </p>
              </li>
            )
          }
        </ul>
      </div>
    );
  }
}
