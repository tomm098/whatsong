import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Thanked.scss';
const image = require('./big-like.svg');
import { connect } from 'react-redux';

@connect(
  state => ({
    active_user: state.auth.user
  }),
  {

  }
)

export default class Thanked extends Component {
  static propTypes = {
    active_user: PropTypes.object,
    profile_user: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { active_user, profile_user } = this.props;
    const is_your_profile = active_user && profile_user && (active_user._id === profile_user._id);
    const username = is_your_profile ? 'You' : profile_user.username;
    return (
      (profile_user || null) &&
      <div className="thanked">
        <div className="thanked__img"><img src={image} /></div>
        {
          profile_user.count_thanks === 0 ?
            <div className="thanked__text">
              <p>{username} haven't been thanked.</p>
              {is_your_profile && <p>Try adding songs or scenes.</p>}
            </div>
            :
            <div className="thanked__text">
              <p>{username} have been thanked {profile_user.count_thanks} times!</p>
              {is_your_profile && <p>Keep up the great work.</p> }
            </div>
        }
      </div>
    );
  }
}
