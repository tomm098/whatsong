import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './ProfileStats.scss';
import { nFormatter } from 'utils/helpers';

const ico1 = require('./heart2.svg');
const ico2 = require('./cinema.svg');
const ico3 = require('./tv.svg');
const ico4 = require('./melody.svg');
const ico5 = require('./cinema-step.svg');
const ico6 = require('./check2.svg');

export default class ProfileStats extends Component {
  static propTypes = {
    data: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { data } = this.props;
    return (
      <div className="profile-stats">
        <div className="profile-stats__wrapp">
          <ul className="profile-stats__list">
            <li>
              <div className="profile-stats__icon"><img src={ico1} /></div>
              <p><span>{ nFormatter(data.favorited_songs_count, 1) }</span> favorited <br /> songs</p>
            </li>
            <li>
              <div className="profile-stats__icon"><img src={ico2} /></div>
              <p><span>{ nFormatter(data.favorited_movies_count, 1) }</span> favorited <br /> movies</p>
            </li>
            <li>
              <div className="profile-stats__icon"><img src={ico3} /></div>
              <p><span>{ nFormatter(data.favorited_tv_shows_count, 1) }</span> favorited <br /> shows</p>
            </li>
            <li>
              <div className="profile-stats__icon"><img src={ico4} /></div>
              <p><span>{ nFormatter(data.added_songs_count, 1) }</span> songs <br /> added</p>
            </li>
            <li>
              <div className="profile-stats__icon"><img src={ico5} /></div>
              <p><span>{ nFormatter(data.added_scenes_count, 1) }</span> scenes <br /> added</p>
            </li>
            <li>
              <div className="profile-stats__icon"><img src={ico6} /></div>
              <p>coming<br /> soon</p>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
