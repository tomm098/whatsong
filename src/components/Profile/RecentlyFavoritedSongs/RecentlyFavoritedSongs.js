import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Link } from 'react-router';
import { Song, SongList } from 'components';

export default class RecentlyFavoritedSongs extends Component {
  static propTypes = {
    songs: PropTypes.array,
    user: PropTypes.object
  }
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    const { songs, user} = this.props; // eslint-disable-line
    return (
      <div>
        <div className="profile__favorited-songs">
          <div className="profile__favorited">RECENTLY FAVORITED SONGS</div>
          <div className="profile__view-all">
            <Link to={`/Favorites/${user.username}`}>VIEW ALL</Link>
            {/*<button className="btn btn--clear">VIEW ALL ({profile.user.favorited_songs_count})</button>*/}
          </div>
        </div>
        <div>
          <SongList songs={songs || []} limit={9999}>
            <Song.ProfileSong
              modifier="profile-song"
            />
          </SongList>
        </div>
      </div>
    );
  }
}
