import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './ProfileUser.scss';
import dateFormat from 'dateformat';

const image = require('../../../images/default-avatar.svg');

export default class ProfileUser extends Component {
  static propTypes = {
    user: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { user } = this.props;
    const register_date = new Date(user.time_registered || user.created_at);
    return (
      <div className="profile-user">
        <div className="profile-user__avatar-radius">
          <div className="profile-user__avatar" style={{backgroundImage: ( user.photo || image ) ? `url(${user.photo || image})` : ''}} ></div>
        </div>
        <div className="profile-user__name">
          <p>{user.username}</p>
          {register_date.valueOf() && <span>member since {dateFormat(register_date, 'dS mmmm yyyy')}</span>}
        </div>
      </div>
    );
  }
}
