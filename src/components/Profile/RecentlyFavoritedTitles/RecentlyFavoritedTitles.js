import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './RecentlyFavoritedTitles.scss';
// const image = require('./img2.jpg');
import { Link } from 'react-router';

export default class RecentlyFavoritedTitles extends Component {
  static propTypes = {
    titles: PropTypes.array,
    user: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { titles, user } = this.props;
    return (
      <div className="recently-titles">
        <div className="recently-titles__top">
          <div className="recently-titles__title">RECENTLY FAVORITED TITLES</div>
          <div className="recently-titles__view-all">
            <Link to={`/Favorites/${user.username}`}>VIEW ALL</Link>
            {/*<button className="btn btn--clear">VIEW ALL ({favorited_movies_count})</button>*/}
          </div>
        </div>
        <ul className="recently-titles__list">

          {
            titles && titles.map((c, i)=>
              <li key={i}>
                <div className="recently-titles__img">
                  {c.poster && <img src={c.poster} />}
                  <i className="icon-font icon-font-heart"></i>
                </div>
                <span>{c.title}</span>
              </li>
            )
          }

        </ul>
      </div>
    );
  }
}
