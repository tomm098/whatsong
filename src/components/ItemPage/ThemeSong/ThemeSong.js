import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './ThemeSong.scss';
import { Like } from 'components';
import * as playerActions from 'redux/modules/player';
import { connect } from 'react-redux';
import * as notificationsActions from 'redux/modules/notifications';

@connect(
  state => ({
    active_song: state.player.active_song,
    playing: state.player.playing
  }),
  {
    play: playerActions.play,
    pause: playerActions.pause,
    pushNotification: notificationsActions.push
  }
)

export default class ThemeSong extends Component {
  static propTypes = {
    song: PropTypes.object,
    active_song: PropTypes.object,
    play: PropTypes.func,
    pause: PropTypes.func,
    playing: PropTypes.bool,
    pushNotification: PropTypes.func
  }

  static defaultProps = {
    song: {}
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { song, active_song, play, pause, playing, pushNotification } = this.props;
    const active = active_song && (active_song._id === song._id);
    return (
      (Object.keys(song).length || null) &&
      <div className="theme-song">
        <h3 className="theme-song__title">THEME SONG</h3>
        <ul className="theme-song__list">
          <li
            onClick={()=>{
              if (active) {
                playing ? pause() : play();
              } else {
                play(song);
              }
            }}
            className={'' + (active && playing ? ' is-playing' : '')}
          >
            <div className="theme-song__name">
              <span>{song.title}</span>
              <span>{song.artist && song.artist.name}</span>
            </div>
            <div onClick={e=>e.stopPropagation()}>
              <Like
                type="song"
                id={song._id}
                liked={song.is_favorited}
                like_count={song.favourited}
                onLike={()=>pushNotification('favorite_song', {name: song.title, by: song.artist && song.artist.name})}
              />
            </div>
          </li>
        </ul>
      </div>
    );
  }
}
