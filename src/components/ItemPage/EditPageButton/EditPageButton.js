import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './EditPageButton.scss';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

@connect(
  state => ({
    location: state.routing.locationBeforeTransitions
  }),
  {
    pushState: push
  }
)

export default class EditMovie extends Component {
  static propTypes = {
    location: PropTypes.object,
    pushState: PropTypes.func,
    children: PropTypes.node,
    path: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { pushState, location, children, path = 'search' } = this.props;
    return (
      <div className="edit-page-btn">
        <button
          onClick={()=>
            pushState(location.pathname + `/edit/${path}`)
          }
          className="btn btn--success"
        >
          {children}
        </button>
      </div>
    );
  }
}
