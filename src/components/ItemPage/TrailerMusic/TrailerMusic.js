import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import TrailerMusicItem from './TrailerItem';
import {} from './TrailerMusic.scss';
import * as notificationsActions from 'redux/modules/notifications';

import { connect } from 'react-redux';

@connect(
  state => ({ // eslint-disable-line

  }),
  {
    pushNotification: notificationsActions.push
  }
)

export default class TrailerMusic extends Component {
  static propTypes = {
    trailers: PropTypes.array,
    pushNotification: PropTypes.func,
    id: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { trailers, pushNotification, id } = this.props;
    return (
      !(trailers && trailers.length) ? null :
      <section className="trailer-music" id={id || ''}>
        <header className="trailer-music__header">
          <h2>Trailer Music</h2>
        </header>
        <article className="trailer-music__content">
          <ul className="trailer-music__list">
            {
              trailers.map((c, i)=>
                <li key={i}><TrailerMusicItem pushNotification={pushNotification} trailer={c}/></li>
              )
            }
          </ul>
        </article>
      </section>
    );
  }
}
