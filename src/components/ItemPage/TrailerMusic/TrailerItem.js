import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Song, SongList } from 'components';
import ReactPlayer from 'react-player';

export default class TrailerMusicItem extends Component {
  static propTypes = {
    trailer: PropTypes.object,
    pushNotification: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { trailer, pushNotification } = this.props;
    return (
      <div className="trailer-music__item">
        <p className="trailer-music__item-title">{trailer.title}</p>
        <div className="trailer-music__item-video">
          <ReactPlayer
            url={`https://www.youtube.com/watch?v=${trailer.youtube_id}&origin=https://www.what-song.com`}
            height="100%"
            width="100%"
          />
        </div>
        <div className="trailer-music__item-songs">
          <SongList songs={trailer.song_list} limit={4}>
            <Song.TrailerSong modifier="trailer-music" pushNotification={pushNotification}/>
          </SongList>
        </div>
      </div>
    );
  }
}
