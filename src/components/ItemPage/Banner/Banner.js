import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Banner } from 'elements';
import {} from './Banner.scss';
import { nFormatter, printDate } from 'utils/helpers';
//import { Like, LikeCount } from 'components';

export default class ItemPageBanner extends Component {
  static propTypes = {
    info: PropTypes.object,
    banner: PropTypes.string,
    like_type: PropTypes.string,
    pushNotification: PropTypes.func,
    handleEdit: PropTypes.func,
    fb_like_props: PropTypes.object,
    headline_first: PropTypes.string,
    headline_second: PropTypes.string,
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { info, banner, handleEdit, like_type, headline_first, headline_second } = this.props;
    return (
      <section className="info-banner">
        <Banner seo={{alt: `${info.title} Soundtrack`}} image={banner}/>
        <div className="info-banner__panel">
          <div className="info-banner__img">
            <img
              className="info-banner__images"
              src={info.poster}
              title={`${info.title} Soundtrack`}
              alt={`${info.title} - Soundtrack Poster`}
            />
          </div>
          <article className="info-banner__content">
            <header className="info-banner__title">
              {headline_first === undefined ? <h1>{info.title} Soundtrack</h1> : <h1>{headline_first}</h1>}
              {info.time_released && <p className="info-banner-subtitle">{printDate(info.time_released)}</p>}
              {
                headline_second === undefined ?
                (typeof info.seasons_count === 'number') ? <h2>{`${info.seasons_count} Season${info.seasons_count !== 1 ? 's' : '' }`}</h2> : null
                : <h2>{headline_second}</h2>
              }
              {handleEdit && <span className="info-banner__edit" onClick={handleEdit}>(edit)</span>}
              {<p className="info-banner-paragraph">{nFormatter(info.songs_favourited, 1)} liked songs • {nFormatter(info.view_count, 1)} views • {like_type !== 'tv_show' ? `composed by ${info.composer || ''}` : `music supervisor ${info.music_supervisor}`}</p>}
            </header>
            {/* <ul className="info-banner__list">
              <li>
                <div className="info-banner__like">
                  <Like
                    modifier = {'banner'}
                    type={like_type}
                    id={info._id}
                    liked={info.is_favorited}
                    like_count={info.favourited}
                    onLike={()=>pushNotification({name: info.title})}
                  />
                </div>
                <div className="info-banner__text-box">
                  <span className="info-banner__top-text">
                    <LikeCount
                      type={like_type}
                      id={info._id}
                      liked={info.is_favorited}
                      like_count={info.favourited}
                    />
                  </span>
                  <span className="info-banner__bottom-text">favorited</span>
                </div>
              </li>
              <li>
                <span className="info-banner__top-text">{nFormatter(info.songs_favourited, 1)}</span>
                <span className="info-banner__bottom-text">songs favorited</span>
              </li>
              <li>
                <span className="info-banner__top-text">{nFormatter(info.view_count, 1)}</span>
                <span className="info-banner__bottom-text">views</span>
              </li>
              {
                info.composer &&
                <li>
                  <span className="info-banner__top-text">{info.composer}</span>
                  <span className="info-banner__bottom-text">composer</span>
                </li>
              }
              {
                info.music_supervisor &&
                <li>
                  <span className="info-banner__top-text">{info.music_supervisor}</span>
                  <span className="info-banner__bottom-text">music supervisor</span>
                </li>
              }
              {
                fb_like_props &&
                <li>
                  <FBlike {...fb_like_props} />
                </li>
              }
            </ul> */}
          </article>
        </div>
      </section>
    );
  }
}
