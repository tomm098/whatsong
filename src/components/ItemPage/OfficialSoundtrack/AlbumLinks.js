import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { connect } from 'react-redux';
const image1 = require('../../../images/itunes.png');
const image2 = require('../../../images/amazon.png');
import { getExternalLink, getAmazonLink } from '../../../utils/helpers';

@connect(
  state => ({
    geolocation: state.auth.location,
  }),
  {}
)
export default class AlbumLinks extends Component {
  static propTypes = {
    geolocation: PropTypes.object,
    album: PropTypes.object,
    className: PropTypes.string
  }

  static defaultProps = {
    className: ''
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    const { className, album, geolocation } = this.props;
    return (
      <div className={`song__links ${className}`}>
        <div className="song__links-wrapp">
          {
            <a
              className={`song__itunes${!album.itunes_url ? ' is-disabled' : ''}`}
              target="_blank"
              rel="nofollow"
              href={getExternalLink(album.itunes_url) || '#'}
              onClick={e=>{
                if (!album.itunes_url) {
                  e.preventDefault();
                }
              }}
            >
              <span>itunes</span>
              <img src={image1} alt="Download OST on iTunes"/>
            </a>
          }
          {
            <a
              className={`song__amazon${!album.title ? ' is-disabled' : ''}`}
              target="_blank"
              rel="nofollow"
              onClick={e=>{
                if (!album.title) {
                  e.preventDefault();
                }
              }}
              href={getExternalLink(getAmazonLink(geolocation && geolocation.country, encodeURIComponent(album.title)))}
            >
              <span>amazon</span>
              <img src={image2} alt="Download OST on Amazon"/>
            </a>
          }
        </div>
      </div>
    );
  }
}
