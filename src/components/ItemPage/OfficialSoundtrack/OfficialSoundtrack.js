import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './OfficialSoundtrack.scss';
import * as notificationsActions from 'redux/modules/notifications';
import { connect } from 'react-redux';
import SoundtrackAlbum from './SoundtrackAlbum';

@connect(
  state => ({ // eslint-disable-line

  }),
  {
    pushNotification: notificationsActions.push
  }
)

export default class OfficialSoundtrack extends Component {
  static propTypes = {
    albums: PropTypes.array,
    pushNotification: PropTypes.func,
    title: PropTypes.string,
    id: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const {albums, id, ...rest } = this.props;
    //const sorted_songs = songs.sort((a, b)=> a.track_number - b.track_number);
    return (
      !(albums && albums.length) ? null :
      <section className="official-soundtrack" id={id || ''}>
        <header className="tofficial-soundtrack__header">
          <h2 className="official-soundtrack__title">Official Soundtrack</h2>
        </header>
        {
          albums.map((c, i)=>
            <SoundtrackAlbum
              key={i}
              album={c}
              {...rest}
            />
          )
        }
      </section>
    );
  }
}
