import React, { Component, PropTypes } from 'react';
import { Song, SongList } from 'components';
import { printDate } from 'utils/helpers';
import AlbumLinks from './AlbumLinks';

export default class SoundtrackAlbum extends Component {
  static propTypes = {
    album: PropTypes.object,
    title: PropTypes.string,
    pushNotification: PropTypes.func,
    default_expanded: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state = {
      is_opened: !!props.default_expanded
    };
  }

  getHTTPSImage(album, title) {
    if (!(album && album.album && album.album.thumbnail)) {
      return false;
    }
    album.album.thumbnail = album.album.thumbnail.replace(/http:\/\/is\d.mzstatic.com/, 'https://is4-ssl.mzstatic.com');
    return album.album.thumbnail.match('https') === null ? '' :
      <img
        itemProp="thumbnailUrl"
        src={album.album.thumbnail}
        title={album.album.title}
        alt={`${title} - Official Soundtrack`}
      />;
  }

  render() {
    const { album, title, pushNotification } = this.props;
    const { is_opened } = this.state;
    return (
      <article className="official-soundtrack__content">
      {/*<article itemScope={true} itemType="http://schema.org/MusicAlbum" className="official-soundtrack__content">*/}
        <div
          className="official-soundtrack__header"
          onClick={() => this.setState({ is_opened: !is_opened })}
        >
          <div className="official-soundtrack__img">
            {this.getHTTPSImage(album, title)}
          </div>
          <div className="official-soundtrack__wrapp">
            <div className="official-soundtrack__box">
              <div className="official-soundtrack__block">
                <div itemProp="name" className="official-soundtrack__title-small">{album.album.title} • <span className="official-soundtrack__album-small"><span itemProp="numTracks">{album.songs.length}</span>{` song${album.songs.length !== 1 ? 's' : ''}`}</span></div>
                <div className="official-soundtrack__links" onClick={e => e.stopPropagation()}>
                  <AlbumLinks album={album.album} />
                </div>
              </div>
            </div>
            <div className="official-soundtrack__date">{printDate(album.album.time_released)}</div>

          </div>
        </div>

        {
          is_opened &&
          <div className="official-soundtrack__list">
            <SongList
              songs={album.songs.sort((a, b) => a.track_number - b.track_number)}
              limit={9e9}
              numbered={true}
              edit_proxy={true}
            >
              <Song.SoundtrackSong
                modifier="official-soundtrack"
                pushNotification={pushNotification}
              />
            </SongList>
          </div>
        }
      </article>
    );
  }
}
