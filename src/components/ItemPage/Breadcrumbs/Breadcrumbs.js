import {} from './Breadcrumbs.scss';
import React from 'react';
import { Link } from 'react-router';
import {} from './Breadcrumbs.scss';

const Breadcrumbs = ({crumbs = [], children}) =>
  <div className="breadcrumbs">
    <ul className="breadcrumbs__list">
      {
        crumbs.map((c, i) =>
          <li key={i}>
            {
              i + 1 === crumbs.length ? <span>{c.name}</span> :
                <Link to={c.link} className="breadcrumbs__link">{c.name}</Link>
            }
          </li>
        )
      }
    </ul>
    {children}
  </div>;
export default Breadcrumbs;
