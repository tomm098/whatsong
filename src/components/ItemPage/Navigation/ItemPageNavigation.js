import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { urlGenerator } from 'utils/helpers';
import {} from './ItemPageNavigation.scss';

export default class ItemPageNavigation extends Component {
  static propTypes = {
    prev: PropTypes.object,
    next: PropTypes.object,
    link_type: PropTypes.string.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  getYear(date) {
    return date ? ` (${(new Date(date)).getFullYear()})` : '';
  }

  render() {
    const { prev, next, link_type } = this.props;
    return (
      <div className="item-page-navigation">
        {
          prev &&
          <div className="item-page-navigation__prev">
            <div className="item-page-navigation__wrap item-page-navigation__wrap--mobile">
              <Link to={urlGenerator(link_type, {...prev, id: prev._id })}>
                {prev.title + ' soundtrack' + this.getYear(prev.time_released)}
                </Link>
              <span className="item-page-navigation__text-prev">Previous</span>
            </div>
          </div>
        }
        {
          next &&
          <div className="item-page-navigation__next">
            <div className="item-page-navigation__wrap">
              <span className="item-page-navigation__text-next">Next</span>
              <Link to={urlGenerator(link_type, {...next, id: next._id })}>
                {next.title + ' soundtrack' + this.getYear(next.time_released)}
              </Link>
            </div>
          </div>
        }
      </div>
    );
  }
}

