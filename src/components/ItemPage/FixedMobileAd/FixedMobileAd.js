import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { GoogleAd } from 'components';
import { ScreenType } from 'elements';
import {} from './FixedMobileAd.scss';

export default class FixedMobileAdd extends Component {
  static propTypes = {

  }

  constructor(props) {
    super(props);
    this.state = {
      is_open: true
    };
  }

  closeAdd = () => this.setState({ is_open: false });

  render() {
    return (
      <ScreenType.Mobile>
        {
          this.state.is_open &&
          <div className="fixed-AD">
            <GoogleAd
              display="block"
              width="300px"
              height="100px"
              {...this.props}
            />
            <i onClick={this.closeAdd} className="icon-font icon-font-close-player"></i>
          </div>
        }
      </ScreenType.Mobile>
    );
  }
}

