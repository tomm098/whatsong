import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Seasons.scss';
import { Link } from 'react-router';
import { urlGenerator } from 'utils/helpers';

export default class Seasons extends Component {
  static propTypes = {
    seasons: PropTypes.array,
    tv_show: PropTypes.object,
    handleAdd: PropTypes.func,
    allowAdd: PropTypes.bool,
    banner: PropTypes.node,
    showBannerWhenLimit: PropTypes.number,
    showBannerAfterIndex: PropTypes.number
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  renderBanner = (index) => {
    const { seasons, banner, showBannerWhenLimit, showBannerAfterIndex } = this.props;
    if (
      banner && seasons.length >= showBannerWhenLimit &&
      showBannerAfterIndex === index
    ) {
      return (
        <div className="seasons__item">
          {banner}
        </div>
      );
    }
    return false;
  }

  render() {
    const { seasons, tv_show, handleAdd, allowAdd } = this.props;

    return (
      (Array.isArray(seasons) || null) &&
      <div className="seasons">
        <div className="seasons__header">
          <h2 className="seasons__title">Seasons</h2>
          {
            allowAdd &&
            <div onClick={handleAdd} className="seasons__add">
              + add season
            </div>
          }
        </div>
        <div className="seasons__list">
          {
            seasons.map((c, i)=> {
              const season = (
                <div key={i} className="seasons__item">
                  <div className="seasons__left">
                    <Link to={urlGenerator('tv_show_season', {season_id: c._id, id: tv_show._id, slug: tv_show.slug})}>
                      <div className="seasons__name">{`Season ${c.season}`}</div>
                      {/*<div className="seasons__date">?? - ??</div>*/}
                    </Link>
                  </div>
                  <div className="seasons__right">
                    <Link to={urlGenerator('tv_show_season', {season_id: c._id, id: tv_show._id, slug: tv_show.slug})}>
                        <div className="seasons__episodes">{`${c.episodes_count} episode${c.episodes_count !== 1 ? 's ' : ' ' }`}</div>
                        <div className="seasons__songs">{`${c.songs_count} song${c.songs_count !== 1 ? 's' : '' }`}</div>
                    </Link>
                  </div>
                </div>
              );
              const banner = this.renderBanner(i);
              if (banner) {
                return (
                  <div key={i}>
                    {banner}
                    {season}
                  </div>
                );
              }
              return season;
            })
          }
        </div>
      </div>
    );
  }
}
