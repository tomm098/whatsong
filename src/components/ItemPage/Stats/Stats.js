import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Stats.scss';
export default class Stats extends Component {
  static propTypes = {
    imdb: PropTypes.number,
    rotten_tomatoes: PropTypes.number,
    metacritic: PropTypes.number,
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { imdb, rotten_tomatoes, metacritic } = this.props;
    return (
      <div className="stats">
        <ul className="stats__list">
          <li>
            <span className="stats__title">imdb</span>
            <div className="stats__number">
              <span>{imdb}</span>
              <span>.</span>
              <span>10</span>
            </div>
          </li>
          <li>
            <span className="stats__title">rotten tomatos</span>
            <span className="stats__percent">{rotten_tomatoes}%</span>
          </li>
          <li>
            <span className="stats__title">metacritic</span>
            <span className="stats__percent">{metacritic}%</span>
          </li>
        </ul>
      </div>
    );
  }
}
