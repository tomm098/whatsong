import React, { Component, PropTypes } from 'react'; // eslint-disable-line
import cx from 'classnames';

import MusicKitClient from 'helpers/MusicKitClient';

export default class AddToAppleMusic extends Component {
  static propTypes = {
    songs: PropTypes.array,
    title: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {
      loaded: false
    };
  }

  handleAdd = () => {
    const { songs, title } = this.props;
    // todo add dynamic types for import object; can be: songs, music-videos, library-songs, or library-music-videos
    const data = songs.filter(c => c.itunes_id).map(c => ({ id: c.itunes_id, type: 'songs' }));
    MusicKitClient.importToNewPlaylist(
      `${title} Soundtrack Playlist - Complete List of Songs`,
      'Brought to you by WhatSong',
      data
    ).then(() => this.setState({ loaded: true }));
  }

  render() {
    const { loaded } = this.state;
    return (
      <button
        disabled={loaded}
        className={cx('btn', {
          'btn--primary': !loaded,
          'btn btn--success': loaded
        })}
        onClick={this.handleAdd}>
        Open playlist in Apple Music
      </button>
    );
  }
}

