import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import { Form } from 'elements';
import EditTvShowFormValidation from './EditTvShowFormValidation';
import { connect } from 'react-redux';
import * as tvShowActions from 'redux/modules/tvShowMain';

@connect(
  state => ({ // eslint-disable-line

  }),
  {
    edit: tvShowActions.editShow
  }
)

@reduxForm({
  form: 'editTvShowForm',
  validate: EditTvShowFormValidation
})

export default class EditTvShowForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    edit: PropTypes.func,
    closePopup: PropTypes.func,
    tv_show: PropTypes.object,
    initialize: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    const { tv_show, initialize } = this.props;
    initialize({
      title: tv_show.title || '',
      year: tv_show.year || '',
      theme_song: tv_show.theme_song || '',
      music_supervisor: tv_show.music_supervisor || '',
      composer: tv_show.composer || '',
      imdb_id: tv_show.imdb_id || '',
      network: tv_show.network || '',
      moviedb_id: tv_show.moviedb_id || '',
      intro: tv_show.intro || '',
    });
  }

  handleSubmit(data) {
    const { closePopup, edit, tv_show } = this.props;
    edit({...data, tv_show: tv_show._id}).then(closePopup);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="add-tv-form">
        <form action="javascript:void(0)" method="post" onSubmit={handleSubmit(::this.handleSubmit)} >
          <Form.Input
            name="title"
            type="text"
            label={"Name"}
          />
          <Form.Input
            classNameGroup="input-year"
            name="year"
            type="text"
            label={"Year"}
          />
          <Form.Input
            name="theme_song"
            type="text"
            label={"Theme song"}
          />
          <div className="poster-image">
            <div className="poster-image-wrapp">
              <div className="replace-image">
                <Form.FileInput
                  name="poster"
                  label="Poster Image"
                  selected_text="replace image"
                  dropzone_options={{
                    multiple: false,
                    accept: 'image/*'
                  }}
                />
              </div>
            </div>
          </div>
          <div className="banner-image">
            <div className="banner-image-wrapp">
              <div className="upload">
                <Form.FileInput
                  name="banner"
                  label="Banner Image"
                  selected_text="replace image"
                  dropzone_options={{
                    multiple: false,
                    accept: 'image/*'
                  }}
                />
              </div>
              <div className="info-wide">Should be at least 1280px wide.</div>
            </div>
          </div>
          <Form.Input
            name="composer"
            type="text"
            label={"Composer"}
          />
          <Form.Input
            name="music_supervisor"
            type="text"
            label={"Supervisor"}
          />
          <Form.Input
            name="network"
            type="text"
            label={"Network"}
          />
          <Form.Input
            name="imdb_id"
            type="text"
            label={"IMDb ID"}
          />
          <Form.Input
            name="moviedb_id"
            type="text"
            label={"MovieDb Id"}
          />
          <Form.Textarea
            name="intro"
            label="Intro"
          />
          <div className="form-btn__container">
            <Form.Button
              type="submit"
              className="btn--success"
            >
              <span>Submit</span>
            </Form.Button>
          </div>
        </form>
      </div>
    );
  }
}
