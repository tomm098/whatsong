import memoize from 'lru-memoize';
import { createValidator, required, minLength, integer, maxLength } from 'utils/validation';

const EditTvShowFormValidation = createValidator({
  title: [required],
  year: [required, minLength(4), maxLength(4), integer],
  theme_song: [integer],
  imdb_id: [integer],
  moviedb_id: [integer],
});
export default memoize(10)(EditTvShowFormValidation);
