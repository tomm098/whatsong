import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Discussion.scss';
import { connect } from 'react-redux';
import * as discussionActions from 'redux/modules/discussion';

@connect(
  state => ({ // eslint-disable-line

  }),
  {
    subscribe: discussionActions.subscribe
  }
)

export default class AddQuestion extends Component {
  static propTypes = {
    discusionId: PropTypes.number,
    post: PropTypes.func,
    subscribe: PropTypes.func,
    closePopup: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      errorForm: false
    };
  }

  handleValidate(data) {
    data.preventDefault();

    const email = this.refs.email.value;
    if (email !== undefined && email !== '') {
      this.handleSubmit();
    } else {
      this.setState({errorForm: true});
    }
  }

  handleSubmit() {
    const success = (response) => { // eslint-disable-line
      this.refs.email.value = '';
      this.props.closePopup();
    };

    const error = (err) => {
      console.log(err);
    };

    return this.props.subscribe(this.props.discusionId, this.refs.email.value).then(success, error);
  }

  render() {
    return (
      <form action="" onSubmit={::this.handleValidate} className="">
        <div className="post_question_form nopadding">
          { this.state.errorForm &&
          <span className="form-error">Error! Please enter your email</span>
          }

          <div className="top">
            <input type="email" placeholder="Your Email" ref="email" required/>
          </div>
          <div className="bottom">
            <div className="left">
              <button className="post_question_btn">Follow</button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
