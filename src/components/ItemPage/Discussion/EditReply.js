import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Discussion.scss';
import { connect } from 'react-redux';
import * as discussionActions from 'redux/modules/discussion';

@connect(
  state => ({ // eslint-disable-line

  }),
  {
    editReply: discussionActions.editReply
  }
)

export default class EditReply extends Component {
  static propTypes = {
    discusionId: PropTypes.number,
    replyName: PropTypes.string,
    post: PropTypes.func,
    editReply: PropTypes.func,
    closePopup: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      errorForm: false
    };
  }

  handleValidate(data) {
    data.preventDefault();

    const name = this.refs.name.value;
    if (name !== '') {
      this.handleSubmit();
    } else {
      this.setState({errorForm: true});
    }
  }

  handleSubmit() {
    const success = (response) => { // eslint-disable-line
      this.refs.name.value = '';
      this.props.closePopup();
    };

    const error = (err) => {
      console.log(err);
    };

    return this.props.editReply(this.props.discusionId, this.refs.name.value).then(success, error);
  }

  render() {
    return (
      <form action="" onSubmit={::this.handleValidate} className="">
        <div className="post_question_form nopadding">
          { this.state.errorForm &&
          <span className="form-error">Error! Please enter your reply</span>
          }

          <div className="top">
            <input type="text" defaultValue={this.props.replyName} ref="name" required/>
          </div>
          <div className="bottom">
            <div className="left">
              <button className="post_question_btn">Save Reply</button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
