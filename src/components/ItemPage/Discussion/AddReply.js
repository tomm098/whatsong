import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Discussion.scss';
//import { Form } from 'elements';
//import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import { connect } from 'react-redux';
import * as discussionActions from 'redux/modules/discussion';
//import AddCommentValidation from './AddCommentFormValidation';

@connect(
  state => ({ // eslint-disable-line

  }),
  {
    post: discussionActions.post
  }
)

export default class AddReply extends Component {
  static propTypes = {
    user: PropTypes.object,
    comment: PropTypes.object,
    //answer_id: PropTypes.string,
    //closePopup: PropTypes.func,
    post: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      errorForm: false,
      captcha: '',
    };
  }

  handleValidate(data) {
    data.preventDefault();
    const reply = this.refs.reply.value;

    if (reply !== undefined && reply !== '') {
      this.setState({errorForm: false});
      this.handleSubmit();
    } else {
      this.setState({errorForm: true});
    }
  }

  handleSubmit() {
    const success = (response) => { // eslint-disable-line
      this.refs.reply.value = '';
    };

    const error = (err) => {
      console.log(err);
    };

    //console.log(this.refs.reply.value, this.props.comment._id, this.props.comment.episode);

    if (this.props.comment.episode) {
      return this.props.post(this.refs.reply.value, String(this.props.comment._id), undefined, this.props.comment.episode._id).then(success, error);
    } else {
      return this.props.post(this.refs.reply.value, String(this.props.comment._id)).then(success, error);
    }

    //return this.props.post(this.refs.reply.value, String(this.props.comment._id)).then(success, error);
  }

  render() {
    const { handleSubmit, submitting, error } = this.props; // eslint-disable-line
    return (
      <form action="" onSubmit={::this.handleValidate} className="">
        <div className="post_question_form">
          <div className="top">
            <textarea placeholder="Add your reply" ref="reply" required></textarea>
          </div>
          <div className="bottom">
            <div className="left">
              <button className="post_question_btn">Reply <span>+1</span></button>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
