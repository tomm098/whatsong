import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Discussion.scss';
import { TimeSince, Dropdown, Popup } from 'elements';
import EditReply from './EditReply';
import DeleteReply from './DeleteReply';
import { connect } from 'react-redux';
import * as discussionActions from 'redux/modules/discussion';
import * as authMoadalActions from 'redux/modules/authModal';

@connect(
  state => ({ // eslint-disable-line

  }),
  {
    openAuthModal: authMoadalActions.open,
    thumbsUp: discussionActions.thumbsUp
  }
)

export default class Reply extends Component {
  static propTypes = {
    user: PropTypes.object,
    openAuthModal: PropTypes.func,
    reply: PropTypes.object,
    thumbsUp: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      is_edit_modal_open: false,
      is_delete_modal_open: false,
    };
  }

  handleThumbUp() {
    if (!this.props.user) {return this.props.openAuthModal('sign-in');}
    if (!this.props.reply.isThumbsUP) {
      return this.props.thumbsUp(this.props.reply._id).then();
    }
  }

  openEditReplyModal() {
    this.setState({is_edit_modal_open: true});
  }

  closeEditReplyModal() {
    this.setState({is_edit_modal_open: false});
  }

  openDeleteReplyModal() {
    this.setState({is_delete_modal_open: true});
  }

  closeDeleteReplyModal() {
    this.setState({is_delete_modal_open: false});
  }

  render() {
    const { reply, user } = this.props;
    const { is_edit_modal_open, is_delete_modal_open } = this.state;

    let userIsOwner = false;
    if (reply.user && user) {
      ((reply.user._id === user._id) || user.role === 'ADMIN') && (userIsOwner = true);
    }

    return (
      <div className="reply">
        <div className="top">
          <span className="user_link_cont">{reply.user.username}</span> • <span className="points_cont">{reply.user.scores} points</span> • <span className="time_cont"><TimeSince time={reply.time_posted}/></span>
        </div>
        <div className="center">
          {reply.body}
        </div>
        <div className="bottom">
          <div className="reply_voting">
            <div className="up">
              <button className={reply.isThumbsUP ? 'green' : null} onClick={::this.handleThumbUp}></button>
            </div>
            <div className="counter">
              <span className={reply.isThumbsUP ? 'green' : null}>{reply.thumbs_up ? '+' + reply.thumbs_up : '+0'}</span>
            </div>
          </div>
          {
            userIsOwner &&
            <div className="comments_menu">
              <Dropdown
                trigger={
                  <button className="dotted_btn"></button>
                }
              >
                <ul className="dropdown-menu dropdown-menu--three-dots dropdown-menu--arrow-left">
                  <li>
                    <a onClick={() => this.openEditReplyModal(reply._id)}>Edit</a>
                    <a onClick={() => this.openDeleteReplyModal(reply._id)}>Delete</a>
                  </li>
                </ul>
              </Dropdown>
            </div>
          }
        </div>

        <Popup
          is_opened={is_edit_modal_open}
          handleClose={::this.closeEditReplyModal}
          title="Edit Reply"
        >
          <EditReply discusionId={reply._id} replyName={reply.body} />
        </Popup>

        <Popup
          is_opened={is_delete_modal_open}
          handleClose={::this.closeDeleteReplyModal}
        >
          <DeleteReply discusionId={reply._id} />
        </Popup>
      </div>
    );
  }
}
