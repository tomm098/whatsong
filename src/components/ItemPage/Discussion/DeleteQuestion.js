import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Discussion.scss';
import { connect } from 'react-redux';
import * as discussionActions from 'redux/modules/discussion';

@connect(
  state => ({ // eslint-disable-line

  }),
  {
    deleteQuestion: discussionActions.deleteQuestion
  }
)

export default class DeleteQuestion extends Component {
  static propTypes = {
    discusionId: PropTypes.number,
    deleteQuestion: PropTypes.func,
    closePopup: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  handleDelete() {
    const success = (response) => { // eslint-disable-line
      this.props.closePopup();
    };

    const error = (err) => {
      console.log(err);
    };

    return this.props.deleteQuestion(this.props.discusionId, success, error);
  }

  render() {
    return (
      <div className="deleteQuestionCont">
        <h3>Are you sure you want to delete this question?</h3>
        <button onClick={::this.handleDelete}>I want to delete this question</button>
      </div>
    );
  }
}
