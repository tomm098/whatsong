import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Discussion.scss';
import { Popup } from 'elements';
import { AddReply, Comment } from './';
import { connect } from 'react-redux';
import * as authMoadalActions from 'redux/modules/authModal';
import AddQuestion from './AddQuestion';
import { Link, Element } from 'react-scroll';


@connect(
  state => ({
    posts: state.discussion.discussions,
    user: state.auth.user
  }),
  {
    openAuthModal: authMoadalActions.open
  }
)

export default class Discussion extends Component {
  static propTypes = {
    posts: PropTypes.array,
    user: PropTypes.object,
    openAuthModal: PropTypes.func,
    hide_question_form: PropTypes.bool,
    episode_link: PropTypes.bool,
    id: PropTypes.string,
  }

  constructor(props) {
    super(props);
    this.state = {
      is_post_modal_open: false,
      answer_id: ''
    };
  }

  openPostModal(answer_to) {
    if (!this.props.user) {return this.props.openAuthModal('sign-in');}
    this.setState({is_post_modal_open: true, answer_id: answer_to || ''});
  }

  closePostModal() {
    this.setState({is_post_modal_open: false, answer_id: ''});
  }

  goToPage = (pageNumber) => {
    this.reactPageScroller.goToPage(pageNumber);
  }

  render() {
    const { posts, hide_question_form, id } = this.props;
    const { is_post_modal_open, answer_id } = this.state;
    return (
      <section className="discussion" id={id || ''}>
        <header className="discussion__header" id="discussion">
          <h2 className="discussion__title">{'Questions'}</h2>
          {
            !hide_question_form &&
            <Link activeClass="active" to="add_question" spy={true} smooth={true} offset={-60} duration={500}>
              <div className="discussion__post"><button className="btn btn--primary">Ask Question <span>+1</span></button></div>
            </Link>
          }
        </header>
        {!posts.length && <div className="discussion__subtitle">Agh! No one has posted yet. Would you like to be the first?</div>}

        <div className="discussion__list">
          <ul>
            {
              posts.map((c, i)=>
                <li key={i}>
                  <Comment user={this.props.user} handleReply={::this.openPostModal} episode_link={this.props.episode_link} comment={c}/>
                </li>
              )
            }
          </ul>
          {
            !hide_question_form &&
            <Element name="add_question">
              <AddQuestion user={this.props.user} answer_id={answer_id} />
            </Element>
          }
        </div>

        <Popup
          is_opened={is_post_modal_open}
          handleClose={::this.closePostModal}
          title="Post Question or Comment"
        >
          <AddReply answer_id={answer_id}/>
        </Popup>

      </section>
    );
  }
}
