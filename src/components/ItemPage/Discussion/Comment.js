import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import Reply from './Reply';
import Subscribe from './Subscribe';
import EditQuestion from './EditQuestion';
import DeleteQuestion from './DeleteQuestion';
import { TimeSince, Dropdown, Popup } from 'elements';
import { Link } from 'react-router';
import { urlGenerator, printDate } from 'utils/helpers'; // eslint-disable-line
import * as authMoadalActions from 'redux/modules/authModal';
import AddReply from './AddReply';
import { connect } from 'react-redux';
import * as discussionActions from 'redux/modules/discussion';

@connect(
  state => ({
    user: state.auth.user,
    tv_show: state.tvShowMain.data,
    tv_show_season: state.tvShowSeason.data,
  }),
  {
    openAuthModal: authMoadalActions.open,
    voteUp: discussionActions.voteUp,
    voteDown: discussionActions.voteDown
  }
)

export default class Comment extends Component {
  static propTypes = {
    user: PropTypes.object,
    comment: PropTypes.object,
    handleReply: PropTypes.func,
    openAuthModal: PropTypes.func,
    voteUp: PropTypes.func,
    voteDown: PropTypes.func,
    tv_show: PropTypes.object,
    tv_show_season: PropTypes.object,
    episode_link: PropTypes.bool
  }

  constructor(props) {
    super(props);
    this.state = {
      reply_list_open: false,
      reply_form_open: false,
      is_subscribe_modal_open: false,
      is_edit_modal_open: false,
      is_delete_modal_open: false
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.comment && nextProps.comment.childs && nextProps.comment.childs.length > this.props.comment.childs.length) {
      this.setState({reply_list_open: true});
    }
  }

  toggleReplyList() {
    if (this.state.reply_form_open) {
      this.toggleReplyForm();
    }

    this.setState({reply_list_open: !this.state.reply_list_open});
  }

  toggleReplyForm() {
    if (!this.props.user) {return this.props.openAuthModal('sign-in');}
    this.setState({reply_form_open: !this.state.reply_form_open});
  }

  handleVoteUp() {
    if (!this.props.user) {return this.props.openAuthModal('sign-in');}
    if (!this.props.comment.isVoted) {
      return this.props.voteUp(this.props.comment._id).then();
    }
  }

  handleVoteDown() {
    if (!this.props.user) {return this.props.openAuthModal('sign-in');}
    if (!this.props.comment.isVoted) {
      return this.props.voteDown(this.props.comment._id).then();
    }
  }

  openSubscribeModal(answer_to) {
    this.setState({is_subscribe_modal_open: true, answer_id: answer_to || ''});
  }

  closeSubscribeModal() {
    this.setState({is_subscribe_modal_open: false});
  }

  openEditQuestionModal() {
    this.setState({is_edit_modal_open: true});
  }

  closeEditQuestionModal() {
    this.setState({is_edit_modal_open: false});
  }

  openDeleteQuestionModal() {
    this.setState({is_delete_modal_open: true});
  }

  closeDeleteQuestionModal() {
    this.setState({is_delete_modal_open: false});
  }

  render() {
    const { comment, user } = this.props;
    let { tv_show, tv_show_season, episode_link } = this.props; // eslint-disable-line

    if (tv_show_season) { tv_show = tv_show_season; }
    const { is_subscribe_modal_open, is_edit_modal_open, is_delete_modal_open } = this.state;
    if (!comment) {return null;}
    let number_of_comments = Array.isArray(comment.childs) ? comment.childs.length : 0;
    number_of_comments = `${number_of_comments} ${number_of_comments === 1 ? 'comment' : 'comments'}`;

    let number_of_comments_mobile = Array.isArray(comment.childs) ? comment.childs.length : 0;
    number_of_comments_mobile = `${number_of_comments_mobile} ${number_of_comments_mobile === 1 ? 'reply' : 'replies'}`;

    let replyBtnForMobile;
    if (this.state.reply_form_open) {
      replyBtnForMobile = <span>Hide</span>;
    } else {
      replyBtnForMobile = <span>Reply <span>+1</span></span>;
    }

    let userIsOwner = false;
    if (comment.user && user) {
      ((comment.user._id === user._id) || user.role === 'ADMIN') && (userIsOwner = true);
    }

    return (
      <div>
        <div className="comment_container">
          {
            tv_show && comment && comment.episode && comment.tv_show &&
            <div className="episode_link"><Link to={urlGenerator('tv_show_episode', {episode_id: comment.episode._id, id: comment.tv_show, slug: tv_show.tv_show.slug})}>{comment.episode.name}</Link></div>
          }
          <div className="comment">
            <div className="left_part">
              <div className="button_up"><button onClick={::this.handleVoteUp} className="button_up_btn"></button></div>
              <div className={comment.isVoted ? 'counter green' : 'counter' }><span>{comment.vote ? comment.vote : '0'}</span></div>
              <div className="button_down"><button onClick={::this.handleVoteDown} className="button_down_btn"></button></div>
            </div>
            <div className="right_part">
              <div className="top">
                { comment.user && comment.user._id ?
                  <div><span className="user_link_cont">{comment.user.username}</span> • <span className="points_cont">{comment.user.scores} points</span> • <span className="time_cont"><TimeSince time={comment.time_posted}/></span></div> :
                  <div><span className="user_link_cont">{'guest'}</span> • <span className="time_cont"><TimeSince time={comment.time_posted}/></span></div>
                }
              </div>
              {
                tv_show ?
                  <div className="center tv-show">
                    <div className="left">
                      {comment.body}
                    </div>
                    <div className="right">
                      {
                        episode_link &&
                        <Link to={urlGenerator('tv_show_episode', {episode_id: comment.episode._id, id: comment.tv_show, slug: tv_show.tv_show.slug})}>{comment.episode.name}</Link>
                      }
                    </div>
                  </div> :
                  <div className="center">
                    {comment.body}
                  </div>
              }
              <div className="bottom">
                { !!comment.childs.length ?
                  <div onClick={::this.toggleReplyList} className={this.state.reply_list_open ? 'comments_counter open' : 'comments_counter'}><span>{number_of_comments}</span></div> :
                  <div onClick={::this.toggleReplyForm} className="reply_btn_cont"><span className="reply_btn">{this.state.reply_form_open ? 'HIDE' : 'REPLY'}</span></div>
                }
                { !this.props.user && <div className="follow_cont"><span onClick={()=>this.openSubscribeModal()} className="follow_btn">follow without registering</span></div> }

                { userIsOwner &&
                <div className="comments_menu">
                  <Dropdown
                    trigger={
                      <button className="dotted_btn"></button>
                    }
                  >
                    <ul className="dropdown-menu dropdown-menu--three-dots dropdown-menu--arrow-left">
                      <li>
                        <a onClick={() => this.openEditQuestionModal(comment._id)}>Edit</a>
                        <a onClick={() => this.openDeleteQuestionModal(comment._id)}>Delete</a>
                      </li>
                    </ul>
                  </Dropdown>
                </div>
                }
              </div>

              <div className="bottom_mobile">
                { !!comment.childs.length ?
                  <div onClick={::this.toggleReplyList} className={this.state.reply_list_open === true ? 'comments_counter open' : 'comments_counter'}>{number_of_comments_mobile}</div> :
                  <div onClick={::this.toggleReplyForm} className="reply_btn_cont"><span className="reply_btn">{replyBtnForMobile}</span></div>
                }
              </div>
            </div>
          </div>
        </div>
        {
          this.state.reply_list_open && !!comment.childs.length &&
          <div className="discussion__reply-list">
            <ul>
              {
                (comment.childs || []).map((c, i)=>
                  <li key={i}>
                    <Reply user={this.props.user} reply={c}/>
                  </li>
                )
              }

              { !this.state.reply_form_open ? <button className="add_comment_btn" onClick={::this.toggleReplyForm}>Add comment <span>+1</span></button> : null }
              { this.state.reply_form_open ? <button className="add_comment_btn" onClick={::this.toggleReplyForm}>Hide</button> : null }
            </ul>
          </div>
        }
        {
          this.state.reply_form_open &&
          <AddReply user={this.props.user} comment={this.props.comment} />
        }

        <Popup
          is_opened={is_subscribe_modal_open}
          handleClose={::this.closeSubscribeModal}
          title="Follow without registering"
        >
          <Subscribe discusionId={comment._id} />
        </Popup>

        <Popup
          is_opened={is_edit_modal_open}
          handleClose={::this.closeEditQuestionModal}
          title="Edit Question"
        >
          <EditQuestion discusionId={comment._id} questionName={comment.body} />
        </Popup>

        <Popup
          is_opened={is_delete_modal_open}
          handleClose={::this.closeDeleteQuestionModal}
        >
          <DeleteQuestion discusionId={comment._id} />
        </Popup>
      </div>
    );
  }
}
