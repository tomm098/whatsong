import memoize from 'lru-memoize';
import { createValidator, required } from 'utils/validation';

const AddCommentValidation = createValidator({
  body: [required]
});
export default memoize(10)(AddCommentValidation);
