import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Discussion.scss';
//import { Form } from 'elements';
//import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import { connect } from 'react-redux';
import * as discussionActions from 'redux/modules/discussion';
//import AddCommentValidation from './AddCommentFormValidation';
const Captcha = require('react-captcha');

@connect(
  state => ({ // eslint-disable-line

  }),
  {
    post: discussionActions.post
  }
)

export default class AddQuestion extends Component {
  static propTypes = {
    user: PropTypes.object,
    answer_id: PropTypes.string,
    //closePopup: PropTypes.func,
    post: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      errorForm: false,
      captcha: '',
    };
  }

  handleValidate(data) {
    data.preventDefault();
    const question = this.refs.question.value;

    if (this.props.user) {
      if (question !== undefined && question !== '') {
        this.setState({errorForm: false});
        this.handleSubmit();
      } else {
        this.setState({errorForm: true});
      }
    } else {
      const email = this.refs.email.value;
      if (this.state.captcha !== '') {
        if (email !== undefined && email !== '') {
          this.handleSubmit();
        } else {
          this.setState({errorForm: true});
        }
      }
    }
  }

  handleSubmit() {
    const success = (response) => { // eslint-disable-line
      this.refs.question.value = '';
      if (!this.props.user) this.refs.email.value = '';
      this.captchaHandler('');
    };

    const error = (err) => {
      console.log(err);
    };

    const args = [this.refs.question.value, this.props.answer_id];
    if (!this.props.user) {
      args.push(this.refs.email.value);
    }

    return this.props.post(...args).then(success, error);
  }

  captchaHandler(value) {
    this.setState({captcha: value});
  }

  render() {
    const { handleSubmit, submitting, error } = this.props; // eslint-disable-line
    return (
      <form action="" onSubmit={::this.handleValidate} className="">
        <div className="post_question_form">
          { this.state.errorForm &&
          <span className="form-error">Error! Please fill in all fields</span>
          }

          <div className="top">
            <textarea placeholder="Ask Question" ref="question" required></textarea>
            { !this.props.user &&
            <input type="email" placeholder="Your Email" ref="email" required/>
            }
          </div>
          <div className="bottom">
            <div className="left">
              <button className="post_question_btn">Post Question <span>+1</span></button>
            </div>
            <div className="right" id="captcha-holder">
              { !this.props.user &&
              <Captcha
                sitekey="6LcdInMUAAAAAIALbJTbNA4bPZkNiBlStssj9GUK"
                lang="en"
                theme="light"
                type="image"
                callback={(value) => this.captchaHandler(value)}/>
              }
            </div>
          </div>
        </div>
      </form>
    );
  }
}
