import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import { Form } from 'elements';
import memoize from 'lru-memoize';
import { createValidator, required } from 'utils/validation';


@reduxForm({
  form: 'editEpisodeForm',
  validate: memoize(10)(createValidator({
    name: [required],
    date_released: [required]
  }))
})

export default class EditEpisodeForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    handleEdit: PropTypes.func,
    closePopup: PropTypes.func,
    initialize: PropTypes.func,
    episode: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {

    };

    this.initValues = this.initValues.bind(this);
  }

  componentDidMount() {
    this.initValues();
  }

  initValues() {
    this.props.initialize({
      name: this.props.episode && this.props.episode.name || '',
      date_released: new Date(this.props.episode.date_released)
    });
  }

  handleSubmit(data) {
    const { closePopup, handleEdit } = this.props;
    data.episode_id = this.props.episode._id;
    handleEdit(data).then(closePopup);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div>
        <form action="javascript:void(0)" method="post" onSubmit={handleSubmit(::this.handleSubmit)} >
          <Form.Input
            name="name"
            type="text"
            label={"Name"}
          />

          <Form.DateSelects
            name="date_released"
            caption=""
            label="Release Date"
          />

          <div className="form-btn__container">
            <Form.Button
              type="submit"
              className="btn--success"
            >
              <span>Submit</span>
            </Form.Button>
            <Form.Button
              type="button"
              className="btn--reset"
              onClick={this.initValues}
            >
              <span>Reset</span>
            </Form.Button>
          </div>
        </form>
      </div>
    );
  }
}
