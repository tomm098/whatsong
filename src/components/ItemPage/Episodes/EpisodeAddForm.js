import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import { Form } from 'elements';
import memoize from 'lru-memoize';
import { createValidator, required } from 'utils/validation';


@reduxForm({
  form: 'addEpisodeForm',
  validate: memoize(10)(createValidator({
    name: [required],
    date_released: [required]
  }))
})

export default class AddEpisodeForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    handleAdd: PropTypes.func,
    closePopup: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleSubmit(data) {
    const { closePopup, handleAdd } = this.props;
    handleAdd(data).then(closePopup);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div>
        <form action="javascript:void(0)" method="post" onSubmit={handleSubmit(::this.handleSubmit)} >
          <Form.Input
            name="name"
            type="text"
            label={"Name"}
          />

          <Form.DateSelects
            name="date_released"
            caption=""
            label="Release Date"
          />

          <div className="form-btn__container">
            <Form.Button
              type="submit"
              className="btn--success"
            >
              <span>Submit</span>
            </Form.Button>
          </div>
        </form>
      </div>
    );
  }
}
