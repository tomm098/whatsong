import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Episodes.scss';
import { Link } from 'react-router';
import { urlGenerator, printDate } from 'utils/helpers'; // eslint-disable-line
import EpisodeAddForm from './EpisodeAddForm';
import EpisodeEditForm from './EpisodeEditForm';
import { Popup } from 'elements';
import {connect} from 'react-redux';

@connect(
  state => ({
    user: state.auth.user
  }),
  {
  }
)

export default class Episodes extends Component {
  static propTypes = {
    episodes: PropTypes.array,
    tv_show: PropTypes.object,
    handleAdd: PropTypes.func,
    handleEdit: PropTypes.func,
    banner: PropTypes.node,
    showBannerWhenLimit: PropTypes.number,
    showBannerAfterIndex: PropTypes.number,
    user: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {
      is_episode_popup_opened: false,
      edit_episode_popup_opened: false
    };
  }

  handleEditEp = (e, episode) => {
    e.preventDefault();
    e.stopPropagation();
    this.setState({edit_episode_popup_opened: true});
    this.setState({
      editEpisode: episode
    });
  };

  renderBanner = (index) => {
    const { episodes, banner, showBannerWhenLimit, showBannerAfterIndex } = this.props;
    if (
      banner && episodes.length >= showBannerWhenLimit &&
      showBannerAfterIndex === index
    ) {
      return (
        <div className="episodes__item">
          {banner}
        </div>
      );
    }
    return false;
  };

  render() {
    const { episodes, tv_show, handleAdd, handleEdit, user } = this.props;
    const { is_episode_popup_opened, edit_episode_popup_opened, editEpisode } = this.state;
    return (
      <div className="episodes">
        <div className="episodes__header">
          <h2 className="episodes__title">Episodes</h2>
          <div onClick={()=>this.setState({is_episode_popup_opened: true})} className="episodes__add">
            + add episode
          </div>
        </div>
        <div className="episodes__list">
          {
            episodes.map((c, i)=> {
              const episode = (
                <div key={i} className="episodes__item">
                  <Link to={urlGenerator('tv_show_episode', {episode_id: c._id, id: tv_show._id, slug: tv_show.slug})}>
                    <div className="episodes__name">{`Episode ${c.number}${c.name ? ' - ' + c.name : ''}`}</div>
                    {/*<div className="episodes__date">?? </div>*/}
                    {user && user.role === 'ADMIN' && <span className="edit_episode_btn" onClick={(e) => {this.handleEditEp(e, c);}}>(edit)</span>}
                    <div className="episodes__songs">
                      {`${c.songs_count} song${c.songs_count !== 1 ? 's' : '' }`}
                    </div>
                    <div className="episodes__release-date"> {printDate(c.date_released)}</div>
                  </Link>
                </div>
              );
              const banner = this.renderBanner(i);
              if (banner) {
                return (
                  <div key={i}>
                    {banner}
                    {episode}
                  </div>
                );
              }
              return episode;
            })
          }
        </div>

        <Popup
          is_opened={is_episode_popup_opened}
          handleClose={()=>this.setState({is_episode_popup_opened: false})}
          title="Add episode"
        >
          <EpisodeAddForm handleAdd={handleAdd}/>
        </Popup>

        <Popup
          is_opened={edit_episode_popup_opened}
          handleClose={()=>this.setState({edit_episode_popup_opened: false})}
          title="Edit episode title & date"
        >
          <EpisodeEditForm episode={editEpisode} handleEdit={handleEdit}/>
        </Popup>
      </div>
    );
  }
}
