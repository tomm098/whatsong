import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Song, SongList } from 'components';
import Header from './Header';
import {} from './MainSongs.scss';

import { connect } from 'react-redux';
import { push } from 'react-router-redux';

const sort_variants = [
  {
    name: 'time_play',
    title: 'Time Played',
    compare: (a, b) => {
      if (!a.time_play && !b.time_play) {return 0;}
      return (a.time_play || 9e9) - (b.time_play || 9e9);
    }
  },
  {
    name: 'az',
    title: 'A-Z',
    compare: (a, b) => a.title.localeCompare(b.title)
  },
  {
    name: 'order_added',
    title: 'Order added',
    // compare: (a, b) => (new Date(a.time_added)).valueOf() > (new Date(b.time_added)).valueOf() ? 1 : -1
    compare: (a, b)=>a._id - b._id
  }
];

@connect(
  state => ({
    location: state.routing.locationBeforeTransitions
  }),
  {
    pushState: push
  }
)

export default class ItemPageMainSongs extends Component {
  static propTypes = {
    list: PropTypes.array,
    sorting_enabled: PropTypes.bool,
    title: PropTypes.string,
    default_sort: PropTypes.number,
    importBtn: PropTypes.node,
    page_type: PropTypes.string,
    episode_name: PropTypes.string,
    id: PropTypes.string,
    location: PropTypes.object,
    pushState: PropTypes.func,
  }

  static defaultProps = {
    sorting_enabled: true,
    default_sort: 0
  }

  constructor(props) {
    super(props);
    this.state = {
      active_sort: sort_variants[this.props.default_sort]
    };
  }

  selectSort(variant) {
    this.setState({active_sort: variant});
  }

  render() {
    const { active_sort} = this.state;
    const { title, list, sorting_enabled, page_type, episode_name, id, ...rest } = this.props;
    const sorted_songs = sorting_enabled ? [...list.sort(active_sort.compare)] : list;
    return (
      <section className="song-list" id={id || ''}>
        <Header
          active_sort={active_sort}
          sort_variants={sort_variants}
          select={::this.selectSort}
          title={title}
          sorting_enabled={sorting_enabled}
          importBtn={this.props.importBtn}
          location={this.props.location}
          pushState={this.props.pushState}
        />
        <SongList
          {...rest}
          songs={sorted_songs}
          edit_proxy={true}
        >
          <Song.SongWithVoting />
        </SongList>

        {
          page_type === 'episode' && sorted_songs.length === 0 ?
          <div className="discussion__subtitle">"{episode_name}" is yet to have any songs added. If you heard a song during this episode, why not help our users out and add it below.</div> :
          (sorted_songs.length === 0) &&
          <div className="discussion__subtitle">This movie either has no credited songs or the playlist information has not yet been released. Scroll down to view the official soundtrack</div>
        }
      </section>
    );
  }
}
