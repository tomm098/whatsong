import React from 'react'; // eslint-disable-line
import { Dropdown } from 'elements';
const ItemPageSongListHeader = ({
    select,
    active_sort,
    sort_variants,
    sorting_enabled,
    title,
    location,
    pushState
    //importBtn = null
  }) =>
    <header className="song-list__header">
      <h2 className="song-list__title">{title}</h2>
      <div className="song-list__header-right">
        {
          sorting_enabled &&
          <div className="song-list__sort-box">
            {/* <div className="song-list__sort-title"></div> */}
            <Dropdown
              self_closing={true}
              trigger={
                <div className="song-list__ordered">{active_sort.title}</div>
              }
            >
              <ul className="dropdown-menu dropdown-menu--right dropdown-menu--arrow-right dropdown-menu--black-gray">
                {
                  sort_variants.map((c, i)=>
                    <li
                      key={i}
                      onClick={()=>select(c)}
                      className={`${c.name === active_sort.name ? 'is-active' : ''}`}
                    >
                      <span>{c.title}</span>
                    </li>
                  )
                }
              </ul>
            </Dropdown>
          </div>
        }
        {
          <div className="add-song-div">
            <button
              className="btn btn-add-song"
              onClick={()=>
                pushState(location.pathname + `/edit/search`)
              }
            > Add Song <span className="green-highlight">+5</span>
            </button>
          </div>
        }
      </div>
    </header>;
export default ItemPageSongListHeader;
