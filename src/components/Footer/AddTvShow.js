import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import AddTvShowForm from './AddTvShowForm/AddTvShowForm';
import {
  Popup } from 'elements';
import { connect } from 'react-redux';

@connect(
  state => ({
    user: state.auth.user
  }),
  {

  }
)

export default class AddTvShow extends Component {
  static propTypes = {
    user: PropTypes.object
  }

  constructor(props) {
    super(props);
    this.state = {
      is_form_opened: false
    };
  }

  render() {
    const { is_form_opened } = this.state;
    const { user } = this.props;
    return (
      (user && user.role === 'ADMIN' || null) &&
      <div className="add-tv_show">
        <a href="#" onClick={e=>{e.preventDefault(); this.setState({is_form_opened: true});}} >+ New TV Show</a>
        <Popup
          is_opened={is_form_opened}
          handleClose={()=>{this.setState({is_form_opened: false});}}
        >
          <AddTvShowForm />
        </Popup>
      </div>
    );
  }
}

