import React, {Component, PropTypes} from 'react';
import {} from './Footer.scss';
import AddTvShow from './AddTvShow';
import AddMovie from './AddMovie';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import AppDownloadImage from './AppDownloadImage';

@connect(
  state => ({
    user: state.auth.user
  }),
  {

  }
)

export default class Footer extends Component {
  static propTypes = {
    user: PropTypes.object
  }

  constructor(props) {
    super(props);
  }

  render() {
    const { user } = this.props;
    return (
      <footer className="footer">
        <div className="footer__left">
          <div className="footer__col">
            <div className="footer__title">
              WE LOVE YOU FOR VISITING US!
            </div>
            <div className="footer__text">We hope you enjoyed your visit today.<br />Please let us know if you have any<br /> suggestions or feedback.</div>
            <AppDownloadImage />
          </div>
        </div>
        <div className="footer__right">
          <div className="footer__row">
            <div className="footer__col">
              <div className="footer__title">
                Contribute
              </div>
                {
                  user &&
                  <ul className="footer__list">
                    <li><AddMovie /></li>
                    <li><AddTvShow /></li>
                  </ul>
                }
            </div>
            <div className="footer__col">
              <div className="footer__title">
              FOLLOW US
              </div>
              <ul className="footer__list">
                <li>
                  <a target="_blank" href="https://www.facebook.com/whatsong.soundtracks">Facebook</a>
                </li>
                <li>
                  <a target="_blank" href="https://twitter.com/what_song">Twitter</a>
                </li>
              </ul>
            </div>
            <div className="footer__col">
              <div className="footer__title">
              Further links
              </div>
              <ul className="footer__list">
                <li>
                  <a href="https://www.what-song.com/blog/">Blog</a>
                </li>
                <li>
                  <Link to="/About-Us">About Us</Link>
                </li>
                <li>
                  <Link to="/Privacy-Policy">Privacy Policy</Link>
                </li>
                <li>
                  <Link to="/Contact-Us">Contact Us</Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="footer__calculate">
          <span>2130 movies</span> <em></em>
          <span>256 tv shows</span> <em></em>
          <span>11.2k episodes</span> <em></em>
          <span>161k songs</span>
        </div>
      </footer>
    );
  }
}
