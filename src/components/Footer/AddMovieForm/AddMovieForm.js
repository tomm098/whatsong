import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './AddMovieForm.scss';
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import { Form } from 'elements';
import AddMovieFormValidation from './AddMovieFormValidation';
import { connect } from 'react-redux';
import * as movieActions from 'redux/modules/movie';

@connect(
  state => ({ // eslint-disable-line

  }),
  {
    add: movieActions.add
  }
)

@reduxForm({
  form: 'addMovieForm',
  validate: AddMovieFormValidation
})

export default class AddMovieForm extends Component {
  static propTypes = {
    handleSubmit: PropTypes.func.isRequired,
    add: PropTypes.func,
    closePopup: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleSubmit(data) {
    const { closePopup, add } = this.props;
    add(data).then(closePopup);
  }

  render() {
    const { handleSubmit } = this.props;
    return (
      <div className="add-movie-form">
        <form action="javascript:void(0)" method="post" onSubmit={handleSubmit(::this.handleSubmit)} >
          <Form.Input
            name="title"
            type="text"
            label={"Name"}
          />
          <Form.Input
            classNameGroup="input-year"
            name="year"
            type="text"
            label={"Year"}
          />
          <Form.DateSelects
            name="time_released"
            caption="Please use US release date as per iMDB"
            label="Release Date"
          />
          <div className="poster-image">
            <div className="poster-image-wrapp">
              <div className="replace-image">
                <Form.FileInput
                  name="poster"
                  label="Poster Image"
                  selected_text="replace image"
                  dropzone_options={{
                    multiple: false,
                    accept: 'image/*'
                  }}
                />
              </div>
            </div>
          </div>
          <div className="banner-image">
            <div className="banner-image-wrapp">
              <div className="upload">
                <Form.FileInput
                  name="banner"
                  label="Banner Image"
                  selected_text="replace image"
                  dropzone_options={{
                    multiple: false,
                    accept: 'image/*'
                  }}
                />
              </div>
              <div className="info-wide">Should be at least 1280px wide.</div>
            </div>
          </div>
          <Form.Input
            name="tags"
            type="text"
            label={"Tags"}
            caption={'Add common mispellings of the titles, or other possible search phrases users might use. Seperate terms with a comma.'}
          />
          <Form.Input
            name="composer"
            type="text"
            label={"Composer"}
          />
          <Form.Input
            name="music_supervisor"
            type="text"
            label={"Supervisor"}
          />
          <Form.Input
            name="imdb_id"
            type="text"
            label={"IMDb Id"}
          />
          <Form.Input
            name="moviedb_id"
            type="text"
            label={"MovieDb Id"}
          />
          <Form.Textarea
            name="intro"
            label="Intro"
          />
          <div className="form-btn__container">
            <Form.Button
              type="submit"
              className="btn--success"
            >
              <span>Submit</span>
            </Form.Button>
          </div>
        </form>
      </div>
    );
  }
}
