import React from 'react';
const image = require('./ios-app-store-download.png');

const AppleStoreImage = () =>
  <div className = "footer__appImage">
    <a href = "https://apps.apple.com/us/app/whatsong-soundtracks/id1472128907?ls=1" target="_blank">
      <img id = "appImage" alt="Download from Apple Store" src={image} />
    </a>
  </div>;

export default AppleStoreImage;
