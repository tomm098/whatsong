import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import AddMovieForm from './AddMovieForm/AddMovieForm';
import { Popup } from 'elements';

export default class AddMovie extends Component {
  static propTypes = {

  }

  constructor(props) {
    super(props);
    this.state = {
      is_form_opened: false
    };
  }

  render() {
    const { is_form_opened } = this.state;
    return (
      <div className="footer__add-movie">
        <a href="#" onClick={e=>{e.preventDefault(); this.setState({is_form_opened: true});}} >+ New Movie</a>
        <Popup
          is_opened={is_form_opened}
          handleClose={()=>{this.setState({is_form_opened: false});}}
        >
          <AddMovieForm />
        </Popup>
      </div>
    );
  }
}

