import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './ExternalLink.scss';

export default class ExternalLink extends Component {
  static propTypes = {
    title: PropTypes.string,
    content: PropTypes.string,
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  openExternalLink() {
    window.open(this.props.content, '_blank');
  }

  render() {
    const { title, content } = this.props;

    if (!content) {
      return null;
    }

    return (
        <span className="externallink" onClick={::this.openExternalLink}>
            {title}
        </span>
    );
  }
}
