import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './BriefIntro.scss';

export default class BriefIntro extends Component {
  static propTypes = {
    title: PropTypes.string,
    content: PropTypes.string,
    children: PropTypes.oneOfType([
      null,
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]),
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { title, content } = this.props;

    if (!content && !this.props.children) {
      return null;
    }

    return (
        <section className="briefintro">
            <header className="briefintro__header">
                <h3 className="briefintro__title">{title}</h3>
            </header>
            <div className="briefintro__container">
                {content}
                {this.props.children}
            </div>
        </section>
    );
  }
}
