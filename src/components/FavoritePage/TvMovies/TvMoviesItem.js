import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './TvMovies.scss';
import { Dropdown } from 'elements';
import { TimeSince } from 'elements';
import { Like } from 'components';
import { Link } from 'react-router';
import { urlGenerator } from 'utils/helpers';

export default class TvMoviesItem extends Component {
  static propTypes = {
    item: PropTypes.object,
    type: PropTypes.string,
    handleDislike: PropTypes.func,
    handleLike: PropTypes.func
  }
  constructor(props) {
    super(props);
    this.state = {

    };
  }
  render() {
    const { item, type, handleDislike, handleLike } = this.props;
    return (
      <div className="tv-movies__item">
        <div className="tv-movies__table">
          <div className="tv-movies__cell">
            <div className="tv-movies__title"><Link to={urlGenerator(type === 'tv' ? 'tv_show' : 'movie', {id: item._id, slug: item.slug || 'no-slug'})} >{item.title}</Link></div>
          </div>
          <div className="tv-movies__cell">
            <div className="tv-movies__song-count">{item.song_count} songs</div>
          </div>
          <div className="tv-movies__cell">
            <div className="tv-movies__dropp">
              <Dropdown
                trigger = {
                  <button className="btn btn--clear"><i className="icon-font icon-font-three-dots"></i></button>
                  }
              >
                <ul className="dropdown-menu dropdown-menu--favorites">
                  <li>
                    <Like
                      onDislike={()=>handleDislike(item._id)}
                      onLike={()=>handleLike({name: item.title})}
                      type={type === 'tv' ? 'tv_show' : 'movie'}
                      id={item._id}
                      liked={item.is_favorited}
                      like_count={item.favourited}
                      modifier="favorites"
                    >
                      <div>
                        <span className="dropp-add">Add to Favorites</span>
                        <span className="dropp-remove">Remove from Favorites</span>
                      </div>
                    </Like>
                  </li>
                </ul>
              </Dropdown>
            </div>
          </div>
          <div className="tv-movies__cell">
            <div className="tv-movies__times"><TimeSince time={item.created_at}/></div>
          </div>
        </div>
      </div>
    );
  }
}
