import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import TvMoviesItem from './TvMoviesItem';
import {} from './TvMovies.scss';
import Waypoint from 'react-waypoint';

export default class TvMoviesList extends Component {
  static propTypes = {
    list: PropTypes.object,
    type: PropTypes.string,
    loadMore: PropTypes.func
  }
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    const { list, loadMore } = this.props;
    if (list && list.data && list.data.length < 15) {loadMore();}
  }

  render() {
    const { list, loadMore, ...rest } = this.props;
    return (
      <div className="tv-movies__list">
        {
          list && list.data && list.data.length ?
            <div>
              {
                list.data.map((c, i)=>
                  <TvMoviesItem key={i} item={c} {...rest}/>
                )
              }
              <Waypoint
                key={list.data.length}
                onEnter={loadMore}
                bottomOffset={-300}
                fireOnRapidScroll={true}
              />
            </div>
           :
          <span className="favorites__no-items">Agh! No one has posted yet. Would you like to be the first?</span>
        }
      </div>
    );
  }
}
