import React, {Component, PropTypes} from 'react';
import { Song, SongList } from 'components';
import Waypoint from 'react-waypoint';

export default class FavoriteSongsList extends Component {
  static propTypes = {
    handleDislike: PropTypes.func,
    pushNotification: PropTypes.func,
    list: PropTypes.object,
    loadMore: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {
      rendered_on_client: false
    };
  }

  componentDidMount() {
    const { list, loadMore } = this.props;
    if (list && list.data && list.data.length < 15) {loadMore();}
  }
  render() {
    const { handleDislike, pushNotification, list, loadMore } = this.props;
    return (
      <div>
        {
          list && list.data && list.data.length ?
            <div>
              <SongList songs={list.data} limit={999999}>
                <Song.FavoriteSong
                  modifier="favorites"
                  handleDislike={handleDislike}
                  handleLike={(...args)=>pushNotification('favorite_song', ...args)}
                />
              </SongList>
              <Waypoint
                key={list.data.length}
                onEnter={loadMore}
                bottomOffset={-300}
                fireOnRapidScroll={true}
              />
            </div>
            :
            <span className="favorites__no-items">Agh! No one has posted yet. Would you like to be the first?</span>
        }
      </div>
    );
  }
}
