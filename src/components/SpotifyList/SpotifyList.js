import React, {Component, PropTypes} from 'react';
import {} from './SpotifyList.scss';

export default class SpotifyList extends Component {
  static propTypes = {
    uri: PropTypes.string,
    view: PropTypes.oneOf(['list', 'coverart']),
    theme: PropTypes.oneOf(['black', 'white']),
    height: PropTypes.string,
    width: PropTypes.string,
    list: PropTypes.array,
    title: PropTypes.string,
    clear: PropTypes.bool
  }

  static defaultProps = {
    view: 'list',
    theme: 'white',
    height: '380',
    width: '300',
    title: 'List of songs'
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  getListUri(list, title) {
    return `spotify:trackset:${title}:${
      this.props.clear ? list.join(',') :
      list.filter(c=>c.spotify).map(c=>c.spotify).join(',')
    }`;
  }

  render() {
    const { view, theme, list, uri, title, ...rest } = this.props;
    const result_uri = uri ? uri : this.getListUri(list, title);
    return (
      <div className="spotify-list">
        <h3>Spotify Playlist</h3>
        <iframe
          {...rest}
          src={`https://embed.spotify.com/?uri=${result_uri}&view=${view}&theme=${theme}`}
          frameBorder="0"
          allowTransparency="true"
        />
      </div>
    );
  }
}
