import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { connect } from 'react-redux';
import * as playerActions from 'redux/modules/player';
import * as songEditActions from 'redux/modules/songEdit';
import * as authModalAction from 'redux/modules/authModal';

@connect(
  state => ({
    active_song: state.player.active_song,
    playing: state.player.playing,
    changed_songs: state.songEdit.changed_songs,
    user_role: state.auth.user && state.auth.user.role,
    username: state.auth.user && state.auth.user.username
  }),
  {
    play: playerActions.play,
    pause: playerActions.pause,
    addSceneDescription: songEditActions.addSceneDescription,
    addSceneTimePlay: songEditActions.addSceneTimePlay,
    editSong: songEditActions.editSong,
    deleteSong: songEditActions.deleteSong,
    openAuthModal: authModalAction.open
  }
)

class ItemPageSongList extends Component {
  static propTypes = {
    songs: PropTypes.array,
    limit: PropTypes.number,
    active_song: PropTypes.object,
    playing: PropTypes.bool,
    play: PropTypes.func,
    pause: PropTypes.func,
    numbered: PropTypes.bool,
    children: PropTypes.node.isRequired,
    edit_proxy: PropTypes.bool,
    changed_songs: PropTypes.object,
    addSceneTimePlay: PropTypes.func,
    addSceneDescription: PropTypes.func,
    user_role: PropTypes.string,
    openAuthModal: PropTypes.func,
    editSong: PropTypes.func,

    inject_node: PropTypes.node,
    inject_index: PropTypes.array
  }

  static defaultProps = {
    limit: 6,
    numbered: false,
    songs: []
  }

  constructor(props) {
    super(props);
    this.state = {
      is_expanded: true
    };
  }

  checkUser() {
    const { user_role, openAuthModal } = this.props;
    if (user_role) {
      return true;
    } else {
      openAuthModal('sign-in');
      return false;
    }
  }

  toggleExpand() {
    this.setState({is_expanded: !this.state.is_expanded});
  }

  addScene(song, scene_description, time_play) {
    if (!scene_description && time_play) {
      this.props.addSceneTimePlay(song, time_play);
    } else if (scene_description && !time_play) {
      this.props.addSceneDescription(song, scene_description);
    } else if (scene_description && time_play) {
      this.props.addSceneTimePlay(song, time_play);
      this.props.addSceneDescription(song, scene_description);
    }
  }

  render() {
    const { songs, limit, active_song, playing, pause, play, numbered, changed_songs, edit_proxy, children = [], inject_node, inject_index, ...rest } = this.props;
    const { is_expanded } = this.state;
    const cutted_songs = songs.slice(0, limit);
    return (
      <article>
        {/*<ul itemScope={true} itemType="http://schema.org/MusicPlaylist" className="list">*/}
        <ul className="list">
          {
            (is_expanded ? songs : cutted_songs).reduce((p, c, i)=>{
              p.push(
                <li key={i}>
                  {/*numbered && <span>{`${i + 1}.`}</span>*/}
                  {numbered && <span>{c.track_number}.</span>}
                  {
                    React.Children.map(children, child=>
                      React.cloneElement(child,
                        {
                          ...rest,
                          ...(edit_proxy ? {addScene: ::this.addScene} : {}),
                          checkUser: ::this.checkUser,
                          song: edit_proxy ? {...c, ...(changed_songs[c._id] || {})} : c,
                          play: (song)=>play(song, is_expanded ? songs : cutted_songs),
                          pause,
                          playing,
                          is_active: active_song && c._id === active_song._id
                        }
                      )
                    )
                  }
                </li>
              );

              if (inject_node && inject_index.includes(i)) {
                p.push(
                  <li key={`inject_on_${i}`}>
                    {inject_node}
                  </li>
                );
              }
              return p;
            }, [])
          }
        </ul>
        {/* {
          (songs.length > limit) &&
          <div onClick={::this.toggleExpand} className="song-list__expand">{is_expanded ? 'COLLAPSE' : `EXPAND ALL ${songs.length} SONGS`}</div>
        } */}
      </article>
    );
  }
}
export default ItemPageSongList;
