import React from 'react';
import {} from './BrowseNavigation.scss';
import { scrollToElement } from 'utils/helpers';

const BrowseNavigation = ({data, active_letters, checkVisibility})=>
   <div className="browse-navigation">
     <div className="browse-navigation__list">
       <ul>
         {
           data.map((c, i)=>
             <li key={i} >
               <span
                 className={active_letters[c] ? 'is-active' : ''}
                 onClick={()=>{scrollToElement('letter_' + c, - document.querySelector('.sticky-inner-wrapper').offsetHeight || 80); checkVisibility();}}
               >
                 {c}
               </span>
             </li>
           )
         }
       </ul>
     </div>
   </div>;
export default BrowseNavigation;
