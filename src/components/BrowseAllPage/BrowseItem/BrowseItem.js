import React from 'react';
import { BrowseAllPage } from 'components';
import {} from './BrowseItem.scss';
import Waypoint from 'react-waypoint';

const BrowseItem = ({item, activateLetter, ...rest}) =>
  <div className="browse-item">
    <div id={'letter_' + item.letter} className="browse-letter">{item.letter}</div>
    <Waypoint
      onEnter={()=>activateLetter(item.letter, true)}
      onLeave={({currentPosition})=>currentPosition === 'below' && activateLetter(item.letter, false)}
      fireOnRapidScroll={true}
    />
    <BrowseAllPage.Table {...rest} content={item.items}/>
    <Waypoint
      onEnter={()=>activateLetter(item.letter, true)}
      onLeave={({currentPosition})=>currentPosition === 'above' && activateLetter(item.letter, false)}
      fireOnRapidScroll={true}
    />
  </div>;
export default BrowseItem;
