import React from 'react';
import {} from './BrowseTable.scss';
import { urlGenerator } from 'utils/helpers';
import { Link } from 'react-router';

const BrowseTable = ({content, link_type}) =>
  <div className="browse-table">
    {
      content.map((c, i)=>
        <div key={i} className="browse-table__coll">
          <Link to={urlGenerator(link_type, {id: c._id, slug: c.slug})}>{c.title}</Link> {c.year}
        </div>
      )
    }
  </div>;
export default BrowseTable;
