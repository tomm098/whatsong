import React, {Component, PropTypes} from 'react';
import { BrowseAllPage } from 'components';

export default class BrowseAllList extends Component {
  static propTypes = {
    data: PropTypes.array
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  shouldComponentUpdate(nextProps) {
    if (nextProps.data.length !== this.props.data.length) {
      return true;
    }
    return false;
  }

  render() {
    const { data, ...rest } = this.props;
    return (
      <div className="">
        {
          data.map((c, i)=>
            <BrowseAllPage.Item {...rest} key={i} item={c}/>
          )
        }
      </div>
    );
  }
}

