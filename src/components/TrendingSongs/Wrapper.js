import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './TrendingSongs.scss';
import { Header, Content } from './';
import { connect } from 'react-redux';
import * as trendingSongsActions from 'redux/modules/trendingSongs';
import * as notificationsActions from 'redux/modules/notifications';

@connect(
  (state, props) => ({
    list: (props.name && state.trendingSongs[props.name]) || null
  }),
  {
    load: trendingSongsActions.load,
    pushNotification: notificationsActions.push
  }
)


export default class TrendingSongsWrapper extends Component {
  static propTypes = {
    list: PropTypes.object,
    name: PropTypes.string.isRequired,
    load: PropTypes.func,
    pushNotification: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  getVariantChanger() {
    const { list: { variants, active_variant }, name, load } = this.props;
    return Object.keys(variants).map(c=>({
      handleChange: () => load(name, c),
      label: variants[c].label,
      is_active: c === active_variant
    }));
  }

  render() {
    const { list, pushNotification } = this.props;
    return (
      <div className="songs-trending">
        {
          list && Object.keys(list).length &&
          <div>
            <Header
              title={list.label}
              variants={this.getVariantChanger()}
            />
            <Content pushNotification={pushNotification} data={list.content} />
          </div>
        }
      </div>
    );
  }
}
