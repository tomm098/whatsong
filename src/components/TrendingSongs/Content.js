import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { SongItem } from './';
import { Scrollbars } from 'react-custom-scrollbars';
import * as playerActions from 'redux/modules/player';
import { connect } from 'react-redux';

@connect(
  state => ({
    song: state.player.active_song,
    playing: state.player.playing
  }),
  {
    play: playerActions.play,
    pause: playerActions.pause
  }
)

export default class TrendingSongsContent extends Component {
  static propTypes = {
    data: PropTypes.array,
    song: PropTypes.object,
    playing: PropTypes.bool,
    play: PropTypes.func,
    pause: PropTypes.func,
    pushNotification: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { data, song, playing, play, pause, pushNotification } = this.props;
    return (
      (data || null) &&
      <div className="songs-trending__content">
        <Scrollbars
          renderTrackVertical={props=><div {...props} className="scroll-vertical"></div>}
          renderThumbVertical={props=><div {...props} className="scrollbar"></div>}
        >
          <ul className="songs-trending__list">
          {
            data.map((c, i)=>
                <li key={i}>
                  <SongItem pushNotification={pushNotification} item={c} active={song && (song._id === c._id)} playing={playing} handlePlay={play} handlePause={pause}/>
                </li>
            )
          }
          </ul>
        </Scrollbars>
      </div>

    );
  }
}

