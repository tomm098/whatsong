import React from 'react';
import { Dropdown } from 'elements';

const TrendingSongsHeader = ({title, variants}) =>
  <div className="songs-trending__header">
    <h3 className="songs-trending__header-title">{title}</h3>
    <div className="songs-trending__header-week">
      <Dropdown
        self_closing={true}
        trigger = {
            <div className="songs-trending__trigger">{variants.find(c=>c.is_active).label}</div>
          }
      >
        <ul className="dropdown-menu dropdown-menu--right dropdown-menu--arrow-right dropdown-menu--black-gray">
          {
            variants.map((c, i)=>
              <li key={i}> <span onClick={c.handleChange} className={` ${c.is_active ? 'is-active' : ''}`}>{c.label}</span></li>
            )
          }
        </ul>
      </Dropdown>
    </div>
  </div>;
export default TrendingSongsHeader;
