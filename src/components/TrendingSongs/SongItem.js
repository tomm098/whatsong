import React from 'react';
import { Link } from 'react-router';
import { Like } from 'components';
import { urlGenerator } from 'utils/helpers';

const TrendingSongsItem = ({item, active, playing, handlePlay, handlePause, pushNotification}) =>
  <div className={'songs-trending__item' + (active && playing ? ' is-playing' : '')}>
    <div
      onClick={()=>{
        if (active) {
          playing ? handlePause() : handlePlay();
        } else {
          handlePlay(item);
        }
      }}
      className="songs-trending__item-left"
    >
      <div className="songs-trending__album">{item.title}</div>
      <div className="songs-trending__title">{item.artist && item.artist.name}</div>
      {item.from && item.from.name && <div className="songs-trending__link"><Link to={urlGenerator(item.from.type, item.from)}>{item.from.name}</Link></div>}
    </div>
    <div className="songs-trending__item-right">
      <Like
        type="song"
        id={item._id}
        liked={item.is_favorited}
        like_count={item.favourited}
        modifier = {'trending'}
        onLike={()=>pushNotification('favorite_song', {name: item.title, by: item.artist && item.artist.name})}
      />
    </div>
  </div>;
export default TrendingSongsItem;
