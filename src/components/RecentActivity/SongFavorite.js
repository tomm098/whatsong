import React from 'react';
import { Dropdown } from 'elements';
import { Like, Headphones } from 'components';
const image = require('../../images/default-avatar.svg');
import { TimeSince } from 'elements';
import { Link } from 'react-router';
import { urlGenerator, fireEvent } from 'utils/helpers';
import { Song } from 'components';

const RecentActivitySongFavorite = ({item, pushNotification, play, pause, playing, active}) =>
  <div onMouseLeave={()=>fireEvent(window.document.body, 'click')} className="recent-activity__content">
    <div className="recent-activity__left">
      <div className="recent-activity__minuts"><TimeSince time={item.created_at} preset="min3" plural={false}/></div>
      <div
        className="recent-activity__img"
        style={{backgroundImage: (item.user && item.user.photo) ? `url(${item.user.photo})` : `url(${image})`}}
      ></div>
    </div>
    <div className="recent-activity__right">
      <div className="recent-activity__song">
        {`${item.user.username} favorited “${item.title}” ${item.artist && item.artist.name ? `by ${item.artist.name}` : '' } `}
        {item.from && <Link to={urlGenerator(item.from.type, item.from)}>{item.from.name}</Link>}.
      </div>
      <div className="recent-activity__like-box">
        <Like
          modifier = {'recent'}
          type="song"
          id={item._id}
          liked={item.is_favorited}
          like_count={item.favourited}
          onLike={()=>pushNotification('favorite_song', {name: item.title, by: item.artist && item.artist.name})}
        />
        <Headphones count={item.played_time}/>
      </div>
      <div className="recent-activity__btn-box">
        <div className="recent-activity__play-song">
          <button
            className="btn btn--success"
            onClick={()=>{
              if (active) {
                playing ? pause() : play();
              } else {
                play(item);
              }
            }}
          >
            {`${active && playing ? 'PAUSE' : 'PLAY' } SONG`}
          </button>
        </div>
        <Dropdown
          trigger = {
            <div className="recent-activity__download">
              <button className="btn btn--download">DOWNLOAD</button>
            </div>
            }
        >
          <Song.DownloadList song={item} className="dropdown-menu dropdown-menu--right dropdown-menu--arrow-center dropdown-menu--download"/>
        </Dropdown>
      </div>
    </div>
  </div>;
export default RecentActivitySongFavorite;
