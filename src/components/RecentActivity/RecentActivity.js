import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './RecentActivity.scss';
import { SongFavorite } from './';
import { connect } from 'react-redux';
import * as recentActivityActions from 'redux/modules/recentActivity';
import * as notificationsActions from 'redux/modules/notifications';
import * as playerActions from 'redux/modules/player';

@connect(
  state => ({
    data: state.recentActivity.data,
    loaded: state.recentActivity.loaded,
    loading: state.recentActivity.loading,

    active_song: state.player.active_song,
    playing: state.player.playing
  }),
  {
    load: recentActivityActions.load,

    play: playerActions.play,
    pause: playerActions.pause,
    pushNotification: notificationsActions.push
  }
)

export default class RecentActivity extends Component {
  static propTypes = {
    data: PropTypes.array,
    loaded: PropTypes.bool,
    loading: PropTypes.bool,
    load: PropTypes.func,

    active_song: PropTypes.object,
    play: PropTypes.func,
    pause: PropTypes.func,
    playing: PropTypes.bool,
    pushNotification: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    const { loaded, load } = this.props;
    if (!loaded) {
      load();
    }
  }

  getActivityOfType(item) {
    const { pushNotification, active_song, play, pause, playing, } = this.props;

    switch (item.action) {
      case 'like song':
        return <SongFavorite {...{pushNotification, play, pause, playing, item}} active={active_song && (active_song._id === item._id)}/>;
      default: return null;
    }
  }

  render() {
    const { data } = this.props;
    return (
      <section className="recent-activity">
        <header className="recent-activity__header">
          <h2>Recent Activity</h2>
        </header>
        {
          data.map((c, i)=>
          <div key={i}>{this.getActivityOfType(c)}</div>
          )
        }
      </section>
    );
  }
}
