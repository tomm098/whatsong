import React from 'react';
import { Link } from 'react-router';
const logo = require('./text-logo-white.png');

const HeaderLogo = ()=>
  <div className="header__logo">
    <Link to="/">
      <img alt="What-Song Soundtracks - Logo" src={logo} />
      {/* <span>what-song</span> */}
      <i className="icon-font icon-font-logo"></i>
    </Link>
  </div>;
export default HeaderLogo;
