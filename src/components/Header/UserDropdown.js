import React from 'react';
import { Link } from 'react-router';
import { Tooltip } from 'elements';
import { User } from 'components';

const HeaderUserDropdown = ({logOut, user})=>
  <div className="header__user-info">
    <Tooltip
      trigger={
        <User.Card user={user} />
      }
    >
      <ul className="dropdown-menu dropdown-menu--black dropdown-menu--left dropdown-menu--header-desktop dropdown-menu--right-header">
        <li>
          <Link to={'/Favorites/' + user.username}>favorites</Link>
        </li>
        <li>
          <Link to="/Settings">settings</Link>
        </li>
        <li>
          <Link to={'/Profile/' + user.username}>profile</Link>
        </li>
        <li>
          <span onClick={logOut}>log out</span>
        </li>
      </ul>
    </Tooltip>
  </div>;

export default HeaderUserDropdown;
