import React from 'react';
import { Link } from 'react-router';
import { Tooltip } from 'elements';

const HeaderStaticLinks = ()=>
  <nav className="header__menu hidden-xs">
    <ul className="header__list">
      <li>
        <Link to="/movies">Movies</Link>
      </li>
      <li>
        <Link to="/television">TV Shows</Link>
      </li>
      <li className="header__li-lg">
        <Link to="/Trending-Music">Trending Music</Link>
      </li>
      <li>
        <Tooltip
          trigger={
            <div className="more-links">...</div>
            }
        >
          <ul className="dropdown-menu dropdown-menu--black dropdown-menu--header-desktop">
            <li className="header__li-md">
              <Link to="/Trending-Music">Trending Music</Link>
            </li>
            <li>
              <Link to="/movies/browse">Browse Movies</Link>
            </li>
            <li>
              <Link to="/tvshow/browse">Browse TV Shows</Link>
            </li>
          </ul>
        </Tooltip>
      </li>
    </ul>
  </nav>;

export default HeaderStaticLinks;
