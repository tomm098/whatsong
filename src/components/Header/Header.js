import React, {Component, PropTypes} from 'react';
import {} from './Header.scss';
import { connect } from 'react-redux';
import * as authActions from '../../redux/modules/auth';
import * as authMoadlActions from '../../redux/modules/authModal';

import Logo from './Logo';
import { MiniSearch, Notifications } from 'components';
import StaticLinks from './StaticLinks';
import UserDropdown from './UserDropdown';
import MobileDropdown from './MobileDropdown';

@connect(
  state => ({
    user: state.auth.user
  }),
  {
    logOut: authActions.logOut,
    openModal: authMoadlActions.open
  }
)

export default class Header extends Component {
  static propTypes = {
    logOut: PropTypes.func,
    openModal: PropTypes.func,
    user: PropTypes.object
  }

  constructor(props) {
    super(props);
  }

  render() {
    const { user, logOut, openModal } = this.props;
    return (
      <header className="header">
        <div className="header__panel">
          <div className="header__left">
            <div className="header__box">
              <MobileDropdown
                user={user}
                logIn={()=>openModal('sign-in')}
                signUp={()=>openModal('sign-up')}
                logOut={logOut}
              />
              <Logo />
              <MiniSearch />
            </div>
            <StaticLinks />
          </div>
          <div className="header__right hidden-xs">
            <ul className="nav navbar-nav navbar-right">
              {
                user ?
                <li className="header__user">
                  <UserDropdown
                    user={user}
                    logOut={logOut}
                  />
                </li>
                :
                <li className="header__btn-login">
                  <button className="btn btn--transparent" onClick={()=>openModal('sign-in')} >Login</button>
                  <button className="btn btn--transparent" onClick={()=>openModal('sign-up')} >Sign Up</button>
                </li>
              }
            </ul>
          </div>
          <Notifications />
        </div>
      </header>
    );
  }
}

