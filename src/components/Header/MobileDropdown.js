import React from 'react';
import { Link } from 'react-router';
import { Dropdown } from 'elements';

const HeaderMobileDropdown = ({logIn, signUp, logOut, user})=>
  <div className="header__user-info">
    <Dropdown
      self_closing={true}
      trigger={
        <div className="header__mobile navbar-default navbar-toggle">
          <button type="button" className="navbar-toggle collapsed">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
        </div>
      }
      >
      <ul className="dropdown-menu dropdown-menu--red dropdown-menu--left dropdown-menu--header-mobile">
        <li>
          <Link to="/movies">movies</Link>
        </li>
        <li>
          <Link to="/television">television</Link>
        </li>
        <li>
          <Link to="/Trending-Music">trending music</Link>
        </li>
        <li>
          <Link to="/movies/browse">browse movies</Link>
        </li>
        <li>
          <Link to="/tvshow/browse">browse shows</Link>
        </li>
        <li>
          <hr/>
        </li>
        {
          user &&
          <li>
            <Link to={'/Favorites/' + user.username}>favorites</Link>
          </li>
        }
        {
          user &&
          <li>
            <Link to="/Settings">settings</Link>
          </li>
        }
        {
          user &&
          <li>
            <Link to={'/Profile/' + user.username}>profile</Link>
          </li>
        }
        {
          !user &&
          <li>
            <span onClick={logIn}>log in</span>
          </li>
        }
        {
          !user &&
          <li>
            <span onClick={signUp}>sign up</span>
          </li>
        }
        {
        user &&
          <li>
            <span onClick={logOut}>log out</span>
          </li>
        }
      </ul>
    </Dropdown>
  </div>;

export default HeaderMobileDropdown;
