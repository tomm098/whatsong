import React, {Component, PropTypes} from 'react';
import { Form } from 'elements'; // eslint-disable-line
import { reduxForm, SubmissionError, formValueSelector } from 'redux-form'; // eslint-disable-line
import { connect } from 'react-redux';
import { SourceSelect } from 'components';
import SettingsFormValidation from './SettingsFormValidation';
import * as authActions from 'redux/modules/auth';
import * as notificationsActions from 'redux/modules/notifications';

const image = require('../../images/default-avatar.svg');
const selector = formValueSelector('SettingsPageForm');
@reduxForm({
  form: 'SettingsPageForm',
  validate: SettingsFormValidation
})

@connect(
  state => ({
    user: state.auth.user,
    photo: selector(state, 'photo') // eslint-disable-line
  }),
  {
    updateProfile: authActions.updateProfile,
    updatePassword: authActions.updatePassword,
    pushNotification: notificationsActions.push
  }
)

export default class SettingsForm extends Component {

  static propTypes = {
    user: PropTypes.object,
    initialize: PropTypes.func,
    photo: PropTypes.any,
    handleSubmit: PropTypes.func.isRequired,
    updateProfile: PropTypes.func,
    updatePassword: PropTypes.func,
    pushNotification: PropTypes.func
  }


  componentDidMount() {
    const {initialize, user} = this.props;
    initialize({
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email
    });
  }

  handleSubmit(data) {
    const { user, updateProfile, updatePassword } = this.props;
    const promises = [];

    const filtered_data = {};
    Object.keys(data).forEach(c=>{
      const item = data[c];
      if (item && item !== user[c]) {
        filtered_data[c] = c === 'photo' ? item[0] : item;
      }
    });

    const {old_password, new_password, confirm_password, ...rest_data} = filtered_data; // eslint-disable-line

    if (Object.keys(rest_data).length) {
      promises.push(updateProfile(rest_data));
    }

    if (new_password && old_password) {
      promises.push(updatePassword(new_password, old_password));
    }

    const success = (response) => { // eslint-disable-line
      this.props.pushNotification('settings_saved', {});
    };

    const error = (err) => { // eslint-disable-line
      throw new SubmissionError({
        _error: err.error.message
      });
    };

    return Promise.all(promises).then().then(success, error);
  }

  render() {
    const { user, photo, handleSubmit } = this.props;
    const user_photo = (photo && photo.length ? photo[0].preview : user.photo || image);
    return (
      <form action="" method="post" onSubmit={handleSubmit(::this.handleSubmit)}>
        <ul className="settings__list">
          <li>
            <p>Username</p>
            <span>{user.username}</span>
          </li>
          <li>
            <p>Avatar</p>
            <div className="settings__avatar-wrapp">
              <div className="settings__avatar" style={{backgroundImage: user_photo ? `url(${user_photo})` : ''}}>
                <Form.FileInput
                  name="photo"
                  default_text=""
                  selected_text=""
                  className="settings__file-input"
                  dropzone_options={{
                    multiple: false,
                    accept: 'image/*'
                  }}
                />
              </div>
            </div>
          </li>

          <li>
            <p>Password</p>
            <Form.Input
              placeholder="Old password"
              name={"old_password"}
              type={"password"}
            />
            <Form.Input
              placeholder="New password"
              name={"new_password"}
              type={"password"}
            />
            <Form.Input
              placeholder="Confirm password"
              name={"confirm_password"}
              type={"password"}
            />
          </li>

          <li>
            <p>First name</p>
            <Form.Input
              placeholder="First name"
              name={"first_name"}
              type={"text"}
            />
          </li>
          <li>
            <p>Last name</p>
            <Form.Input
              placeholder="Last name"
              name={"last_name"}
              type={"text"}
            />
          </li>
          <li>
            <p>Email</p>
            <Form.Input
              placeholder="Email"
              name={"email"}
              type={"email"}
            />
          </li>
          <li>
            <p>Default player</p>
            <SourceSelect />
          </li>
        </ul>
        <Form.Button type="submit" className="btn--success" >Save</Form.Button>
      </form>
    );
  }
}
