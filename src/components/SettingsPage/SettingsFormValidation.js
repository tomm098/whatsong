import memoize from 'lru-memoize';
import { createValidator, minLength, email, match, dependsOn } from 'utils/validation';

const SettingsFormValidation = createValidator({
  first_name: [minLength(2)],
  last_name: [minLength(2)],
  email: [email],
  new_password: [match('confirm_password'), minLength(4)],
  old_password: [dependsOn('new_password'), minLength(4)],
  confirm_password: [dependsOn('new_password'), minLength(4)]
});
export default memoize(10)(SettingsFormValidation);
