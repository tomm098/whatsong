import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './SourceSelect.scss';
import { Dropdown } from 'elements';
import { connect } from 'react-redux';
import * as playerActions from 'redux/modules/player';
import cookie from 'react-cookie';

const variants = ['youtube', 'itunes'];

@connect(
  state => ({
    active_source: state.player.source
  }),
  {
    setSource: playerActions.setSource
  }
)

export default class SourceSelect extends Component {
  static propTypes = {
    active_source: PropTypes.string,
    setSource: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {
    const saved_source = cookie.load('player_source');
    if (saved_source && (this.props.active_source !== saved_source)) {
      this.props.setSource(saved_source);
    }
  }

  render() {
    const { active_source, setSource } = this.props;
    return (
      <div className="source-select">
        <div className="source-select__content">
          <div className="source-select__left">PLAYING SONGS USING:</div>
          <div className="source-select__right">
            <Dropdown
              trigger = {
                <div className="source-select__trigger">{active_source}</div>
              }
              self_closing={true}
            >
              <ul className="dropdown-menu dropdown-menu--right dropdown-menu--arrow-right dropdown-menu--black-gray">
                {
                  variants.map((c, i)=>
                    <li
                      key={i}
                      onClick={()=>setSource(c)}
                    >
                      <span>{c}</span>
                    </li>
                  )
                }
              </ul>
            </Dropdown>
          </div>
        </div>
      </div>
    );
  }
}
