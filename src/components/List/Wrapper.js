import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { connect } from 'react-redux';
import * as listActions from '../../redux/modules/lists'; // eslint-disable-line
import { Header, Content, Pagination } from './';
import {} from './ContentList.scss';
import * as notificationsActions from 'redux/modules/notifications';

@connect(
  (state, props) => ({
    list: (props.name && state.lists[props.name]) || null
  }),
  {
    load: listActions.load,
    paginate: listActions.paginate,
    pushNotification: notificationsActions.push
  }
)

export default class ListWrapper extends Component {
  static propTypes = {
    list: PropTypes.object,
    name: PropTypes.string.isRequired,
    load: PropTypes.func,
    paginate: PropTypes.func,
    pushNotification: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  getVariantChanger() {
    const { list: { variants, active_variant }, name, load } = this.props;
    return Object.keys(variants).map(c=>({
      handleChange: () => load(name, c),
      label: variants[c].label,
      is_active: c === active_variant
    }));
  }

  render() {
    const { list, paginate, name, pushNotification, id } = this.props;
    return (
      <section className="content-list" id={id || ''}>
        {
          list && Object.keys(list).length &&
          <div className="content-list__container">
            <Header
              title={list.label}
              variants={this.getVariantChanger()}
              links={list.static_links}
              variants_view={list.variants_view}
            />
           <Content
             pushNotification={(...args)=>pushNotification(list.notification_type || 'favourite_unknown', ...args)}
             url_type={list.url_type}
             like_type={list.like_type}
             data={list.content}
             loading={list.loading}
           />
           <Pagination
             current_offset={list.offset}
             page_size={list.page_size}
             items_count={list.items_count}
             paginate={offset=>paginate(name, offset)}
           />
          </div>
        }
      </section>
    );
  }
}
