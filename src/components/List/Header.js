import React from 'react';
import { Link } from 'react-router';
import { SimpleSelect } from 'elements';

const ListHeader = ({title, variants, links, variants_view}) =>
  <div className="content-list__category">
    <header className="content-list__header">
      <h2>{title}</h2>
    </header>
    <div className="content-list__btn-box">
      {
        variants_view === 'select' ?
          <SimpleSelect
            list={variants.map((c, i)=>({label: c.label, value: i}))}
            onChange={(e)=>variants[e.target.value] && variants[e.target.value].handleChange()}
          /> :
          variants.map((c, i)=>
            <button key={i} onClick={c.handleChange} className={`btn btn--clear ${c.is_active ? 'is-active' : ''}`}>{c.label}</button>
          )
      }
      {
        links && links.map((c, i)=><Link key={i} to={c.to}>{c.label}</Link>)
      }
    </div>
  </div>;
export default ListHeader;
