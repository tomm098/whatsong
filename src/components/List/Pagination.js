import React from 'react';
import {} from './Pagination.scss';

const ListPagination = ({current_offset, page_size, paginate, items_count}) =>
  <div className="pagination">
    {
      (current_offset > 0) && <button className="btn btn--clear btn--left" onClick={()=>paginate(current_offset - page_size)}>{'< previous page'}</button>
    }
    {
      ((current_offset + page_size) < items_count ) && <button className="btn btn--clear btn--right" onClick={()=>paginate(current_offset + page_size)}>{'next page >'}</button>
    }
  </div>;
export default ListPagination;
