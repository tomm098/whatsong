import React from 'react';
import { ListItem } from './';
import {} from './Grid.scss';
import { urlGenerator } from 'utils/helpers';
import { Loader } from 'components';

const ListContent = ({data, url_type, like_type, pushNotification, loading}) =>
  <article className="grid container">
    {(loading || null) && <div className="grid__loader-wrapper"><Loader /></div> }
    <div className="row">
    {
      data.map((c, i)=>
        <ListItem pushNotification={pushNotification} url={urlGenerator(url_type, {id: c._id, slug: c.slug})} like_type={like_type} item={c} key={i} />
      )
    }
    </div>
  </article>;

export default ListContent;
