import React from 'react';
import {} from './Grid.scss';
import { Like } from 'components';
import { Link } from 'react-router';

const ListItem = ({item, url, like_type, pushNotification}) =>
  <div className="col-xs-12 col-sm-4 col-md-4 col-lg-3">
    <Link to={url} className="grid__item" style={{backgroundImage: `url(${item.poster_url})`}}>
      {
        !!item.song_count &&
        <div className="grid__songs-count">
          {item.song_count + (parseInt(item.song_count, 10) === 1 ? ' song' : ' songs')}
        </div>
      }
      <div className="grid__like">
      <div className="grid__like-wrapp">
        <span className="grid__title">{item.title}</span>
        <Like
          modifier = {'grid'}
          type={like_type}
          id={item._id}
          liked={item.is_favorited}
          like_count={item.favourited}
          onLike={()=>pushNotification({name: item.title})}
        />
      </div>
    </div>
    </Link>
  </div>;
export default ListItem;
