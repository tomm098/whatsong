import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import * as movieActions from 'redux/modules/movie';
import {connect} from 'react-redux';
import './SoundtrackInfo.scss';

@connect(
  state => ({
    dispatch: state.dispatch,
    movie: state.movie.data,
    error: state.movie.error,
  })
)
export default class SoundtrackInfo extends Component {
  static propTypes = {
    movie: PropTypes.object,
    dispatch: PropTypes.func,
    onClose: PropTypes.func,
    error: PropTypes.any,
    params: PropTypes.object
  };

  constructor(props) {
    super(props);
    this.state = {
      error: false,
      areaValue: props && props.movie && props.movie.movie && props.movie.movie.soundtrack_info ? props.movie.movie.soundtrack_info : ''
    };
    this.modalTextArea = null;

    this.onChangeText = this.onChangeText.bind(this);
    this.addLink = this.addLink.bind(this);
    this.submitEditSoundtrackInfo = this.submitEditSoundtrackInfo.bind(this);
    this.handleCallback = this.handleCallback.bind(this);
  }

  componentWillUnmount() {
    this.setState({areaValue: '', error: false});
  }

  onChangeText(e) {
    this.setState({areaValue: e.target.value, error: false});
  }

  addLink() {
    const pos = this.modalTextArea.selectionStart;
    const linkItem = '<a href="https://google.com">Change this text</a>';
    let {areaValue} = this.state;
    areaValue = areaValue ? areaValue : '';
    areaValue = [areaValue.slice(0, pos), linkItem, areaValue.slice(pos)].join('');
    this.setState({areaValue});
  }

  submitEditSoundtrackInfo() {
    const {movie, dispatch} = this.props;
    const {areaValue} = this.state;
    const id = movie && movie.movie && movie.movie._id;
    dispatch(movieActions.edit_soundtrack_info(id, areaValue, this.handleCallback));
  }

  handleCallback(status) {
    const {onClose} = this.props;
    if (status && status === 'success') {
      this.setState({error: false});
      onClose && onClose();
    } else if (status && status === 'error') {
      this.setState({error: true});
    }
  }

  render() {
    const {areaValue, error} = this.state;
    return (
      <div>
        {error && <div className="modal-error-text">Something went wrong</div>}
        <button className="btn btn--primary" onClick={this.addLink}>Add link</button>
        <textarea
          ref={el => this.modalTextArea = el}
          className={'form-control modal-text-area' + (error ? ' modal-error' : '')}
          rows={5}
          autoComplete={false}
          placeholder={'Add soundtrack info here'}
          value={areaValue || ''}
          onChange={this.onChangeText}
        />
        <button className="btn btn--success btn-in-modal" onClick={this.submitEditSoundtrackInfo}>Submit</button>
      </div>
    );
  }
}
