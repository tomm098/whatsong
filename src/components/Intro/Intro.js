import React, { PropTypes } from 'react';
import {} from './Intro.scss';

const Intro = ({ text }) => text ?
  <div className="intro">
    {text}
  </div>
  : null;
Intro.propTypes = {
  text: PropTypes.string
};

export default Intro;
