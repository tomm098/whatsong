import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Like.scss';
import { connect } from 'react-redux';
import { nFormatter } from 'utils/helpers';
import * as likesActions from 'redux/modules/likes';
import * as authMoadalActions from 'redux/modules/authModal';

@connect(
  (state, props) => ({
    changed: state.likes[props.type] && state.likes[props.type][props.id],
    user: state.auth.user
  }),
  {
    like: likesActions.like,
    openAuthModal: authMoadalActions.open
  }
)

export default class Like extends Component {
  static propTypes = {
    modifier: PropTypes.string,
    type: PropTypes.string,
    id: PropTypes.number,
    liked: PropTypes.bool,
    like_count: PropTypes.number,
    like: PropTypes.func,
    openAuthModal: PropTypes.func,
    user: PropTypes.object,
    onLike: PropTypes.func,
    onDislike: PropTypes.func,
    children: PropTypes.node
  }

  static defaultProps = {
    onLike: ()=>{},
    onDislike: ()=>{}
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleLike(liked, like_count) {
    const { id, type, like, user, openAuthModal, onLike, onDislike } = this.props;
    if (!user) {return openAuthModal('sign-in');}
    like(type, id, !liked, like_count).then(()=>liked ? onDislike() : onLike());
  }

  render() {
    const { modifier, changed, children } = this.props;
    const { liked, like_count = 0 } = changed || this.props;
    return (
      <div onClick={()=>this.handleLike(liked, like_count)} className={`like ${modifier ? 'like--' + modifier : '' } ${liked ? 'is-liked' : ''}`}>
        <i className="icon-font icon-font-heart"></i>
        <i className="icon-font icon-font-heart-empty"></i>
        <span className="like__like-text">{nFormatter(like_count, 1)}</span>
        {children}
      </div>
    );
  }
}
