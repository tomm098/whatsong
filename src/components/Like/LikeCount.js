import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { connect } from 'react-redux';
import { nFormatter } from 'utils/helpers';

@connect(
  (state, props) => ({
    changed: state.likes[props.type] && state.likes[props.type][props.id]
  }),
  {

  }
)

export default class LikeCount extends Component {
  static propTypes = {
    type: PropTypes.string,
    id: PropTypes.number,
    liked: PropTypes.bool,
    like_count: PropTypes.number
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { changed } = this.props;
    const { like_count = 0 } = changed || this.props;
    return (
      <span >{nFormatter(like_count, 1)}</span>
    );
  }
}
