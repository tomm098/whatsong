import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import TypedList from './MiniSearchTypedList';
import {} from './Minisearch.scss';

export default class MiniSearchResults extends Component {
  static propTypes = {
    lists: PropTypes.array
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { lists, ...rest } = this.props;
    return (
      <div className="mini-search__drop">
        {
          lists.filter(c=>c.data.length).map((c, i)=>
            <TypedList key={i} {...rest} list={c}/>
          )
        }
      </div>
    );
  }
}
