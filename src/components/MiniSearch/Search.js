import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import { Form } from 'elements';
import { reduxForm, SubmissionError, formValueSelector } from 'redux-form'; // eslint-disable-line
import SerarchResults from './MiniSearchResults';
import {} from './Minisearch.scss';
import { connect } from 'react-redux';
const selector = formValueSelector('miniSearch');
import * as miniSearchActions from 'redux/modules/miniSearch';
import { debounce } from 'utils/helpers';
import { push } from 'react-router-redux';
import { Loader } from 'components';

@connect(
  state => ({
    query: selector(state, 'query'),
    results: state.miniSearch.data,
    loading: state.miniSearch.loading
  }),
  {
    search: miniSearchActions.search,
    clear: miniSearchActions.clear,
    pushState: push
  }
)

@reduxForm({
  form: 'miniSearch'
})


export default class HeaderSearch extends Component {
  static propTypes = {
    search: PropTypes.func,
    clear: PropTypes.func,
    results: PropTypes.array,
    pushState: PropTypes.func,
    submitting: PropTypes.bool.isRequired,
    loading: PropTypes.bool,
    // form validation
    active: PropTypes.string,
    dirty: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired,
    reset: PropTypes.func,
    query: PropTypes.string,
    dispatch: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.handleSearch = debounce(this.handleSearch, 600).bind(this);
    this.close = this.close.bind(this);
    this.state = {
      is_mobile_open: false
    };
  }

  componentDidMount() {
    window.addEventListener('click', this.close);
  }

  componentWillReceiveProps(newProps) {
    if (this.props.query !== newProps.query) {
      this.handleSearch();
    }
  }

  componentWillUnmount() {
    window.removeEventListener('click', this.close);
  }

  close(e) {
    const { results } = this.props; // eslint-disable-line
    if (results && results.length && !this._container.contains(e.target)) {
      this.closeSearch();
    }
  }

  closeSearch() {
    const {clear, reset, dispatch } = this.props;
    clear();
    dispatch(reset());
  }

  toggleMobile() {
    this.setState({is_mobile_open: !this.state.is_mobile_open});
  }

  handleSearch() {
    const { query, search, clear } = this.props;
    if (query && query.length > 3) {
      search(query);
    } else {
      clear();
    }
  }

  handleSubmit(data) {
    const { pushState } = this.props;
    if (data.query) {
      pushState(`/Activity/Search?q=${encodeURIComponent(data.query)}`);
      this.closeSearch();
    }
  }

  render() {
    const { results, handleSubmit, loading } = this.props;
    return (
      <div ref={c=>this._container = c} className={'header__search ' + (this.state.is_mobile_open ? ' is-mobile-open-search' : '')} >
        <div className="header__search-icon" onClick={::this.toggleMobile} >
          <i className="icon-font icon-font-search2"></i>
        </div>
        {loading && <Loader type="type1"/>}
        <form onSubmit={handleSubmit(::this.handleSubmit)}>
          <div className="header__search-wrapp">
            <Form.Input
              placeholder="Search titles"
              type="text"
              name="query"
              className={'header__input-search form-control' + (this.state.is_mobile_open ? ' is-mobile-open' : '')}
            />
          </div>
        </form>
        {
          results && !!results.length &&
          <SerarchResults lists={results} closeSearch={()=>this.closeSearch()}/>
        }
      </div>
    );
  }
}
