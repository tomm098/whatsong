import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './Minisearch.scss';
import { urlGenerator, search2urlTypes } from 'utils/helpers';
import { Link } from 'react-router';

export default class MiniSearchTypedList extends Component {
  static propTypes = {
    list: PropTypes.object,
    closeSearch: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    const { list, closeSearch } = this.props;
    return (
      <div className="mini-search__drop-wrapp">
        <div className="mini-search__title">{list.group}</div>
        <ul className="mini-search__list">
          {
            list.data && list.data.map((c, i)=>
              <li onClick={closeSearch} key={i}>
                <Link
                  to={
                    urlGenerator(
                      search2urlTypes(list.group),
                      {id: c._id, slug: c.slug}
                    )
                  }
                >
                  {c.title || c.name} <span>{c.year || ''}</span>
                </Link>
              </li>
            )
          }
        </ul>
      </div>
    );
  }
}
