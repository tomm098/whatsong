import React, { Component, PropTypes } from 'react';
// import {} from './AmazonAd.scss';

export default class AmazonAd extends Component {
  static propTypes = {
    link: PropTypes.string,
    title: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  shouldComponentUpdate() {
    return false;
  }

  render() {
    const { link, title } = this.props;
    return (
      <div id="adunit0">
        <script dangerouslySetInnerHTML={{
          __html: `
            amzn_assoc_placement = "adunit0";
            amzn_assoc_search_bar = "false";
            amzn_assoc_tracking_id = "whatsong-20";
            amzn_assoc_ad_mode = "search";
            amzn_assoc_ad_type = "smart";
            amzn_assoc_marketplace = "amazon";
            amzn_assoc_region = "US";
            amzn_assoc_title = "Shop Related Products";
            amzn_assoc_default_search_phrase = "${title}";
            amzn_assoc_default_category = "All";
            amzn_assoc_linkid = "ef7f50fad6492072e701a8951ab3cb01";
          `
        }}/>
        <script src={link} />
      </div>
    );
  }
}
