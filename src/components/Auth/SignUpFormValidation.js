import memoize from 'lru-memoize';
import { createValidator, required, minLength, email, match } from 'utils/validation';

const SignUpValidation = createValidator({
  password: [required, minLength(4)],
  confirm: [required, match('password')],
  username: [required],
  email: [required, email]
});
export default memoize(10)(SignUpValidation);
