import memoize from 'lru-memoize';
import { createValidator, required} from 'utils/validation';

const SendRestoreValidation = createValidator({
  login: [required]
});
export default memoize(10)(SendRestoreValidation);
