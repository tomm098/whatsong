import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import * as authActions from '../../redux/modules/auth';
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import SendRestoreFormValidation from './SendRestoreFormValidation';
import * as authMoadlActions from '../../redux/modules/authModal';
import * as notificationsActions from 'redux/modules/notifications';
import { Form } from 'elements';

@connect(
  () => ({}),
  {
    sendRestore: authActions.sendPasswordRestore,
    pushNotification: notificationsActions.push,
    closeModal: authMoadlActions.close
  }
)

@reduxForm({
  form: 'SendRestoreForm',
  validate: SendRestoreFormValidation
})

export default class SendRestoreForm extends Component {
  static propTypes = {
    sendRestore: PropTypes.func,
    pushNotification: PropTypes.func,
    closeModal: PropTypes.func,
    // form
    error: PropTypes.string,
    pushState: PropTypes.func,
    submitting: PropTypes.bool.isRequired,
    // form validation
    active: PropTypes.string,
    dirty: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleSubmit(data) {

    const success = (response) => { // eslint-disable-line
      this.props.closeModal();
      this.props.pushNotification('restore_sent', {}, 5e3);
    };

    const error = (err) => {
      throw new SubmissionError({
        _error: err.error
      });
    };

    return this.props.sendRestore(data).then(success, error);

  }

  render() {
    const {
      handleSubmit,
      submitting,
      error // eslint-disable-line
      } = this.props;
    return (
      <div>
        <form action="javascript:void(0)" method="post" onSubmit={handleSubmit(::this.handleSubmit)} className="">
          <Form.Input
            className=""
            disabled = {submitting}
            label={"Email"}
            name={"login"}
            type={"text"}
          />
          <Form.Caption>
            An email will be sent to this address with a link that will allow you to reset your password.
          </Form.Caption>
          <div className="form-btn__container">
            <Form.Button
              type="submit"
              disabled = {submitting}
              className="btn--success"
            >
              <span>Send email</span>
            </Form.Button>
          </div>
        {error && <Form.Alert title={error} className="alert-danger" />}
        </form>
      </div>
    );
  }
}
