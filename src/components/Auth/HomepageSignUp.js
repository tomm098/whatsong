import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form'; // eslint-disable-line
import SignUpFormValidation from './SignUpFormValidation';
import { Form } from 'elements';
import * as authMoadlActions from '../../redux/modules/authModal';

@connect(
  () => ({}),
  {
    openModal: authMoadlActions.open
  }
)

@reduxForm({
  form: 'homepageSignUp',
  validate: SignUpFormValidation
})

export default class HomepageSignUp extends Component {
  static propTypes = {
    openModal: PropTypes.func,
    // form
    error: PropTypes.string,
    pushState: PropTypes.func,
    submitting: PropTypes.bool.isRequired,
    // form validation
    active: PropTypes.string,
    dirty: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSubmit(data) {
    this.props.openModal('sign-up', data);
  }

  render() {
    const {
      handleSubmit,
      submitting,
      openModal
      } = this.props;
    return (
      <div className="banner__user-info">
        <h1>Discover music with the power of film and television</h1>
        <h2>Join the WhatSong community to create playlists, enjoy Spotify and YouTube integration. Even contribute to our growing community.</h2>
        <form onSubmit={handleSubmit(::this.handleSubmit)} action="">
          <div className="banner__wrapp hidden-xs">
            <Form.Input
              className=""
              disabled = {submitting}
              placeholder={"Email address"}
              name={"email"}
              type={"email"}
              error_hidden={true}
            />
            <Form.Button
              type="submit"
              disabled = {submitting}
              className="btn--danger"
            >
              <span>Create Account</span>
            </Form.Button>
          </div>
          <div className="caption hidden-xs">
            <span>or log in with facebook </span>
            <Form.Button
              type="submit"
              disabled = {submitting}
              className=""
              onClick={()=>openModal('sign-in')}
            >
              <span>here</span>
            </Form.Button>
          </div>
          <div className="banner__btn-mobile visible-xs-flex">
            <Form.Button
              disabled = {submitting}
              className="btn--danger"
              onClick={()=>openModal('sign-in')}
            >
              <span>Create account</span>
            </Form.Button>

            <Form.Button
              disabled = {submitting}
              className="btn--primary"
              onClick={()=>openModal('sign-in')}
            >
              <span>Login with Facebook</span>
            </Form.Button>

          </div>
        </form>
      </div>
    );
  }
}
