import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import * as authActions from '../../redux/modules/auth';
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import SignInFormValidation from './SignInFormValidation';
import { Form } from 'elements';
import * as authMoadlActions from '../../redux/modules/authModal';
import FacebookLogin from './FacebookLogin';
import * as notificationsActions from 'redux/modules/notifications';

@connect(
  () => ({}),
  {
    logIn: authActions.logIn,
    openModal: authMoadlActions.open,
    closeModal: authMoadlActions.close,
    socialAuth: authActions.socialAuth,
    pushNotification: notificationsActions.push
  }
)

@reduxForm({
  form: 'signIn',
  validate: SignInFormValidation,
})

export default class SignInForm extends Component {
  static propTypes = {
    logIn: PropTypes.func,
    openModal: PropTypes.func,
    closeModal: PropTypes.func,
    socialAuth: PropTypes.func,
    // form
    error: PropTypes.string,
    pushState: PropTypes.func,
    submitting: PropTypes.bool.isRequired,
    pushNotification: PropTypes.func,
    // form validation
    active: PropTypes.string,
    dirty: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSubmit(data) {
    const success = (response) => { // eslint-disable-line
      //this.props.openModal('sign-in-success');
      this.props.closeModal();
      this.props.pushNotification('sign_in_success', {}, 5e3);
    };
    const error = (err) => {
      throw new SubmissionError({
        _error: err.error
      });
    };
    return this.props.logIn(data).then(success, error);
  }

  handleSocialSignIn(data) {
    const success = (response) => { // eslint-disable-line
      //this.props.openModal('sign-in-success');
      this.props.closeModal();
      this.props.pushNotification('sign_in_success', {}, 5e3);
    };
    if (data && data.accessToken) {
      this.props.socialAuth(data.accessToken).then(success);
    }
  }

  render() {
    const {
      handleSubmit,
      submitting,
      error, // eslint-disable-line
      openModal
      } = this.props;
    return (
      <div>
        <form action="" onSubmit={handleSubmit(::this.handleSubmit)} className="">
          <Form.Input
            className=""
            disabled = {submitting}
            label={"Username"}
            name={"login"}
            type={"text"}
            />
          <Form.Input
            className=""
            disabled = {submitting}
            label={"Password"}
            name={"password"}
            type={"password"}
            />
          <div className="form-btn__container">
            <Form.Button
              type="submit"
              disabled = {submitting}
              className="btn--success"
            >
              <span>Submit</span>
            </Form.Button>
            <span>or</span>
            <FacebookLogin className="btn--primary" textButton={'LOGIN WITH FACEBOOK'} callback={::this.handleSocialSignIn}/>
          </div>
          {error && <Form.Alert title={error} className="alert-danger" />}
        </form>
        <Form.Caption>
          Have you forgotten your username or password? Click <Form.Button className="" onClick={()=>openModal('send-restore')} >here </Form.Button>
        </Form.Caption>
      </div>
    );
  }
}
