import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import * as authActions from '../../redux/modules/auth';
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import PasswordRestoreFormValidation from './PasswordRestoreFormValidation';
import { Form } from 'elements';


@connect(
  () => ({}),
  {
    restorePassword: authActions.restorePassword
  }
)

@reduxForm({
  form: 'restorePasswordForm',
  validate: PasswordRestoreFormValidation,
})

export default class RestorePasswordForm extends Component {
  static propTypes = {
    restorePassword: PropTypes.func,
    restore_code: PropTypes.string,
    handleSuccess: PropTypes.func,
    // form
    error: PropTypes.string,
    pushState: PropTypes.func,
    submitting: PropTypes.bool.isRequired,
    // form validation
    active: PropTypes.string,
    dirty: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  handleSubmit(data) {
    const success = (response) => { // eslint-disable-line
      this.props.handleSuccess();
    };

    const error = (err) => {
      throw new SubmissionError({
        _error: err.error
      });
    };

    return this.props.restorePassword({...data, restore_code: this.props.restore_code}).then(success, error);
  }

  render() {
    const {
      handleSubmit,
      submitting,
      error // eslint-disable-line
      } = this.props;
    return (
      <div className="form-password">
        {
          <form action="javascript:void(0)" method="post" onSubmit={handleSubmit(::this.handleSubmit)} className="">
            <Form.Input
              className=""
              disabled = {submitting}
              label={"enter new password"}
              name={"password"}
              type={"password"}
              />
            <Form.Input
              className=""
              disabled = {submitting}
              label={"confirm"}
              name={"confirm"}
              type={"password"}
            />
            <Form.Button
              type="submit"
              disabled = {submitting}
              className="btn--success"
              >
              <span>Save</span>
            </Form.Button>
            {error && <Form.Alert title={error} className="alert-danger" />}
          </form>
        }
      </div>
    );
  }
}
