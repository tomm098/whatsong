import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import * as authActions from '../../redux/modules/auth';
import { reduxForm, SubmissionError } from 'redux-form'; // eslint-disable-line
import SignUpFormValidation from './SignUpFormValidation';
import { Form } from 'elements';
import FacebookLogin from './FacebookLogin';
import * as authMoadlActions from '../../redux/modules/authModal';
import * as notificationsActions from 'redux/modules/notifications';

@connect(
  (state) => ({
    initialValues: state.authModal.initial
  }),
  {
    register: authActions.register,
    openModal: authMoadlActions.open,
    closeModal: authMoadlActions.close,
    socialAuth: authActions.socialAuth,
    pushNotification: notificationsActions.push
  }
)

@reduxForm({
  form: 'signUp',
  validate: SignUpFormValidation
})

export default class SignUpForm extends Component {
  static propTypes = {
    register: PropTypes.func,
    socialAuth: PropTypes.func,
    openModal: PropTypes.func,
    closeModal: PropTypes.func,
    pushNotification: PropTypes.func,
    // form
    error: PropTypes.string,
    pushState: PropTypes.func,
    submitting: PropTypes.bool.isRequired,
    // form validation
    active: PropTypes.string,
    dirty: PropTypes.bool.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    invalid: PropTypes.bool.isRequired,
    pristine: PropTypes.bool.isRequired,
    valid: PropTypes.bool.isRequired
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  handleSubmit(data) {

    const success = (response) => { // eslint-disable-line
      //this.props.openModal('sign-up-success');
      this.props.closeModal();
      this.props.pushNotification('sign_up_success', {}, 5e3);
    };

    const error = (err) => {
      console.log(err);
      if (err.error && err.error.errors && (err.error.errors.email || err.error.errors.username)) {
        throw new SubmissionError({
          email: err.error.errors.email && err.error.errors.email.message,
          username: err.error.errors.username && err.error.errors.username.message,
          _error: err.error.message,
        });
      } else {
        throw new SubmissionError({
          _error: err.error.message
        });
      }
    };

    return this.props.register(data).then(success, error);
  }

  handleSocialSignUp(data) {
    const success = (response) => { // eslint-disable-line
      //this.props.openModal('sign-in-success');
      this.props.closeModal();
      this.props.pushNotification('sign_in_success', {}, 5e3);
    };
    if (data && data.accessToken) {
      this.props.socialAuth(data.accessToken).then(success);
    }
  }

  render() {
    const {
      handleSubmit,
      submitting,
      error // eslint-disable-line
      } = this.props;

    return (
      <div>
        {
          <form action="javascript:void(0)" method="post" onSubmit={handleSubmit(::this.handleSubmit)} className="">
            <Form.Input
              className=""
              disabled = {submitting}
              label={"Email"}
              name={"email"}
              type={"email"}
              caption={'Please enter a valid email. You will need to verify this email to activate your account.'}
              />
            <Form.Input
              className=""
              disabled = {submitting}
              label={"Username"}
              name={"username"}
              type={"text"}
              caption={'Only characters A-Z , hyphens and fullstops.'}
              />
            <Form.Input
              className=""
              disabled = {submitting}
              label={"Password"}
              name={"password"}
              type={"password"}
              />
            <Form.Input
              className=""
              disabled = {submitting}
              label={"Confirm"}
              name={"confirm"}
              type={"password"}
              />
            <div className="form-btn__container">
              <Form.Button
                type="submit"
                disabled = {submitting}
                className="btn--success"
              >
                <span>Submit</span>
              </Form.Button>
              <span>or</span>
              <FacebookLogin className="btn--primary" textButton={'LOGIN WITH FACEBOOK'} callback={::this.handleSocialSignUp}/>
            </div>
            {error && <Form.Alert title={error} className="alert-danger" />}
          </form>
        }
      </div>
    );
  }
}
