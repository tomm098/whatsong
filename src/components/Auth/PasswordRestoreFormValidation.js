import memoize from 'lru-memoize';
import { createValidator, required, minLength, match } from 'utils/validation';

const PasswordRestoreValidation = createValidator({
  password: [required, minLength(4), match('confirm')]
});
export default memoize(10)(PasswordRestoreValidation);
