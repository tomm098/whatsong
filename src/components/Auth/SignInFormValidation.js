import memoize from 'lru-memoize';
import { createValidator, required, minLength } from 'utils/validation';

const SignInValidation = createValidator({
  password: [required, minLength(4)],
  login: [required]
});
export default memoize(10)(SignInValidation);
