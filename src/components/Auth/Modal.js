import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import * as authMoadlActions from '../../redux/modules/authModal';
import { Auth } from 'components';
import { Popup } from 'elements';

@connect(
  state => ({
    is_open: state.authModal.is_open,
    view_type: state.authModal.view_type
  }),
  {
    close: authMoadlActions.close
  }
)


export default class AuthModal extends Component {
  static propTypes = {
    is_open: PropTypes.bool,
    view_type: PropTypes.string,
    close: PropTypes.func
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  chooseForm() {
    switch (this.props.view_type) {
      case 'sign-in':
        return <Auth.SignInForm />;
      case 'sign-up':
        return <Auth.SignUpForm />;
      case 'send-restore':
        return <Auth.SendRestoreForm />;
      case 'sign-in-success':
        return <Auth.SignInSuccessMsg />;
      case 'sign-up-success':
        return <Auth.SignUpSuccessMsg />;
      default :
        return <div>unknown form</div>;
    }
  }

  render() {
    const {is_open, close} = this.props;
    return (
      <Popup
        is_opened={is_open}
        handleClose={close}
      >
        {this.chooseForm()}
      </Popup>
    );
  }
}
