import React, {Component, PropTypes} from 'react'; // eslint-disable-line
import {} from './ThankCount.scss';
import { nFormatter } from 'utils/helpers';

export default class ThankCount extends Component {
  static propTypes = {
    score: PropTypes.number,
    highlight: PropTypes.bool
  }

  static defaultProps = {
    score: 0,
    highlight: false
  }

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {
    return (
      <div className={`thank-count ${this.props.highlight ? 'thank-count--green' : ''}` }>{nFormatter(this.props.score, 1)}</div>
    );
  }
}
