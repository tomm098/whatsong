import config from 'config';
import cookie from 'react-cookie';

export function truncateMiddleString(str, length, separator) {
  if (str.length <= length) return str;

  separator = separator || '...';

  const sepLen = separator.length;
  const charsToShow = length - sepLen;
  const frontChars = Math.ceil(charsToShow / 2);
  const backChars = Math.floor(charsToShow / 2);

  return str.substr(0, frontChars) + separator + str.substr(str.length - backChars);
}

export function truncString(str, length, separator) {
  separator = separator || '...';
  return (typeof str === 'string' && str.length > length ? str.substring(0, length) + separator : str);
}

export function fireEvent(node, eventName) {
  let doc = {};
  if (node.ownerDocument) {
    doc = node.ownerDocument;
  } else if (node.nodeType === 9) {
    doc = node;
  }
  if (node.dispatchEvent) {
    let eventClass = '';
    switch (eventName) {
      case 'click':
      case 'mousedown':
      case 'mouseup':
        eventClass = 'MouseEvents';
        break;
      case 'focus':
      case 'change':
      case 'blur':
      case 'select':
        eventClass = 'HTMLEvents';
        break;
      default:
        break;
    }
    const event = doc.createEvent(eventClass);
    event.initEvent(eventName, true, true); // All events created as bubbling and cancelable.

    event.synthetic = true; // allow detection of synthetic events
    // The second parameter says go ahead with the default action
    node.dispatchEvent(event, true);
  } else if (node.fireEvent) {
    // IE-old school style
    const event = doc.createEventObject();
    event.synthetic = true; // allow detection of synthetic events
    node.fireEvent('on' + eventName, event);
  }
}

export function executionEnvironment() {
  const canUseDOM = !!(typeof window !== 'undefined' && window.document && window.document.createElement);

  return {
    canUseDOM: canUseDOM,
    canUseWorkers: typeof Worker !== 'undefined',
    canUseEventListeners:
    canUseDOM && !!(window.addEventListener || window.attachEvent),
    canUseViewport: canUseDOM && !!window.screen
  };
}

export function executionLocation() {
  if (executionEnvironment().canUseDOM) {
    const location = cookie.load(config.apiLocationTokenKey);
    if (location && typeof location === 'string') {
      return JSON.parse(location);
    } else if (location && typeof location === 'object') {
      return location;
    }
    return null;
  }

  if (global && global[config.apiLocationTokenKey]) {
    if (typeof global[config.apiLocationTokenKey] === 'string') {
      return JSON.parse(global[config.apiLocationTokenKey]);
    }
    return global[config.apiLocationTokenKey];
  }
  return null;
}

export function getWindowHeight() {
  if (!executionEnvironment().canUseDOM) {
    return 0;
  }
  const w = window;
  const d = document;
  const e = d.documentElement;
  const g = d.getElementsByTagName('body')[0];
  return w.innerHeight || e.clientHeight || g.clientHeight;
}

export function getWindowWidth() {
  if (!executionEnvironment().canUseDOM) {
    return 0;
  }
  const w = window;
  const d = document;
  const e = d.documentElement;
  const g = d.getElementsByTagName('body')[0];
  return w.innerWidth || e.clientWidth || g.clientWidth;
}

export function getDeviceType() {
  if (!executionEnvironment().canUseDOM) {
    return 0;
  }
  const w = window || null;
  const d = document;
  const e = d.documentElement;
  const g = d.getElementsByTagName('body')[0];
  const witdh = w.innerWidth || e.clientWidth || g.clientWidth;
  if (witdh < 768) {
    return 'mobile';
  }
  if (witdh >= 768 && witdh < 1024) {
    return 'tablet';
  }
  return 'desktop';
}

export function getScrollTop() {
  return window.pageYOffset || document.documentElement.scrollTop;
}

export function getScreenHeight() {
  return Math.max(
    document.body.scrollHeight, document.documentElement.scrollHeight,
    document.body.offsetHeight, document.documentElement.offsetHeight,
    document.body.clientHeight, document.documentElement.clientHeight
  );
}

export function isEmpty(obj) {
  // null and undefined are "empty"
  if (obj === null) return true;
  // Assume if it has a length property with a non-zero value
  // that that property is correct.
  if (obj && obj.length > 0) return false;
  if (obj && obj.length === 0) return true;
  // Otherwise, does it have any properties of its own?
  // Note that this doesn't handle
  // toString and valueOf enumeration bugs in IE < 9
  for (const key in obj) {
    if (hasOwnProperty.call(obj, key)) return false;
  }
  return true;
}

export function wrappWithObj(name, value) {
  if (!name) {return {};}
  const res = {};
  res[name] = value;
  return res;
}

/*eslint-disable */
export function debounce(func, wait, immediate) {
  var timeout;
  return function() {
    var context = this, args = arguments;
    var later = function() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};


export function nFormatter(num = 0, digits) {
  if (num === null || num === undefined) {
    return '0';
  }
  var si = [
    { value: 1E18, symbol: "E" },
    { value: 1E15, symbol: "P" },
    { value: 1E12, symbol: "T" },
    { value: 1E9,  symbol: "G" },
    { value: 1E6,  symbol: "M" },
    { value: 1E3,  symbol: "k" }
  ], i;
  for (i = 0; i < si.length; i++) {
    if (num >= si[i].value) {
      return (num / si[i].value).toFixed(digits).replace(/\.0+$|(\.[0-9]*[1-9])0+$/, "$1") + si[i].symbol;
    }
  }
  return num.toString();
}

export function timeSince(date, custom_units, plural = true) {
  var units = custom_units || [
    { name: "second", limit: 60, in_seconds: 1 },
    { name: "minute", limit: 3600, in_seconds: 60 },
    { name: "hour", limit: 86400, in_seconds: 3600  },
    { name: "day", limit: 604800, in_seconds: 86400 },
    { name: "week", limit: 2629743, in_seconds: 604800  },
    { name: "month", limit: 31556926, in_seconds: 2629743 },
    { name: "year", limit: null, in_seconds: 31556926 }
  ];
  var diff = Math.floor((new Date() - date) / 1000);
  if (diff < 5) return "now";

  var i = 0, unit;
  while (unit = units[i++]) {
    if (diff < unit.limit || !unit.limit){
      var diff =  Math.floor(diff / unit.in_seconds);
      return diff + " " + unit.name + (plural && diff>1 ? "s" : "");
    }
  };
}

export function timeInterval(interval) {
  var sec_num = parseInt(interval / 1000, 10);
  var hours   = Math.floor(sec_num / 3600);
  var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  var seconds = sec_num - (hours * 3600) - (minutes * 60);
  return {hours, minutes, seconds};
}

export function stringHashCode(string) {
  var hash = 0, i, chr, len;
  if (string.length === 0) return hash;
  for (i = 0, len = string.length; i < len; i++) {
    chr   = string.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

export function isInViewport(id) {
  const element = document.getElementById(id);
  if (!element) { return false; }
  var rect = element.getBoundingClientRect();
  var html = document.documentElement;
  const window_height = (window.innerHeight || html.clientHeight);
  return (
    (rect.top >= 0 && rect.top <= window_height) ||
    rect.bottom <= window_height
  );
}

/*eslint-enable */

export function urlGenerator(type, params) {
  const types = {
    movie: ({id, slug})=> id && slug && `/Movies/Soundtrack/${id}/${slug}`,
    tv_show: ({id, slug})=> id && slug && `/Tvshow/${id}/${slug}`,
    tv_show_season: ({id, slug, season_id})=> id && slug && season_id && `/Tvshow/${id}/${slug}/s/${season_id}`,
    tv_show_episode: ({id, slug, episode_id})=> id && slug && episode_id && `/Tvshow/${id}/${slug}/e/${episode_id}`,
    artist: ({id, slug})=> id && slug && `/Artist/${id}/${slug}`,
    games: ({id, slug})=> id && slug && `/Games/Soundtrack/${id}/${slug}`,
  };
  if (type && types[type]) {
    return types[type](params) || '/';
  }

  return '/';
}

export function search2urlTypes(type) {
  const types = {
    'Television': 'tv_show',
    'Movies': 'movie',
    'Artists': 'artist',
    'Games': 'games',
  };
  if (type && types[type]) {return types[type];}
  return 'not_found';
}

export function scrollToElement(id, offset = 0) {
  if (!window || !window.document) { return false; }
  const el = document.getElementById(id);
  if (el) {
    window.scrollTo(0, el.getBoundingClientRect().top + window.scrollY + offset);
  }
}

export function printDate(time) {
  const month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
  const date = new Date(time);
  return `${date.getDate()} ${month_names[date.getMonth()]} ${date.getFullYear()}`;
}

export function getExternalLink(link) {
  return link ? `/out?url=${encodeURIComponent(link)}` : link;
}

export function getAmazonLink(country, keywords) {
  const patterns = {
    general: q => `https://www.amazon.com/gp/search?ie=UTF8&index=digital-music&keywords=${q}&tag=whatsong-20`,
    CA: q => `https://www.amazon.ca/gp/search?ie=UTF8&tag=what007-20&linkCode=ur2&linkId=97f5a7f3606b7211da97a4324e8cd6c7&camp=15121&creative=330641&index=music-ca&keywords=${q}`,
    GB: q => `https://www.amazon.co.uk/gp/search?ie=UTF8&camp=1634&creative=6738&index=digital-music&keywords=${q}&linkCode=ur2&tag=whatsong03-21`
  };
  return (patterns[country] || patterns.general)(keywords);
}


export function parseForwardURL(song, store, geolocation = '') {
  const type = song.movie ? 'movie' : song.tv_show ? 'tv_show' : song.from && song.from.type || '';
  const typeId = song.movie ? song.movie : song.tv_show ? song.tv_show : song.from && song.from.id || '';
  if (!song) return '/';
  if (type === 'movie') {
    return `/forward/song/${typeId}/${song._id}/movie?store=${store}${geolocation ? '&geo=' + geolocation : ''}`;
  } else if (type === 'tv_show') {
    return `/forward/song/${typeId}/${song._id}/tv_show?store=${store}${geolocation ? '&geo=' + geolocation : ''}`;
  } else {
    return ``;
  }
}

function iTunesURLPushUTM(url, utmFields) {
  if (url && typeof url === 'string') {
    Object.keys(utmFields).forEach(key => {
      const regE = new RegExp('&' + key + '=\\w+', 'g');
      url = url.replace(regE, '') + '&' + key + '=' + utmFields[key];
    });

    return url;
  }
  return url;
}

export function parseForwardiTunesURL(song) {
  const type = song.movie ? 'movie' : song.tv_show ? 'tv_show' : song.from && song.from.type || '';
  if (!song) return '/';
  if (song.itunes_url) {
    const exceptCtUTMRegexp = /[&?]ct=tv|[&?]ct=movie/g;
    song.itunes_url = song.itunes_url.replace(exceptCtUTMRegexp, '');
    song.itunes_url = iTunesURLPushUTM(song.itunes_url, config.itunes_url_fields);
    if (type === 'movie') {
      return `${song.itunes_url}&ct=movie`;
    } else if (type === 'tv_show') {
      return `${song.itunes_url}&ct=tv`;
    } else {
      return ``;
    }
  }
}

export function abbreviateNumber(value) {
  let newValue = value;
  if (value >= 1000) {
    const suffixes = ['', 'k', 'm', 'b', 't'];
    const suffixNum = Math.floor( ('' + value).length / 3 );
    let shortValue = '';
    for (let precision = 2; precision >= 1; precision--) {
      shortValue = parseFloat( (suffixNum !== 0 ? (value / Math.pow(1000, suffixNum) ) : value).toPrecision(precision));
      const dotLessShortValue = (shortValue + '').replace(/[^a-zA-Z 0-9]+/g, '');
      if (dotLessShortValue.length <= 2) { break; }
    }
    newValue = shortValue + suffixes[suffixNum];
  }
  return newValue;
}
