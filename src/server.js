import Express from 'express';
import React from 'react';
import ReactDOM from 'react-dom/server';
import config from './config';
import favicon from 'serve-favicon';
import compression from 'compression';
import httpProxy from 'http-proxy';
import path from 'path';
import createStore from './redux/create';
import ApiClient from './helpers/ApiClient';
import Html from './helpers/Html';
import PrettyError from 'pretty-error';
import http from 'http';
import os, { hostname } from 'os';
import { match } from 'react-router';
import { ReduxAsyncConnect, loadOnServer } from 'redux-connect';
import createHistory from 'react-router/lib/createMemoryHistory';
import {Provider} from 'react-redux';
import getRoutes from './routes';
import { syncHistoryWithStore } from 'react-router-redux';

process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = 0;
var device = require('express-device');
import cookieServer from 'utils/cookie';
import { getAmazonLink } from 'utils/helpers';

let targetUrl = '';

if (config.apiPort && config.apiPort !== '') {
  targetUrl = 'https://' + config.apiHost + ':' + config.apiPort;
} else {
  targetUrl = 'https://' + config.apiHost;
}

const pretty = new PrettyError();
const app = new Express();
const server = new http.Server(app);
// const proxy = httpProxy.createProxyServer({
//   target: targetUrl,
//   ws: false,
//   changeOrigin: true
// });

app.disable('etag');
app.disable('x-powered-by');
app.use(compression());
app.use(favicon(path.join(__dirname, '..', 'static', 'favicon.ico')));

app.use(Express.static(path.join(__dirname, '..', 'static'), {etag: false}));

// // Proxy to API server
// app.use('/api', (req, res) => {
//   proxy.web(req, res, {target: targetUrl});
// });
//
// // added the error handling to avoid https://github.com/nodejitsu/node-http-proxy/issues/527
// proxy.on('error', (error, req, res) => {
//   let json;
//   if (error.code !== 'ECONNRESET') {
//     console.error('proxy error', error);
//   }
//   if (!res.headersSent) {
//     res.writeHead(500, {'content-type': 'application/json'});
//   }
//
//   json = {error: 'proxy_error', reason: error.message};
//   res.end(JSON.stringify(json));
// });

app.use(device.capture());
// app.use((req, res, next) => {
//   if (req.device.type === 'bot') {
//     const client = new ApiClient(req);
//     const fail = ()=> res.status(404).send('Not found');
//     const success = (resp, err)=> next();
//     client.get('/url/check', {data:{url: req.url}}).then(success, fail);
//   } else {
//     next();
//   }
// });


app.route('/out').get((req, res)=>{
  const redirect_path = req.query.url || '/';
  res.set('X-Robots-Tag', 'noindex');
  res.redirect(redirect_path);
});

app.route('/forward/song/:movieId/:songId/:type').get((req, res) => {
  const store = req.query.store;
  const movie_id = req.params.movieId;
  const song_id = req.params.songId;
  const type = req.params.type;
  const geolocation = req.query.geo;

  if (!req.params.songId || !req.params.songId || !store) {
    res.redirect('/404');
    return;
  }

  const client = new ApiClient(req);

  const fail = ()=> res.redirect('/404');
  const success = (resp, err) => {

    const song = resp.data;

    if (typeof song === 'undefined') {
      res.redirect('/404');
    }
    let url = '';
    switch (store) {
      case 'itunes':
        url = song.itunes_url;
        break;
      case 'spotify':
        url = `https://open.spotify.com/embed?uri=${song.spotify_uri}`;
        break;
      case 'amazon':
        if (
          typeof song !== 'undefined' &&
          typeof song.title !== 'undefined'
        ) {
          url = getAmazonLink(geolocation && geolocation.country, encodeURIComponent(song.title + (song.artist && song.artist.name ? ' by ' + song.artist.name : '')));
        }
        break;
    }
    res.set('X-Robots-Tag', 'noindex');

    if (url === '') {
      res.redirect('/404');
    } else {
      res.redirect(url);
    }
  };

  client.get('/song-info', {params: { songId: parseInt(song_id),type: type }}).then(success, fail);
});

app.use((req, res) => {

  if (__DEVELOPMENT__) {
    // Do not cache webpack stats: the script file would change since
    // hot module replacement is enabled in the development env
    webpackIsomorphicTools.refresh();
  }
  const client = new ApiClient(req);
  const browser_history = createHistory(req.originalUrl);

  const store = createStore(browser_history, client);

  const history = syncHistoryWithStore(browser_history, store);

  function hydrateOnClient() {
    res.send('<!doctype html>\n' +
      ReactDOM.renderToString(<Html assets={webpackIsomorphicTools.assets()} store={store}/>));
  }

  if (__DISABLE_SSR__) {
    hydrateOnClient();
    return;
  }
  function isMovie(state){
    return !!(state && state.movie && state.movie.data && state.movie.data.movie && state.movie.data.movie.slug) || true;
  }

  match({ history, routes: getRoutes(store), location: req.originalUrl }, (error, redirectLocation, renderProps) => {
    if (redirectLocation) {
      res.redirect(redirectLocation.pathname + redirectLocation.search);
    } else if (error) {
      console.error('ROUTER ERROR:', pretty.render(error));
      res.status(500);
      hydrateOnClient();
    } else if (renderProps) {
      loadOnServer({...renderProps, store, helpers: {client}}).then(() => {
        const component = (
          <Provider store={store} key="provider">
            <ReduxAsyncConnect {...renderProps} />
          </Provider>
        );

        const redirectLink = rightUrlRedirect(renderProps, store);
        if (redirectLink) {
          res.status(301);
          return res.redirect(redirectLink);
        }
        res.status(200);

        global.navigator = {userAgent: req.headers['user-agent']};

        res.send('<!doctype html>\n' +
          ReactDOM.renderToString(<Html assets={webpackIsomorphicTools.assets()} component={component} store={store}/>));
      });
    } else {
      res.status(404).send('Not found');
    }
  });
});

function checkUrl(stateObj, renderProps, paramsPref, keys = false) {
  keys = keys || {
      title: 'title',
      slug: 'slug'
    };
  paramsPref = paramsPref || '';
  return (renderProps.params[paramsPref + keys.title] === stateObj[keys.slug]) && (parseInt(renderProps.params[paramsPref + 'id']) === parseInt(stateObj._id))
}

function getStateObj(state, params) {
  if (state && params[0] && params[1] && state[params[0]] && state[params[0]].data && state[params[0]].data[params[1]]){
    return state[params[0]].data[params[1]];
  }
  return false;
}

function getRedirectLink(renderProps, stateObj, paramsPref = '', keys) {
  keys = keys || {
      title: 'title',
      slug: 'slug'
    };
  let rout = renderProps.routes[1].path.replace(':' + keys.title, stateObj[keys.slug]).replace(':id', renderProps.params.id);
  if (paramsPref) {
    return rout.replace(':' + paramsPref + 'id', stateObj._id).replace(':' + paramsPref + keys.title, stateObj.slug);
  }
  return rout;
}

function rightUrlRedirect(renderProps, store) {
  const curPath = renderProps && renderProps.params && renderProps.routes && renderProps.routes[1] && renderProps.routes[1].path;
  if (!(curPath && store)) {
    return false;
  }

  const state = store.getState();
  const config = {
    '/Games/Soundtrack/:id/:title': { getStateObjParams: ['game', 'game']},
    '/Movies/Soundtrack/:id/:title': { getStateObjParams: ['movie', 'movie']},
    '/Tvshow/:id/:title': { getStateObjParams: ['tvShowMain', 'tv_show']},
    '/Tvshow/:tv_show_id/:tv_show_title/s/:id': { getStateObjParams: ['tvShowSeason', 'tv_show'], paramsPref: 'tv_show_'},
    '/Tvshow/:tv_show_id/:tv_show_title/e/:id': { getStateObjParams: ['tvShowEpisode', 'tv_show'], paramsPref: 'tv_show_'},
    '/Artist/:id/:name': { getStateObjParams: ['artistPage', 'Artist'], keys: {title: 'name', slug: 'slug'}}
  };
  if (curPath in config) {
    const stateObj = getStateObj(state, config[curPath].getStateObjParams);

    if (!checkUrl(stateObj, renderProps, config[curPath].paramsPref, config[curPath].keys)) {
      return getRedirectLink(renderProps, stateObj, config[curPath].paramsPref, config[curPath].keys);
    }
  }
  return false;
}

if (config.port) {
  server.listen(config.port, (err) => {
    if (err) {
      console.error(err);
    }
    console.info('----\n==> ✅  %s is running, talking to API server on %s.', config.app.title, config.apiPort);
    console.info('==> 💻  Open http://%s:%s in a browser to view the app.', config.host, config.port);
  });
} else {
  console.error('==>     ERROR: No PORT environment variable has been specified');
}
