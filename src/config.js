require('babel-polyfill');

const environment = {
  development: {
    isProduction: false
  },
  production: {
    isProduction: true
  }
}[process.env.NODE_ENV || 'development'];

module.exports = Object.assign({
  host: process.env.HOST || 'localhost',
  port: process.env.PORT,
  apiHost: process.env.APIHOST || 'localhost',
  apiPort: process.env.APIPORT,
  apiGamePort: process.env.APIGAMEPORT,
  apiPrefix: '/api',
  apiUrl: `https://${process.env.APIHOST}${process.env.APIPORT ? (':' + process.env.APIPORT) : ''}`,
  apiTokenKey: 'x-access-token',
  apiLocationTokenKey: 'location',
  tokenExpire: 14, // in days
  facebookAppId: '260016127347363',
  itunes_url_fields: {
    uo: '4',
    at: '1l3uZPF',
    mt: '1',
    app: 'music'
  },
  app: {
    title: 'WhatSong Soundtracks - Listen to Songs from the Latest Movies',
    //email: 'support@What-song.com',
    description: 'What-song description',
    home: {
      htmlAttributes: {
        lang: 'en'
      },
      title: 'WhatSong Soundtracks - Stream Songs from the Latest Movies & TV Shows',
      meta: [
        {name: 'description', content: 'Stream soundtracks and full-length songs from the latest movies and television shows. Now with Youtube & Spotify playlists.'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'What-song'},
        {property: 'og:locale', content: 'en_US'},
        {property: 'og:title', content: 'WhatSong Soundtracks - Stream Songs from the Latest Movies & TV Shows'},
        {property: 'og:description', content: 'Stream soundtracks and full-length songs from the latest movies and television shows. Now with Youtube & Spotify playlists.'},
        {property: 'og:image', content: '/logo.jpg'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    },
    tvShows: {
      htmlAttributes: {
        lang: 'en'
      },
      title: 'Latest in Television - Stream Songs from the Latest TV Shows',
      meta: [
        {name: 'description', content: 'Stream the soundtracks and list of full-length songs from the latest trending TV shows'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'What-song'},
        {property: 'og:locale', content: 'en_US'},
        {property: 'og:title', content: 'Latest in Television - Stream Songs from the Latest TV Shows'},
        {property: 'og:description', content: 'Stream the soundtracks and list of full-length songs from the latest trending TV shows'},
        {property: 'og:image', content: '/logo.jpg'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    },
    movies: {
      htmlAttributes: {
        lang: 'en'
      },
      title: 'Latest in Film - Stream Songs from the Latest Movies',
      meta: [
        {name: 'description', content: 'Stream the soundtracks and list of full-length songs from the latest trending movies.'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'What-song'},
        {property: 'og:locale', content: 'en_US'},
        {property: 'og:title', content: 'WhatSong Soundtracks - Listen to Songs from the Latest Movies'},
        {property: 'og:description', content: 'Stream the soundtracks and list of full-length songs from the latest trending movies.'},
        {property: 'og:image', content: '/logo.jpg'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    },
    games: {
      htmlAttributes: {
        lang: 'en'
      },
      title: 'WhatSong Soundtracks - Listen to Songs from the Latest Movies',
      meta: [
        {name: 'description', content: 'Listen to the soundtracks and complete list of full-length songs from the latest movies and TV shows.'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'What-song'},
        {property: 'og:locale', content: 'en_US'},
        {property: 'og:title', content: 'WhatSong Soundtracks - Listen to Songs from the Latest Movies'},
        {property: 'og:description', content: 'Listen to the soundtracks and complete list of full-length songs from the latest movies and TV shows.'},
        {property: 'og:image', content: '/logo.jpg'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    },
    notFound: {
      title: 'What-song',
      meta: [
        {name: 'description', content: 'What-song description'},
        {charset: 'utf-8'},
        {property: 'og:site_name', content: 'What-song'},
        {property: 'og:locale', content: 'en_US'},
        {property: 'og:title', content: 'What-song'},
        {property: 'og:description', content: 'What-song description'},
        {property: 'og:image', content: '/logo.jpg'},
        {property: 'og:image:width', content: '200'},
        {property: 'og:image:height', content: '200'}
      ]
    }
  }
}, environment);
